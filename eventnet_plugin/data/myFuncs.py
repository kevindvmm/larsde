import json
from flask import jsonify

class TreeNode(dict):
	def __init__(self, _name, _type="category", _children=None):
		#super().__init__()
		self.__dict__ = self
		self.text = _name
		self.type = _type
		self.data = {}
		self.data["videoURLs"] = []

		self.children = [] if not _children else _children

class userNode(dict):
	def __init__(self, _name, _password, _untaggedURLs, _ontology):
		#super().__init__()
		self.__dict__ = self
		self.username = _name
		self.passoword = _password
		self.untaggedURLs = _untaggedURLs
		self.ontology = _ontology

class videoNode(dict):
	def __init__(self, _url, _description):
		#super().__init__()
		self.__dict__ = self
		self.url = _url
		self.description = _description
		self.state = 0

def from_dict(dict_):
	""" Recursively (re)construct TreeNode-based tree from dictionary. """
	root = TreeNode(dict_['name'], dict_['children'])
	root.children = list(map(TreeNode.from_dict, root.children))
	return root

def TreeTest():
	tree = TreeNode('Parent', "category")
	tree.children.append(TreeNode('Child 1', "event"))
	child2 = TreeNode('Child 2',"category")
	tree.children.append(child2)
	child2.children.append(TreeNode('Grand Kid',"category"))
	child2.children[0].children.append(TreeNode('Great Grand Kid', "event"))






	json_str = json.dumps(tree, sort_keys=True, indent=4)
	fout = open("sample.json","w")
	fout.write(json_str)
	fout.close()
	print(json_str)

def EventNetTreeTest():
	f = open("Eventnet_ontology.txt", "r")

	pool = {}
	root = TreeNode("EventNet")
	pool["EventNet"] = root

	for line in f:
		data = line.strip().split("\t")
		eventName = data[0].strip().replace(" ", "_")
		#print eventName
		path = data[1].split(";")
		path = ["EventNet"] + path + ["*" + eventName]
		
		#print path
		for nodeName in path:
			if nodeName not in pool:
				if nodeName[0] == "*":
					pool[nodeName] = TreeNode(nodeName, "event")
				else:
					pool[nodeName] = TreeNode(nodeName, "category")



		for i in range(len(path) - 1):
			if pool[path[i + 1]] not in pool[path[i]].children:
				pool[path[i]].children.append(pool[path[i + 1]])

	#print root.children[0].text
	#print root.children[0].children[1].text

	untaggedURLs = []
	f = open("./sampleURLs.txt","r")
	for line in f:
		data = line.strip().split()
		v = videoNode(data[0], "Sample Descriptions")
		untaggedURLs.append(v)
	

	user = userNode("hongyi", "hongyi_pwd", untaggedURLs, root)



	json_str = json.dumps(user, sort_keys=True, indent=4)
	fout = open("userInfo.json","w")
	fout.write(json_str)
	fout.close()
	#print(json_str)



def mongoDBTest():
	from pymongo import MongoClient
	from datetime import datetime
	import pymongo


	client = MongoClient() 
	db = client.eventnet

	db.eventTree.update({"username": "hongyi"}, {'$set':{"password": "hehehe"}})

	cursor = db.eventTree.find({"username": "hongyi"})
	print cursor.count()
	a =  cursor.next()
	print a["password"]

	


	