import json
from pymongo import MongoClient


# obtain default event-url dictionary
with open("video_events_eventIdx.txt") as f:
    content = f.readlines()
    url_dict = {}
    for line in content:
        l = line.split("\t")
        if l[1] not in url_dict:
            url_dict[l[1]] = []
        url_dict[l[1]].append(l[0])


# create [{event:event1,urls:[{description:'XX', state:False, videoType: 0, url:'skldfjsdl'}, {...}]}, event2:[{url21}, {url22}...]}
# urlType = 0 means system default   urlType = 1 means user custom
def createDefaultEventJson(url_dict):
    res = []
    for k,urls in url_dict.items():
        l = {}
        l['event'] = k
        l['urls'] = []
        for url in urls:
            dd = {}
            dd['description'] = 'System Default'
            dd['url'] = url
            dd['state'] = False
            dd['urlType'] = 0
            l['urls'].append(dd)
        res.append(l)
    return res


# integrate system default urls directly into user json file
def integrateURLs(url_dict, data):
    if data['type'] == 'event':
        urls = data['data']['videoURLs']
        if data['text'][1:] not in url_dict:
            return
        for url in url_dict[(data['text'][1:])]:
            dd = {}
            dd['description'] = 'System Default'
            dd['url'] = url
            dd['state'] = False
            dd['url_type'] = 0
            urls.append(dd)
    else:
        for item in data['children']:
            integrate(url_dict, item)


# insert all event-urls pairs into mongodb
def insertEventURL(data):
    client = MongoClient()
    db = client.eventnet
    collec = db.defaultEventURL
    result = collec.insert_many(data)

# create the new defaultEventJson collection 
insertEventURL(createDefaultEventJson(url_dict))














# with open('userInfo.json') as data_file:    
#     data = json.load(data_file)
#     insertURLs(url_dict, data['ontology'])
#     print(data)





# a = []
# b = {}
# if (isinstance(a, dict)):
#     print("nimabi")
# print(isinstance(a, dict))
# print(isinstance(b, dict))









# import json

# def convertToJstree(data):
#     l = []
#     if not isinstance(data, dict):
#         for item in data:
#             dd = {}
#             dd['text'] = str(item)
#             #dd['children'] = {}
#             l.append(dd)
#         return l
#     for k,v in data.items():
#         dd = {}
#         dd['text'] = k
#         dd['children'] = convertToJstree(v)
#         l.append(dd)
#     return l

# with open('result.txt') as data_file:
#     data = json.load(data_file)
#     data = convertToJstree(data)
#     data = json.dumps(data)
#     f = open('result1.txt', 'w')
#     f.write(data)
#     f.close()




