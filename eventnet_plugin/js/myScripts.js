var SERVER_BASE_ADDRESS = "./"
var BOOTSTRAP_TABLE = [
    "tbl_videos_tagged", 
    "tbl_videos_untagged", 
    "tbl_videos_tagged_event", 
    //"tbl_videos_tagged_job", 
    "tbl_videos_tagged_default", 
    "tbl_job_custom_url", 
    "tbl_job_default_url" ,
]

$.ajax({
    url: SERVER_BASE_ADDRESS + 'getTree',
    type: 'POST',
    dataType: 'text',
    data: { "header": "test" },
    success: function(response) {

        currentSel = ""
        origin_ontology = ""
        origin_untaggedURLs = ""

        data = JSON.parse(response)
        origin_ontology = data.ontology
        origin_untaggedURLs = data.untaggedURLs
        console.log(origin_ontology)
        console.log(origin_untaggedURLs)
        $('#tbl_videos_untagged').bootstrapTable('load', origin_untaggedURLs)
        var taggedURLs = []
        getAllTaggedURLs(origin_ontology, taggedURLs, [])
        $('#tbl_videos_tagged').bootstrapTable('load', taggedURLs)

        $('#jstree').jstree({
            'core': {
                'data': origin_ontology,
                "check_callback": true,
            },
            "plugins": [
                "contextmenu",
                //"dnd",
                "search",
                //"state", 
                "types",
                "wholerow",
                "sort"
            ],

            "contextmenu": {
                'items': customMenu
            },

            "types": {
                "event": {
                    "icon": "./css/2.png",
                },
                "category": {
                    //"icon" : "default",
                }

            },
        }).bind('loaded.jstree', function(e, data) {
            // invoked after jstree has been loaded
            $(this).jstree("open_node", $("#j1_1"));
        });

        getJobInfoFromServer("tbl_trainning_jobs")

    },
});

//set the jstree search box
var to = false;
$('#jstree_search').keyup(function() {
    if (to) { clearTimeout(to); }
    to = setTimeout(function() {
        var v = $('#jstree_search').val();
        $('#jstree').jstree(true).search(v);
    }, 250);
});


// fires when clicking entries in jstree
$('#jstree').on('select_node.jstree', function(e, data) {
    currentSel = data.node
    console.log(currentSel)

    // return if not clicking leaf nodes
    if (data.node.type != 'event') return

    $("#selectedEvent").html(" for " + currentSel.text)

    // Videos tagged to event
    $('#tbl_videos_tagged_event').bootstrapTable('load', currentSel.data.videoURLs)
    // Get all event-specific jobs
    $.ajax({
        url: SERVER_BASE_ADDRESS + 'getJobInfo',
        type: 'POST',
        dataType: 'text',
        success: function(response) {
            var jobs = JSON.parse(response)
            var eventJobs = []
            for (var i = 0; i < jobs.length; i++) {
                if ("*" + jobs[i].event === currentSel.text)
                    eventJobs.push(jobs[i])
            }
            $('#tbl_videos_tagged_job').bootstrapTable('load', eventJobs)
        }
    });
    // update tagged default table
    $.ajax({
    url: './getDefaultEventURL',
    type: 'POST',
    dataType: 'text',
    data: { "eventname": (data.node.text).substr(1) },
    success: function(response) {
        var urls = []
        if (response) urls = JSON.parse(response).urls;
        $('#tbl_videos_tagged_default').bootstrapTable('load', urls);
        disableCheckBox("tbl_videos_tagged_default");
        $("#eventSpecificModal").modal("show");
    }});
});


$("#btn_save").click(function() {
    var newTree = $("#jstree").jstree(true).get_json("#")
    newTree = get_simpleTree(newTree[0])
    untaggedURLs = $('#tbl_videos_untagged').bootstrapTable('getData')
    console.log(newTree)
    $.ajax({
        url: './saveTree',
        type: 'POST',
        dataType: 'text',
        //data: { ontology: JSON.stringify(newTree) },
        data :{
            ontology: JSON.stringify(newTree),
            untaggedURLs : JSON.stringify(untaggedURLs),
        },
        success: function(response) {
            data = JSON.parse(response)
            //console.log(response);
            if(data["status"] == 1){
                alert("Success!")
            }
            else{
                alert("Fail!")
            }
        }
    });
});

$("#btn_tag_URLs").click(function() {
    if (currentSel.length == 0) {
        alert("Please select an event!")
        return
    }
    if(currentSel.type == "category"){
        alert("Can only tag videos to an event!")
        return
    }

    sel = $('#tbl_videos_untagged').bootstrapTable('getSelections');
    console.log(sel)
    //Tag selected videoURLs to selected event
    currentSel.data.videoURLs = currentSel.data.videoURLs.concat(JSON.parse(JSON.stringify(sel)))

    //Delete selected URLs from untagged video table
    var urls = $.map($('#tbl_videos_untagged').bootstrapTable('getSelections'), function(row) {
        return row.url;
    });

    $('#tbl_videos_untagged').bootstrapTable('remove', {
        field: 'url',
        values: urls,
    });

    //Refresh tagged event videos table
    $('#tbl_videos_tagged_event').bootstrapTable('load', JSON.parse(JSON.stringify(currentSel.data.videoURLs)))

    

    //Update tagged videos
    var curJSTree = getCurrentJSTree()
    var taggedURLs = []
    getAllTaggedURLs(curJSTree, taggedURLs, [])
    $('#tbl_videos_tagged').bootstrapTable('load', taggedURLs)

    if (sel.length != 0) $('#eventSpecificModal').modal('show');
});


$("#btn_remove_URLs").click(function() {
    $table = $('#tbl_videos_untagged');
    var urls = $.map($table.bootstrapTable('getSelections'), function(row) {
        return row.url;
    });
    $table.bootstrapTable('remove', {
        field: 'url',
        values: urls
    });
});


$("#btn_untag_URLs_event").click(function() {
    $table = $('#tbl_videos_tagged_event');

    //Get selected rows
    sel = $('#tbl_videos_tagged_event').bootstrapTable('getSelections');

    //Append selecetd rows to untagged tables
    var untaggedURLs = $('#tbl_videos_untagged').bootstrapTable('getData')
    untaggedURLs = untaggedURLs.concat(JSON.parse(JSON.stringify(sel)))
    $('#tbl_videos_untagged').bootstrapTable('load', JSON.parse(JSON.stringify(untaggedURLs)))

    //remove selected rows in tagged event table
    var urls = $.map($table.bootstrapTable('getSelections'), function(row) {
        return row.url;
    });
    $table.bootstrapTable('remove', {
        field: 'url',
        values: urls
    });

    //remove corresponding rows in tagged table
    $('#tbl_videos_tagged').bootstrapTable('remove', {
        field: 'url',
        values: urls
    });

    //Re-assgin current videos from untagged table to current event
    currentSel.data.videoURLs = JSON.parse(JSON.stringify($table.bootstrapTable('getData')))

    if (sel.length != 0) closeModalByID("eventSpecificModal")
    
});


$("#btn_untag_URLs").click(function() {
    $table = $('#tbl_videos_tagged');
    //Get selected rows
    sel = $table.bootstrapTable('getSelections');

    console.log(sel)

    //Append selecetd rows to untagged tables
    var untaggedURLs = $('#tbl_videos_untagged').bootstrapTable('getData')
    untaggedURLs = untaggedURLs.concat(JSON.parse(JSON.stringify(sel)))
    $('#tbl_videos_untagged').bootstrapTable('load', JSON.parse(JSON.stringify(untaggedURLs)))

    //remove selected rows in tagged event table
    var urls = $.map($table.bootstrapTable('getSelections'), function(row) {
        return row.url;
    });
    $table.bootstrapTable('remove', {
        field: 'url',
        values: urls
    });

    //remove corresponding rows in event tables
    for (var i = 0; i < sel.length; i++) {
        var path = sel[i].path;
        path.shift();
        var curJSTree = $("#jstree").jstree(true).get_json("#");
        curJSTree = get_simpleTree(curJSTree[0]);
        removeURLsFromEventTables(curJSTree, path)
    }

    //Update tagged videos
    var curJSTree = getCurrentJSTree()
    var taggedURLs = []
    getAllTaggedURLs(curJSTree, taggedURLs, [])
    $('#tbl_videos_tagged').bootstrapTable('load', taggedURLs)

    if (sel.length != 0) closeModalByID('eventSpecificModal')
    
});

$("#btn_train").click(function() {
    $table_user = $('#tbl_videos_tagged_event');
    $table_default = $('#tbl_videos_tagged_default');

    // Get user selected jobs
    sel_user = $('#tbl_videos_tagged_event').bootstrapTable('getSelections');

    // Submit jobs
    $.ajax({
    url: SERVER_BASE_ADDRESS + 'submit_jobs',
    type: 'POST',
    dataType: 'text',
    data: { "eventname": currentSel.text.substr(1), "customURLs": JSON.stringify(sel_user)},
    success: function(response) {
        if (response === "no") alert("Please login!")
        else {
            closeModalByID('eventSpecificModal')
            getJobInfoFromServer("tbl_trainning_jobs")
        }
    }});

});


// get job info when cliking refresh button
$("#btn_refresh_jobs").click(function() {
    getJobInfoFromServer("tbl_trainning_jobs")
});

$("#btn_delete_jobs").click(function() {
    sel_jobs = $('#tbl_trainning_jobs').bootstrapTable('getSelections');
    job_list = []
    for (var i = 0; i < sel_jobs.length; i++) job_list.push(sel_jobs[i]._id)
    deleteJobs(job_list)
});


function addOnTableListener() {
    for (var i = 0; i < BOOTSTRAP_TABLE.length; i++) {
        $('#' + BOOTSTRAP_TABLE[i]).on('all.bs.table', function (e, row, ele) {
            autoPlayYouTubeModal()
        });        
    }
}

function autoPlayYouTubeModal(){
    var trigger = $("body").find('[data-toggle="modal"]');
    trigger.click(function() {
        var theModal = $(this).data( "target" ),
        videoSRC = $(this).attr( "data-theVideo" ), 
        videoSRCauto = videoSRC+"?autoplay=1" ;
        $(theModal+' iframe').attr('src', videoSRCauto);
        $(theModal+' button.close').click(function () {
            $(theModal+' iframe').attr('src', videoSRC);
        });   
    });
}


function usageDropdownFormatter(value, row, index) {
    var dropdown = 
    '<select class="form-control" id="' + row.url + "-usageDropdown" + '" onchange="changeURLType(\'' + row.url + '\')">' +
        '<option value="1"' + (row.urlType === 1 ? ' selected="selected"' : '') + '>training</option>' + 
        '<option value="2"' + (row.urlType === 2 ? ' selected="selected"' : '') + '>testing</option>' +
        '<option value="3"' + (row.urlType === 3 ? ' selected="selected"' : '') + '>validation</option>' +
    '</select>'
    return dropdown
}

function usageFormatter(value, row, index) {
    var res = "training"
    switch(value){
        case 1:
            res = "training"
            break
        case 2:
            res = "testing"
            break
        case 3:
            res = "validation"
            break
        default:
    }
    return res    
}

function eventFormatter(value, row, index) {
    return '<a href="#" data-toggle="modal" onclick="openEventSpecificModal(\'' + row.event +'\')" data-target="#eventSpecificModal">' + value + '</a>'
}

function urlFormatter(value, row, index) {
    return '<a href="#" data-toggle="modal" onclick="closeAllModal()" data-target="#videoModal" data-theVideo="http://www.youtube.com/embed/' + value + '">' + value + '</a>'
}

function stateFormatter(value, row, index) {
    // default url
    if (row['urlType'] == 0) return {
        disabled: true,
        checked: true,
    };
}

function jobIDFormatter(value, row, index) {
    return '<a href="#" data-toggle="modal" onclick="openJobModal(\'' + row._id + '\')" data-target="#jobModal">' + value + '</a>'
}

function jobStatusFormatter(value, row, index) {
    var res = "Pending"
    switch(value){
        case "1":
            res = "Pending"
            break
        case "0":
            res = "Complete"
            break
        default:
    }
    return res
}

function disableCheckBox(tableID) {
    if (tableID.charAt(0) != "#") button = $('#' + tableID);
    button[0].checked = true;
    button[0].disabled = true;
}

function closeModalByID(modalID) {
    $('#' + modalID).modal('hide');
}

function closeAllModal() {
    $('.modal').modal('hide');
}

function openEventSpecificModal(eventName) {
    var curJSTree = $("#jstree").jstree(true).get_json("#")
    var id = findJSTreeElementID(curJSTree[0], eventName);
    $('#jstree').jstree(true).deselect_all();
    $('#jstree').jstree(true).select_node(id);
}

function openJobModal(jobID) {
    closeAllModal()
    var curJSTree = getCurrentJSTree()
    // Get and display job info
    var job = getJobElementByID(jobID)
    console.log(job)
    var jobWrapper = []
    jobWrapper.push(job)
    $("#tbl_job_detail").bootstrapTable('load', jobWrapper)
    // Get custom URLs info
    $("#tbl_job_custom_url").bootstrapTable('load', job.customURLs)
    // Get system default URLs info
    $.ajax({
    url: './getDefaultEventURL',
    type: 'POST',
    dataType: 'text',
    data: { "eventname": job.event },
    success: function(response) {
        var urls = []
        if (response) urls = JSON.parse(response).urls;
        $('#tbl_job_default_url').bootstrapTable('load', urls);
        disableCheckBox("tbl_job_default_url");
    }});    
}


function changeURLType(url) {
    var type = parseInt($("#" + url + "-usageDropdown").val())
    //Get current jstree
    //var curJSTree = get_simpleTree($("#jstree").jstree(true).get_json("#")[0])
    var curJSTree = getCurrentJSTree()
    video = getJSTreeVideoElementByURL(curJSTree, url)
    video.urlType = type
    console.log(video)
}

function getAllTaggedURLs(data, res, path) {
    if (data.type != 'event') {
        for (var i = 0; i < data.children.length; i++) {
            path.push(data.text);
            getAllTaggedURLs(data.children[i], res, path);
            path.pop();
        }
    } else {
        for (var i = 0; i < data.data.videoURLs.length; i++) {
            var eventURL = jQuery.extend({}, data.data.videoURLs[i]);
            eventURL['event'] = data.text.substr(1);
            eventURL['path'] = jQuery.extend([], path);
            eventURL['path'].push(data.text);
            eventURL['path'].push(eventURL['url'])
            res.push(eventURL);
        }
    }
}

function getJSTreeEventElementByName(data, eventName) {
    var res = ""
    if (data.type != 'event') {
        for (var i = 0; i < data.children.length; i++) {
            res = getJSTreeEventElementByName(data.children[i], eventName)
            if (res) return res
        }
    } else {
        if (data.text != eventName && data.text != "*" + eventName) return ""
        else return data
    }
}

function getJSTreeVideoElementByURL(data, url) {
    var res = ""
    if (data.type != 'event') {
        for (var i = 0; i < data.children.length; i++) {
            res = getJSTreeVideoElementByURL(data.children[i], url)
            if (res) return res
        }
    } else {
        videos = data.data.videoURLs
        for (var i = 0; i < videos.length; i++) {
            if (url === videos[i].url) return videos[i]
        }
        return ""
    }
}


function getJobElementByID(jobID) {
    var jobs = $("#tbl_trainning_jobs").bootstrapTable('getData')
    for (var i = 0; i < jobs.length; i++) 
        if (jobID === jobs[i]._id) 
            return jobs[i]    
}


function get_simpleTree(node) {
    var newNode = {}
    newNode["text"] = node["text"]
    newNode["type"] = node["type"]
    newNode.children = []
    newNode.data = node["data"]

    if (node.children) {
        for (var i = 0; i < node.children.length; i++) {
            newNode.children.push(get_simpleTree(node.children[i]))
        }
    }
    return newNode
}

// when clicking untag buttun in ALL Tagged events table, remove all the chosen rows both in tagged table and event-specific tables
function removeURLsFromEventTables(data, path) {
    if (path.length == 1 && data.type == 'event') {
        for (var i = 0; i < data.data.videoURLs.length; i++) {
            if (data.data.videoURLs[i].url == path[0]) {
                data.data.videoURLs.splice(i, 1);
                return
            }
        }
    } else {
        var curPath = path.shift();
        for (var i = 0; i < data.children.length; i++) {
            if (data.children[i].text == curPath) {
                data = data.children[i];
                removeURLsFromEventTables(data, path);
                break;
            }
        }
    }
}

// find jstree node ID by eventName
function findJSTreeElementID(data, eventName) {
    var res = "";
    if (data.type != 'event') {
        for (var i = 0; i < data.children.length; i++) {
            res = findJSTreeElementID(data.children[i], eventName);
            if (res != "") return res;
        }
        return ""
    } else {
        if ("*" + eventName == data.text || eventName == data.text) return data.id;
        else return "";
    }
}

//customized the right click menu
function customMenu(node) {
    //Show a different label for renaming files and folders
    var tree = $("#jstree").jstree(true);
    var ID = $(node).attr('id');
    var $mynode = $('#' + ID);
    var renameLabel;
    var deleteLabel;
    var folder = false;
    if (tree.get_type(node) == "category") { //If node is a folder
        //renameLabel = "Rename Folder";
        deleteLabel = "Delete Category";
        folder = true;
    } else {
        //renameLabel = "Rename File";
        deleteLabel = "Delete Event";
    }

    var items = {
        "create_Event": {
            "label": "Create Event",
            "action": function(obj) {
                var ref = $('#jstree').jstree(true),
                    sel = ref.get_selected();
                if (!sel.length) {
                    return false;
                }
                sel = sel[0];
                sel = ref.create_node(sel, { "type": "event", "data": {"videoURLs": []} });
                if (sel) {
                    console.log(sel)

                    ref.edit(sel);
                }

            }
        },

        "create_Category": {
            "label": "Create Category",
            "action": function(obj) {
                var ref = $('#jstree').jstree(true),
                    sel = ref.get_selected();
                if (!sel.length) {
                    return false;
                }
                sel = sel[0];
                sel = ref.create_node(sel, { "type": "category","data": {"videoURLs": []} });
                if (sel) {
                    console.log(sel)
                    ref.edit(sel);
                }

            }
        },

        "delete": {
            "label": deleteLabel,
            "action": function(obj) {
                tree.delete_node(node)
            }
        }
    };

    if (tree.get_type(node) == "event") {
        delete items["create_Event"]
        delete items["create_Category"]
    }

    return items;
}


// get jobs info from server
function getJobInfoFromServer(tableID) {
    $.ajax({
        url: SERVER_BASE_ADDRESS + 'getJobInfo',
        type: 'POST',
        dataType: 'text',
        success: function(response) {
            response = JSON.parse(response)
            console.log(response)
            $("#" + tableID).bootstrapTable('load', response);
        }
    });
}

// Delete jobs
function deleteJobs(sel_jobs) {
    $.ajax({
        url: SERVER_BASE_ADDRESS + 'delete_jobs',
        type: 'POST',
        dataType: 'text',
        data: {"customURLs": JSON.stringify(sel_jobs)},
        success: function(response) {
            if (response === "no") alert("error!")
            else {
                getJobInfoFromServer("tbl_trainning_jobs")
            }
        }
    });   
}

function getCurrentJSTree() {
    return get_simpleTree($("#jstree").jstree(true).get_json("#")[0])
}

addOnTableListener()