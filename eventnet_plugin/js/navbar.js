document.write(
'\
	<header id="header" role="banner">\
	    <div id="navbar" class="navbar navbar-default">\
			<div class="container">\
                <div class="navbar-header">\
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">\
                    </button>\
                </div>\
                <div class="collapse navbar-collapse">\
                    <ul class="nav navbar-nav">\
                        <li id="navHome"><a href="http://mvso.cs.columbia.edu">Home</a></li>\
						<li id="navDownload"><a href="download.html">Download</a></li>\
                        <li id="navAbout"><a href="about.html">About</a></li>\
                    </ul>\
                </div>\
            </div>\
        </div>\
    </header><!--/#header-->\
'
)