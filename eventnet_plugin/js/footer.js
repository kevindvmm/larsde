document.write(
'\
	<footer id="footer">\
		<div class="container">\
			<div class="row">\
				<div class="col-sm-12">\
					&copy; 2015. All Rights Reserved. Affiliated with the <a href="http://www.ee.columbia.edu/ln/dvmm" target="_blank">DVMM Lab at Columbia University</a>, <a href="http://www.idiap.ch" target="_blank">Idiap Research Institute and EPFL</a>, <a href="https://labs.yahoo.com" target="_blank">Yahoo! Labs</a>, & <a href="https://www.jwplayer.com" target="_blank">JWPlayer</a>. <br>Disclaimer: The views and conclusions contained here are those of the MVSO authors and should not be interpreted as representing their employers or sponsors.\
				</div>\
			</div>\
		</div>\
	</footer>\
'
)