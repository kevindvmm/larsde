import json
from pymongo import MongoClient
import logging
import time

# create logger
lgr = logging.getLogger('trainJob')
lgr.setLevel(logging.DEBUG)
# add a file handler
fh = logging.FileHandler('trainJob.log')
fh.setLevel(logging.WARNING)
# create a formatter and set the formatter for the handler.
frmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(frmt)
# add the Handler to the logger
lgr.addHandler(fh)

client = MongoClient()
db = client.eventnet


def svmTrain():
	print "TBD"
	#time.sleep(2)

def main():
	while 1:

		# find pending jobs, N jobs each time
		cursor = db.trainingJob.find({"jobStatus":"1"}).limit(6) 

		print "Handling New " + str(cursor.count(with_limit_and_skip=True))+ " Jobs"
		for job in cursor:
			print "#" * 10
			print job["_id"]
			print job["username"]
			print job["event"]
			print job["customURLs"]
			print job["jobStatus"]
			#print job["customTrainVideos"]
			#print job["customTestVidoes"]

			#Check if all the user-collected videos(Train + Test) are downloaded
			for url in job["customURLs"]:
				if db.newVidoes.find({"youtube_ID": url, "downloaded": "1"}).count() == 0:
					print url " is not downloaded, pending job " + job["_id"]
					lgr.critical(url " is not downloaded, pending job " + job["_id"]) 


			try:
				#Train Job
				status = svmTrain()

				#If training fails
				if status == False:
					#Logging
					lgr.error(job["_id"] + "; " +job["event"] + " Training Failed!")
					continue


				#Update Database if success
				db.trainingJob.update({"_id": job["_id"]}, {'$set': {"jobStatus": "2"}})

				#Logging
				lgr.critical(job["_id"] + "; " + job["event"] + " Training Success") 

			except:
				lgr.error(job["_id"] + "; " +job["event"] + " Training Failed!")
				#lgr.critical('Something critical happened. HongyiTest') 

		# Wait before next query
		time.sleep(2)


if __name__ == '__main__':
	main()