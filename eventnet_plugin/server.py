# -*- coding: utf-8 -*-
from flask import Flask, request,redirect, session
from flask import send_file, make_response, send_from_directory, render_template
from flask import g, jsonify
from datetime import *
import time
import os
import sys, urllib, urllib2, json
import requests
from werkzeug import secure_filename
import codecs
import re
import collections
import ast

from pymongo import MongoClient
from datetime import datetime
import pymongo
from bson.objectid import ObjectId


import sys
reload(sys)
sys.setdefaultencoding("utf-8")



app = Flask(__name__)
BASE_PATH = os.path.dirname(os.path.abspath(__file__))

app.config.from_object(__name__)
#app.config['UPLOAD_FOLDER'] = BASE_PATH + '/uploads'

client = MongoClient() 
db = client.eventnet




@app.route('/js/<path:path>')
def send_js(path):
	return send_from_directory(os.path.join(BASE_PATH,'js'), path)

@app.route('/fonts/<path:path>')
def send_fonts(path):
	return send_from_directory(os.path.join(BASE_PATH,'fonts'), path)

@app.route('/img/<path:path>')
def send_img(path):
	return send_from_directory(os.path.join(BASE_PATH,'img'), path)

@app.route('/css/<path:path>')
def send_css(path):
	return send_from_directory(os.path.join(BASE_PATH,'css'), path)

@app.route('/data/<path:path>')
def send_data(path):
	return send_from_directory(os.path.join(BASE_PATH,'data'), path)



@app.route('/index.html')
def send_index():
	return send_file(os.path.join(BASE_PATH, "index.html"))


@app.route('/miaomiao')
def send_miaomiao():
	return send_file(os.path.join(BASE_PATH, "miaomiao.html"))


	#return render_template(os.path.join(BASE_PATH, "index.html"))

@app.route('/login.html')
def send_login():
	return send_file(os.path.join(BASE_PATH, "login.html"))



# @app.route('/login', methods=['POST'])
# def send_login_check():
# 	if request.method == 'POST':
# 		username = request.form.get("username").strip()
# 		password = request.form.get("password").strip()

# 		print username, password
# 		cursor = db.eventTree.find({"username": username, "password": password})


# 		if cursor.count() == 1:
# 			print "111"
# 			session["username"] = username
# 			return render_template("index.html",data = "")
# 		else:
# 			return "fail to login"

# 		#print username, password
# 		#print newTree


@app.route('/saveTree', methods=['POST'])
def send_saveTree():
	if request.method == 'POST':
		try:
			ontology = json.loads(request.form.get("ontology"))
			untaggedURLs = json.loads(request.form.get("untaggedURLs"))
			#print untaggedURLs
			#print ontology


			if 'username' in session:
				db.eventTree.update({"username": session["username"]}, {'$set':{"ontology": ontology, "untaggedURLs": untaggedURLs}})
				#db.eventTree.update({"username": session["username"]}, {'$set':{"untaggedURLs": untaggedURLs}})


			return jsonify({"status": 1})
		except:
			return jsonify({"status": 0})


@app.route('/getDefaultEventURL', methods=['POST', 'GET'])
def send_defaultEventURL():
    eventName = request.form.get('eventname')
    try:
        client = MongoClient()
        db = client.eventnet
        collec = db.defaultEventURL
        result = collec.find_one({"event": eventName})
        result.pop('_id')
        print(json.dumps(result))
        return json.dumps(result)
    except:
        print("no default eventname: " + eventName)
        return ""


@app.route('/getTree', methods=['POST', 'GET'])
def send_getTree():
	if request.method == 'POST':
		header = request.form.get("header")
		#print newTree
		
		if "username" in session:
			print "user in sesion", session["username"]
			cursor = db.eventTree.find({"username": session["username"]})
			print cursor.count()
			data = cursor.next()
			print "ready to send"

			return jsonify({"ontology": data["ontology"], "untaggedURLs": data["untaggedURLs"]})

		# filePath = "./data/userInfo.json"

		# with open(filePath) as data_file:    
		# 	data = json.load(data_file)
		# #print data
		else:
			print "Failed to get tree"
			return jsonify({})


@app.route('/getJobInfo', methods=['POST'])
def send_jobInfo():
	if request.method == 'POST':
		if "username" in session:
			cursor = db.trainingJob.find({"username": session["username"]})
			res = []
			for item in cursor:
				item['_id'] = str(item['_id'])
				res.append(item)
			return json.dumps(res)
	else:
		return json.dumps([])

		
@app.route('/plugin_login', methods=['POST'])
def send_plugin_login():
	if request.method == 'POST':
		username = request.form.get("username")
		password = request.form.get("password")

		print username, password
		cursor = db.eventTree.find({"username": username, "password": password})

		if cursor.count() == 1:
			session["username"] = username
			return "yes"
		else:
			return "no"


@app.route('/plugin_saveURL', methods=['POST'])
def send_plugin_saveURL():
	if request.method == 'POST':
		username = request.form.get("username")
		url = request.form.get("url")
		des = request.form.get("des")
		youtube_id = url.strip()

		print username, youtube_id, des

		try:
			db.eventTree.update({"username": session["username"]}, {'$push':{"untaggedURLs": {"url": youtube_id, "state":False, "description": des, "urlType": 1, "date":datetime.now().strftime("%Y-%m-%d %H:%M")}}})
			return "yes"
		except:
			return "no"

@app.route('/submit_jobs', methods=['POST'])
def submit_jobs():
	if "username" in session:
		newJob = {}
		newJob['username'] = session['username']
		newJob['event'] = request.form.get("eventname")
		newJob['state'] = False
		newJob['customURLs'] = json.loads(request.form.get("customURLs"))
		newJob['jobStatus'] = "1"
		newJob['date'] = datetime.now().strftime("%Y-%m-%d %H:%M")
		newJob['result'] = {}
		try:
			db.trainingJob.insert_one(newJob)
			return "yes"
		except:
			return "no"
	else:
		return "no"		


@app.route('/delete_jobs', methods=['POST'])
def delete_jobs():
	if "username" in session:
		username = session['username']
		jobIDs = json.loads(request.form.get("customURLs"))
		try:
			for jobID in jobIDs:
				db.trainingJob.delete_one({'_id': ObjectId(jobID)})
			return "yes"
		except:
			return "no"
	else:
		return "no"		



app.secret_key = 'asdr98j/3yX R~XHH!jmN]LWX/,?RT'


if __name__ == '__main__':
	print datetime.now().strftime("%Y-%m-%d %H:%M")
	app.debug = True
	app.run(host='0.0.0.0', port=5678)





	

