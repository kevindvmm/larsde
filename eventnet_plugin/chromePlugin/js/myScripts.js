// Custom JS

$("#user_module").hide()
chrome.tabs.getSelected(null, function(tab) {
    var url = tab.url
    var regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);
    if (match && match[2].length == 11) {
        youtube_id = match[2];

        myFunction(youtube_id);
        localStorage.url = youtube_id;


    } else {
        //alert("unrecogized url!")
        return

    }


});

function myFunction(tablink) {
    // do stuff here
    $("#youtube_embed").attr("src", "http://www.youtube.com/embed/" + tablink)
    //$("#tablink").html("<h4>Current URL: " + tablink + "</h4>");
    console.log(tablink);
}

function render_loggedin() {
    var usr = localStorage.username;
    var psd = localStorage.password;
    var login_module = document.getElementById("login_module");
    //login_module.innerHTML = 'User Name: ' + usr + '<div><button id="save_url">Save URL</button></div><br><div><button id="logout">Logout</button></div><a href="http://localhost:5678/index.html"><button>My Homepage</button></a>';
    $("#login_module").hide()
    $("#user_module").show()

    $("#logout").click(user_logout);
    $("#save_url").click(save_url);
}

function save_url() {
    var des = $("#url_des").val();
    console.log(des)

    $.post("http://localhost:5678/plugin_saveURL", { username: localStorage.username, url: localStorage.url, des: des }, function(data, status) {
        if (data.trim().split('\n')[0] == "yes") alert("URL saved!");
        else alert("URL already exists!");
    });
}

function user_login() {
    var usr = document.getElementById("usrname").value;
    var psd = document.getElementById("passwd").value;


    //$.post("http://128.59.65.112:8080/EventNet_Plugin/UserServlet", {flag:"2", username:usr, password:psd}, function(data, status) {
    $.post("http://localhost:5678/plugin_login", { flag: "2", username: usr, password: psd }, function(data, status) {

        if (data.trim().split('\n')[0] == "yes") {
            localStorage.username = usr;
            localStorage.password = psd;
            render_loggedin();
        } else alert("Login failed!");
    });
}

function user_logout() {
    //string = 'Sure to log out ?'; 
    //if (confirm(string) == true) {
    localStorage.clear();
    location.reload();
    //}
}

if (localStorage.username && localStorage.password) render_loggedin();
else $("#submit").click(user_login);
