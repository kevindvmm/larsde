import os
import sys

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath) + '/'
print dname
# os.chdir(dname)
sys.path.append(dname)

import lasagne

from core.common import *
from core.deep_matcher import *
from core.deep_clf import *
from core.data_manager import *
from core.model_factory import *
from core.base_model import *
# from core.deep_set_models import *
from core.pretrained_models import VGGS, VGG16
from core.set_data_generator import *
from core.hasher import *

'''
how to finetune
'''
# inherit a model from a pretrained model
class MyModel(DeepMatcher, VGG16):
	# this will be called after loading the pretrained model
	def build_model(self):
		''' modify current network architecture, e.g. change output nodes '''
		self.load_pretrained_model()
		# change pretrained model, e.g. change class numbers
		# self.net['fcx'] = DenseLayer(self.net['fc7'], num_units=self.model_params.cls_num)
		# self.net['output'] = NonlinearityLayer(self.net['fcx'], softmax)

		# self.net['output'] = lasagne.layers.FlattenLayer(self.net['pool5'])
		self.net['output'] = lasagne.layers.FlattenLayer(self.net['fc6'])
		# self.net['output'] = self.net['pool5']

class LarsdeRunner(ExptRunnerBase):

	def prepare_data(self):
		data_fn = dname + 'data/larsde/match.lst'
		splited_train_data, self.test_data = load_larsde(data_fn, test_ratio=0.2)
		self.train_data = np.load(dname+'data3/train_data_wenxi.npy')
		# self.train_data = self.train_data[0:2]

		print "splited train data has shape", splited_train_data.shape	
		print "loaded train data has shape", self.train_data.shape
		# assert np.array_equal(splited_train_data, self.train_data), "two train data are not equal."

		print "test data has shape", self.test_data.shape
		
		'''
		for i in range(self.train_data.shape[0]):
			for j in range(self.train_data.shape[1]):
				self.train_data[i][j] = "/home/tianyu/" + self.train_data[i][j][30:]
		for i in range(self.test_data.shape[0]):
			for j in range(self.test_data.shape[1]):
				self.test_data[i][j] = "/home/tianyu/" + self.test_data[i][j][30:]
		self.test_data = self.test_data[0:2]
		'''
		print 'training data shape: {}'.format(self.train_data.shape)
		print 'test data shape: {}'.format(self.test_data.shape)

	def train_model(self):
		# self.train_data = tensor_to_cvimg(self.train_data)
		# self.train_data = self.model.prepare_imgs_for_input(self.train_data)
		self.model.learn_model_with_tripletfns(self.train_data, self.model.train_params.num_epochs)
		self.model.train_params.model_fn = dname + 'data3/train_wenxi_fc6_only.pkl'
		self.model.save_model(self.model.train_params.model_fn)
		print "hello save"

	def build_model(self):
		assert (self.expt_config.train_params.loss_type == LossType.Triplet)
		self.model = MyModel(self.expt_config.model_params, self.expt_config.train_params)
		self.model.build_model()
		# self.model.net = ModelFactory().build_model(self.model.model_params.model_type, self.model.model_params.img_sz)

	def test_model(self):
		# for thr in [0.90, 0.91, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.99, 1.00, 1.01, 1.02, 1.03, 1.04, 1.05]:
		# for thr in [0.60, 0.65, 0.70, 0.75, 0.80, 0.85, 0.90, 0.95]:
		for thr in [1.00, 1.05, 1.10, 1.15, 1.20]:
			self.model.eval_with_tripletfns(self.test_data, thr)
		# self.model.eval_with_tripletfns(self.test_data, 0.75)

	def test_output(self):
		pass
		# self.model.model_params.output_layer_name = 'fc4'
		# output = self.model.get_outputs(self.train_data[0], flatten=False)
		# print output.shape

def run():
	model_type = 0
	if model_type == 0:
		''' train a matcher from scratch '''
		larsde_params = DeepMatcherExptConfig()
		larsde_params.model_params.model_name = 'larsde_2'
		# larsde_params.model_params.model_type = ModelType.CIFAR10
		larsde_params.model_params.output_layer_name = 'output'
		# larsde_params.model_params.cls_num = 10
		larsde_params.model_params.img_sz = (32,32)
		larsde_params.train_params.loss_type = LossType.Triplet
		larsde_params.train_params = TripletLossTrainParams()
		larsde_params.train_params.batch_sz = 16
		# larsde_params.train_params = TripletLossTrainParams()
		larsde_params.train_params.triplet_margin = 0.2
		# larsde_params.train_params.num_triplets = 440
		# larsde_params.train_params.num_epochs = 20
		larsde_params.train_params.num_epochs = 50
		larsde_params.train_params.lrate = 0.001
		# larsde_params.train_params.param_layer_names = ['conv5_1', 'conv5_2', 'conv5_3']
		larsde_params.train_params.param_layer_names = ['fc6']

	runner = LarsdeRunner(larsde_params)
	runner.build_model()
	runner.prepare_data()
	runner.train_model()
	# runner.test_model()

if __name__ == '__main__':
	run()
