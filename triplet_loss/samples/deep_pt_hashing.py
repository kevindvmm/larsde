﻿# custom models and runners for point hashing


import sys
import os
import gzip
import math
import theano
import theano.typed_list
import theano.tensor as T
import lasagne
import cPickle as pickle
from sets import Set
import random
import numpy as np
import time
#import cv2
import scipy
import sklearn
import matplotlib.pyplot as plt
plt.switch_backend('agg')

from lasagne.layers import InputLayer, DenseLayer, DropoutLayer
from lasagne.layers import NonlinearityLayer
from lasagne.layers import Conv2DLayer as ConvLayer
#from lasagne.layers.dnn import Conv2DDNNLayer as ConvLayer
from lasagne.layers import MaxPool2DLayer as PoolLayer
from lasagne.layers import Pool2DLayer
from lasagne.layers import LocalResponseNormalization2DLayer as NormLayer
from lasagne.nonlinearities import softmax, sigmoid, identity

from data_manager import *
from model_factory import ModelType, ModelFactory
from base_model import BaseDeepModel, LossType, TripletType
from FaceDataManager import load_janus_cs2, load_casia, load_ytf, load_ijba
from search_tools import *

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
data_dir = dname + '../data/'
#data_dir="/media/03/JANUS_DATA/"

# prototype model
def build_simple_cnn(img_sz):
    net = {}
    net['input'] = InputLayer((None, 1, img_sz[0], img_sz[1]))
    net['conv1'] = ConvLayer(net['input'], num_filters=32, filter_size=(5, 5),
                             nonlinearity=lasagne.nonlinearities.LeakyRectify(0.25),
                             W=lasagne.init.GlorotUniform())
    net['pool1'] = PoolLayer(net['conv1'], pool_size=(2,2))
    net['conv2'] = ConvLayer(net['pool1'], num_filters=32, filter_size=(5,5),
                             nonlinearity=lasagne.nonlinearities.LeakyRectify(0.25))
    net['pool2'] = PoolLayer(net['conv2'], pool_size=(2,2))
    net['conv3'] = ConvLayer(net['pool2'], num_filters=64, filter_size=(5,5),
                             nonlinearity=lasagne.nonlinearities.LeakyRectify(0.25))
    net['pool3'] = Pool2DLayer(net['conv3'], pool_size=(3,3), mode='average_inc_pad')
    net['fc4'] = DenseLayer(net['dropout3'], num_units=64, nonlinearity=lasagne.nonlinearities.LeakyRectify(0.25))
    net['dropout4'] = DropoutLayer(net['fc4'], p=0.5)
    net['output'] = DenseLayer(net['dropout4'], num_units=10, nonlinearity=softmax)
    #net['fc4'] = DenseLayer(net['dropout4'], num_units=128, nonlinearity=None)
    #net['output'] = NonlinearityLayer(net['fc3'], softmax)
    return net


class DeepHashCustom(BaseDeepHasher):
    def build_model(self):
        #factory = ModelFactory()
        #self.net = factory.build_model(ModelType.CASIA)
        self.net = build_simple_cnn(self.img_sz)
        #self.net = build_set_cnn_hash(self.img_sz)
        return
        # build custom model
        self.net = {}
        self.net['input'] = InputLayer((None, 3, self.img_sz[0], self.img_sz[1]))
        self.net['conv1_1'] = ConvLayer(self.net['input'], num_filters=32, filter_size=3)
        self.net['conv1_2'] = ConvLayer(self.net['conv1_1'], num_filters=64, filter_size=3)
        self.net['pool1'] = PoolLayer(self.net['conv1_2'], pool_size=2, stride=2, ignore_border=False)
        self.net['conv2_1'] = ConvLayer(self.net['pool1'], num_filters=64, filter_size=3)
        self.net['conv2_2'] = ConvLayer(self.net['conv2_1'], num_filters=128, filter_size=3)
        self.net['pool2'] = PoolLayer(self.net['conv2_2'], pool_size=2, stride=2, ignore_border=False)
        self.net['conv3_1'] = ConvLayer(self.net['pool2'], num_filters=96, filter_size=3)
        self.net['conv3_2'] = ConvLayer(self.net['conv3_1'], num_filters=192, filter_size=3)
        self.net['pool3'] = Pool2DLayer(self.net['conv3_2'],
                                 pool_size=7,
                                 stride=2, 
                                 ignore_border=False,
                                 mode='average_exc_pad')
        self.net['drop3'] = DropoutLayer(self.net['pool3'], p=0.4)
        self.net['fc4'] = DenseLayer(self.net['drop3'], 
                                num_units=self.cls_num, 
                                nonlinearity=None)
        self.net['output'] = NonlinearityLayer(self.net['fc4'], softmax)

    def build_hash_model(self):
        # add a hashing layer
        #self.net['hash'] = DenseLayer(self.net['pool5'], num_units=self.code_len, nonlinearity=sigmoid)
        self.net['pred'] = DenseLayer(self.net['fc6'], num_units=self.cls_num, nonlinearity=None)
        self.net['output'] = NonlinearityLayer(self.net['pred'], softmax)
        self.has_built_model = True
        print('classifier hash model built. code len: {}, class number: {}'.format(self.code_len, self.cls_num))

    def get_hash_loss(self):
        # compute hashing cost, compute pair dot product
        pos_pairs = T.imatrix('pos_pairs')  # each row is a pair
        neg_pairs = T.imatrix('neg_pairs')
        neg_loss = T.sum(T.abs_(output[neg_pairs[:,0],:] - output[neg_pairs[:,1],:]), axis=None)
        pos_loss = T.sum(T.abs_(output[pos_pairs[:,0],:] - output[pos_pairs[:,1],:]), axis=None)
        hamming_loss = pos_loss - neg_loss

class DeepHasherMNIST(DeepMLHasher):
    
    def build_model(self):
        factory = ModelFactory()
        self.net = factory.build_model(ModelType.MNIST, None)
        self.model_params.prev_hash_layer = 'dropout4'

''' mnist '''

def init_pthasher_exptconfig_mnist():
    expt_configs = DeepPtHashExptConfig()
    expt_configs.db_name = 'mnist'
    expt_configs.loss_name = 'clf'
    expt_configs.data_format = InputDataFormat.IMG_DATA
    expt_configs.model_params.code_dims = [32]
    expt_configs.model_params.img_sz = (28,28)
    expt_configs.fill_params()
    return expt_configs

class DeepPtHasherMNISTRunner(DeepPtHashRunner):
    def build_model(self):
        self.model = DeepHasherMNIST(self.expt_config.model_params, self.expt_config.train_params)
        self.model.build_model()
        self.model.build_hash_model()

    def prepare_data(self):
        self.expt_config.data_format = InputDataFormat.IMG_DATA
        train_data, train_labels, val_data, val_labels, test_data, test_labels = load_mnist(dname + '/../../../Data/mnist.pkl.gz')
        self.train_data = train_data
        self.train_labels = train_labels
        self.model.model_params.cls_num = len(set(self.train_labels))
        self.test_data = test_data
        self.test_labels = test_labels
        print('loaded training images: {}; testing images: {}'.format(self.train_data.shape[0], self.test_data.shape[0]))

# run tasks related to mnist
def run_mnist():
    mnist_expt_config = init_pthasher_exptconfig_mnist()
    mnist_expt_config.extra_info = 'realsets'
    mnist_expt_config.test_draw_pr = True

    mnist_runner = DeepPtHasherMNISTRunner(mnist_expt_config)
    mnist_runner.build_model()
    mnist_runner.prepare_data()
    mnist_runner.test_model_on_sets()

''' cifar10 '''

def init_pthasher_exptconfig_cifar10():
    expt_configs = DeepPtHashExptConfig()
    expt_configs.db_name = 'cifar10'
    expt_configs.loss_name = 'clf'
    expt_configs.data_format = InputDataFormat.IMG_DATA
    expt_configs.model_params.code_dims = [32]
    expt_configs.model_params.img_sz = (32,32)
    expt_configs.fill_params()
    return expt_configs

class DeepHasherCifar10(DeepMLHasher):
    
    def build_model(self):
        factory = ModelFactory()
        self.net = factory.build_model(ModelType.CIFAR10, None)
        self.model_params.prev_hash_layer = 'dropout4'

class DeepPtHasherCIFAR10Runner(DeepPtHashRunner):
    def build_model(self):
        self.model = DeepHasherCifar10(self.expt_config.model_params, self.expt_config.train_params)
        self.model.build_model()
        self.model.build_hash_model()

    def prepare_data(self):
        self.expt_config.data_format = InputDataFormat.IMG_DATA
        train_data, train_labels, test_data, test_labels = load_cifar10(data_dir+'/cifar-10-python/')
        self.train_data = train_data
        self.train_labels = train_labels
        self.model.model_params.cls_num = len(set(self.train_labels))
        self.test_data = test_data
        self.test_labels = test_labels
        print('loaded training images: {}; testing images: {}'.format(self.train_data.shape[0], self.test_data.shape[0]))

def run_cifar10():
    expt_config = init_pthasher_exptconfig_cifar10()

    expt_config.extra_info = 'realsets'
    expt_config.test_draw_pr = True

    cifar_runner = DeepPtHasherCIFAR10Runner(expt_config)
    cifar_runner.build_model()
    cifar_runner.prepare_data()
    #cifar_runner.test_model()
    cifar_runner.test_model_on_sets()

# main
def train_hash_from_pretrained():
    
    ''' prepare model '''
    print 'loading pretrained model...'
    clf_hasher = DeepHasher()
    # load pretrained model
    #test_img = 'E:\\Images\\Bruce-Lee.jpg'
    clf_hasher.build_pretrained_model()
    #vgg_s_fn = '/mnt/data2/jiefeng/dev/deeplearning/Theano/models/vgg_cnn_s.pkl'
    vgg_s_fn = 'E:/Projects/Github/deeplearning/Theano/models/vgg_cnn_s.pkl'
    clf_hasher.load_model(vgg_s_fn, {"class_names":"synset words", "mean_img":"mean image", "param_vals":"values"})
    #clf_hasher.predict_img(test_img)

    ''' prepare data '''
    #janus_root = '/mnt/data2/Datasets/JANUS/CS2/'
    janus_root = 'N:/Face/JANUS/CS2/data/'
    janus_data = load_janus_cs2(janus_root, 1)
    train_img_fns, train_labels, train_templates, train_class_names, train_label_dict = janus_data['train']
    #casia_img_fns, casia_labels, casia_class_names = load_casia('K:/CASIAWebFace/')

    # learn new model
    img_sz = (224,224)
    clf_hasher.img_sz = img_sz
    clf_hasher.class_names = train_class_names
    clf_hasher.code_len = 32
    clf_hasher.cls_num = len(set(train_labels))
    clf_hasher.build_hash_model()
    #clf_hasher.load_model(savefn)
    savefn = 'clf_hash_janus_32'
    batch_sz = 32
    lrate = 0.01
    to_train = True
    if to_train:
        # load several batches at a time
        batch_num = 0
        for epoch in range(3):
            # randomize for each whole pass
            ids = range(len(train_labels))
            np.random.shuffle(ids)
            rand_img_fns = [train_img_fns[id] for id in ids]
            rand_labels = [train_labels[id] for id in ids]
            for id in range(0, len(rand_img_fns)-batch_sz, batch_sz):
                batch_num += 1
                # decrease learning rate after certain iterations
                if batch_num % 200 == 0 and lrate >= 0.0001:
                    tmp_fn = savefn + '_iter' + str(batch_num) + '.pkl'
                    clf_hasher.save_model(tmp_fn)
                    lrate /= 2
                print('processing epoch {} batch {}'.format(epoch, batch_num))
                cur_batch_fns = rand_img_fns[id:id+batch_sz]
                cur_batch_labels = rand_labels[id:id+batch_sz]
                imgs = load_cv_imgs(cur_batch_fns, img_sz, True)
                imgs = batch_prepare_imgs(imgs, clf_hasher.mean_img)
                clf_hasher.learn_model(imgs, cur_batch_labels, None, None, batch_sz, lrate, 1)
                #clf_hasher.predict_img(test_img)

    #''' evaluate '''
    #db_codes = clf_hasher.encode_samples(db_data)
    #print('db encoded')
    #query_codes = clf_hasher.encode_samples(query_data)
    #print('query encoded')
    #dist_mat = clf_hasher.search_by_codes(db_codes, query_codes)
    #res = clf_hasher.evaluate(dist_mat, db_labels, query_labels)

def train_casia_hash():
    ''' prepare model '''
    print 'loading pretrained model...'
    clf_hasher = DeepHasher()
    test_img = 'E:\\Images\\Bruce-Lee.jpg'

    ''' prepare data '''
    #casia_dir = 'K:/CASIAWebFace/'
    casia_dir = '/mnt/data2/Datasets/CASIAWebFace/'
    casia_img_fns, casia_labels, casia_class_names = load_casia(casia_dir)

    # learn new model
    img_sz = (100,100)
    clf_hasher.img_sz = img_sz
    clf_hasher.class_names = casia_class_names
    clf_hasher.code_len = 64
    clf_hasher.cls_num = len(set(casia_labels))
    clf_hasher.build_model()
    clf_hasher.build_hash_model()
    #clf_hasher.load_model(savefn)
    savefn = 'clf_hash_casia_64bit_64batch'
    batch_sz = 64
    lrate = 0.01
    to_train = True
    if to_train:
        # load several batches at a time
        batch_num = 0
        for epoch in range(3):
            # randomize for each whole pass
            ids = range(len(casia_labels))
            np.random.shuffle(ids)
            img_fns = [casia_img_fns[id] for id in ids]
            labels = [casia_labels[id] for id in ids]
            for id in range(0, len(casia_img_fns)-batch_sz, batch_sz):
                batch_num += 1
                # decrease learning rate after certain iterations
                if batch_num % 300 == 0 and lrate >= 0.00001:
                    tmp_fn = savefn + '_iter' + str(id) + '.pkl'
                    clf_hasher.save_model(tmp_fn)
                    lrate /= 2
                print('processing epoch {} batch {}'.format(epoch, batch_num))
                cur_batch_fns = img_fns[id:id+batch_sz]
                cur_batch_labels = labels[id:id+batch_sz]
                imgs = load_imgs(cur_batch_fns, img_sz, True)
                imgs = batch_prepare_imgs(imgs, clf_hasher.mean_img)
                clf_hasher.learn_model(imgs, cur_batch_labels, None, None, batch_sz, lrate, 1, savefn)

def train_face_model():
    model = DeepFace()
    img_sz = (100,100)
    model.img_sz = img_sz

    #janus_root = '/mnt/data2/Datasets/JANUS/CS2/'
    #janus_root = 'K:/JANUS/CS2/'
    #janus_data = load_janus_cs2(janus_root, 1)
    #img_fns, labels, templates, class_names, dict = janus_data['train']
    casia_dir = 'K:/Face/CASIAWebFace/'
    #casia_dir = '/mnt/data2/Datasets/CASIAWebFace/'
    img_fns, labels, casia_class_names = load_casia(casia_dir)
    ytf_data_dir='/media/03/JANUS_DATA/YouTubeFaces/'
    # dict(
    #     train=[train_img_fns, train_bboxes, train_labels, train_class_names, train_label_dict],
    #     probe=[test_img_fns, test_bboxes, test_labels, test_label_dict]
    #     )
    data = load_ytf(ytf_data_dir, 1)



    model.class_names = casia_class_names
    model.cls_num = len(set(labels))
    model.build_model(ModelType.FACE_CASIA)
    savefn = 'clf_casia'
    batch_sz = 16
    lrate = 0.01
    to_train = True
    if to_train:
        batch_num = 0
        for epoch in range(2):
            # randomize for each whole pass
            ids = range(len(labels))
            np.random.shuffle(ids)
            img_fns = [img_fns[id] for id in ids]
            labels = [labels[id] for id in ids]
            for id in range(0, len(img_fns)-batch_sz, batch_sz):
                batch_num += 1
                # decrease learning rate after certain iterations
                if batch_num % 200 == 0 and lrate >= 0.0001:
                    tmp_fn = savefn + '_iter' + str(batch_num) + '.pkl'
                    model.save_model(tmp_fn)
                    lrate /= 2
                print('processing epoch {} batch {}'.format(epoch, batch_num))
                cur_batch_fns = img_fns[id:id+batch_sz]
                cur_batch_labels = labels[id:id+batch_sz]
                imgs = load_imgs(cur_batch_fns, img_sz, True)
                
                model.learn_model(imgs, cur_batch_labels, None, None, batch_sz, lrate, 1)

def train_face_hash_YTF():
    model = DeepFace()
    img_sz = (100,100)
    model.img_sz = img_sz

    model.build_model(ModelType.FACE_CASIA)
    # Import here

    ytf_data_dir='/media/03/JANUS_DATA/YouTubeFaces/'
    # dict(
    #     train=[train_img_fns, train_bboxes, train_labels, train_class_names, train_label_dict],
    #     probe=[test_img_fns, test_bboxes, test_labels, test_label_dict]
    #     )
    data = load_ytf(ytf_data_dir, 1)

    train_img_fns=data['train'][0]
    train_bboxes=data['train'][1]
    train_labels=data['train'][2]
    train_class_names=data['train'][3]

    model.class_names = train_class_names
    model.cls_num = len(set(train_labels))

    savefn = 'clf_YTF'
    batch_sz = 16
    lrate = 0.01
    to_train = True
    if to_train:
        batch_num = 0
        for epoch in range(2):
            # randomize for each whole pass
            ids = range(len(train_labels))
            np.random.shuffle(ids)
            img_fns = [train_img_fns[id] for id in ids]
            labels = [train_labels[id] for id in ids]
            bboxes = [train_bboxes[id] for id in ids]
            for id in range(0, len(img_fns)-batch_sz, batch_sz):
                batch_num += 1
                # decrease learning rate after certain iterations
                if batch_num % 200 == 0 and lrate >= 0.0001:
                    tmp_fn = savefn + '_iter' + str(batch_num) + '.pkl'
                    model.save_model(tmp_fn)
                    lrate /= 2
                print('processing epoch {} batch {}'.format(epoch, batch_num))
                cur_batch_fns = img_fns[id:id+batch_sz]
                cur_batch_bboxes = bboxes[id:id+batch_sz]
                cur_batch_labels = labels[id:id+batch_sz]
                #imgs = load_imgs(cur_batch_fns, img_sz, True)
                imgs = load_crop_imgs(cur_batch_fns, cur_batch_bboxes, img_sz, True)

                model.learn_model(imgs, cur_batch_labels, None, None, batch_sz, lrate, 1)


def train_triplet_loss():
    # load data
    #janus_root = '/mnt/data2/Datasets/JANUS/CS2/'
    #janus_root = 'K:/Face/JANUS/CS2/'
    #janus_data = load_janus_cs2(janus_root, 1)
    #img_fns, labels, templates, class_names, dict = janus_data['train']
    #gal_img_fns, gal_labels, _ = janus_data['gal']
    #probe_img_fns, probe_labels, _ = janus_data['probe']
    #train_data = load_imgs(img_fns, (28,28))
    #gal_data = load_imgs(gal_img_fns, (28,28))
    #probe_data = load_imgs(probe_img_fns, (28,28))
    
    train_data, train_labels, x_val, y_val, x_test, y_test = load_mnist('E:\Projects\Github\deeplearning\Data\mnist.pkl.gz')
    mode = 0 # 0: train, 1: test
    model_fn = 'mnist_hardtriplet_hash_32.pkl' #'janus_split1_triplet_hash_32.pkl'

    model = DeepHasher()
    model.img_sz = (28,28)
    model.cls_num = 10
    model.build_model()
    if mode == 0:
        # build model
        model.learn_triplets(train_data, train_labels, 128, 0.001, 30)
        model.save_model(model_fn)
    else:
        model.load_model(model_fn)
        #train_codes = model.encode_samples(x_train)
        test_codes = model.encode_samples(x_test)
        dist_mat = comp_distmat(test_codes, None)
        evaluate('MNIST 32bits w triplet loss + hard mining', dist_mat, y_test, y_test)

def form_hash_model_fn(db_name, code_len, loss, extra):
    return '{}_hash_{}_{}_{}.pkl'.format(db_name, code_len, loss, extra)


def run_pt_expt(expt):
    # build model

    # load data

    # train

    # test
    if expt['mode'] == 1:
        model.load_model()
        # get test codes
        code_fn = 'codes/test_codes_{}_set_hash_{}_{}_{}.pkl'.format(db_name, model.code_len, loss_name, extra_info)
        if os.path.exists(code_fn) == False:
            # compute codes
            data = {}       
            # form test point data
            test_pt_data = []
            test_pt_labels = []
            for id in range(len(test_data)):
                aug_imgs = augment_img(test_data[[id]], 5)
                test_pt_data.append(aug_imgs)
                test_pt_labels.append(test_labels[id])
            # encode
            test_set_codes = model.encode_sets(test_set_data)
            #test_pt_codes = model.encode_sets(test_pt_data)
            data['test_set_labels'] = test_set_labels
            #data['test_pt_labels'] = test_pt_labels
            data['test_set_codes'] = test_set_codes
            #data['test_pt_codes'] = test_pt_codes
            pickle.dump(data, open(code_fn, 'wb'))

        # load codes
        print('Loading precomputed codes')
        data = pickle.load(open(code_fn,'rb'))
        test_codes = data['test_set_codes'] #np.concatenate((data['test_set_codes'], data['test_pt_codes']), axis=0)
        test_all_labels = data['test_set_labels']# + data['test_pt_labels']
        test_all_labels = np.asarray(test_all_labels)
        
        cls_labels = gen_cls_labels(test_all_labels)
        # draw codes
        if test_draw_codes == True:
            for label, ids in cls_labels.iteritems():
                plt.figure()
                plt.imshow(test_codes[ids], cmap='jet', aspect='auto')
                save_fn = 'viscodes/{}_ptset_codes_{}_label_{}_test_{}.png'.format(db_name, loss_name, label, extra_info)
                plt.savefig(save_fn)
            print 'codes drawn'
        order_ids=[]
        for label, ids in cls_labels.iteritems():
            if len(order_ids)==0:
                order_ids=ids
            else:
                order_ids=np.hstack((order_ids,ids))

        dist_mat = comp_distmat(test_codes[order_ids], None)
        # visualize distance matrix
        if test_draw_distmat:
            save_fn = 'dists/{}_ptset_hash_{}_{}_{}_test.png'.format(db_name, model.code_len, loss_name, extra_info)
            plt.figure()
            plt.imshow(dist_mat, cmap='jet', aspect='auto')
            plt.colorbar()
            plt.savefig(save_fn)
        # pr curve
        if test_draw_pr:
            evaluate('{} point set hashing {}bits test'.format(db_name, model.code_len), dist_mat, 
                        test_all_labels[order_ids], test_all_labels[order_ids], 
                        'res/{}_set_hash_{}_{}_{}_test.png'.format(db_name, model.code_len, loss_name, extra_info))

if __name__ == '__main__':
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    #run_mnist()
    run_cifar10()

