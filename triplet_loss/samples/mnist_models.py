'''
examples on how to use train different models on mnist dataset

'''


import os,sys
abspath = os.path.abspath(__file__)
dname = os.path.join(os.path.dirname(abspath),'../')
print dname
sys.path.append(dname)
# os.chdir(dname)

def trace(frame, event, arg):
    print "%s, %s:%d" % (event, frame.f_code.co_filename, frame.f_lineno)
    return trace

# for debug
#sys.settrace(trace)

import core
from core.base_model import *
from core.deep_matcher import *
from core.deep_clf import *
from core.data_manager import *
from core.model_factory import *


class MNISTRunner(ExptRunnerBase):
    
    def prepare_data(self):
        data_fn = dname + 'data/mnist.pkl.gz'
        self.train_data, self.train_labels, self.val_data, self.val_labels, self.test_data, self.test_labels = load_mnist(data_fn)
        
    def train_model(self):
        self.model.learn_model(self.train_data, self.train_labels, self.val_data, self.val_labels, self.model.train_params.num_epochs)
        
    def build_model(self):
        factory = ModelFactory()
        if self.expt_config.train_params.loss_type == LossType.Classification:
            # classification
            self.model = DeepClf(self.expt_config.model_params, self.expt_config.train_params)
            self.model.net = factory.build_model(ModelType.MNIST, self.model.model_params.img_sz)
        if self.expt_config.train_params.loss_type == LossType.Triplet:
            # triplet
            self.model = DeepMatcher(self.expt_config.model_params, self.expt_config.train_params)
            self.model.net = factory.build_model(ModelType.MNIST, self.model.model_params.img_sz)
 

def run():
    model_type = 0
    if model_type == 0:
        ''' train a matcher '''
        mnist_params = DeepMatcherExptConfig()
        mnist_params.model_params.model_name = 'mnist'
        mnist_params.model_params.model_type = ModelType.MNIST
        mnist_params.model_params.output_layer_name = 'fc3'
        mnist_params.model_params.cls_num = 10
        mnist_params.model_params.img_sz = (28,28)
        mnist_params.train_params.loss_type = LossType.Triplet
        mnist_params.train_params = TripletLossTrainParams()
        mnist_params.train_params.triplet_margin = 5
        mnist_params.train_params.num_triplets = 3000
        mnist_params.train_params.num_epochs = 5
        mnist_params.train_params.lrate = 0.01
    else:
        ''' train a classifier '''
        mnist_params = DeepClfExptConfig()
        mnist_params.model_params.model_name = 'mnist'
        mnist_params.model_params.model_type = ModelType.MNIST
        mnist_params.model_params.output_layer_name = 'output'
        mnist_params.model_params.cls_num = 10
        mnist_params.model_params.img_sz = (28,28)
        mnist_params.train_params.loss_type = LossType.Classification
        mnist_params.train_params.num_epochs = 5
        mnist_params.train_params.lrate = 0.01
    
    runner = MNISTRunner(mnist_params)
    runner.build_model()
    runner.prepare_data()
    runner.train_model()
    
    
if __name__ == '__main__':
    run()
