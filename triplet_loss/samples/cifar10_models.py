
import os
import sys

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath) + '/../'
print dname
# os.chdir(dname)
sys.path.append(dname)

import lasagne

from core.common import *
from core.deep_matcher import *
from core.deep_clf import *
from core.data_manager import *
from core.model_factory import *
from core.base_model import *
#from core.deep_set_models import *
from core.pretrained_models import VGGS, VGG16
from core.set_data_generator import *
from core.hasher import *

'''
how to finetune
'''
# inherit a model from a pretrained model
class MyModel(DeepClf, VGG16):
    # this will be called after loading the pretrained model
    def build_model(self):
        ''' modify current network architecture, e.g. change output nodes '''
        pass

class MyModelRunner(ExptRunnerBase):
    # all other methods are not affected when finetuning 
    def build_model(self):
        # create model
        self.model = MyModel(self.expt_config.model_params, self.expt_config.train_params)
        # create your own model based on the pretrained model
        #self.model.build_model()



class CIFAR10Runner(ExptRunnerBase):
    
    def prepare_data(self):
        data_fn = dname + 'data/cifar-10-python/'
        self.train_data, self.train_labels, self.test_data, self.test_labels = load_cifar10(data_fn)
        #self.train_data, self.train_labels = gen_real_sets(self.train_data, self.train_labels, None, 10)
        print 'training data shape: {}'.format(self.train_data[0].shape)
        
    def train_model(self):
        #self.train_data = tensor_to_cvimg(self.train_data)
        #self.train_data = self.model.prepare_imgs_for_input(self.train_data)
        self.model.learn_model(self.train_data, self.train_labels, self.val_data, self.val_labels, self.model.train_params.num_epochs)
        #self.model.save_model(self.model.train_params.model_fn)
        
    def build_model(self):
        if self.expt_config.train_params.loss_type == LossType.Classification:
            # classification
            self.model = DeepClf(self.expt_config.model_params, self.expt_config.train_params)
            self.model.net = ModelFactory().build_model(self.model.model_params.model_type, self.model.model_params.img_sz)
        
        if self.expt_config.train_params.loss_type == LossType.Triplet:
            # triplet
            self.model = DeepMatcher(self.expt_config.model_params, self.expt_config.train_params)
            self.model.net = ModelFactory().build_model(self.model.model_params.model_type, self.model.model_params.img_sz)
 
    def test_model(self):
        self.model.eval(self.test_data, self.test_labels, self.test_data, self.test_labels, InputDataFormat.IMG_DATA)
        
    def test_output(self):
        self.model.model_params.output_layer_name = 'fc4'
        output = self.model.get_outputs(self.train_data[0], flattern=False)
        print output.shape


def run():
    model_type = 0
    if model_type == 0:
        ''' train a matcher '''
        cifar10_params = DeepMatcherExptConfig()
        cifar10_params.model_params.model_name = 'cifar10'
        cifar10_params.model_params.model_type = ModelType.CIFAR10
        cifar10_params.model_params.output_layer_name = 'fc4'
        cifar10_params.model_params.cls_num = 10
        cifar10_params.model_params.img_sz = (32,32)
        cifar10_params.train_params.loss_type = LossType.Triplet
        cifar10_params.train_params.batch_sz = 16
        cifar10_params.train_params = TripletLossTrainParams()
        cifar10_params.train_params.triplet_margin = 0.2
        cifar10_params.train_params.num_triplets = 3000
        cifar10_params.train_params.num_epochs = 100
        cifar10_params.train_params.lrate = 0.005
        
    else:
        ''' train a classifier '''
        cifar10_params = DeepClfExptConfig()
        cifar10_params.model_params = DeepHasherParams()
        cifar10_params.model_params.model_name = 'cifar10'
        cifar10_params.model_params.model_type = ModelType.CIFAR10
        cifar10_params.model_params.output_layer_name = 'output'
        cifar10_params.model_params.cls_num = 10
        cifar10_params.model_params.code_dims = [512, 256]
        cifar10_params.model_params.img_sz = (32,32)
        cifar10_params.train_params.loss_type = LossType.Classification
        cifar10_params.train_params.num_epochs = 5
        cifar10_params.train_params.lrate = 0.01 
    
    runner = CIFAR10Runner(cifar10_params)
    runner.build_model()
    runner.prepare_data()
    runner.train_model()
    runner.test_model()
    
if __name__ == '__main__':
    run()