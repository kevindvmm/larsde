﻿
import sys
import os
import gzip
import math
import theano
import theano.typed_list
import theano.tensor as T
import lasagne
import cPickle as pickle
from sets import Set
import random
import numpy as np
import time
#import cv2
import scipy
import sklearn
import matplotlib.pyplot as plt
plt.switch_backend('agg')

from lasagne.layers import InputLayer, DenseLayer, DropoutLayer
from lasagne.layers import NonlinearityLayer
from lasagne.layers import Conv2DLayer as ConvLayer
#from lasagne.layers.dnn import Conv2DDNNLayer as ConvLayer
from lasagne.layers import MaxPool2DLayer as PoolLayer
from lasagne.layers import Pool2DLayer
from lasagne.layers import LocalResponseNormalization2DLayer as NormLayer
from lasagne.nonlinearities import softmax, sigmoid, identity

# add lib path
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
core_dir = os.path.join(dname, '../core/')
sys.path.append(core_dir)
pretrained_model_dir = os.path.join(dname, '../models/')
data_dir = os.path.join(dname, '../data/')

from model_factory import ModelType, ModelFactory
from base_model import BaseDeepModel, LossType
from model_tools import *
from FaceDataManager import load_janus_cs2, load_casia
from search_tools import *
from data_manager import *
from set_data_generator import *
from deep_set_models import *



''' mnist '''

def init_sethasher_exptconfig_mnist():
    expt_configs = DeepSetHashExptConfig()
    expt_configs.db_name = 'mnist'
    expt_configs.loss_name = 'clf'
    expt_configs.data_format = InputDataFormat.IMG_DATA
    expt_configs.model_params.code_dims = [32]
    expt_configs.model_params.img_sz = (28,28)
    expt_configs.train_params.loss_type = LossType.Classification
    expt_configs.fill_params()
    return expt_configs

class DeepSetHasherMNIST(DeepSetMLHasher):
    def build_model(self):
        factory = ModelFactory()
        self.net = factory.build_model(ModelType.MNIST, None)

    def build_set_model(self):
        self.build_model()
        # aggregate set features
        self.net['aggregate5'] =  AggregationLayer(self.net['dropout4'])
        self.model_params.prev_hash_layer = 'aggregate5'

class DeepSetHasherRunnerMNIST(DeepSetHashRunner):
    
    def build_model(self):
        self.model = DeepSetHasherMNIST(self.expt_config.model_params, self.expt_config.train_params)
        self.model.build_set_model()
        self.model.build_hash_model()

    def prepare_data(self):
        self.expt_config.data_format = InputDataFormat.IMG_DATA
        train_data, train_labels, val_data, val_labels, test_data, test_labels = load_mnist(dname + '/../../../Data/mnist.pkl.gz')
        #self.train_data, self.train_labels = gen_real_sets(train_data, train_labels, None, 5)
        self.train_data, self.train_labels = gen_mixed_sets(train_data, train_labels, None, 5, 5, [AugmentType.Crop])
        #self.train_data, self.train_labels = gen_augmented_sets(train_data, train_labels, None, 5, 5, [AugmentType.Crop])
        #self.test_data, self.test_labels = gen_real_sets(test_data, test_labels, None, 5)
        #self.test_data, self.test_labels = gen_augmented_sets(test_data, test_labels, None, 5, 5, [AugmentType.Crop])
        self.test_data, self.test_labels = gen_mixed_sets(test_data, test_labels, None, 5, 5, [AugmentType.Crop])
        self.model.model_params.cls_num = len(set(self.train_labels))
        print('loaded training images: {}; testing images: {}'.format(len(self.train_data), len(self.test_data)))

def run_mnist():
    mnist_expt_config = init_sethasher_exptconfig_mnist()
    mnist_expt_config.extra_info = 'mixedsets'
    mnist_expt_config.train_params.lrate = 0.01
    mnist_expt_config.train_params.step_lr = 1000
    mnist_expt_config.train_params.batch_sz = 32
    mnist_expt_config.train_params.num_epochs = 2
    mnist_expt_config.test_draw_pr = True

    mnist_runner = DeepSetHasherRunnerMNIST(mnist_expt_config)
    mnist_runner.build_model()
    mnist_runner.prepare_data()
    #mnist_runner.train_model()
    mnist_runner.test_model()

''' cifar10 '''

def init_sethasher_exptconfig_cifar10():
    expt_configs = DeepSetHashExptConfig()
    expt_configs.db_name = 'cifar10'
    expt_configs.loss_name = 'clf'
    expt_configs.data_format = InputDataFormat.IMG_DATA
    expt_configs.model_params.code_dims = [32]
    expt_configs.model_params.img_sz = (32,32)
    expt_configs.train_params.loss_type = LossType.Classification
    expt_configs.train_params.param_layer_names = ['OUTPUT']
    expt_configs.fill_params()
    return expt_configs

class DeepSetHasherCifar10(DeepSetMLHasher):
    def build_model(self):
        factory = ModelFactory()
        self.net = factory.build_model(ModelType.CIFAR10, None)

    def build_set_model(self):
        self.build_model()
        # aggregate set features
        self.net['aggregate5'] =  AggregationLayerAll(self.net['dropout4'])
        #self.net['fc5'] = DenseLayer(self.net['aggregate5'], num_units=256)
        #self.net['dropout5'] = DropoutLayer(self.net['fc5'], p=.5)
        #self.net['fc6'] = DenseLayer(self.net['fc5'], num_units=128)
        #self.net['dropout6'] = DropoutLayer(self.net['fc6'], p=.5)
        self.model_params.prev_hash_layer = 'aggregate5'

class DeepSetHasherRunnerCIFAR10(DeepSetHashRunner):
    
    def build_model(self):
        self.model = DeepSetHasherCifar10(self.expt_config.model_params, self.expt_config.train_params)
        self.model.build_set_model()
        self.model.build_hash_model()

    def prepare_data(self):
        self.expt_config.data_format = InputDataFormat.IMG_DATA
        train_data, train_labels, test_data, test_labels = load_cifar10(data_dir+'/cifar-10-python/')
        self.train_data, self.train_labels = gen_real_sets(train_data, train_labels, None, 10)
        #self.train_data, self.train_labels = gen_mixed_sets(train_data, train_labels, None, 5, 5, [AugmentType.Crop])
        #self.train_data, self.train_labels = gen_augmented_sets(train_data, train_labels, None, 5, 5, [AugmentType.Crop])
        self.test_data, self.test_labels = gen_real_sets(test_data, test_labels, None, 20)
        self.test_data, self.test_labels = gen_augmented_sets_for_sets(self.test_data, self.test_labels, 30, 8, [AugmentType.Crop])
        #self.test_data, self.test_labels = gen_augmented_sets(test_data, test_labels, None, 5, 5, [AugmentType.Crop])
        #self.test_data, self.test_labels = gen_mixed_sets(test_data, test_labels, None, 5, 5, [AugmentType.Crop])
        self.model.model_params.cls_num = len(set(self.train_labels))
        print('loaded training images: {}; testing images: {}'.format(len(self.train_data), len(self.test_data)))

def run_cifar10():
    expt_config = init_sethasher_exptconfig_cifar10()
    expt_config.extra_info = ''

    expt_config.model_params.code_dims = [32]
    expt_config.train_params.lrate = 0.01
    expt_config.train_params.step_lr = 1000
    expt_config.train_params.num_epochs = 10
    expt_config.test_draw_pr = True
    expt_config.fill_params()

    runner = DeepSetHasherRunnerCIFAR10(expt_config)
    runner.build_model()
    runner.prepare_data()
    #runner.load_model()
    #runner.train_model()
    runner.test_model()

# deep set hasher based on casia face model
class DeepSetModelFromCasia(DeepMLHasher):
    def build_model(self):
        factory = ModelFactory()
        self.net = factory.build_model(ModelType.FACE_CASIA, None)

    def build_set_model(self):
        self.net['aggregate5'] =  AggregationLayer(self.net['pool5'])
        self.net['hash'] = DenseLayer(self.net['aggregate5'], num_units=self.code_len, nonlinearity=sigmoid)
        if self.loss_type == LossType.Classification:
            self.net['fc7'] = DenseLayer(self.net['hash'], num_units=self.cls_num,
                                nonlinearity=None)
            self.net['output'] = NonlinearityLayer(self.net['fc7'], softmax)

def run_set_hash(mode):
    # setting
    db_name = 'janus'
    loss_name = 'clf'
    target = ''
    extra_info = '_batch32'
    test_draw_distmat = True
    test_draw_codes = False
    test_draw_pr = False

    # build model
    model = DeepSetModelFromCasia(LossType.Classification)  #DeepSetModelCifar10(LossType.Classification)
    model.img_sz = (100,100)
    model.cls_num = 10
    model.code_len = 64
    model.step_lr = 1
    model.step_to_save = 6
    model.gamma_lr = 0.5
    model.model_fn_prefix = 'models/{}_set_hash_{}bit_{}_{}'.format(db_name, model.code_len, loss_name, extra_info)
    model_fn = model.model_fn_prefix + '.pkl'   # final save file
    model.build_model()
    model.build_set_model()

    # load data
    train_data, train_labels, test_data, test_labels = load_cifar10(data_dir+'cifar-10-python/')
    #train_data, train_labels, val_data, val_labels, test_data, test_labels = load_mnist(data_dir+'mnist.pkl.gz')

    # train model
    if mode == 0:
        # form training set data
        print("Length train_data is {} and length train_labels is {}.".format(train_data,train_labels))
        train_set, train_set_labels = form_sets(train_data, train_labels)
        # form point data
        print "Filling points samples"
        sys.stdout.flush()
        for id in range(len(train_data)):
            # augment images
            aug_imgs = augment_img(train_data[[id]], 5)
            train_set.append(aug_imgs)
            #train_set.append(train_data[[id]])
            train_set_labels.append(train_labels[id])
            count += 1
            if np.mod(count,500)==0:
                sys.stdout.write(".")
                #sys.stdout.write(".{}-{}".format(id,train_labels[id]))
        sys.stdout.write("\n")
        print "Creating numpy arrays"
        sys.stdout.flush()
        train_set = np.asarray(train_set)
        train_set_labels = np.asarray(train_set_labels)
        print train_set_labels
        sys.stdout.flush()
        # train model
        print('total training sample num: {}'.format(len(train_set)))
        model.learn_model(train_set, train_set_labels, None, None, 32, 0.01, 5)
        model.save_model(model_fn)

    # test model
    if mode == 1:
        model.load_model(model_fn)
        # get test codes
        code_fn = 'codes/test_codes_{}_set_hash_{}_{}_{}.pkl'.format(db_name, model.code_len, loss_name, extra_info)
        if os.path.exists(code_fn) == False:
            # compute codes
            data = {}     
            # form testing set data
            test_set_data = []
            test_set_labels = []
            set_ids = form_set_data(test_labels, 5)
            for key, ids in set_ids.iteritems():
                test_set_data.append(test_data[ids])
                test_set_labels.append(test_labels[key])    
            # form testing point data
            # test_pt_data = []
            # test_pt_labels = []
            # for id in range(len(test_data)):
            #     aug_imgs = augment_img(test_data[[id]], 5)
            #     test_pt_data.append(aug_imgs)
            #     test_pt_labels.append(test_labels[id])
            # encode
            test_set_codes = model.encode_sets(test_set_data)
            #test_pt_codes = model.encode_sets(test_pt_data)
            data['test_set_labels'] = test_set_labels
            #data['test_pt_labels'] = test_pt_labels
            data['test_set_codes'] = test_set_codes
            #data['test_pt_codes'] = test_pt_codes
            pickle.dump(data, open(code_fn, 'wb'))

        # load codes
        print('Loading precomputed codes')
        data = pickle.load(open(code_fn,'rb'))
        test_codes = data['test_set_codes'] #np.concatenate((data['test_set_codes'], data['test_pt_codes']), axis=0)
        test_all_labels = data['test_set_labels']# + data['test_pt_labels']
        test_all_labels = np.asarray(test_all_labels)
        
        cls_labels = gen_cls_labels(test_all_labels)
        # draw codes
        if test_draw_codes == True:
            for label, ids in cls_labels.iteritems():
                plt.figure()
                plt.imshow(test_codes[ids], cmap='jet', aspect='auto')
                save_fn = 'viscodes/{}_ptset_codes_{}_label_{}_test_{}.png'.format(db_name, loss_name, label, extra_info)
                plt.savefig(save_fn)
            print 'codes drawn'
        order_ids=[]
        for label, ids in cls_labels.iteritems():
            if len(order_ids)==0:
                order_ids=ids
            else:
                order_ids=np.hstack((order_ids,ids))

        dist_mat = comp_distmat(test_codes[order_ids], None)
        # visualize distance matrix
        if test_draw_distmat:
            save_fn = 'dists/{}_ptset_hash_{}_{}_{}_test.png'.format(db_name, model.code_len, loss_name, extra_info)
            plt.figure()
            plt.imshow(dist_mat, cmap='jet', aspect='auto')
            plt.colorbar()
            plt.savefig(save_fn)
        # pr curve
        if test_draw_pr:
            evaluate('{} point set hashing {}bits test'.format(db_name, model.code_len), dist_mat, 
                     test_all_labels[order_ids], test_all_labels[order_ids], 
                     'res/{}_set_hash_{}_{}_{}_test.png'.format(db_name, model.code_len, loss_name, extra_info))

def run_face_set_hash(mode):
    # setting
    db_name = 'ytf'
    loss_name = 'clf'
    target = ''
    extra_info = '_batch32'
    test_draw_distmat = True
    test_draw_codes = False
    test_draw_pr = False

    # build model
    model = None
    model.build_model(ModelType.FACE_CASIA)
    # Import here
    model.load_caffe_weights(dname+'data/casia/CASIA_deploy.prototxt', 
                             dname+'data/casia/casia_7pts_similarity_v2_iter_2000000.caffemodel')
    model = DeepSetModelCifar10(LossType.Classification)
    model.img_sz = (32,32)
    model.cls_num = 10
    model.code_len = 64
    model.step_lr = 1
    model.step_to_save = 6
    model.gamma_lr = 0.5
    model.model_fn_prefix = 'models/{}_set_hash_{}bit_{}_{}'.format(db_name, model.code_len, loss_name, extra_info)
    model_fn = model.model_fn_prefix + '.pkl'   # final save file
    model.build_model()
    model.build_set_model()


if __name__ == '__main__':
    #run_mnist()
    run_cifar10()
