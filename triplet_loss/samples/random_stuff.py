'''
some random stuff
'''

import cPickle as pickle
import os
import sys
import numpy as np
from scipy.io import loadmat, savemat

#from face.face_set import CASIAFaceModel

def test_model():
    model = CASIAFaceModel(None, None)
    model.build_model()
    

def load_janus_mat():
    try:
        mat_fn = './face/TemplateResultsCS2-2016-05-09/results1_512bits_Deepbinary_rgb_HashCNN_noavgwithFlipProbeMeanMin.mat'
        mat_data = loadmat(mat_fn)
        probe_temp_ids = mat_data['query_ids'][0]
        gal_temp_ids = mat_data['target_ids'][0]
        tmp = np.where(probe_temp_ids == 1000)[0]
        print tmp
    except Exception as ex:
        print ex
    

def run():
    with open('../data/face/cs2_alldata.pickle', 'rb') as f:
        print 'loading cs2 alldata'
        data = pickle.load(f)
        print 'cs2 alldata loaded'
    feats_jc = data['FEATS_JC']
    feats_swami = data['FEATS_SWAMI']
    subject_ids = data['METADATA']['SUBJECT_ID']
    template_ids = data['METADATA']['TEMPLATE_ID']
    splits_info = data['METADATA']['SPLITS_INFO']
    media_type = data['METADATA']['MEDIA_TYPE']
    # get all images for split_id
    split_samp_ids = np.argwhere(splits_info==1)
    all_sets = []
    all_set_labels = []
    temp_id_set = np.unique(template_ids[split_samp_ids])
    for i in temp_id_set:
        feat_ids = np.argwhere(template_ids == i)[:,0]
        #print feat_ids
        jc = feats_jc[feat_ids]
        swami = feats_swami[feat_ids]
        #print jc.shape
        #print swami.shape
        cur_feats = np.concatenate((jc, swami), axis=1)
        #print cur_feats.shape
        all_sets.append(cur_feats)
        #print feat_ids[0]
        cur_label = int(subject_ids[feat_ids[0]])
        #print cur_label
        all_set_labels.append(cur_label)
    print 'loaded {} templates'.format(len(all_sets))
    
    
if __name__ == '__main__':
    #test_model()
    run()
    #load_janus_mat()
