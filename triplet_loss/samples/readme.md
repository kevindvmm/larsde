#### What to put here

simple, small code to demonstrate the usage of the library (usually accompany new features or models).
if you are working on a larger project using deepmodels, please create a separate repo and reference it.