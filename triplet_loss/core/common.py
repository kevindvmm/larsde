'''
library-wise definition
'''

import os
import sys

# constant values
core_dir = os.path.dirname(os.path.abspath(__file__))


# types
class InputDataFormat():
    FILE = 0,
    IMG_DATA = 1
    
class ModelType:
    IMAGENET_VGG16 = 0
    IMAGENET_VGG19 = 1
    IMAGENET_GNET = 2
    IMAGENET_VGG_S = 3
    IMAGENET_VGG_S_GRAY = 13
    FACE_CASIA = 4
    FACE_CASIA_COLOR = 5
    FACE_CASIA_COLOR_PRELU = 6
    MNIST = 8
    CIFAR10 = 9
    CIFAR100 = 10
    CIFAR10_NIN = 11
    FACE_VGG = 12
    GOOGLENET = 13
    
class LossType:
    Classification = 0
    Triplet = 1
    TripletClassification = 2
    PairClassification = 3

    
class TripletDist:
    SquaredL2 = 0
    Hamming = 1
    L1 = 2

class TripletType:
    Random = 0
    Hard = 1