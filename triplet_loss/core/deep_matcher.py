'''
deep model for matching images
use metric loss, e.g. triplet loss

'''

import time
import random
import sys

import theano
import theano.tensor as T

import lasagne
from lasagne.layers import InputLayer, DenseLayer, DropoutLayer
from lasagne.layers import NonlinearityLayer
from lasagne.layers import Conv2DLayer as ConvLayer
#from lasagne.layers.dnn import Conv2DDNNLayer as ConvLayer
from lasagne.layers import Pool2DLayer
from lasagne.layers import LocalResponseNormalization2DLayer as NormLayer
from lasagne.nonlinearities import softmax, sigmoid, identity

from common import LossType, TripletDist, TripletType, InputDataFormat
from base_model import ModelParamsBase, TrainParamsBase, BaseModel, ExptConfigBase
from data_manager import *
from search_tools import *


class TripletModelParams(ModelParamsBase):
    pass
    
class TripletLossTrainParams(TrainParamsBase):
    loss_type = LossType.Triplet
    triplet_margin = 1
    triplet_mode = TripletType.Random
    num_triplets = 100
    
class DeepMatcherExptConfig(ExptConfigBase):
    def fill_params():
        self.train_params.model_fn_prefix = \
            '../models/{}_matcher_{}_{}'.format(self.db_name, self.loss_name, self.extra_info)
        self.train_params.model_fn = self.train_params.model_fn_prefix + '.pkl'
 

# it is usually advised to train a classifier first as initialization and finttune a matcher
class DeepMatcher(BaseModel):
    def create_iter_train_func(self, lrate, th=0.5):
        if self.train_params.loss_type == LossType.Triplet:
            # TODO: move customized code to a subclass instead of in the base class
            # if input is all datal plus ids
            #triplet_ids = T.imatrix("triplet_ids") 
            #dist1 = ((output[triplet_ids[:,0]]-output[triplet_ids[:,1]])**2).sum(axis=1)
            #dist2 = ((output[triplet_ids[:,0]]-output[triplet_ids[:,2]])**2).sum(axis=1)
            
            # output are stacked: repeating (anchor, positive, negative)
            output = lasagne.layers.get_output(self.net[self.model_params.output_layer_name])
            
            if hasattr(self.train_params, 'triplet_dist') and self.train_params.triplet_dist == TripletDist.L1:
                # L1
                dist1 = (abs(output[::3]-output[1::3])).sum(axis=1)
                dist2 = (abs(output[::3]-output[2::3])).sum(axis=1)
            else:
                # l2 normalization
                output  = output / output.norm(2, axis=1).reshape((output.shape[0],1))
                dist1 = ((output[::3]-output[1::3])**2).sum(axis=1)
                dist2 = ((output[::3]-output[2::3])**2).sum(axis=1)
                
            dist = dist1 - dist2 + self.train_params.triplet_margin
            samp_sgn = T.gt(dist, 0.0)
            train_loss = T.sum(dist * samp_sgn)/T.shape(output)[0]*3
            # compute percentage of triplet with loss 0
            useful_num = samp_sgn.sum()
            # updates for training
            #params = lasagne.layers.get_all_params(self.net['output'], trainable=True)
            params = self.get_target_params(for_training=True)
            #print('total trainable parameter number: {}'.format(lasagne.layers.count_params(self.net['output'])))
            print('total trainable parameter number: {}'.format(lasagne.layers.count_params(self.net[self.model_params.output_layer_name])))
            print('params parameter number: {}'.format(len(params)))
            print('train with learning rate: {:.6f}'.format(lrate))
            updates = lasagne.updates.nesterov_momentum(train_loss, params, learning_rate=lrate, momentum=0.9)
            # functions
            start_comp = time.time()
            train_fn = theano.function(
                [self.net['input'].input_var], 
                train_loss, updates=updates)
            print "Train function compilation took:",str(time.time()-start_comp)+"s."
            return train_fn
        else:
            raise NotImplementedError('other matching loss is not implemented')
            
    # NOT TESTED
    # triplet_fns is a Nx3 array, each row is a triplet (anchor, positive, negative), each cell is a filename
    def learn_model_with_tripletfns(self, train_triplet_fns, num_epochsm, verbose = False):
        if train_triplet_fns.shape[1] != 3:
            raise Exception('invalid triplet files for training, it has to be a Nx3 string matrix')
        
        if 'train' not in self.iter_funcs:
            self.create_iter_funcs(self.train_params.lrate)
        
        train_triplet_fns = np.asarray(train_triplet_fns)
        total_batches = 0
        total_loss = 0
        # We iterate over epochs:
        # cls_combs = gen_cls_combs(train_labels)
        start_time = time.time()
        flag = time.time()
        for epoch in range(self.train_params.num_epochs): # this is a local epoch when training in large scale mode
            if np.mod(epoch, self.train_params.step_lr)==0 and epoch>0:
                lrate = self.train_params.gamma_lr * lrate
                print("Learning rate is now {}".format(lrate))
                self.create_iter_funcs(lrate)

            # print 'running random triplets mode...'
            # load image and form triplet data
            # In each epoch, we do a full pass over the training data:
            train_loss = 0
            train_batches = 0
            # batch triplets
            for batch in range(0, train_triplet_fns.shape[0], self.train_params.batch_sz):
                batch_startt = time.time()
                cur_triplets_fns = train_triplet_fns[batch:min(train_triplet_fns.shape[0],batch+self.train_params.batch_sz)]
                # print "cur_triplets.shape: {}".format(cur_triplets_fns.shape)
                # form input data
                input_data = []
                for id in range(len(cur_triplets_fns)):
                    # print batch
                    # print id
                    img_fns = [cur_triplets_fns[id][0], cur_triplets_fns[id][1], cur_triplets_fns[id][2]]
                    imgs = self.prepare_imgfns_for_input(img_fns)
                    if len(imgs) < 3:
                        continue
                    input_data.append(imgs[0])
                    input_data.append(imgs[1])
                    input_data.append(imgs[2])
                loss = self.iter_funcs['train'](input_data)
                train_loss += loss
                total_loss += loss
                train_batches += 1
                total_batches += 1
                
		sys.stdout.write(".")
		sys.stdout.flush()
                if verbose: 
                    print("Epoch {}/{}, batch {} took {:.3f}s".format(
                        epoch+1, self.train_params.num_epochs, train_batches, 
                        time.time() - batch_startt))
                    print("  training loss:\t\t{:.6f}".format(total_loss / total_batches))
            if time.time()-flag >= 60: 
		sys.stdout.write("\n")
		sys.stdout.flush()
                print("Epoch {}/{}, took {:.3f}s".format(
                    epoch+1, self.train_params.num_epochs,  
                    time.time() - start_time))
                print("  training loss:\t\t{:.6f}".format(total_loss / total_batches))
                flag = time.time()
        
        if total_batches==0:
                total_batches=1
        return total_loss / total_batches
        
    def learn_model(self, train_data, train_labels, val_data, val_labels, num_epochs):
        ## Only for hard triplets?
        #outputs = lasagne.layers.get_output(self.net['output']) # output or output_layer_name?
        #output_fn = theano.function([self.net['input'].input_var], outputs)
        # launch the training loop
        #mode = 1 # 0: random triplets; 1: hard triplets
        if 'train' not in self.iter_funcs:
            self.create_iter_funcs(self.train_params.lrate)
        
        print 'training in normal mode'
        total_batches = 0
        total_loss = 0
        # We iterate over epochs:
        cls_combs = gen_cls_combs(train_labels)
        for epoch in range(self.train_params.num_epochs): # this is a local epoch when training in large scale mode
            start_time = time.time()
            ''' generate meaningful triplets
                idea1: 1) must cover each identity in each iteration
                        2) each batch selects a subset of identity and a subset of samples within each identity
                        3) triplets are selected from the subset of samples
                idea2: 1) must cover each sample (as anchor) in each iteration
                        2) each batch selects a subset of samples as anchor points
                        3) triplets are selected from the subset of samples
            '''
            if np.mod(epoch, self.train_params.step_lr)==0 and epoch>0:
                lrate=self.train_params.gamma_lr*lrate
                print("Learning rate is now {}".format(lrate))
                self.create_iter_funcs(lrate)

            if self.train_params.triplet_mode == TripletType.Random:
                print 'running random triplets mode...'
                # create triplets for current epoch
                all_triplet_ids = gen_random_triplets(train_labels, self.train_params.num_triplets)
                print "all_triplet_ids.shape",all_triplet_ids.shape
                if all_triplet_ids.shape[0]==0:
                    print "Triplet generation did not produce valid triplets, skipping epoch"
                    continue
                # get unique triplets
                triplet_ids = np.vstack({tuple(row) for row in all_triplet_ids})
                print "triplet_ids.shape",triplet_ids.shape
                random.shuffle(triplet_ids)
                # In each epoch, we do a full pass over the training data:
                train_loss = 0
                train_batches = 0
                # batch triplets
                for batch in range(0, triplet_ids.shape[0], self.train_params.batch_sz):
                    batch_startt = time.time()
                    cur_triplets = triplet_ids[batch:min(triplet_ids.shape[0],batch+self.train_params.batch_sz)]
                    print "cur_triplets.shape",cur_triplets.shape
                    # form input data
                    input_data = []
                    for id in range(len(cur_triplets)):
                        input_data.append(train_data[cur_triplets[id,0]])
                        input_data.append(train_data[cur_triplets[id,1]])
                        input_data.append(train_data[cur_triplets[id,2]])
                    #input_data = np.empty((cur_triplets.shape[0]*3, train_data.shape[1], train_data.shape[2], train_data.shape[3]), dtype=theano.config.floatX)
                    #input_data[::3] = train_data[cur_triplets[:,0]]
                    #input_data[1::3] = train_data[cur_triplets[:,1]]
                    #input_data[2::3] = train_data[cur_triplets[:,2]]
                    loss = self.iter_funcs['train'](input_data)
                    train_loss += loss
                    total_loss += loss
                    train_batches += 1
                    total_batches += 1
                    print("Epoch {}/{}, batch {} took {:.3f}s".format(
                        epoch+1, self.train_params.num_epochs, train_batches, 
                        time.time() - batch_startt))
                    print("  training loss:\t\t{:.6f}".format(total_loss / total_batches))
            else:
                print 'running hard triplets mode...'
                # each comb defines a triplet space
                random.shuffle(cls_combs)
                train_loss = 0
                train_batches = 0
                batch_step = 3
                for batch in np.arange(0, len(cls_combs), step=batch_step):
                    batch_startt = time.time()
                    # select identities for current batch
                    cur_cls_combs = cls_combs[batch:batch+batch_step]
                    triplets = np.asarray([])
                    for comb in cur_cls_combs:
                        # sample images for class combs
                        cls_samps = gen_samps_from_cls(train_labels, comb, 10)
                        cur_sel_samps = cls_samps[0] + cls_samps[1]
                        # find hard triplets
                        outputs = output_fn(train_data[cur_sel_samps])
                        cur_labels = train_labels[cur_sel_samps]
                        cur_triplets = gen_hard_triplets(outputs, cur_labels, batch_size*2)
                        # update to global ids
                        cur_triplets[:,0] = np.asarray(cur_sel_samps)[cur_triplets[:,0]]
                        cur_triplets[:,1] = np.asarray(cur_sel_samps)[cur_triplets[:,1]]
                        cur_triplets[:,2] = np.asarray(cur_sel_samps)[cur_triplets[:,2]]
                        if triplets.size == 0:
                            triplets = cur_triplets
                        else:
                            triplets = np.vstack((triplets, cur_triplets))
                    
                    print triplets.shape
                    for sel_triplets_ind in np.arange(0, len(triplets), step=batch_size):
                        sel_triplets = triplets[sel_triplets_ind:sel_triplets_ind+batch_size]
                        print sel_triplets.shape
                        # veritify triplet correctness
                        for triplet in sel_triplets:
                            assert train_labels[triplet[0]] == train_labels[triplet[1]] and train_labels[triplet[0]] != train_labels[triplet[2]]
                        # form input data
                        input_data = np.empty((batch_size*3, train_data.shape[1], train_data.shape[2], train_data.shape[3]), dtype=theano.config.floatX)
                        input_data[::3] = train_data[sel_triplets[:,0]]
                        input_data[1::3] = train_data[sel_triplets[:,1]]
                        input_data[2::3] = train_data[sel_triplets[:,2]]
                        if self.loss_type == LossType.TripletClassification:
                            train_targets = np.empty((batch_size*3), dtype=np.int32)
                            train_targets[::3] = train_labels[sel_triplets[:,0]]
                            train_targets[1::3] = train_labels[sel_triplets[:,1]]
                            train_targets[2::3] = train_labels[sel_triplets[:,2]]
                            loss = self.iter_funcs['train'](input_data,train_targets)
                        else:
                            loss = self.iter_funcs['train'](input_data)
                        #print('useful triplets ratio: {}'.format(useful_num/batch_size))
                        train_loss += loss
                        total_loss += loss
                        train_batches += 1
                        total_batches += 1
                        print("Epoch {}/{}, batch {} took {:.3f}s".format(
                            epoch+1, num_epochs, train_batches,
                            time.time() - batch_startt))
                        print("  training loss:\t\t{:.6f}".format(train_loss / train_batches))
            if np.mod(epoch, self.train_params.step_save)==0 and epoch>0:
                self.save_model(self.train_params.model_fn_prefix+"_epoch"+str(epoch)+".pkl")
            print("Epoch {}/{} took {:.3f}s".format(epoch+1, self.train_params.num_epochs, time.time() - start_time))
        if total_batches==0:
            total_batches=1
        return total_loss / total_batches
        
    # find nearest neighors and test accuracy
    def eval(self, gal_data, gal_labels, probe_data, probe_labels, input_type=InputDataFormat.IMG_DATA):
        # get output
        if input_type == InputDataFormat.IMG_DATA:
            gal_outputs = self.get_outputs(gal_data)
            probe_outputs = self.get_outputs(probe_data)
        if input_type == InputDataFormat.FILE:
            gal_outputs = self.get_outputs_from_files(gal_data)
            probe_outputs = self.get_outputs_from_files(probe_data)
        # do match
        dist_mat = comp_distmat(probe_outputs, gal_outputs, DistType.L2)
        evaluate('match results', dist_mat, gal_labels, probe_labels)

    '''
    # find nearest neighors and test accuracy
    def eval_with_tripletfns(self, gal_data, thr):
        gal_outputs = self.get_outputs_from_files(gal_data.flatten())
        pos_thr = 0
        pos_rnk = 0
        avg_pos = 0.
        avg_neg = 0.
        for i in range(len(gal_data)):
            dist_mat = comp_distmat(gal_outputs[i*3:i*3+3], None, DistType.L2)
            # print dist_mat
#            if dist_mat[0,1] < dist_mat[0,2]:

            avg_pos += dist_mat[0,1]
            avg_neg += dist_mat[0,2]
            print "hello"
            print
            print dist_mat
            print
            print dist_mat.shape
            print
            exit()
            if dist_mat[0,1] < thr and thr < dist_mat[0,2]:
                pos_thr += 1
            if dist_mat[0,1] < dist_mat[0,2]:
                pos_rnk += 1
                
        avg_pos /= float(len(gal_data))
        avg_neg /= float(len(gal_data))
        print ("avg pos= %f; avg neg= %f" %(avg_pos, avg_neg))
        
        acc_thr = float(pos_thr)/float(len(gal_data))
        print ("thresholding accuracy= %f" %acc_thr)
        acc_rnk = float(pos_rnk)/float(len(gal_data))
        print ("ranking accuracy= %f" %acc_rnk)
    '''

    # find nearest neighors and test accuracy
    def eval_with_tripletfns(self, frame_count, frame_1_dir, frame_1_location, frame_2_dir, frame_2_location, gal_data, thr):
        frame_1_dir_len = len(frame_1_dir)
        frame_2_dir_len = len(frame_2_dir)
        gal_outputs = self.get_outputs_from_files(gal_data.flatten())
	print "gal_outputs has type", type(gal_outputs), "and shape", gal_outputs.shape
	print gal_outputs
        dist_mat = comp_distmat(gal_outputs, None, DistType.L2)
        dist_mat = dist_mat[0:frame_1_dir_len, frame_1_dir_len:(frame_1_dir_len+frame_2_dir_len)]
        while True:
            imin = thr
            for i in range(frame_1_dir_len):
                for j in range(frame_2_dir_len):
                    if dist_mat[i][j] < imin:
                        imin = dist_mat[i][j]
                        ix = i
                        iy = j
            if imin < thr:
                ilocation = frame_2_location[iy]
                frame_2_location[iy] = frame_1_location[ix]
                frame_2_location[iy].extend(ilocation)
                for j in range(frame_2_dir_len):
                    dist_mat[ix][j] = thr
                for i in range(frame_1_dir_len):
                    dist_mat[i][iy] = thr
            else:
                break

        return frame_1_dir, frame_1_location, frame_2_dir, frame_2_location

        '''
        if frame_count > 0:
            print "hello"
            print
            print frame_1_dir_len
            print
            print frame_2_dir_len
            print
            print gal_data
            print
            print gal_data.shape
            print
            print dist_mat
            print
            print dist_mat.shape
            print
            print frame_count
            print
        '''

        '''
        pos_thr = 0
        pos_rnk = 0
        avg_pos = 0.
        avg_neg = 0.
        for i in range(len(gal_data)):
            dist_mat = comp_distmat(gal_outputs[i*3:i*3+3], None, DistType.L2)
            # print dist_mat
#            if dist_mat[0,1] < dist_mat[0,2]:

            avg_pos += dist_mat[0,1]
            avg_neg += dist_mat[0,2]
            print "hello"
            print
            print dist_mat
            print
            print dist_mat.shape
            print
            exit()
            if dist_mat[0,1] < thr and thr < dist_mat[0,2]:
                pos_thr += 1
            if dist_mat[0,1] < dist_mat[0,2]:
                pos_rnk += 1
                
        avg_pos /= float(len(gal_data))
        avg_neg /= float(len(gal_data))
        print ("avg pos= %f; avg neg= %f" %(avg_pos, avg_neg))
        
        acc_thr = float(pos_thr)/float(len(gal_data))
        print ("thresholding accuracy= %f" %acc_thr)
        acc_rnk = float(pos_rnk)/float(len(gal_data))
        print ("ranking accuracy= %f" %acc_rnk)
        '''

    # find nearest neighors and test accuracy
    def eval_with_tripletfns_velocity(self, frame_count, frame_1_dir, frame_1_location, frame_1_location_box, frame_2_dir, frame_2_location, frame_2_location_box, gal_data, thr):
        frame_1_dir_len = len(frame_1_dir)
        frame_2_dir_len = len(frame_2_dir)
        gal_outputs = self.get_outputs_from_files(gal_data.flatten())
	print "gal_outputs has type", type(gal_outputs), "and shape", gal_outputs.shape
	print gal_outputs
        dist_mat = comp_distmat(gal_outputs, None, DistType.L2)
	print "dist_mat has type", type(dist_mat), "and shape", dist_mat.shape
	print dist_mat
        dist_mat = dist_mat[0:frame_1_dir_len, frame_1_dir_len:(frame_1_dir_len+frame_2_dir_len)]
        while True:
            imin = thr
            for i in range(frame_1_dir_len):
                for j in range(frame_2_dir_len):
                    if dist_mat[i][j] < imin:
                        imin = dist_mat[i][j]
                        ix = i
                        iy = j
            if imin < thr:
                ilocation = frame_2_location[iy]
                ilocation_box = frame_2_location_box[iy]
                frame_2_location[iy] = frame_1_location[ix]
                frame_2_location_box[iy] = frame_1_location_box[ix]
                frame_2_location[iy].extend(ilocation)
                frame_2_location_box[iy].extend(ilocation_box)
                for j in range(frame_2_dir_len):
                    dist_mat[ix][j] = thr
                for i in range(frame_1_dir_len):
                    dist_mat[i][iy] = thr
            else:
                break

        return frame_1_dir, frame_1_location, frame_1_location_box, frame_2_dir, frame_2_location, frame_2_location_box

        '''
        if frame_count > 0:
            print "hello"
            print
            print frame_1_dir_len
            print
            print frame_2_dir_len
            print
            print gal_data
            print
            print gal_data.shape
            print
            print dist_mat
            print
            print dist_mat.shape
            print
            print frame_count
            print
        '''

        '''
        pos_thr = 0
        pos_rnk = 0
        avg_pos = 0.
        avg_neg = 0.
        for i in range(len(gal_data)):
            dist_mat = comp_distmat(gal_outputs[i*3:i*3+3], None, DistType.L2)
            # print dist_mat
#            if dist_mat[0,1] < dist_mat[0,2]:

            avg_pos += dist_mat[0,1]
            avg_neg += dist_mat[0,2]
            print "hello"
            print
            print dist_mat
            print
            print dist_mat.shape
            print
            exit()
            if dist_mat[0,1] < thr and thr < dist_mat[0,2]:
                pos_thr += 1
            if dist_mat[0,1] < dist_mat[0,2]:
                pos_rnk += 1
                
        avg_pos /= float(len(gal_data))
        avg_neg /= float(len(gal_data))
        print ("avg pos= %f; avg neg= %f" %(avg_pos, avg_neg))
        
        acc_thr = float(pos_thr)/float(len(gal_data))
        print ("thresholding accuracy= %f" %acc_thr)
        acc_rnk = float(pos_rnk)/float(len(gal_data))
        print ("ranking accuracy= %f" %acc_rnk)
        '''
