import numpy as np
from optparse import OptionParser
import sys, pdb
import matplotlib.pyplot as plt
from utils import print_data_size, load_all_data, run_benchmark, TARGET_NAMES, load_unclassified_data, connect_to_larsde_database
from random import shuffle

# Import everything we need from scikit_learn
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.linear_model import SGDClassifier
from sklearn.naive_bayes import BernoulliNB
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
from sklearn.externals import joblib

# The three classifiers that we decided worked best after evaluating several options
# You can add more classifiers here if you'd like. Their performances will be scored as well.
CLASSIFIERS = [
      (RandomForestClassifier(n_estimators=100), "Random forest"),
      (SGDClassifier(alpha=.0001, n_iter=50, penalty="elasticnet"), "SGD Elastic Net"),
      (BernoulliNB(alpha=.01), "Bernoulli Naive Bayes")]

# Break the data into train and test samples - 80% train, 20% test
def get_classifier_data():
  data = load_all_data()
  shuffle(data)

  target_names = TARGET_NAMES

  # Break the data into train and test samples - 80% train, 20% test
  train = data[0 : int(len(data) * .80)]
  train_data = [datum[0] for datum in train]
  train_target = [datum[1] for datum in train]

  test = data[int(len(data) * .80) : int(len(data))]
  test_data = [datum[0] for datum in test]
  test_target = [datum[1] for datum in test]

  # TODO: Clean the actual tweets. Would likely be worthwhile.

  return (train_data, train_target, test_data, test_target)

# Pull out the most indicative n features of a class
def extract_best_features(train_data, train_target, test_data, prev_feature_names, n):
  ch2 = SelectKBest(chi2, k=n)
  X_train = ch2.fit_transform(train_data, train_target)
  X_test = ch2.transform(test_data)
  if prev_feature_names:
      feature_names = [prev_feature_names[i] for i
                       in ch2.get_support(indices=True)]
  print ""

  if feature_names:
    feature_names = np.asarray(feature_names)

  return feature_names

# Create a graph of the different results we got for our different classifiers
def plot_classification_results(results):
  indices = np.arange(len(results))
  results = [[x[i] for x in results] for i in range(2)]
  clf_names, score = results

  plt.figure(figsize=(12, 8))
  plt.title("Score")
  plt.barh(indices, score, .2, label="Score", color='navy')
  plt.yticks(())
  plt.legend(loc='best')
  plt.subplots_adjust(left=.25)
  plt.subplots_adjust(top=.95)
  plt.subplots_adjust(bottom=.05)

  for i, c in zip(indices, clf_names):
    plt.text(-.3, i, c)

  plt.savefig('classifier-performance.pdf')
  plt.show()

# Dump classified tweets to database.
def dump_tweets_to_database(data, results):
  connect = connect_to_larsde_database()
  traffic_count = 0
  # Insert data into the database.
  for (datum, result) in zip(data, results):
    if result == 1:
      traffic_count = traffic_count + 1
      insert_query = "INSERT INTO event_tweet_ids (id) VALUES (%d)" % datum['id'];
      connect.execute(insert_query)
  print 'Classified ' + str(traffic_count) + ' tweets to database'

# Setup the options our script will take from the command line
def setup_option_parser():
  # Get our command line arguments
  parser = OptionParser()
  parser.add_option("-t", "--train",
                  action="store_true", dest="train",
                  help="Train our classifiers and view their performances.")
  parser.add_option("-c", "--classify",
                  action="store_true", dest="classify",
                  help="Classify new data given one of our models.")

  return parser

if __name__ == '__main__':
  parser = setup_option_parser()
  (options, args) = parser.parse_args()

  # Classify tweets using our SGD Elastic Net Classifier
  if options.classify:
    print 'Classifying...'

    clf = joblib.load('models/SGD Elastic Net.pkl')
    vectorizer = joblib.load('models/vectorizer.pkl')

    unclassified_data = load_unclassified_data(True)
    # TODO: Maybe want to use more data about the tweet in the future.
    verify_data = [datum['content'] for datum in unclassified_data]
    verify_vector = vectorizer.transform(verify_data)

    results = clf.predict(verify_vector)

    dump_tweets_to_database(unclassified_data, results)

  # Train the different classifiers we specified above
  elif options.train:
    data = load_all_data()
    shuffle(data)

    target_names = TARGET_NAMES

    (train_data, train_target, test_data, test_target) = get_classifier_data()

    # Print information about the size of our data
    print_data_size(train_data, 'training set')
    print_data_size(test_data, 'test set')

    # Use TFIDF as our vectorizer
    vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=0.5, stop_words='english')
    X_train = vectorizer.fit_transform(train_data)
    X_test = vectorizer.transform(test_data)

    # Use Chi-Squared to get the most informative features
    prev_feature_names = vectorizer.get_feature_names()
    feature_names = extract_best_features(X_train, train_target, X_test, prev_feature_names, 10)

    # Train on each of our classifiers, storing the data
    results = []
    for clf, name in CLASSIFIERS:
      print '=' * 80
      print name
      results.append(run_benchmark(clf, X_train, train_target, X_test, test_target))
      joblib.dump(clf, 'models/' + name + '.pkl') 

    joblib.dump(vectorizer, 'models/vectorizer.pkl')

    plot_classification_results(results)
  else:
    parser.print_help()