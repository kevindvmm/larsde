from sqlalchemy import *
from datetime import datetime
from sklearn import metrics

# File names to keep our training data
TRAIN_DATA_FILE = 'data/train_data.txt'
TRAIN_DATA_LABELS = 'data/train_labels.txt'
TEST_DATA_FILE = 'data/test_data.txt'
TEST_DATA_LABELS = 'data/test_labels.txt'

# Login for the PostgreSQL database.
LARS_USER = 'matt'
LARS_PASSWORD = '#asdf3456Boy'
LARS_HOST = 'larsde.cs.columbia.edu'
LARS_DATABASE = 'larsde_other'

# Names for our two classes.
TARGET_NAMES = ['non-traffic', 'traffic']

# Print the size of the data of a specific label
def print_data_size(data, label):
  size_data = sum(len(s.encode('utf-8')) for s in data) / 1e6
  print("%d documents - %0.3fMB (%s)" % ( len(data), size_data, label))

# Pull all of our formatted training data as well as the labels
def load_all_data():
  data = [line.rstrip('\n').decode('utf-8') for line in open(TRAIN_DATA_FILE)]
  labels = [int(line.rstrip('\n')) for line in open(TRAIN_DATA_LABELS)]
  return zip(data, labels)

# Connect to the LARSDE database using the above credentials
def connect_to_larsde_database():
  engine_string = 'postgresql://%s:%s@%s/%s' % (LARS_USER, LARS_PASSWORD, LARS_HOST, LARS_DATABASE)
  database = create_engine(engine_string)
  connect = database.connect()
  return connect

# Custom JSON Serializer so that we can correctly serialize datetime objects.
def JSONSerializer(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial
    raise TypeError ("Type not serializable")

# Fit our classifier using training data, evaluate it using test data
def run_benchmark(clf, train_data, train_target, test_data, test_target):
  print '_' * 80 
  print "Training: "
  print clf

  clf.fit(train_data, train_target)
  pred = clf.predict(test_data)

  score = metrics.accuracy_score(test_target, pred)
  print("Accuracy:   %0.3f" % score)

  print "Classification Report: "
  print metrics.classification_report(test_target, pred,
                                          target_names=TARGET_NAMES)
  print ""

  print "Confusion Matrix: "
  print metrics.confusion_matrix(test_target, pred)
  print ""

  clf_descr = str(clf).split('(')[0]
  return clf_descr, score

# Grab a bunch of data that hasn't been classified yet
def load_unclassified_data(load_everything):
  connect = connect_to_larsde_database()

  # Start with a million tweets, see what happens.
  SAMPLE_QUERY = "SELECT * FROM tweets WHERE time BETWEEN '12-01-2015' AND '02-01-2016' ORDER BY RANDOM() LIMIT 50000"
  QUERY = "SELECT * FROM tweets WHERE time BETWEEN '12-01-2015' AND '02-01-2016' ORDER BY RANDOM()"

  query = QUERY if load_everything else SAMPLE_QUERY 

  results = connect.execute(query)
  cleaned_results = []
  for row in results:
    tweet_result = {}
    tweet_result['id'] = row[0]
    tweet_result['time'] = row[1]
    tweet_result['long'] = row[2]
    tweet_result['lat'] = row[3]
    tweet_result['addr'] = row[4]
    tweet_result['content'] = row[5]
    tweet_result['userid'] = row[6]
    cleaned_results.append(tweet_result)
  return cleaned_results

# Convert a database result into a Python dict with labels
def result_to_tweet(row):
  tweet_result = {}
  tweet_result['id'] = row[0]
  tweet_result['time'] = row[1]
  tweet_result['long'] = row[2]
  tweet_result['lat'] = row[3]
  tweet_result['addr'] = row[4]
  tweet_result['content'] = row[5]
  tweet_result['userid'] = row[6]
  return tweet_result