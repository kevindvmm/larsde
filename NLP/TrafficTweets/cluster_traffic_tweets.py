from utils import connect_to_larsde_database, result_to_tweet
from sklearn.externals import joblib
from sklearn.cluster import Birch
from sklearn.feature_extraction.text import TfidfVectorizer

LOAD_TRAFFIC_TWEETS_QUERY = "SELECT * FROM tweets t, event_tweet_ids e WHERE t.tweetid = e.id"

# TODO: Yizhou
# Tweak this and add another threshold parameter for a new clustering based on location and time
THRESHOLD = 0.8

# Pull classified tweets from LARSDE database by joining our event_tweet_ids table with tweets
def load_traffic_tweets():
    connect = connect_to_larsde_database()

    results = connect.execute(LOAD_TRAFFIC_TWEETS_QUERY)
    cleaned_results = []
    for row in results:
        tweet_result = result_to_tweet(row)
        cleaned_results.append(tweet_result)
    return cleaned_results

# Load all tweets, not just the ones marked as events
def load_all_tweets():
    print 'Not yet implemented...'

# Perform BIRCH clustering on our tweets.
# TODO: Look at more than just the text.
# TODO: Yizhou
def birch_cluster_tweets(tweets):
    vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=0.5, stop_words='english')
    tweet_text = [tweet['content'] for tweet in tweets]
    tweet_data = vectorizer.fit_transform(tweet_text)

    brc = Birch(branching_factor=50, n_clusters=None, threshold=THRESHOLD, compute_labels=True)
    brc.fit(tweet_data)
    return brc.predict(tweet_data)

# Print some results about clusters. Also print all the tweets in clusters with
# more than one tweet inside of them.
def analyze_cluster_results(tweets, results):
    clusters = [[] for i in range(max(results) + 1)]
    for tweet, result in zip(traffic_tweets, results):
        clusters[result].append(tweet)

    non_single_cluster_count = 0
    non_single_clusters = [cluster for cluster in clusters if len(cluster) > 1]
    for cluster in non_single_clusters:
        print 'Cluster: ' + str(len(cluster)) + ' tweets'
        non_single_cluster_count = non_single_cluster_count + 1
        for tweet in cluster:
            print str(tweet['time']) + ' ' + tweet['content']
        print ''

    print 'Number of Tweets: ' + str(len(tweets))
    print 'Number of Clusters: ' + str(max(results))
    print 'Number of Non-Single Clusters: ' + str(non_single_cluster_count)

if __name__ == '__main__':
    traffic_tweets = load_traffic_tweets()

    results = birch_cluster_tweets(traffic_tweets)
    analyze_cluster_results(traffic_tweets, results)