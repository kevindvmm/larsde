/* All traffic-related tweets from traffic accounts */
SELECT * FROM news_event_text WHERE source LIKE '%twitter%';

/* All traffic-related tweets NOT from traffic accounts.   
 * Change the IDs in this query if you'd like to see them. */ 
SELECT * FROM tweets t, event_tweet_ids e WHERE t.tweetid = e.id AND t.userid NOT IN ('52272942', '2869012466', '531603768', '42640432');

