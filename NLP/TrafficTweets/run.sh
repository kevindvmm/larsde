#!/bin/bash

# Step 1: Generate Traffic Training Data
# Label data incoming from the LARSDE database
# python generate_training_data_sets.py --classify
# Generate our training data from the subset of training runs
# python generate_training_data_sets.py --generate

# Step 2: Classify Raw Tweets
# Train our classifiers first - these then get saved locally
# python classify_traffic_tweets.py --train
# Classify a random group of ~50,000 tweets using our above classifer
# python classify_traffic_tweets.py --classify

# Step 3: Cluster Traffic Tweets
# python cluster_traffic_tweets.py

echo 'Thanks for running! If nothing happened, remember to uncomment one or more parts of the script.'
