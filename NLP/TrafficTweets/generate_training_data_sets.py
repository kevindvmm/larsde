import sys, time
from utils import connect_to_larsde_database, JSONSerializer
from sqlalchemy import *
import json
from os import listdir
from os.path import isfile, join
from utils import TRAIN_DATA_FILE, TRAIN_DATA_LABELS, result_to_tweet
from optparse import OptionParser

# A training session consists of looking at a random subset of 1000 tweets with an optional query
BASE_SQL_QUERY = "SELECT * FROM tweets WHERE content ILIKE '%% %s %%' AND time BETWEEN '12-01-2015' AND '02-01-2016' ORDER BY RANDOM() LIMIT 1000"

# We use timestamps to name our files. These are 14 characters long.
DATA_FILE_NAME_LENGTH = 14

# Fetch tweets to label given a search query, using the string above
def fetch_tweets(query):
  search_query = BASE_SQL_QUERY % query
  connect = connect_to_larsde_database()
  results = connect.execute(text(search_query))
  cleaned_results = []
  for row in results:
    tweet_result = result_to_tweet(row)
    cleaned_results.append(tweet_result)
  return cleaned_results

# Provide a summary of the training data available to us i.e. how many positives versus negatives
def training_data_summary():
  positive_count = 0
  negative_count = 0
  with open(TRAIN_DATA_LABELS, 'r') as f:
    labels = [int(line.rstrip('\n')) for line in f.readlines()]
    for label in labels:
      if label == 1:
        positive_count = positive_count + 1
      else:
        negative_count = negative_count + 1
  print 'Total Examples: ' + str(positive_count + negative_count)
  print 'Positives: ' + str(positive_count)
  print 'Negatives: ' + str(negative_count)

# In order to not lose all of our training data in the case something goes wrong during training,
# we create a directory called data/training_runs/ and add a new file there every time we train.
# This function goes through and aggregates all of those different runs into one set of training data.
def format_training_data():
  file_names = [f for f in listdir('data/training_runs') if isfile(join('data/training_runs', f))]

  training_data_file = open(TRAIN_DATA_FILE, 'w+')
  training_label_file = open(TRAIN_DATA_LABELS, 'w+')

  for name in file_names:
    if len(name) == DATA_FILE_NAME_LENGTH:
      # Remove .txt, which is 4 characters
      file_name =  name[0:DATA_FILE_NAME_LENGTH-4]
      data_file_name = 'data/training_runs/' + file_name + '.txt'
      label_file_name = 'data/training_runs/' + file_name + '-labels.txt'
      output_data_file = open(data_file_name, "r")
      label_data_file = open(label_file_name, "r")

      data = [json.loads(line.rstrip('\n')) for line in output_data_file]
      labels = [line.rstrip('\n') for line in label_data_file]

      for (datum, label) in zip(data, labels):
        training_data_file.write(datum['content'].encode('utf-8') + '\n')
        training_label_file.write(label + '\n')

      output_data_file.close()
      label_data_file.close()

  training_data_file.close()
  training_label_file.close()

  training_data_summary()

# Setup the options our Python script can take
def setup_option_parser():
  # Get our command line arguments
  parser = OptionParser()
  parser.add_option("-c", "--classify",
                  action="store_true", dest="classify",
                  help="Label tweet data")
  parser.add_option("-g", "--generate",
                  action="store_true", dest="generate",
                  help="Generate our training data")

  return parser

# Wait for user input on each tweet, labeling it as either 0 (not traffic) or 1 (traffic)
# Results are written to a file as we go
def label_input_data(results):
  file_name = str(int(time.time()))
  data_file_name = 'data/training_runs/' + file_name + '.txt'
  label_file_name = 'data/training_runs/' + file_name + '-labels.txt'

  output_data_file = open(data_file_name, "w")
  label_data_file = open(label_file_name, "w")

  for result in results:
    label = input(result['content'].encode('utf-8') + '\n')
    if label == -1:
      break
    output_data_file.write(json.dumps(result, default=JSONSerializer) + "\n")
    label_data_file.write(str(label) + "\n")

  output_data_file.close()
  label_data_file.close()

if __name__ == '__main__':
  parser = setup_option_parser()
  (options, args) = parser.parse_args()
  search_term = '' if len(args) == 0 else args[0]

  if options.generate:
    format_training_data()
  elif options.classify:
    search_term = '' if len(sys.argv) == 2 else sys.argv[2]
    results = fetch_tweets(search_term)

    label_input_data(results)

    print 'Finishing up...'
  else:
    parser.print_help()