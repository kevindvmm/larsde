# --------------------------------------------------------
# This python file is create a CSV file about when there exists image for every day
# After run the code, 'checkimg.csv' will be created
# place the CSV file in the 'website/static' the website is able to detect when there exists image
# Output: 'checkimg.csv'
# --------------------------------------------------------
import psycopg2
import datetime
import csv

try:
	conn = psycopg2.connect("dbname='larsde_other' user='flask' host='larsde.cs.columbia.edu' password='dvmm32123'")
	conn_images = psycopg2.connect("dbname='larsde_images' user='flask' host='larsde.cs.columbia.edu' password='dvmm32123'")
	conn.autocommit = True
	conn_images.autocommit = True
except Exception as e:
	print e, "Connection Unsucessful"

cur = conn.cursor()
cur_images = conn_images.cursor()

csvfile = file('checkimg.csv', 'wb')
columns = ['date','img']
f_csv = csv.DictWriter(csvfile, columns)
f_csv.writeheader()

terminatetime = datetime.datetime(2016,4,1)
starttime = datetime.datetime(2015,12,1)
endtime = datetime.datetime(2015,12,2)

while (starttime!=terminatetime):
	strstarttime = starttime.strftime("%Y/%m/%d")
	strendtime = endtime.strftime("%Y/%m/%d")
	#print strstarttime
	#print strendtime
	try:
		cur_images.execute("select exists (select time from images where time between %s and %s limit 1)", [strstarttime, strendtime])
	except Exception,e:
		print ("Exception executing query: ",e)

	row = cur_images.fetchall()
	#print row[0][0]
	#print type(row[0][0])
	if (row[0][0]):
		writenum='1'
	else:
		writenum='0'
	f_csv.writerows([{'date':strstarttime,'img':writenum}])
	starttime = starttime + datetime.timedelta(days=1)
	endtime = endtime + datetime.timedelta(days=1)
