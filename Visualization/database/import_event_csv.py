#############################################
#This is the code to import CSV file of event and create a corresponding Database table
#The CSV file has column 'id','description','cameraid','starttime','endtime'
#The Database table has column 'id','description','cameraid','starttime','endtime', 'tag'
#Input CSV file name: meta_all.csv
#Output table name: 'event' in larsde_other
#############################################
import psycopg2
import csv

#connect database
def connect_database():
	try:
		conn = psycopg2.connect("dbname='larsde_other' user='flask' host='larsde.cs.columbia.edu' password='dvmm32123'")
		conn.autocommit = True
	except Exception as e:
		print e, "Connection Unsucessful"

	cur = conn.cursor()
	return cur

cur = connect_database()

#all tag is waze
taguser = 'waze'

with open('meta_all.csv') as f:
	#read CSV with no header
	f_csv = csv.DictReader(f, fieldnames=['id','description','cameraid','starttime','endtime'])
	for row in f_csv:
		try:
			cur.execute("INSERT INTO event (description,camera_id,starttime,endtime,tag) VALUES (%s,%s,%s,%s,%s);",[row['description'], row['cameraid'], row['starttime'], row['endtime'], taguser])
		except Exception,e:
			print e
			cur = connect_database()
			cur.execute("INSERT INTO event (description,camera_id,starttime,endtime,tag) VALUES (%s,%s,%s,%s,%s);",[row['description'], row['cameraid'], row['starttime'], row['endtime'], taguser])
