import numpy as np
import cv2
import caffe
import time
import psycopg2
import io


# Default parameters are for VGG net
# Input: Height x Width x Channel
# Output: #Sample x Channel x Height x Width
def transform_image(img, mean_pix = [103.939, 116.779, 123.68], image_dim = 256, crop_dim = 224):
    # convert to BGR
    if len(img.shape) < 3 or img.shape[2] == 1:
        img = cv2.cvtColor(img, cv2.cv.CV_GRAY2BGR)
    # resize image, the shorter side is set to image_dim
    if img.shape[0] < img.shape[1]:
        # Note: OpenCV uses width first...
        dsize = (int(np.floor(float(image_dim)*img.shape[1]/img.shape[0])), image_dim)
    else:
        dsize = (image_dim, int(np.floor(float(image_dim)*img.shape[0]/img.shape[1])))
    img = cv2.resize(img, dsize, interpolation=cv2.INTER_CUBIC)

    # convert to float32
    img = img.astype(np.float32, copy=False)

    imgs = np.zeros((1, crop_dim, crop_dim, 3), dtype=np.float32)

    # crop
    indices_y = [0, img.shape[0]-crop_dim]
    indices_x = [0, img.shape[1]-crop_dim]
    center_y = np.floor(indices_y[1]/2)
    center_x = np.floor(indices_x[1]/2)

    imgs[0] = img[center_y:center_y+crop_dim, center_x:center_x+crop_dim, :]

    # subtract mean
    for c in range(3):
        imgs[:, :, :, c] = imgs[:, :, :, c] - mean_pix[c]
    # reorder axis
    return np.rollaxis(imgs, 3, 1)



class FeatExtractor(caffe.Net):
    def __init__(self, model_file, pretrained_file):
        caffe.Net.__init__(self, model_file, pretrained_file, caffe.TEST)

    def xfeat_batch(self, data, blobs):
        feats = {}
        for blob in blobs:
            feats[blob] = []
        out = self.forward_all(**{self.inputs[0]: data, 'blobs': blobs})
        for blob in blobs:
            feat = out[blob]
            feat = feat.reshape((len(feat) / data.shape[0], data.shape[0], -1))
            for i in xrange(data.shape[0]):
                feats[blob].append(feat[:, i, :].flatten())
        return feats

    
def attributes (filepath):    
    try:
        a = filepath.split('/')
        fname = a[-1].split('_')

        camid = int(fname [0])
        date = fname [1]

        minsec = fname[2].split('.')[0]
        minu = minsec.split('-')[0]
        seco = minsec.split('-')[1]
        hour = a[-2]

        timestamp = date + ' ' + hour + ':' + minu + ':' + seco    

        return camid, timestamp
    
    except Exception, e:
        print ("Something happened in " + filepath + ". The error message is: " + str(e))

        
def write_feats_db (feats, blobs, img_attr, cur):
    for blob in blobs:
        for featvec, attr in zip (feats[blob], img_attr):
            camid, timestamp = attr
            out = io.BytesIO()
            np.save (out, featvec)
            out.seek(0)
            bindata = psycopg2.Binary(out.read())
            try:                
                cur.execute("INSERT INTO image_features (content, time, new_id, layer, model) VALUES \
                            (%s,\'%s\',%d, \'%s\', 'VGG16')" %(bindata.getquoted(), timestamp, camid, blob))
            except Exception, e:
                print ("Error writing to database: " + str(e))        

                
                
    
if __name__ == '__main__':

    img_dim = 256
    crop_dim = 224
    mean = [103.939, 116.779, 123.68]    
    batch_size = 64
    blobs = ['pool5', 'fc6', 'fc7']
    
    caffe.set_mode_gpu()
    model_file = './VGG_ILSVRC_16_layers_deploy.prototxt'
    pretrained_file = '/home/alireza/tools/cnnfeatureextraction/VGG_ILSVRC_16_layers.caffemodel'
    img_list = '/home/alireza/larsde/database/parseimages/refine/larsde_filelist'
    
    conf = open ('dbconfig.txt', 'r').read()
    conn = psycopg2.connect(conf)
    conn.autocommit= True
    cur = conn.cursor()
    
    logfile = open('log_xfeat', 'w')
    
    start = time.time()
    extractor = FeatExtractor(model_file, pretrained_file)

    data = None
    img_attr = None

    print 'making the batch ...'
    start = time.time()
    
    listfile = open(img_list, 'r')
    for line in listfile:
        img_name = line.rstrip()
        
        
        img = cv2.imread(img_name)
        if data is None:
            img_attr = [attributes (img_name)]
            data = transform_image(img, mean, img_dim, crop_dim)
        else:
            img_attr.append (attributes (img_name))
            data = np.vstack((data, transform_image(img, mean, img_dim, crop_dim)))
        if data.shape[0] == batch_size:
            print 'took', time.time() - start
            print 'extracting features ...'
            start = time.time()
            feats = extractor.xfeat_batch(data, blobs)
            print 'took', time.time() - start
            print 'writing to database ...'
            start = time.time()
            write_feats_db (feats, blobs, img_attr, cur)
            print 'took', time.time() - start
            print 'making the batch ...'
            start = time.time()
            data = None
            img_attr = None
    feats = extractor.xfeat_batch(data, blobs)
    write_feats_db (feats, blobs, img_attr, cur)

    logfile.close()
    cur.close()
    conn.close()










