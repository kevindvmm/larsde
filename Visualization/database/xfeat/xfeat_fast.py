import numpy as np
import cv2
import caffe
import time
import psycopg2
import io
from Queue import Empty
from multiprocessing import Process,Queue


# Default parameters are for VGG net
# Input: Height x Width x Channel
# Output: #Sample x Channel x Height x Width
def transform_image(img, mean_pix = [103.939, 116.779, 123.68], image_dim = 256, crop_dim = 224):
    # convert to BGR
    if len(img.shape) < 3 or img.shape[2] == 1:
        img = cv2.cvtColor(img, cv2.cv.CV_GRAY2BGR)
    # resize image, the shorter side is set to image_dim
    if img.shape[0] < img.shape[1]:
        # Note: OpenCV uses width first...
        dsize = (int(np.floor(float(image_dim)*img.shape[1]/img.shape[0])), image_dim)
    else:
        dsize = (image_dim, int(np.floor(float(image_dim)*img.shape[0]/img.shape[1])))
    img = cv2.resize(img, dsize, interpolation=cv2.INTER_LINEAR)

    # convert to float32
    img = img.astype(np.float32, copy=False)

    imgs = np.zeros((1, crop_dim, crop_dim, 3), dtype=np.float32)

    # crop
    indices_y = [0, img.shape[0]-crop_dim]
    indices_x = [0, img.shape[1]-crop_dim]
    center_y = np.floor(indices_y[1]/2)
    center_x = np.floor(indices_x[1]/2)

    imgs[0] = img[center_y:center_y+crop_dim, center_x:center_x+crop_dim, :]

    # subtract mean
    for c in range(3):
        imgs[:, :, :, c] = imgs[:, :, :, c] - mean_pix[c]
    # reorder axis
    return np.rollaxis(imgs, 3, 1)



class FeatExtractor(caffe.Net):
    def __init__(self, model_file, pretrained_file):
        caffe.Net.__init__(self, model_file, pretrained_file, caffe.TEST)

    def xfeat_batch(self, data, blobs):
        feats = {}
        for blob in blobs:
            feats[blob] = []
        out = self.forward_all(**{self.inputs[0]: data, 'blobs': blobs})
        for blob in blobs:
            feat = out[blob]
            feat = feat.reshape((len(feat) / data.shape[0], data.shape[0], -1))
            for i in xrange(data.shape[0]):
                feats[blob].append(feat[:, i, :].flatten())
        return feats

    
def attributes (filepath):    
    try:
        a = filepath.split('/')
        fname = a[-1].split('_')

        camid = int(fname [0])
        date = fname [1]

        minsec = fname[2].split('.')[0]
        minu = minsec.split('-')[0]
        seco = minsec.split('-')[1]
        hour = a[-2]

        timestamp = date + ' ' + hour + ':' + minu + ':' + seco    

        return camid, timestamp
    
    except Exception, e:
        print ("Something happened in " + filepath + ". The error message is: " + str(e))

        
def write_feats_db (feats, blobs, img_attr, cur):
    for blob in blobs:
        for featvec, attr in zip (feats[blob], img_attr):
            camid, timestamp = attr
            out = io.BytesIO()
            np.save (out, featvec)
            out.seek(0)
            bindata = psycopg2.Binary(out.read())
            try:                
                cur.execute("INSERT INTO image_features (content, time, new_id, layer, model) VALUES \
                            (%s,\'%s\',%d, \'%s\', 'VGG16')" %(bindata.getquoted(), timestamp, camid, blob))
            except Exception, e:
                print ("Error writing to database: " + str(e))        

                
                
    
if __name__ == '__main__':

    img_dim = 256
    crop_dim = 224
    mean = [103.939, 116.779, 123.68]    
    batch_size = 128
    blobs = ['fc6', 'fc7']
    NUM_THREADS = 10
    INTERRUPT = False

    img_list = '/home/alireza/larsde/database/parseimages/refine/toy2_filelist'        
    logfile = open('log_xfeat', 'w')
    
    queue_filelist = Queue ()
    queue_batch = Queue ()
    queue_feat = Queue ()
    
    def loadList ():
        listfile = open(img_list, 'r')
        for line in listfile:
            
            if INTERRUPT:
                break           
            
            img_name = line.rstrip()
            queue_filelist.put (img_name)
        
        for ii in range (NUM_THREADS):
            queue_filelist.put ('finished')
        
        listfile.close ()
        return
        
    
    def makeBatch (thread_id):
        
        data = None
        img_attr = None
        
        while True:        
            while True:
                if INTERRUPT:
                    pass
                try:
                    img_name = queue_filelist.get (block=False)
                    break
                except Empty:
                    pass     
                
            if img_name == 'finished':
                if data is not None:
                    queue_batch.put ((data.copy(),list(img_attr)))
                queue_batch.put ('finished')
                break
                
            img = cv2.imread(img_name)
            if data is None:
                img_attr = [attributes (img_name)]
                data = transform_image(img, mean, img_dim, crop_dim)
            else:
                img_attr.append (attributes (img_name))
                data = np.vstack((data, transform_image(img, mean, img_dim, crop_dim)))
                #print "Thread %d size %d"%(thread_id, data.shape[0])
                
            if data.shape[0] == batch_size:
                queue_batch.put ((data.copy(), list(img_attr)))
                print "Thread %d made a batch with size %d"%(thread_id, data.shape[0])
                data = None
                img_attr = None
        return

                                
    def processBatch ():
        caffe.set_mode_gpu()
        model_file = './VGG_ILSVRC_16_layers_deploy.prototxt'
        pretrained_file = '/home/alireza/tools/cnnfeatureextraction/VGG_ILSVRC_16_layers.caffemodel'
        extractor = FeatExtractor(model_file, pretrained_file)
        
        numfinished = 0
        
        while True:   
            
            print 'unprocessed batches:', queue_batch.qsize()
            
            while True:
                if INTERRUPT:
                    pass
                try:
                    batch = queue_batch.get (block=False)
                    break
                except Empty:
                    pass     
                
            print "took a batch"
            if batch == 'finished':
                numfinished += 1
                if numfinished == NUM_THREADS:
                    queue_feat.put ('finished')
                    break
                else:
                    continue
                
            data, img_attr = batch
            feats = extractor.xfeat_batch(data, blobs)
            queue_feat.put ((feats, img_attr))
        return
    
    
    def writeBatch ():
        conf = open ('dbconfig.txt', 'r').read()
        conn = psycopg2.connect(conf)
        conn.autocommit= True
        cur = conn.cursor()
        
        while True:        

            print 'unwritten batches:', queue_feat.qsize()
            
            while True:
                if INTERRUPT:
                    pass
                try:
                    batch = queue_feat.get (block=False)
                    break
                except Empty:
                    pass     
                
            if batch == 'finished':
                break
            
            feats, img_attr = batch
            write_feats_db (feats, blobs, img_attr, cur)
        cur.close()
        conn.close()
        return
    
    makeBatch_threads = []        
    all_threads = []
    for ii in range (NUM_THREADS):
        thread = Process (target = makeBatch, args=(ii,))
        makeBatch_threads.append (thread)
        all_threads.append (thread)

    loadList_thread = Process (target = loadList)
    all_threads.append (loadList_thread)

    processBatch_thread = Process (target = processBatch)
    all_threads.append (processBatch_thread)

    writeBatch_thread = Process (target = writeBatch)
    all_threads.append (writeBatch_thread)

    for thread in all_threads:
        thread.start()
    
    try:
        for thread in all_threads:
            thread.join(1)
    except KeyboardInterrupt:
        print "Sending terminate signal ..."
        INTERRUPT = True
        time.sleep(1)
        for thread in all_threads:
            thread.terminate()
    
    
    logfile.close()










