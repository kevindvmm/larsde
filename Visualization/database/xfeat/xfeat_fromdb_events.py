import numpy as np
import cv2
import caffe
import time
import psycopg2
import io
from Queue import Empty
from multiprocessing import Process,Queue


# Default parameters are for VGG net
# Input: Height x Width x Channel
# Output: #Sample x Channel x Height x Width
def transform_image(img, mean_pix = [103.939, 116.779, 123.68], image_dim = 256, crop_dim = 224):
    # convert to BGR
    if len(img.shape) < 3 or img.shape[2] == 1:
        img = cv2.cvtColor(img, cv2.cv.CV_GRAY2BGR)
    # resize image, the shorter side is set to image_dim
    if img.shape[0] < img.shape[1]:
        # Note: OpenCV uses width first...
        dsize = (int(np.floor(float(image_dim)*img.shape[1]/img.shape[0])), image_dim)
    else:
        dsize = (image_dim, int(np.floor(float(image_dim)*img.shape[0]/img.shape[1])))
    img = cv2.resize(img, dsize, interpolation=cv2.INTER_CUBIC)

    # convert to float32
    img = img.astype(np.float32, copy=False)

    imgs = np.zeros((1, crop_dim, crop_dim, 3), dtype=np.float32)

    # crop
    indices_y = [0, img.shape[0]-crop_dim]
    indices_x = [0, img.shape[1]-crop_dim]
    center_y = np.floor(indices_y[1]/2)
    center_x = np.floor(indices_x[1]/2)

    imgs[0] = img[center_y:center_y+crop_dim, center_x:center_x+crop_dim, :]

    # subtract mean
    for c in range(3):
        imgs[:, :, :, c] = imgs[:, :, :, c] - mean_pix[c]
    # reorder axis
    return np.rollaxis(imgs, 3, 1)

def transform_image2(img, mean_pix = [103.939, 116.779, 123.68], image_dim = 224):
    if len(img.shape) < 3 or img.shape[2] == 1:
        img = cv2.cvtColor(img, cv2.cv.CV_GRAY2BGR)
    img = cv2.resize(img, (image_dim, image_dim), interpolation=cv2.INTER_CUBIC)
    img = img.astype(np.float32, copy=False)
    for c in range(3):
        img[:, :, c] = img[:, :, c] - mean_pix[c]
    img = np.rollaxis(img, 2)        
    return img [np.newaxis,...]


class FeatExtractor(caffe.Net):
    def __init__(self, model_file, pretrained_file):
        caffe.Net.__init__(self, model_file, pretrained_file, caffe.TEST)

    def xfeat_batch(self, data, blobs):
        feats = {}
        for blob in blobs:
            feats[blob] = []
        out = self.forward_all(**{self.inputs[0]: data, 'blobs': blobs})
        for blob in blobs:
            feat = out[blob]
            feat = feat.reshape((len(feat) / data.shape[0], data.shape[0], -1))
            for i in xrange(data.shape[0]):
                feats[blob].append(feat[:, i, :].flatten())
        return feats

    
def attributes (filepath):    
    try:
        a = filepath.split('/')
        fname = a[-1].split('_')

        camid = int(fname [0])
        date = fname [1]

        minsec = fname[2].split('.')[0]
        minu = minsec.split('-')[0]
        seco = minsec.split('-')[1]
        hour = a[-2]

        timestamp = date + ' ' + hour + ':' + minu + ':' + seco    

        return camid, timestamp
    
    except Exception, e:
        print ("Something happened in " + filepath + ". The error message is: " + str(e))

        
def write_feats_db (feats, blobs, img_attr, cur, runnum):
    for blob in blobs:
        if feats is None:
            camid, timestamp = img_attr
            try:                
                cur.execute("INSERT INTO image_features (time, new_id, layer, model, run_num) VALUES \
                            (\'%s\',%d, \'%s\', 'VGG16', %d)" %(timestamp, camid, blob, runnum))
            except Exception, e:
                print ("Error writing to database: " + str(e))                    
            continue
        for featvec, attr in zip (feats[blob], img_attr):
            camid, timestamp = attr
            out = io.BytesIO()
            np.save (out, featvec)
            out.seek(0)
            bindata = psycopg2.Binary(out.read())
            try:                
                cur.execute("INSERT INTO image_features (content, time, new_id, layer, model, run_num) VALUES \
                            (%s,\'%s\',%d, \'%s\', 'VGG16', %d)" %(bindata.getquoted(), timestamp, camid, blob, runnum))
            except Exception, e:
                print ("Error writing to database: " + str(e))        

                
                
    
if __name__ == '__main__':

    event_tag = 'waze_untrimmed'
#    img_dim = 256
    img_dim = 224
#    crop_dim = 224
    mean = [103.939, 116.779, 123.68]    
    batch_size = 80
    blobs = ['fc7']
    NUM_THREAD_PROCESS = 2
    INTERRUPT = False

    logfile = open('log_xfeat', 'w')
    
    queue_imglist = Queue ()
    queue_feat = Queue ()
    
    def loadList ():
        conf = open ('dbconfig_other.txt', 'r').read()
        conn = psycopg2.connect(conf)
        conn.autocommit= True
        cur = conn.cursor()

        cur.execute("select camera_id, starttime, endtime from event where tag = \'%s\'" % event_tag)
        intervals = cur.fetchall()

        cur.close()
        conn.close()
        
        conf = open ('dbconfig_images.txt', 'r').read()
        conn = psycopg2.connect(conf)
        conn.autocommit= True
        cur = conn.cursor()
        
        for interval in intervals:       
            camid, start_time, end_time = interval
            cur.execute("SELECT new_id,time FROM images where time between \'%s\' and \'%s\' and new_id = %d order by time" % (start_time, end_time, camid))       
            img_attr = cur.fetchone()
            while img_attr is not None:
                queue_imglist.put (img_attr)
                img_attr = cur.fetchone()
            
        for ii in range (NUM_THREAD_PROCESS):
            queue_imglist.put ('finished')
        
        cur.close()
        conn.close()
        return
        
    
    def process (thread_id):        
        conf = open ('dbconfig_images.txt', 'r').read()
        conn = psycopg2.connect(conf)
        conn.autocommit= True
        cur = conn.cursor()

        batch_img_attr = []
        batch_img = np.zeros ((batch_size, 3, img_dim, img_dim))
#        batch_img = np.zeros ((batch_size, 3, crop_dim, crop_dim))
        idx_inbatch = 0
        caffe.set_mode_gpu()
        model_file = './VGG_ILSVRC_16_layers_deploy.prototxt'
        pretrained_file = '/home/alireza/tools/cnnfeatureextraction/VGG_ILSVRC_16_layers.caffemodel'
        extractor = FeatExtractor(model_file, pretrained_file)                

        while True: 
            starttime = time.time()
            while True:
                if INTERRUPT:
                    pass
                try:
                    img_attr = queue_imglist.get (block=False)
                    break
                except Empty:
                    pass     
                
            if img_attr == 'finished':
                if idx_inbatch > 0:
                    batch_img = batch_img [0:idx_inbatch]
                    feats = extractor.xfeat_batch(batch_img, blobs)
                    queue_feat.put ((feats, batch_img_attr))
                queue_feat.put ('finished')
                break
                
            cur.execute("SELECT content FROM images where new_id=%d and time=\'%s\'"%img_attr)
            img_str = cur.fetchone()[0]
            nparr = np.fromstring(img_str, np.uint8)
            img = cv2.imdecode(nparr, cv2.CV_LOAD_IMAGE_COLOR)
            if img is None:
                queue_feat.put ((None, img_attr))
                continue
            img = transform_image2(img, mean, img_dim)            
            batch_img [idx_inbatch] = img
            batch_img_attr.append (img_attr)
            idx_inbatch += 1    

            if idx_inbatch == batch_size:                
                feats = extractor.xfeat_batch(batch_img, blobs)
                queue_feat.put ((feats, batch_img_attr))
                idx_inbatch = 0
                batch_img_attr = []
                print "Processed a batch"
                
        cur.close()
        conn.close()
        return
    
    def writeBatch ():
        conf = open ('dbconfig_other.txt', 'r').read()
        conn = psycopg2.connect(conf)
        conn.autocommit= True
        cur = conn.cursor()
        numfinished = 0       
        
        cur.execute("SELECT * FROM imgfeat_lastrun")
        lastrun = cur.fetchone()[0]
        runnum = lastrun + 1
        cur.execute("DELETE FROM imgfeat_lastrun")
        cur.execute("INSERT INTO imgfeat_lastrun VALUES (%d)"%runnum)
        
        while True:        
            while True:
                if INTERRUPT:
                    pass
                try:
                    data = queue_feat.get (block=False)
                    break
                except Empty:
                    pass     
                
            if data == 'finished':
                numfinished += 1
                if numfinished == NUM_THREAD_PROCESS:
                    break
                else:
                    continue
            
            feats, img_attr = data
            write_feats_db (feats, blobs, img_attr, cur, runnum)
            print 'unwritten batches:', queue_feat.qsize()
            
        cur.close()
        conn.close()
        return
    
    all_threads = []

    loadList_thread = Process (target = loadList)
    all_threads.append (loadList_thread)

    processBatch_threads = []
    for ii in range (NUM_THREAD_PROCESS):
        thread = Process (target = process, args=(ii,))
        processBatch_threads.append (thread)
        all_threads.append (thread)
    
    writeBatch_thread = Process (target = writeBatch)
    all_threads.append (writeBatch_thread)

    for thread in all_threads:
        thread.start()
    
    try:
        for thread in all_threads:
            thread.join(1)
    except KeyboardInterrupt:
        print "Sending terminate signal ..."
        INTERRUPT = True
        time.sleep(1)
        for thread in all_threads:
            thread.terminate()
    
    
    logfile.close()










