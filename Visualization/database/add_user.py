# --------------------------------------------------------
# This python file is create new user for visualization tool
# change the user information in 'userid', 'username', 'password'
# then run the code is able to create a new user in database
# Input: 'userid', 'username', 'password'
# Output: new user in database
# --------------------------------------------------------
import os
import base64
from hashlib import sha256
from hmac import HMAC
import psycopg2

#INSERT USER INFORMATION HERE
userid = 3
username = "test"
password = "dvmm"

try:
	conn = psycopg2.connect("dbname='larsde_other' user='' host='larsde.cs.columbia.edu' password=''")
	conn.autocommit = True
except Exception as e:
	print e, "Connection Unsucessful"
cur = conn.cursor()

def encrypt_password(password, salt=None):
	# create salt
	if salt is None:
		salt = os.urandom(8)

	assert 8 == len(salt)
	assert isinstance(salt, str)

	if isinstance(password, unicode):
		password = password.encode('UTF-8')

	assert isinstance(password, str)

	result = password
	for i in xrange(10):
		result = HMAC(result, salt, sha256).digest()

	return salt+result

def validate_password(hashed, input_password):
	return hashed == encrypt_password(input_password, salt=hashed[:8])

hashed = encrypt_password(password)
print hashed
uni_hashed = unicode(base64.b64encode(hashed))
print type(uni_hashed)
print uni_hashed

deuni_hashed = str(base64.b64decode(uni_hashed))
print validate_password(deuni_hashed, password)

try:
	cur.execute("INSERT INTO users (id, username, password) VALUES (%s,%s,%s)",[userid, username, uni_hashed])
except Exception,e:
	print "Exception happened when accessing the database, reconnecting..."
	print e
	try:
		conn = psycopg2.connect("dbname='larsde_other' user='' host='larsde.cs.columbia.edu' password=''")
		conn.autocommit = True
		print "reconnected"
	except Exception as e:
		print e, "Connection Unsucessful"
	cur = conn.cursor()
	cur.execute("INSERT INTO users (id, username, password) VALUES (%s,%s,%s)",[userid, username, uni_hashed])

try:
	cur.execute("INSERT INTO userstatus (id, failuretime) VALUES (%s,%s)",[userid, 0])
except Exception,e:
	print "Exception happened when accessing the database, reconnecting..."
	print e
	try:
		conn = psycopg2.connect("dbname='larsde_other' user='' host='larsde.cs.columbia.edu' password=''")
		conn.autocommit = True
		print "reconnected"
	except Exception as e:
		print e, "Connection Unsucessful"
	cur = conn.cursor()
	cur.execute("INSERT INTO userstatus (id, failuretime) VALUES (%s,%s)",[userid, 0])


