import pprint, threading, time, Queue
import psycopg2
import base64
import os
import string 


def getDirList( p ):
        p = str( p )
        if p=="":
              return [ ]
        a = os.listdir( p )
        return a

fconf = open ('dbconfig.txt', 'r')
conf = fconf.read()


tnum = 7
queue = Queue.Queue()
threadLock = threading.Lock()

class threadClass(threading.Thread):

    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue

    def run(self):

        conn = psycopg2.connect(conf)
        conn.autocommit= True
        cur = conn.cursor()


        while (not queue.empty()):
            threadLock.acquire()
            if (not queue.empty()):
                filepath = self.queue.get()

                print ("thread %s: got %s from queue." %(self.getName(), filepath))

                threadLock.release()
                a = filepath.split('/')
                b = ''
                for name in a:
                    b += name

                try:
                    cur.execute('DROP TABLE IF EXISTS ' + str(b) + ';');
                except psycopg2.Error as e:
                    print e.pgerror

                try:
                    cur.execute('CREATE TABLE IF NOT EXISTS ' + str(b) + ' (\
                        content BYTEA,\
                        time TIMESTAMP,\
                        camname varchar(50),\
                        FOREIGN KEY(camname) REFERENCES CAMERA(name)\
                    );');
                except psycopg2.Error as e:
                    print e.pgerror

                secondlist = getDirList(filepath)
                for secfile in secondlist:#157balabala
                    if(secfile == '.DS_Store'): continue
                    else: thirdlist = getDirList(filepath + '/' + secfile)
                    for thirdfile in thirdlist:#0,1,2
                        if(thirdfile == '.DS_Store'): continue
                        else: wenjian = getDirList(filepath + '/' + secfile+ '/' + thirdfile)
                        for item in wenjian: 
                            f = open(filepath + '/' + secfile + '/' + thirdfile + '/' + item,'rb')
                            if(item == '.DS_Store'): continue
                            else:
                                time = item.split('_')[1]
                                if(len(item.split('_'))== 3):
                                    timesec = (item.split('_')[2]).split('.')[0]
                                else: 
                                    timesec = '0-0'
                                    print item
                                if(len(timesec.split('-')) == 2):
                                    minu = timesec.split('-')[0]
                                    sec = timesec.split('-')[1]
                                    timestamp = time + ' ' + thirdfile + ':' + minu + ':' + sec
                                else: timestamp = time + ' ' + thirdfile + ':' + '0' + ':' + '0'
                                
                                cameinfo = secfile.split('_')
                                nameofcamera = ''
                                for i in range(1, len(cameinfo) - 1):
                                    nameofcamera += (cameinfo[i] + '_')
                                nameofcamera += cameinfo[len(cameinfo) - 1]

                                fin = f.read()
                                bin = psycopg2.Binary(fin)

                                try:
                                    bin = psycopg2.Binary(fin)
                                    cur.execute("INSERT INTO " + str(b) + " (content, time, camname) VALUES (%s,\'%s\',\'%s\')" %(bin.getquoted(), timestamp, nameofcamera))
                                    
                                except psycopg2.Error as e:
                                    if thirdfile == '0':
                                        pass#print nameofcamera
                                    #print e.pgerror  
                # threadLock.release()
            else:
                threadLock.release()
                # return
            # threadLock.release()

def main():
    #gidList = [1,2,3,4,5,6,7,8,9,10,1,2,3,4,5,6,7,8,9,10]
    filedic = []
    path = '/home/jiayu/data/images/'
    # path = '/Users/SongJiayu/Desktop/project/images/'
    FileList = getDirList(path)
    for year in FileList:
        if(year == '.DS_Store'): continue
        else: yearlist = getDirList(path + year)
        for month in yearlist:
            if(month == '.DS_Store'): continue
            else: monthlist = getDirList(path + year + '/' + month)
            for firstfile in monthlist:#5,6,7
                if(firstfile == '.DS_Store'): continue
                else: filedic.append(path + year + '/' + month + '/' +firstfile)
    print filedic
    print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    for filepath in filedic:
            queue.put(filepath)

    tlist = []
    for i in range(tnum):
        tlist.append(threadClass(queue))
        tlist[i].setDaemon(True)
        tlist[i].start()
    for i in range(tnum):
        tlist[i].join()

    conn = psycopg2.connect(conf)
    conn.autocommit= True
    cur = conn.cursor()


    print ("Merging...")

    try:
        cur.execute('DROP TABLE IF EXISTS images2;');
    except psycopg2.Error as e:
        print e.pgerror

    try:
        cur.execute('CREATE TABLE IF NOT EXISTS images2 (\
            content BYTEA,\
            time TIMESTAMP,\
            camname varchar(50),\
            FOREIGN KEY(camname) REFERENCES CAMERA(name)\
        );');
    except psycopg2.Error as e:
        print e.pgerror



    for subtable in filedic:
        a = subtable.split('/')
        b = ''
        for name in a:
            b += name
        try:
            cur.execute('insert into images2 (select * from '+ str(b) + ');');
            cur.execute('DROP TABLE IF EXISTS ' + str(b) + ';');
        except psycopg2.Error as e:
            print e.pgerror



    print ("Creating primary key...")


    try:
        cur.execute('ALTER TABLE images2 ADD COLUMN imageid BIGSERIAL PRIMARY KEY;');
    except psycopg2.Error as e:
        print e.pgerror



main()




