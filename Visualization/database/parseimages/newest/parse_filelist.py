import psycopg2

conf = open ('dbconfig.txt', 'r').read()
conn = psycopg2.connect(conf)
conn.autocommit= True
cur = conn.cursor()

logfile = open('log_parse_filelist', 'w')
listfile = open ('larsde_filelist', 'r')

for line in listfile:
    filepath = line.split()[0]
    try:
        a = filepath.split('/')
        fname = a[-1].split('_')

        camid = int(fname [0])
        date = fname [1]

        minsec = fname[2].split('.')[0]
        minu = minsec.split('-')[0]
        seco = minsec.split('-')[1]
        hour = a[-2]

        timestamp = date + ' ' + hour + ':' + minu + ':' + seco    

        cur.execute("SELECT EXISTS (SELECT time from images where time = \'%s\' and new_id = %d)"%(timestamp, camid))

        exists = cur.fetchone()[0]
                
        if exists == False:
            logfile.write ('parsing %s\n' %(filepath))
            with open (filepath, 'rb') as fin:
                bindata = psycopg2.Binary(fin.read())                
                cur.execute("INSERT INTO images (content, time, new_id) VALUES (%s,\'%s\',%d)" %(bindata.getquoted(), timestamp, camid))
                                        
    except Exception, e:
        print ("Something happened in " + filepath + ". The error message is: " + str(e))
        logfile.write ("Something happened in " + filepath + ". The error message is: " + str(e) + ".\n")
                
logfile.write ("\n\nSuccessfully parsed all data\n")
logfile.close()
cur.close()
conn.close()






