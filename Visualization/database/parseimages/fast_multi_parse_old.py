import pprint, threading, time, Queue
import psycopg2
import base64
import os
import string 


def getDirList( p ):
    try:
        p = str( p )
        if p=="":
            return [ ]
        a = os.listdir( p )
        return a
    except Exception, e:
        print ("Could not parse folder: " + p)
        print e
        return []

fconf = open ('dbconfig.txt', 'r')
logfile = open('log_multi.txt', 'w')
conf = fconf.read()
P = open('cams.txt', 'r')

#If only parse camera in the ParseList, set True
IsList = False 
ParseList = P.readlines()
D = []

#enter start time, format xxxx-xx-xx, don't forget to enter 0
starttime = '2015-12-01' 
#enter end time, format xxxx-xx-xx, don't forget to enter 0
endtime = '2016-12-15'  
#Please enter number of threads
tnum = 10 
queue = Queue.Queue()
threadLock = threading.Lock()

class threadClass(threading.Thread):

    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue

    def run(self):
        conn = psycopg2.connect(conf)
        conn.autocommit= False
        cur = conn.cursor()

        while (not queue.empty()):
            threadLock.acquire()
            if (not queue.empty()):
                filepath = self.queue.get()

                print ("thread %s: got %s from queue." %(self.getName(), filepath))

                threadLock.release()
                a = filepath.split('/')
                b = ''
                for name in a:
                    b += name

                try:
                    cur.execute('DROP TABLE IF EXISTS ' + str(b) + ';');
                except psycopg2.Error as e:
                    print e.pgerror

                try:
                    cur.execute('CREATE TABLE IF NOT EXISTS ' + str(b) + ' (\
                        content BYTEA,\
                        time TIMESTAMP,\
                        new_id int\
                    );');
                except psycopg2.Error as e:
                    print e.pgerror

                secondlist = getDirList(filepath)
                for secfile in secondlist:
                    if(secfile == '.DS_Store'): continue
                    else: thirdlist = getDirList(filepath + '/' + secfile)
                    for thirdfile in thirdlist:
                        if(thirdfile == '.DS_Store'): continue
                        else: wenjian = getDirList(filepath + '/' + secfile+ '/' + thirdfile)
                        for item in wenjian: 
                            f = open(filepath + '/' + secfile + '/' + thirdfile + '/' + item,'rb')
                            if(item == '.DS_Store'): continue
                            else:
                                time = item.split('_')[1]
                                if(len(item.split('_'))== 3):
                                    timesec = (item.split('_')[2]).split('.')[0]
                                else: 
                                    timesec = '0-0'
                                    print item
                                timetrans = time.split('-')
                                if(int(timetrans[2]) < 10):
                                    time = timetrans[0] +'-' + timetrans[1] + '-' + str(0) + timetrans[2]
                                if (time < starttime) or (time > endtime): 
                                    print time
                                    continue
                                if(len(timesec.split('-')) == 2):
                                    minu = timesec.split('-')[0]
                                    sec = timesec.split('-')[1]
                                    timestamp = time + ' ' + thirdfile + ':' + minu + ':' + sec
                                else: timestamp = time + ' ' + thirdfile + ':' + '0' + ':' + '0'
                                
                                cameinfo = secfile.split('_')
                                ids = cameinfo[0]
                                new_id = int(ids.split('-')[1])
                                nameofcamera = ''
                                for i in range(1, len(cameinfo) - 1):
                                    nameofcamera += (cameinfo[i] + '_')
                                nameofcamera += cameinfo[len(cameinfo) - 1]

                                fin = f.read()
                                bin = psycopg2.Binary(fin)

                                if IsList:
                                    if nameofcamera not in ParseList:
                                        continue

                                try:
                                    bin = psycopg2.Binary(fin)
                                    cur.execute("INSERT INTO " + str(b) + " (content, time, new_id) VALUES (%s,\'%s\', %d)" %(bin.getquoted(), timestamp, new_id))
                                    
                                except psycopg2.Error as e:
                                    # Use dictionary to eliminate duplicate
                                    if nameofcamera not in D:
                                    # if secfile not in D:
                                    #     D.append(secfile) # this is to find id for missing camera
                                        D.append(nameofcamera)

                    conn.commit()

            else:
                threadLock.release()

def main():
    filedic = []
    path = '/projects/LSDE/NYCWebcams/OurCrawl/'
    # path = '/Users/SongJiayu/Desktop/project/images/'
    FileList = getDirList(path)
    for year in FileList:
        if(year == '.DS_Store'): continue
        else: yearlist = getDirList(path + year)
        for month in yearlist:
            if(month == '.DS_Store'): continue
            else: monthlist = getDirList(path + year + '/' + month)
            for firstfile in monthlist:#5,6,7
                if(firstfile == '.DS_Store'): continue
                else: filedic.append(path + year + '/' + month + '/' +firstfile)


    # Create task queue
    for filepath in filedic:
            queue.put(filepath)

    # Distribute task from queue to each thread
    tlist = []
    for i in range(tnum):
        tlist.append(threadClass(queue))
        tlist[i].setDaemon(True)
        tlist[i].start()
    for i in range(tnum):
        tlist[i].join()

    conn = psycopg2.connect(conf)
    conn.autocommit= True
    cur = conn.cursor()


    print ("Merging...")

    try:
        cur.execute('DROP TABLE IF EXISTS images2;');
    except psycopg2.Error as e:
        print e.pgerror

    try:
        cur.execute('CREATE TABLE IF NOT EXISTS images2 (\
            content BYTEA,\
            time TIMESTAMP,\
            new_id int\
        );');
    except psycopg2.Error as e:
        print e.pgerror



    for subtable in filedic:
        a = subtable.split('/')
        b = ''
        for name in a:
            b += name
        try:
            print b
            cur.execute('insert into images2 (select * from '+ str(b) + ');');
#            cur.execute('DROP TABLE IF EXISTS ' + str(b) + ';');
        except psycopg2.Error as e:
            print e.pgerror

    print ("Creating primary key...")

    try:
        cur.execute('ALTER TABLE images2 ADD COLUMN imageid BIGSERIAL PRIMARY KEY;');
    except psycopg2.Error as e:
        print e.pgerror

    # Write logfile to record missing camera
    for cam in D:
        logfile.write(cam + '\n')
    logfile.close()

main()




