import base64
import psycopg2
import os
import string


def getDirList( p ):
	try:
		p = str( p )
		if p=="":
			return [ ]
		a = os.listdir( p )
		return a
	except e:
		print e
		return []

try: 
	fconf = open ('dbconfig.txt', 'r')
	conf = fconf.read()
	conn = psycopg2.connect(conf)
	conn.autocommit= True
except Exception, e:
	print e, "Connection Unsucessful"

cur = conn.cursor()
logfile = open('log.txt', 'w')
P = open('cams.txt', 'r')

#If only parse camera in the ParseList, set True
IsList = False 
ParseList = P.readlines()
D = []

#enter start time, format xxxx-xx-xx, don't forget to enter 0
starttime = '2015-12-01' 

#enter end time, format xxxx-xx-xx, don't forget to enter 0
endtime = '2016-12-15'  

try:
	cur.execute('DROP TABLE IF EXISTS IMAGES4;');
except psycopg2.Error as e:
    print e.pgerror

try:
    cur.execute('CREATE TABLE IF NOT EXISTS IMAGES4 (\
        content BYTEA,\
        time TIMESTAMP,\
        new_id int\
    );');
	#conn.commit ()
except psycopg2.Error as e:
    print e.pgerror

path = '/projects/LSDE/NYCWebcams/OurCrawl/'
FileList = getDirList(path)

for year in FileList:
	if(year == '.DS_Store'): continue
	else: yearlist = getDirList(path + year)
	for month in yearlist:
		if(month == '.DS_Store'): continue
		else: monthlist = getDirList(path + year + '/' + month)
		for firstfile in monthlist:#5,6,7
			if(firstfile == '.DS_Store'): continue
			else: secondlist = getDirList(path + year + '/' + month + '/' +firstfile)
			for secfile in secondlist:#157balabala
				if(secfile == '.DS_Store'): continue
				else: thirdlist = getDirList(path + year + '/' + month + '/' + firstfile + '/' + secfile)
				for thirdfile in thirdlist:#0,1,2
					if(thirdfile == '.DS_Store'): continue
					else: wenjian = getDirList(path + year + '/' + month + '/' +firstfile + '/' + secfile+ '/' + thirdfile)
					for item in wenjian: 
						try: 
							f = open(path + year + '/' + month + '/' + firstfile + '/' + secfile + '/' + thirdfile + '/' + item,'rb')
						except e:
							print e
							continue
						if(item == '.DS_Store'): continue
						else:
							new_id = int(item.split('_')[0])
							time = item.split('_')[1]
							if(len(item.split('_'))== 3):
								timesec = (item.split('_')[2]).split('.')[0]
							else: 
								timesec = '0-0'
								print item
							timetrans = time.split('-')
							if(int(timetrans[2]) < 10):
								time = timetrans[0] +'-' + timetrans[1] + '-' + str(0) + timetrans[2]
							if (time < starttime) or (time > endtime): 
								print time
								continue
							if(len(timesec.split('-')) == 2):
								minu = timesec.split('-')[0]
								sec = timesec.split('-')[1]
								timestamp = time + ' ' + thirdfile + ':' + minu + ':' + sec
							else: timestamp = time + ' ' + thirdfile + ':' + '0' + ':' + '0'

							"""				
							cameinfo = secfile.split('_')
							ids = cameinfo[0]
							new_id = int(ids.split('-')[1])
							nameofcamera = ''
							for i in range(1, len(cameinfo) - 1):
								nameofcamera += (cameinfo[i] + '_')
							nameofcamera += cameinfo[len(cameinfo) - 1]
							"""

							fin = f.read()
							bin = psycopg2.Binary(fin)
							if IsList:
								if nameofcamera not in ParseList:
									continue
							try:
								bin = psycopg2.Binary(fin)
								cur.execute("INSERT INTO images4(content, time, new_id) VALUES (%s,\'%s\', %d)" %(bin.getquoted(), timestamp, new_id))						
							except psycopg2.Error as e:
								# Use dictionary to eliminate duplicate
								if nameofcamera not in D:
									D.append(nameofcamera)
				#conn.commit ()

# Write logfile to record missing camera
for cam in D:
    logfile.write(cam + '\n')
logfile.close()
