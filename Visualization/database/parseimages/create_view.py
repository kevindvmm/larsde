import psycopg2

tables = [
    'projectslsdenycwebcamsourcrawl20151210',
    'projectslsdenycwebcamsourcrawl20151211', 
    'projectslsdenycwebcamsourcrawl20151212', 
    'projectslsdenycwebcamsourcrawl20151213', 
    'projectslsdenycwebcamsourcrawl20151214', 
    'projectslsdenycwebcamsourcrawl20151215', 
    'projectslsdenycwebcamsourcrawl20151216', 
    'projectslsdenycwebcamsourcrawl20151217', 
    'projectslsdenycwebcamsourcrawl20151218',
    'projectslsdenycwebcamsourcrawl20151219', 
    'projectslsdenycwebcamsourcrawl20151220', 
    'projectslsdenycwebcamsourcrawl20151221', 
    'projectslsdenycwebcamsourcrawl20151222', 
    'projectslsdenycwebcamsourcrawl20151223', 
    'projectslsdenycwebcamsourcrawl20151224', 
    'projectslsdenycwebcamsourcrawl20151225', 
    'projectslsdenycwebcamsourcrawl2015123', 
    'projectslsdenycwebcamsourcrawl2015124',  
    'projectslsdenycwebcamsourcrawl2015125',  
    'projectslsdenycwebcamsourcrawl2015126',  
    'projectslsdenycwebcamsourcrawl2015127',  
    'projectslsdenycwebcamsourcrawl2015128',  
    'projectslsdenycwebcamsourcrawl2015129',  
    'projectslsdenycwebcamsourcrawl2016110',  
    'projectslsdenycwebcamsourcrawl2016111',  
    'projectslsdenycwebcamsourcrawl2016112',  
    'projectslsdenycwebcamsourcrawl2016113',  
    'projectslsdenycwebcamsourcrawl2016114',  
    'projectslsdenycwebcamsourcrawl2016115',  
    'projectslsdenycwebcamsourcrawl2016116',  
    'projectslsdenycwebcamsourcrawl2016118',  
    'projectslsdenycwebcamsourcrawl2016119',  
    'projectslsdenycwebcamsourcrawl2016120',  
    'projectslsdenycwebcamsourcrawl2016121',  
    'projectslsdenycwebcamsourcrawl2016122',  
    'projectslsdenycwebcamsourcrawl2016123',  
    'projectslsdenycwebcamsourcrawl2016124',  
    'projectslsdenycwebcamsourcrawl2016125',  
    'projectslsdenycwebcamsourcrawl2016126',  
    'projectslsdenycwebcamsourcrawl2016127',  
    'projectslsdenycwebcamsourcrawl2016128',  
    'projectslsdenycwebcamsourcrawl2016129',  
    'projectslsdenycwebcamsourcrawl201613',  
    'projectslsdenycwebcamsourcrawl2016130',  
    'projectslsdenycwebcamsourcrawl2016131',  
    'projectslsdenycwebcamsourcrawl201614',   
    'projectslsdenycwebcamsourcrawl201615',   
    'projectslsdenycwebcamsourcrawl201616',   
    'projectslsdenycwebcamsourcrawl201617',   
    'projectslsdenycwebcamsourcrawl201618',   
    'projectslsdenycwebcamsourcrawl201619',   
    'projectslsdenycwebcamsourcrawl201621',   
    'projectslsdenycwebcamsourcrawl201631',   
    'projectslsdenycwebcamsourcrawl2016310', 
    'projectslsdenycwebcamsourcrawl2016316',  
    'projectslsdenycwebcamsourcrawl2016317',  
    'projectslsdenycwebcamsourcrawl2016318',
    'projectslsdenycwebcamsourcrawl201632',
    'projectslsdenycwebcamsourcrawl201633',
    'projectslsdenycwebcamsourcrawl201634',
    'projectslsdenycwebcamsourcrawl201635',   
    'projectslsdenycwebcamsourcrawl201636', 
    'projectslsdenycwebcamsourcrawl201637', 
    'projectslsdenycwebcamsourcrawl201638',   
    'projectslsdenycwebcamsourcrawl201639']



fconf = open ('dbconfig.txt', 'r')
conf = fconf.read()

conn = psycopg2.connect(conf)
conn.autocommit= True
cur = conn.cursor()

print ("Merging...")

try:
    cur.execute('DROP VIEW IF EXISTS images_view;');
except psycopg2.Error as e:
    print e.pgerror


qry = 'CREATE VIEW images_view as'

isFirst=True
for table in tables:
    if isFirst:
        isFirst=False
    else:
        qry += ' UNION'
    qry += ' SELECT * FROM ' + table

try:
    cur.execute(qry);
except psycopg2.Error as e:
    print e.pgerror





