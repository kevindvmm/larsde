//Function for time chart in selecting time of video player

var placeholderdata=[];

var timemargin = {top: 5, right: 10, bottom: 135, left: 40},
    timemargin2 = {top: 190, right: 10, bottom: 20, left: 40},
    timewidth = 600 - timemargin.left - timemargin.right,
    timeheight = 300 - timemargin.top - timemargin.bottom,
    timeheight2 = 300 - timemargin2.top - timemargin2.bottom;

var parseDate = d3.time.format("%b %Y").parse;

//define x,y range and axis
var tx = d3.time.scale().range([0, timewidth]),
    tx2 = d3.time.scale().range([0, timewidth]),
    ty = d3.scale.linear().range([timeheight, 0]),
    ty2 = d3.scale.linear().range([timeheight2, 0]);

var txAxis = d3.svg.axis().scale(tx).orient("bottom");
var txAxis2 = d3.svg.axis().scale(tx2).orient("bottom").ticks(d3.time.weeks,1)
                .tickFormat(function(d){//need to fix the ticks
                  date = new Date(d);
                  return (date.getMonth()+1)+"/"+date.getDate();
                })
                .tickSize(0)
                .tickPadding(height/4);;
var tyAxis = d3.svg.axis().scale(ty).orient("left");

var timebrush = d3.svg.brush()
                 .x(tx2)
                 .on("brush", timebrushed)
                 .on("brushend", timebrushend);

var area = d3.svg.area()
            .interpolate("monotone")
            .x(function(d) { return tx(d.date); })
            .y0(timeheight)
            .y1(function(d) { return ty(d.img); });

var timesvg = d3.select("#timechart").append("svg")
                .attr("width", timewidth + timemargin.left + timemargin.right)
                .attr("height", timeheight + timemargin.top + timemargin.bottom);

timesvg.append("defs").append("clipPath")
       .attr("id", "clip")
       .append("rect")
         .attr("width", timewidth)
         .attr("height", timeheight);

var focus = timesvg.append("g")
                   .attr("class", "focus")
                   .attr("transform", "translate(" + timemargin.left + "," + timemargin.top + ")");

var context = timesvg.append("g")
                     .attr("class", "context")
                     .attr("transform", "translate(" + timemargin2.left + "," + timemargin2.top + ")");

$.get( "/checkimg",{}, function( data, status ) {
  //console.log(data.coords[0])
  for (i=0; i<data.coords.length; i++){
    var parts=data.coords[i][0].split('/');
    var newdate=new Date(parts[0],parts[1]-1,parts[2]);
    placeholderdata.push ({img: data.coords[i][1], date: newdate});
  }

  tx.domain([new Date(2015,11,1,0,0,0),new Date(2016,2,31,0,0,0)]);
  ty.domain([0, 3]);
  tx2.domain(tx.domain());
  ty2.domain(ty.domain());

  barwidth = timewidth/(placeholderdata.length*1.2);
  focus.append("path")
       .datum(placeholderdata)
       .attr("class", "area")
       .attr("d", area);

  focus.append("g")
       .attr("class", "x axis")
       .attr("transform", "translate(0," + timeheight + ")")
       .call(txAxis);

  focus.append("g")
       .attr("class", "y axis")
       .call(tyAxis);

  context.append("g")
         .attr("class", "bars")
         .selectAll(".bar")
         .data(placeholderdata)
           .enter().append("rect")
                   .attr("class", "bar")
                   .attr("x", function(d) { return tx2(d.date) - 3; })
                   .attr("width", barwidth)
                   .attr("y", function(d) { return ty2(d.img); })
                   .attr("height", function(d) { return timeheight2 - ty2(d.img); });

  context.append("g")
         .attr("class", "x axis")
         .attr("transform", "translate(0," + timeheight2 + ")")
         .call(txAxis2);

  //set brush
  contextbrush = context.append("g")
                       .attr("class", "x brush")
                       .call(timebrush);
  contextbrush.selectAll("rect")
              .attr("height", timeheight2);
  contextbrush.selectAll(".resize").append("path").attr("d", resizePath);

  //set initial brush
  timebrush.extent([new Date(2016,0,10,0,0,0),new Date(2016,0,30,0,0,0)]);
  //console.log(timebrush.extent());
  context.select('.x.brush').call(timebrush);
  timebrushed();
});

function timebrushed() {
  //console.log(timebrush.extent());
  tx.domain(timebrush.empty() ? tx2.domain() : timebrush.extent());
  focus.select(".area").attr("d", area);
  focus.select(".x.axis").call(txAxis);
}

function timebrushend() {
  //console.log(timebrush.extent());
  if (timebrush.empty()){
    mainStartDT = new Date(2015,11,1,0,0,0);
    mainEndDT = new Date(2016,2,31,0,0,0);
  }
  else {
    mainStartDT = timebrush.extent()[0];
    mainEndDT = timebrush.extent()[1];
  }
  //console.log(mainStartDT);
  //console.log(mainEndDT);
  $('#datetimepicker2').data('DateTimePicker').date(moment(mainEndDT));
  $('#datetimepicker1').data('DateTimePicker').date(moment(mainStartDT));
}

function type(d) {
  d.date = parseDate(d.date);
  d.price = +d.price;
  return d;
}

//set brush according to external input
function changetimebrush() {
  console.log("change brush");
  if (!timebrush.empty()){
    timebrush.extent([mainStartDT,mainEndDT]);
    //console.log(timebrush.extent());
    context.select('.x.brush').call(timebrush);
    timebrushed();
  }
}

function resizePath(d) {
  var e = +(d == "e"),
      x = e ? 1 : -1,
      y = timeheight2 / 3;
  return "M" + (.5 * x) + "," + y
             + "A6,6 0 0 " + e + " " + (6.5 * x) + "," + (y + 6)
             + "V" + (2 * y - 6)
             + "A6,6 0 0 " + e + " " + (.5 * x) + "," + (2 * y)
             + "Z"
             + "M" + (2.5 * x) + "," + (y + 8)
             + "V" + (2 * y - 8)
             + "M" + (4.5 * x) + "," + (y + 8)
             + "V" + (2 * y - 8);
}

