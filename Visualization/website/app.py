# --------------------------------------------------------
# This python file is the server of visualization tool
# run on port number 5000
# --------------------------------------------------------
from flask import Flask, render_template, request, url_for, jsonify, Response, redirect, flash
import psycopg2
import base64
from datetime import timedelta
from datetime import datetime
from pprint import pprint
import json
from wordcloud import WordCloud
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import re
import random
import shlex
from flask.ext.login import LoginManager, UserMixin, login_required, login_user, logout_user, current_user
import os
from hashlib import sha256
from hmac import HMAC
import csv

app = Flask(__name__)
#app.config['DEBUG'] = True
app.config.update(DEBUG = False, SECRET_KEY = 'dvmm32123')

# flask-login
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

acceptfailurelogintime = 5

def connect_database():
	try:
		conn = psycopg2.connect("dbname='larsde_other' user='flask' host='larsde.cs.columbia.edu' password='dvmm32123'")
		conn_images = psycopg2.connect("dbname='larsde_images' user='flask' host='larsde.cs.columbia.edu' password='dvmm32123'")
		conn.autocommit = True
		conn_images.autocommit = True
	except Exception as e:
		print e, "Connection Unsucessful"

	cur = conn.cursor()
	cur_images = conn_images.cursor()
	return cur, cur_images

cur, cur_images = connect_database()

#force to check the file on server
@app.after_request
def add_header(response):
	response.cache_control.max_age=0
	return response

@app.route('/',methods=["GET","POST"])
def homepage():
	#print "HOME"
        print current_user
	return render_template("index.html")

@app.route("/dashboard.html")
@login_required
def dashboard():
	print current_user
	return render_template("dashboard.html")

@app.route("/logout.html")
@login_required
def logout():
	logout_user()
	return render_template("logout.html")

#get camaera info
@app.route("/camcoords")
def camcoords():
	global cur
	global cur_images
	try:
		cur.execute("select distinct camname, location_lat, location_long, facing, new_id from cameras")
	except Exception,e:
		print "Exception happened when accessing the database, reconnecting..."
		print e
		cur, cur_images = connect_database()
		cur.execute("select distinct camname, location_lat, location_long, facing, new_id from cameras")

	data = cur.fetchall()
	return jsonify(coords = data)

#get tweets info
@app.route("/alltweets")
def alltweets():
	global cur
	global cur_images
	try:
		cur.execute("select time from tweets")
	except Exception,e:
		print e
		cur, cur_images = connect_database()
		cur.execute("select time from tweets")

	data = cur.fetchall()
	rand_smpl = [ data[i] for i in sorted(random.sample(xrange(len(data)), 1000)) ]
	return jsonify(tweets = rand_smpl)

#check camera image existence
@app.route("/checkimg")
def checkimg():
	data=[]
	with open('static/checkimg.csv') as f:
		f_csv = csv.DictReader(f)
		for row in f_csv:
			data.append((row['date'],row['img']))
	#print data
	return jsonify(coords = data)

#get image
@app.route("/images")
def images():
	print current_user
	global cur
	global cur_images
	args = request.args
	starttime = args.get('starttime')
	endtime = args.get('endtime')
	camid = args.get('camid')
	querystring = request.query_string
	try:
		#print cur.mogrify("SELECT content from images WHERE new_id=%s AND time > %s AND time <= %s order by time asc LIMIT 1;", [camid, starttime, endtime])
		cur_images.execute("SELECT content from images WHERE new_id=%s AND time >= %s AND time < %s order by time asc LIMIT 1;", [camid, starttime, endtime])
	except Exception,e:
		print ("Exception executing query: ",e)
		cur, cur_images = connect_database()
		cur_images.execute("SELECT content from images WHERE new_id=%s AND time >= %s AND time < %s order by time asc LIMIT 1;", [camid, starttime, endtime])
		#return "cannot find image"
	
	row = cur_images.fetchone()
	#print row
	if (row==None):
		bindata = "="
		return bindata
	else:
		try:
			bindata = base64.b64encode(row[0]).decode('utf-8')
		except Exception as e:
			print e
			return "error using base64"
		return bindata

#get content
@app.route("/click_content")
def click_content():
	global cur
	global cur_images
	args = request.args
	print args
	type_sub = args.get('type_sub')
	typename = args.get('typename')
	startD = args.get('startD')
	endD = args.get('endD')
	startT = args.get('startT')
	endT = args.get('endT')
	Lat = args.get('lat')
	Lng = args.get('lng')
	keywords = args.get('keywords')
	print 'startD '+startD
	print 'endD '+endD
	print 'startT '+startT
	print 'endT '+endT
	print 'Lat ' + Lat
	print 'Lng ' + Lng
	if(typename == 'TWITTER'):
		#do something
		#parse the data
		print("for TWITTER")
		words = keywords.split()

		### AVOID SQL INJECTION
		newwords=[]
		for word in words:
			newword="%"+word.upper()+"%"
			newwords.append(newword)
		print newwords
		try:
			print cur.mogrify("SELECT time, content from tweets WHERE time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s AND lat IN (%s) AND long IN (%s) AND UPPER(content) ILIKE ALL(%s);" 
						 , [startD, endD, startT, endT, Lat, Lng, newwords])
			cur.execute("SELECT time, content from tweets WHERE time::date BETWEEN %s and %s \
							 AND time::time BETWEEN %s and %s AND lat IN (%s) AND long IN (%s) AND UPPER(content) ILIKE ALL(%s);" 
						 , [startD, endD, startT, endT, Lat, Lng, newwords])
		except Exception,e:
			print e
			cur, cur_images = connect_database()
			cur.execute("SELECT time, content from tweets WHERE time::date BETWEEN %s and %s \
							 AND time::time BETWEEN %s and %s AND lat IN (%s) AND long IN (%s) AND UPPER(content) ILIKE ALL(%s);" 
						 , [startD, endD, startT, endT, Lat, Lng, newwords])
			#return "cannot find tweets"
	else:
		if(type_sub=='true'):
			try:
				cur.execute("SELECT time, type, subtype from alerts WHERE type= %s \
						 AND time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s \
						AND location_lat IN (%s) AND location_long IN (%s);" 
						 , [typename, startD, endD, startT, endT, Lat, Lng])
			except Exception,e:
				print e
				cur, cur_images = connect_database()
				cur.execute("SELECT time, type, subtype from alerts WHERE type= %s \
						 AND time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s \
						AND location_lat IN (%s) AND location_long IN (%s);" 
						 , [typename, startD, endD, startT, endT, Lat, Lng])
				#return "cannot find alerts"
		else:
			try:
				cur.execute("SELECT time, type, subtype from alerts WHERE subtype= %s \
						 AND time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s \
						AND location_lat IN (%s) AND location_long IN (%s);" 
						 , [typename, startD, endD, startT, endT, Lat, Lng])
			except Exception,e:
				print e
				cur, cur_images = connect_database()
				cur.execute("SELECT time, type, subtype from alerts WHERE subtype= %s \
						 AND time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s \
						AND location_lat IN (%s) AND location_long IN (%s);" 
						 , [typename, startD, endD, startT, endT, Lat, Lng])
				#return "cannot find alerts"
	row = cur.fetchall()
	#print(row)
	return jsonify(coords = row)

@app.route("/waze_twitter")
def waze_twitter():
	global cur
	global cur_images
	args = request.args
	type_sub = args.get('type_sub')
	typename = args.get('typename')
	startD = args.get('startD')
	endD = args.get('endD')
	startT = args.get('startT')
	endT = args.get('endT')
	keywords = args.get('keywords')
	qry = ""
	#print("Keywords: "+str(keywords))
	if(typename == 'TWITTER'):
		#do something
		#parse the data
		print("for TWITTER")
		words = keywords.split()

		### AVOID SQL INJECTION
		newwords=[]
		for word in words:
			newword="%"+word.upper()+"%"
			newwords.append(newword)
		print newwords
		try:
			print cur.mogrify("SELECT lat, long from tweets WHERE time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s AND UPPER(content) ILIKE ALL(%s);" 
						 , [startD, endD, startT, endT, newwords])
			cur.execute("SELECT lat, long from tweets WHERE time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s AND UPPER(content) ILIKE ALL(%s);" 
						 , [startD, endD, startT, endT, newwords])
		except Exception,e:
			print e
			cur, cur_images = connect_database()
			cur.execute("SELECT lat, long from tweets WHERE time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s AND UPPER(content) ILIKE ALL(%s);" 
						 , [startD, endD, startT, endT, newwords])
			#return "cannot find tweets"
	
	else:
		if(type_sub=='true'):
			try:
				cur.execute("SELECT location_lat, location_long from alerts WHERE type= %s \
						 AND time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s;" 
						 , [typename, startD, endD, startT, endT])
			except Exception,e:
				print e
				cur, cur_images = connect_database()
				cur.execute("SELECT location_lat, location_long from alerts WHERE type= %s \
						 AND time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s;" 
						 , [typename, startD, endD, startT, endT])
				#return "cannot find alerts"
		else:
			try:
				cur.execute("SELECT location_lat, location_long from alerts WHERE subtype= %s \
						 AND time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s;" 
						 , [typename, startD, endD, startT, endT])
			except Exception,e:
				print e
				cur, cur_images = connect_database()
				cur.execute("SELECT location_lat, location_long from alerts WHERE subtype= %s \
						 AND time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s;" 
						 , [typename, startD, endD, startT, endT])
				#return "cannot find alerts"
	
	row = cur.fetchall()
	return jsonify(coords = row)

#get jam level
@app.route("/jam_level")
def jam_level():
	global cur
	global cur_images
	args = request.args
	startD = args.get('startD')
	endD = args.get('endD')
	startT = args.get('startT')
	endT = args.get('endT')
	print("jam query: SELECT location_lat, location_long,level from jams WHERE \
				 time::date BETWEEN %s and %s \
				 AND time::time BETWEEN %s and %s order by level;" 
				 , [startD, endD, startT, endT])
	try:
		cur.execute("SELECT location_lat, location_long,level from jams WHERE \
				 time::date BETWEEN %s and %s \
				 AND time::time BETWEEN %s and %s order by level;" 
				 , [startD, endD, startT, endT])
	except Exception,e:
		print e
		cur, cur_images = connect_database()
		cur.execute("SELECT location_lat, location_long,level from jams WHERE \
				 time::date BETWEEN %s and %s \
				 AND time::time BETWEEN %s and %s order by level;" 
				 , [startD, endD, startT, endT])
		return "cannot find jams"
	
	row = cur.fetchall()

	return jsonify(jam = row)

#get content according to rectangle
@app.route('/wordCloud',methods=['POST'])
def wordCloud():
	global cur
	global cur_images
	try:
		coords = json.loads(request.form['coord'])
		print(coords)
		lat1 = coords[0]['lat']
		lat2 = coords[2]['lat']
		lng1 = coords[0]['lng']
		lng2 = coords[2]['lng']
	except Exception,e:
		print "error1"
		print e
		
	sT= request.form['startTime']
	sD = request.form['startDate']
	eT = request.form['endTime']
	eD = request.form['endDate']

	try:
		cur.execute("SELECT content from tweets WHERE \
					 	 lat BETWEEN %s and %s		  \
					 AND long BETWEEN %s and %s		  \
					 AND time::date BETWEEN %s and %s \
					 AND time::time BETWEEN %s and %s;" 
					 , [lat1, lat2, lng1, lng2, sD, eD, sT, eT])
	except Exception,e:
		print "error2"
		print e
		cur, cur_images = connect_database()
		cur.execute("SELECT content from tweets WHERE \
					 	 lat BETWEEN %s and %s		  \
					 AND long BETWEEN %s and %s		  \
					 AND time::date BETWEEN %s and %s \
					 AND time::time BETWEEN %s and %s;" 
					 , [lat1, lat2, lng1, lng2, sD, eD, sT, eT])
	row = cur.fetchall()
	data = ''
	for ele in row:
		tweet = str(ele)
		for word in tweet.split():
			word=word.strip('(')
			word = re.sub(r'[^\w]', '',word.strip(',)'))
			data+=' %s' %word	
	wordcloud = WordCloud(background_color="white", max_font_size=40, relative_scaling=.5).generate(data)
	plt.imshow(wordcloud)
	plt.axis("off")
	plt.savefig('static/img/wordcloud.png', bbox_inches='tight')
	return 'Showing World Cloud'

#add event
@app.route("/event",methods=['POST'])
def createEvent():
	global cur
	global cur_images
	name = str(request.form['description'])
	cameraid = request.form['cameraid']
	sT= request.form['startTime']
	eT = request.form['endTime']
	taguser = str(current_user)
	print name
	print cameraid
	print sT
	print eT
	print taguser
	try:
		print cur.mogrify("INSERT INTO event (description,camera_id,starttime,endtime,tag) VALUES (%s,%s,%s,%s,%s);",[name, cameraid, sT, eT, taguser])
		cur.execute("INSERT INTO event (description,camera_id,starttime,endtime,tag) VALUES (%s,%s,%s,%s,%s);",[name, cameraid, sT, eT, taguser])
	except Exception,e:
		print e
		cur, cur_images = connect_database()
		cur.execute("INSERT INTO event (description,camera_id,starttime,endtime,tag) VALUES (%s,%s,%s,%s,%s);",[name, cameraid, sT, eT, taguser])
	return "inserted data"

#load existing event
@app.route("/load_event")
def loadevent():
	global cur
	global cur_images
	try:
		cur.execute("select id, description, camera_id, starttime, endtime, tag from event")
	except Exception,e:
		print "Exception happened when accessing the database, reconnecting..."
		print e
		cur, cur_images = connect_database()
		cur.execute("select id, description, camera_id, starttime, endtime, tag from event")
	data = cur.fetchall()
	return jsonify(coords = data)

#User class
class User(UserMixin):

	def __init__(self, id, username):
		self.id = id
		self.username = username

	def __repr__(self):
		return "%s" % (self.username)

	def getId(self):
		return self.id

def checkUsername(Username):
	global cur
	global cur_images
	try:
		cur.execute("SELECT * from users WHERE username=%s;" , [Username])
	except Exception,e:
		print "error in checkusername"
		print e
		cur, cur_images = connect_database()
		cur.execute("SELECT * from users WHERE username=%s;" , [Username])
	row = cur.fetchall()
	return row

#check user existence
def checkUserStatus(userid):
	global cur
	global cur_images
	try:
		cur.execute("SELECT failuretime from userstatus WHERE id=%s;" , [userid])
	except Exception,e:
		print "error in checkusername"
		print e
		cur, cur_images = connect_database()
		cur.execute("SELECT failuretime from userstatus WHERE id=%s;" , [userid])
	row = cur.fetchall()
	return row

#check if blocked
def addFailuretime(userid, failtimes):
	global cur
	global cur_images
	try:
		cur.execute("UPDATE userstatus SET failuretime=failuretime+1 WHERE id=%s;", [userid])
	except Exception,e:
		print "error in increase failuretime"
		print e
		cur, cur_images = connect_database()
		cur.execute("UPDATE userstatus SET failuretime=failuretime+1 WHERE id=%s;", [userid])
	return "failtimes increased"

#encode password
def encrypt_password(password, salt=None):
	# create salt
	if salt is None:
		salt = os.urandom(8)

	assert 8 == len(salt)
	assert isinstance(salt, str)

	if isinstance(password, unicode):
		password = password.encode('UTF-8')

	assert isinstance(password, str)

	result = password
	for i in xrange(10):
		result = HMAC(result, salt, sha256).digest()

	return salt+result

def validate_password(hashed, input_password):
	return hashed == encrypt_password(input_password, salt=hashed[:8])

def resetFailuretime(userid):
	global cur
	global cur_images
	try:
		cur.execute("UPDATE userstatus SET failuretime=0 WHERE id=%s;", [userid])
	except Exception,e:
		print "error in failuretime reset"
		print e
		cur, cur_images = connect_database()
		cur.execute("UPDATE userstatus SET failuretime=0 WHERE id=%s;", [userid])
	return "failtimes reset"

#login page support
@app.route("/login",methods=["GET","POST"])
def login():
	global acceptfailurelogintime
	error = None
	if request.method == 'POST':
		#print "POST Login"
		Username = request.form['Username']
		Password = request.form['Password']
		Rememberme = request.form.getlist("Rememberme")
		row = checkUsername(Username)
		if (len(row) == 0):
			print "Incorrect Username or Password"
			flash("Incorrect Username or Password")
			return render_template("login.html")
		else:
			getid = row[0][0]
			getusername = row[0][1]
			getpassword = row[0][2]
			getpassword.rstrip()
			getpassword = str(base64.b64decode(getpassword))
			statusrow = checkUserStatus(getid)
			failtimes = statusrow[0][0]
			if (failtimes>=acceptfailurelogintime):
				print "Blocked User"
				flash("Blocked User")
				return render_template("login.html")
			elif (validate_password(getpassword, Password)):
				print "correct login"
				user = User(getid, Username)
				resetFailuretime(getid)
				if (len(Rememberme)==0):
					login_user(user)
				else:
					login_user(user, remember=True)
				return redirect(url_for("dashboard"))
			else:
				print "Incorrect Username or Password"
				flash("Incorrect Username or Password")
				addFailuretime(getid,failtimes)
				return render_template("login.html")
	else:
		#print "GET Login"
		return render_template("login.html")

# callback to reload the user object
@login_manager.user_loader
def load_user(userid):
	global cur
	global cur_images
	#print userid
	try:
		cur.execute("SELECT id, username from users WHERE id=%s;" , [userid])
	except Exception,e:
		print "error in login"
		print e
		cur, cur_images = connect_database()
		cur.execute("SELECT id, username from users WHERE id=%s;" , [userid])
	row = cur.fetchall()
	username = row[0][1]
	nowuserid = row[0][0]
	nowuser = User(userid, username)
	if (unicode(nowuserid) == userid):
		#print 'load user'
		return nowuser
	else:
		#print 'no load'
		return None

if __name__ == "__main__":	
	app.run(host='0.0.0.0',port=5000)
