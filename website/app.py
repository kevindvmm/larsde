import os
import sys

abspath_2 = os.path.abspath(__file__)
dname_2 = os.path.dirname(abspath_2)
sys.path.append(dname_2+'/../../triplet_loss/')

import lasagne
from core.common import *
from core.deep_matcher import *
from core.deep_clf import *
from core.data_manager import *
from core.model_factory import *
from core.base_model import *
# from core.deep_set_models import *
from core.pretrained_models import VGGS, VGG16
from core.set_data_generator import *
from core.hasher import *
from real_time_test_3 import *

frame_count = 0
frame_1_dir = []
frame_1_location = []
frame_1_location_box = []
frame_2_dir = []
frame_2_location = []
frame_2_location_box = []
im_velocity_array = []
user_play_status = {}
user_object_detection_status = 'false'
user_object_tracking_status = 'false'
precomputed_start_time = '12/7/2015, 8:00:00 AM'
precomputed_end_time = '12/7/2015, 8:29:59 AM'
#image_process_interval = 0.9
image_process_interval = 0.2

def run():
	model_type = 0
	if model_type == 0:
		''' train a matcher from scratch '''
		larsde_params = DeepMatcherExptConfig()
		larsde_params.model_params.model_name = 'larsde_2'
		# larsde_params.model_params.model_type = ModelType.CIFAR10
		larsde_params.model_params.output_layer_name = 'output'
		# larsde_params.model_params.cls_num = 10
		larsde_params.model_params.img_sz = (32,32)
		larsde_params.train_params.loss_type = LossType.Triplet
		larsde_params.train_params = TripletLossTrainParams()
		larsde_params.train_params.batch_sz = 16
		# larsde_params.train_params = TripletLossTrainParams()
		larsde_params.train_params.triplet_margin = 0.2
		# larsde_params.train_params.num_triplets = 440
		# larsde_params.train_params.num_epochs = 20
		larsde_params.train_params.num_epochs = 0
		larsde_params.train_params.lrate = 0.001
		larsde_params.train_params.param_layer_names = ['conv5_1', 'conv5_2', 'conv5_3']

	runner = LarsdeRunner(larsde_params)
	runner.build_model()
	runner.prepare_data()
	runner.train_model()
	# runner.test_model()
	return runner

runner = run()

# --------------------------------------------------------
# This python file is the server of visualization tool
# with automatic car detection
# run on port number 5001
# --------------------------------------------------------
from flask import Flask, render_template, request, url_for, jsonify, Response, redirect, flash
import psycopg2
import base64
from datetime import timedelta
from datetime import datetime
from pprint import pprint
import json
from wordcloud import WordCloud
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import re
import random
import shlex
from flask.ext.login import LoginManager, UserMixin, login_required, login_user, logout_user, current_user
import os
from hashlib import sha256
from hmac import HMAC
import csv
import time
import flask

app = Flask(__name__)
#app.config['DEBUG'] = True
app.config.update(DEBUG = False, SECRET_KEY = 'dvmm32123')

# flask-login
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

acceptfailurelogintime = 5

# --------------------------------------------------------
# Faster R-CNN
import _init_paths
from fast_rcnn.config import cfg
from fast_rcnn.test import im_detect
from fast_rcnn.nms_wrapper import nms
from utils.timer import Timer
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
import caffe, os, sys, cv2
import argparse
from PIL import Image
CLASSES = ('__background__',
           'car')
NETS = {'vgg16': ('VGG16',
                  'VGG16_faster_rcnn_final.caffemodel'),
        'zf': ('ZF',
                  'ZF_faster_rcnn_final.caffemodel')}
# --------------------------------------------------------

def parse_args():
    """Parse input arguments."""
    parser = argparse.ArgumentParser(description='Faster R-CNN demo')
    parser.add_argument('--gpu', dest='gpu_id', help='GPU device id to use [0]',
                        default=0, type=int)
    parser.add_argument('--cpu', dest='cpu_mode',
                        help='Use CPU mode (overrides --gpu)',
                        action='store_true')
    parser.add_argument('--net', dest='demo_net', help='Network to use [vgg16]',
                        choices=NETS.keys(), default='vgg16')

    args = parser.parse_args()

    return args

# --------------------------------------------------------

# --------------------------------------------------------
# Faster R-CNN	
cfg.TEST.HAS_RPN = True  # Use RPN for proposals

args = parse_args()

prototxt = os.path.join('/home/zhengshou/work01/larsde/py-faster-rcnn-onecar', 'models', 'ZF',
						'faster_rcnn_end2end', 'test.prototxt')
caffemodel = os.path.join('/home/zhengshou/work01/larsde/py-faster-rcnn-onecar', 'output/faster_rcnn_end2end/voc_2007_trainval','zf_faster_rcnn_iter_50000.caffemodel')

if not os.path.isfile(caffemodel):
	raise IOError(('{:s} not found.\nDid you run ./data/script/'
				   'fetch_faster_rcnn_models.sh?').format(caffemodel))

if args.cpu_mode:
   caffe.set_mode_cpu()
else:
	caffe.set_mode_gpu()
	caffe.set_device(args.gpu_id)
	cfg.GPU_ID = args.gpu_id
net = caffe.Net(prototxt, caffemodel, caffe.TEST)

print '\n\nLoaded network {:s}'.format(caffemodel)

# Warmup on a dummy image
im = 128 * np.ones((300, 500, 3), dtype=np.uint8)
for i in xrange(2):
	_, _= im_detect(net, im)
# --------------------------------------------------------

def connect_database():
	try:
		conn = psycopg2.connect("dbname='larsde_other' user='flask' host='larsde.cs.columbia.edu' password='dvmm32123'")
		conn_images = psycopg2.connect("dbname='larsde_images' user='flask' host='larsde.cs.columbia.edu' password='dvmm32123'")
		conn.autocommit = True
		conn_images.autocommit = True
	except Exception as e:
		print e, "Connection Unsucessful"

	cur = conn.cursor()
	cur_images = conn_images.cursor()
	return cur, cur_images

def connect_database1(dbname, user, host, password):
	try:
		conn = psycopg2.connect("dbname='" + dbname + "' user='" + user + "' host='" + host + "' password='" + password + "'")
		conn.autocommit = True
	except Exception as e:
		print e, "Connection Unsucessful"
	cur = conn.cursor()
	return cur

cur, cur_images = connect_database()

#force to check the file on server
@app.after_request
def add_header(response):
	response.cache_control.max_age=0
	return response

@app.route('/',methods=["GET","POST"])
def homepage():
	#print "HOME"
        print current_user
	return render_template("index.html")

@app.route("/dashboard.html")
@login_required
def dashboard():
	print current_user
	return render_template("dashboard.html")

@app.route("/logout.html")
@login_required
def logout():
	logout_user()
	return render_template("logout.html")

#get camaera info
@app.route("/camcoords")
def camcoords():
	global cur
	global cur_images
	try:
		cur.execute("select distinct camname, location_lat, location_long, facing, new_id from cameras")
	except Exception,e:
		print "Exception happened when accessing the database, reconnecting..."
		print e
		cur, cur_images = connect_database()
		cur.execute("select distinct camname, location_lat, location_long, facing, new_id from cameras")

	data = cur.fetchall()
	return jsonify(coords = data)

#get tweets info
@app.route("/alltweets")
def alltweets():
	global cur
	global cur_images
	try:
		cur.execute("select time from tweets")
	except Exception,e:
		print e
		cur, cur_images = connect_database()
		cur.execute("select time from tweets")

	data = cur.fetchall()
	rand_smpl = [ data[i] for i in sorted(random.sample(xrange(len(data)), 1000)) ]
	return jsonify(tweets = rand_smpl)

#check camera image existence
@app.route("/checkimg")
def checkimg():
	data=[]
	with open('static/checkimg.csv') as f:
		f_csv = csv.DictReader(f)
		for row in f_csv:
			data.append((row['date'],row['img']))
	#print data
	return jsonify(coords = data)

#get image
@app.route("/images")
def images():

	global frame_count
	global frame_1_dir
	global frame_1_location
	global frame_1_location_box
	global frame_2_dir
	global frame_2_location
	global frame_2_location_box
	global runner
	global im_velocity_array
	global user_play_status
	image_process_interval = 0.4

	response = {}
	response["byteImage"] = ""
	response["coordinates"] = []

	start_time = time.time()
#	print "------------------------------------------------------ " + user_play_status[current_user]
	if (current_user in user_play_status and (user_play_status[current_user] == 'false' or time.time() - user_play_status[current_user][1] < image_process_interval)):
		print "discarding requests since still processing ........"
		return flask.jsonify(response)
	user_play_status[current_user] = ('true', time.time())
	print current_user
	global cur
	global cur_images
	args = request.args
	starttime = args.get('starttime')
	endtime = args.get('endtime')
	camid = args.get('camid')
	querystring = request.query_string
	try:
		cur_images.execute("SELECT content from images WHERE new_id=%s AND time > %s AND time <= %s order by time asc LIMIT 1;", [camid, starttime, endtime])
		print "retrieve image from database time: " + str(time.time() - start_time)
		start_time = time.time()
	except Exception,e:
		print ("Exception executing query: ",e)
		cur, cur_images = connect_database()
		cur_images.execute("SELECT content from images WHERE new_id=%s AND time > %s AND time <= %s order by time asc LIMIT 1;", [camid, starttime, endtime])

	row = cur_images.fetchone()
	if row == None:
		print "hello"
		print
	else:
		print "aaa"
		print
	print "camid: " + str(camid)
	print "starttime: " + str(starttime)
	print "endtime: " + str(endtime)

	#print row
	if (row==None):
		print "no image found in database, returning"
		return flask.jsonify(response)
	else:
		try:
# --------------------------------------------------------
# Faster R-CNN
                        #define the path for image storage
			# current_dir = os.path.dirname(__file__)
			abspath = os.path.abspath(__file__)
			current_dir = os.path.dirname(abspath)

			# image_dir = 'static/viz'
			image_dir = 'temp'

			frame_count = frame_count + 1
			if frame_count > 1:
				im_temp = cv2.imread(current_dir+'/'+image_dir+'/'+'2_1.jpg')
				print "im_temp: " + str(time.time() - start_time)
				start_time = time.time()	
				cv2.imwrite(current_dir+'/'+image_dir+'/'+'1_1.jpg', im_temp)
				print "imwrite: " + str(time.time() - start_time)
				start_time = time.time()

			image_name = str(current_user) + ".jpg"
			fh = open(os.path.join(current_dir, image_dir, image_name),'wb')
			fh.write(row[0])
			fh.close()
			print "write image to path: " + os.path.join(current_dir, image_dir, image_name)
			print "write image to jpg: " + str(time.time() - start_time)
			start_time = time.time()
			image_full_dir = os.path.join(current_dir, image_dir)
			if (user_object_detection_status == 'false'):
				response['byteImage'] = base64.b64encode(row[0]).decode('utf-8')
				return flask.jsonify(response)
			else:
				response['coordinates'] = getCoordinates(net, image_full_dir, image_name, camid, endtime)
			print "demo: " + str(time.time() - start_time)
			start_time = time.time()
                        print "open path: " + os.path.join(current_dir, image_dir,str(current_user)+'.jpg')
                        #fh = open(os.path.join(current_dir, image_dir,'2_1_'+CLASSES[1]+'.jpg'),'rb')
                        fh = open(os.path.join(current_dir, image_dir,str(current_user)+'.jpg'),'rb')
                        response['byteImage'] = base64.b64encode(fh.read()).decode('utf-8')
                        print "decode: " + str(time.time() - start_time)
                        start_time = time.time()
                        fh.close()

# --------------------------------------------------------
			print "elapsed time: " + str(time.time() - start_time)
		except Exception as e:
			print e
			return flask.jsonify(response)
		return flask.jsonify(response) 

#the original function for processing images
@app.route("/imagesOri")
def imagesOri():

	global frame_count
	global frame_1_dir
	global frame_1_location
	global frame_1_location_box
	global frame_2_dir
	global frame_2_location
	global frame_2_location_box
	global runner
	global im_velocity_array
	global user_play_status
	global cur
	global cur_images
	image_process_interval = 0.3
	start_time = time.time()
	if (current_user in user_play_status and (user_play_status[current_user] == 'false' or time.time() - user_play_status[current_user][1] < image_process_interval)):
		print "discarding requests since still processing ........"
		response = {}
		response["byteImage"] = ""
		response["coordinates"] = []
		return flask.jsonify(response)
	user_play_status[current_user] = ('true', time.time())
	print "current_user: " + str(current_user)
	args = request.args
	starttime = args.get('starttime')
	endtime = args.get('endtime')
	camid = args.get('camid')
	querystring = request.query_string
	try:
		cur_images.execute("SELECT content from images WHERE new_id=%s AND time > %s AND time <= %s order by time asc LIMIT 1;", [camid, starttime, endtime])
		print "retrieve image from database time: " + str(time.time() - start_time)
		start_time = time.time()
	except Exception,e:
		print ("Exception executing query: ",e)
		cur, cur_images = connect_database()
		cur_images.execute("SELECT content from images WHERE new_id=%s AND time > %s AND time <= %s order by time asc LIMIT 1;", [camid, starttime, endtime])
		#return "cannot find image"

	row = cur_images.fetchone()
	if row == None:
		print "hello"
		print
	else:
		print "aaa"
		print
	print "camid: " + camid
	print "starttime: " + starttime
	print "endtime: " + endtime

	#print row
	if (row==None):
		bindata = ""
		response = {}
		response["byteImage"] = bindata
		response['coordinates'] = []
		print "no image found in database, returning"
		return flask.jsonify(response)
	else:
		try:
# --------------------------------------------------------
# Faster R-CNN
                        #define the path for image storage
			abspath = os.path.abspath(__file__)
			current_dir = os.path.dirname(abspath)

			# image_dir = 'static/viz'
			image_dir = 'temp'

			frame_count = frame_count + 1
			if frame_count > 1:
				im_temp = cv2.imread(current_dir+'/'+image_dir+'/'+'2_1.jpg')
				print "im_temp: " + str(time.time() - start_time)
				start_time = time.time()	
				cv2.imwrite(current_dir+'/'+image_dir+'/'+'1_1.jpg', im_temp)
				print "imwrite: " + str(time.time() - start_time)
				start_time = time.time()

			image_name = str(current_user) + ".jpg"
			fh = open(os.path.join(current_dir, image_dir, image_name),'wb')
			fh.write(row[0])
			fh.close()
			print "write image to jpg: " + str(time.time() - start_time)
			start_time = time.time()
			image_full_dir = os.path.join(current_dir, image_dir)
			if (user_object_detection_status == 'false'):
				response = {}
				response['byteImage'] = base64.b64encode(row[0]).decode('utf-8')
				response['coordinates'] = []
				return flask.jsonify(response)
			else:
				demo(net, image_full_dir, image_name, camid, endtime)
			print "demo: " + str(time.time() - start_time)
			start_time = time.time()
			#fh = open(os.path.join(current_dir, image_dir, image_name[:-4]+'_'+CLASSES[1]+'.jpg'),'rb')
			print "open path: " + os.path.join(current_dir, image_dir,'2_1_'+CLASSES[1]+'.jpg')
			fh = open(os.path.join(current_dir, image_dir,'2_1_'+CLASSES[1]+'.jpg'),'rb')
			bindata = base64.b64encode(fh.read()).decode('utf-8')
			print "decode: " + str(time.time() - start_time)
			start_time = time.time()
			fh.close()
# --------------------------------------------------------
			print "elapsed time: " + str(time.time() - start_time)
		except Exception as e:
			print e
			return "error using base64"
		response = {}
		response['byteImage'] = bindata
		response['coordinates'] = [1,2,3,4]
		return flask.jsonify(response)




#get content
@app.route("/click_content")
def click_content():
	global cur
	global cur_images
	args = request.args
	print args
	type_sub = args.get('type_sub')
	typename = args.get('typename')
	startD = args.get('startD')
	endD = args.get('endD')
	startT = args.get('startT')
	endT = args.get('endT')
	Lat = args.get('lat')
	Lng = args.get('lng')
	keywords = args.get('keywords')
	print 'startD '+startD
	print 'endD '+endD
	print 'startT '+startT
	print 'endT '+endT
	print 'Lat ' + Lat
	print 'Lng ' + Lng
	if(typename == 'TWITTER'):
		#do something
		#parse the data
		print("for TWITTER")
		words = keywords.split()

		### AVOID SQL INJECTION
		newwords=[]
		for word in words:
			newword="%"+word.upper()+"%"
			newwords.append(newword)
		print newwords
		try:
			print cur.mogrify("SELECT time, content from tweets WHERE time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s AND lat IN (%s) AND long IN (%s) AND UPPER(content) ILIKE ALL(%s);" 
						 , [startD, endD, startT, endT, Lat, Lng, newwords])
			cur.execute("SELECT time, content from tweets WHERE time::date BETWEEN %s and %s \
							 AND time::time BETWEEN %s and %s AND lat IN (%s) AND long IN (%s) AND UPPER(content) ILIKE ALL(%s);" 
						 , [startD, endD, startT, endT, Lat, Lng, newwords])
		except Exception,e:
			print e
			cur, cur_images = connect_database()
			cur.execute("SELECT time, content from tweets WHERE time::date BETWEEN %s and %s \
							 AND time::time BETWEEN %s and %s AND lat IN (%s) AND long IN (%s) AND UPPER(content) ILIKE ALL(%s);" 
						 , [startD, endD, startT, endT, Lat, Lng, newwords])
			#return "cannot find tweets"
	else:
		if(type_sub=='true'):
			try:
				cur.execute("SELECT time, type, subtype from alerts WHERE type= %s \
						 AND time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s \
						AND location_lat IN (%s) AND location_long IN (%s);" 
						 , [typename, startD, endD, startT, endT, Lat, Lng])
			except Exception,e:
				print e
				cur, cur_images = connect_database()
				cur.execute("SELECT time, type, subtype from alerts WHERE type= %s \
						 AND time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s \
						AND location_lat IN (%s) AND location_long IN (%s);" 
						 , [typename, startD, endD, startT, endT, Lat, Lng])
				#return "cannot find alerts"
		else:
			try:
				cur.execute("SELECT time, type, subtype from alerts WHERE subtype= %s \
						 AND time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s \
						AND location_lat IN (%s) AND location_long IN (%s);" 
						 , [typename, startD, endD, startT, endT, Lat, Lng])
			except Exception,e:
				print e
				cur, cur_images = connect_database()
				cur.execute("SELECT time, type, subtype from alerts WHERE subtype= %s \
						 AND time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s \
						AND location_lat IN (%s) AND location_long IN (%s);" 
						 , [typename, startD, endD, startT, endT, Lat, Lng])
				#return "cannot find alerts"
	row = cur.fetchall()
	#print(row)
	return jsonify(coords = row)

@app.route("/waze_twitter")
def waze_twitter():
	global cur
	global cur_images
	args = request.args
	type_sub = args.get('type_sub')
	typename = args.get('typename')
	startD = args.get('startD')
	endD = args.get('endD')
	startT = args.get('startT')
	endT = args.get('endT')
	keywords = args.get('keywords')
	qry = ""
	#print("Keywords: "+str(keywords))
	if(typename == 'TWITTER'):
		#do something
		#parse the data
		print("for TWITTER")
		words = keywords.split()

		### AVOID SQL INJECTION
		newwords=[]
		for word in words:
			newword="%"+word.upper()+"%"
			newwords.append(newword)
		print newwords
		try:
			print cur.mogrify("SELECT lat, long from tweets WHERE time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s AND UPPER(content) ILIKE ALL(%s);" 
						 , [startD, endD, startT, endT, newwords])
			cur.execute("SELECT lat, long from tweets WHERE time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s AND UPPER(content) ILIKE ALL(%s);" 
						 , [startD, endD, startT, endT, newwords])
		except Exception,e:
			print e
			cur, cur_images = connect_database()
			cur.execute("SELECT lat, long from tweets WHERE time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s AND UPPER(content) ILIKE ALL(%s);" 
						 , [startD, endD, startT, endT, newwords])
			#return "cannot find tweets"
	
	else:
		if(type_sub=='true'):
			try:
				cur.execute("SELECT location_lat, location_long from alerts WHERE type= %s \
						 AND time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s;" 
						 , [typename, startD, endD, startT, endT])
			except Exception,e:
				print e
				cur, cur_images = connect_database()
				cur.execute("SELECT location_lat, location_long from alerts WHERE type= %s \
						 AND time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s;" 
						 , [typename, startD, endD, startT, endT])
				#return "cannot find alerts"
		else:
			try:
				cur.execute("SELECT location_lat, location_long from alerts WHERE subtype= %s \
						 AND time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s;" 
						 , [typename, startD, endD, startT, endT])
			except Exception,e:
				print e
				cur, cur_images = connect_database()
				cur.execute("SELECT location_lat, location_long from alerts WHERE subtype= %s \
						 AND time::date BETWEEN %s and %s \
						 AND time::time BETWEEN %s and %s;" 
						 , [typename, startD, endD, startT, endT])
				#return "cannot find alerts"
	
	row = cur.fetchall()
	return jsonify(coords = row)

#get jam level
@app.route("/jam_level")
def jam_level():
	global cur
	global cur_images
	args = request.args
	startD = args.get('startD')
	endD = args.get('endD')
	startT = args.get('startT')
	endT = args.get('endT')
	print("jam query: SELECT location_lat, location_long,level from jams WHERE \
				 time::date BETWEEN %s and %s \
				 AND time::time BETWEEN %s and %s order by level;" 
				 , [startD, endD, startT, endT])
	try:
		cur.execute("SELECT location_lat, location_long,level from jams WHERE \
				 time::date BETWEEN %s and %s \
				 AND time::time BETWEEN %s and %s order by level;" 
				 , [startD, endD, startT, endT])
	except Exception,e:
		print e
		cur, cur_images = connect_database()
		cur.execute("SELECT location_lat, location_long,level from jams WHERE \
				 time::date BETWEEN %s and %s \
				 AND time::time BETWEEN %s and %s order by level;" 
				 , [startD, endD, startT, endT])
		return "cannot find jams"
	
	row = cur.fetchall()

	return jsonify(jam = row)

#get content according to rectangle
@app.route('/wordCloud',methods=['POST'])
def wordCloud():
	global cur
	global cur_images
	try:
		coords = json.loads(request.form['coord'])
		print(coords)
		lat1 = coords[0]['lat']
		lat2 = coords[2]['lat']
		lng1 = coords[0]['lng']
		lng2 = coords[2]['lng']
	except Exception,e:
		print "error1"
		print e
		
	sT= request.form['startTime']
	sD = request.form['startDate']
	eT = request.form['endTime']
	eD = request.form['endDate']

	try:
		cur.execute("SELECT content from tweets WHERE \
					 	 lat BETWEEN %s and %s		  \
					 AND long BETWEEN %s and %s		  \
					 AND time::date BETWEEN %s and %s \
					 AND time::time BETWEEN %s and %s;" 
					 , [lat1, lat2, lng1, lng2, sD, eD, sT, eT])
	except Exception,e:
		print "error2"
		print e
		cur, cur_images = connect_database()
		cur.execute("SELECT content from tweets WHERE \
					 	 lat BETWEEN %s and %s		  \
					 AND long BETWEEN %s and %s		  \
					 AND time::date BETWEEN %s and %s \
					 AND time::time BETWEEN %s and %s;" 
					 , [lat1, lat2, lng1, lng2, sD, eD, sT, eT])
	row = cur.fetchall()
	data = ''
	for ele in row:
		tweet = str(ele)
		for word in tweet.split():
			word=word.strip('(')
			word = re.sub(r'[^\w]', '',word.strip(',)'))
			data+=' %s' %word	
	wordcloud = WordCloud(background_color="white", max_font_size=40, relative_scaling=.5).generate(data)
	plt.imshow(wordcloud)
	plt.axis("off")
	plt.savefig('static/img/wordcloud.png', bbox_inches='tight')
	return 'Showing World Cloud'

#add event
@app.route("/event",methods=['POST'])
def createEvent():
	global cur
	global cur_images
	name = str(request.form['description'])
	cameraid = request.form['cameraid']
	sT= request.form['startTime']
	eT = request.form['endTime']
	taguser = str(current_user)
	print name
	print cameraid
	print sT
	print eT
	print taguser
	try:
		print cur.mogrify("INSERT INTO event (description,camera_id,starttime,endtime,tag) VALUES (%s,%s,%s,%s,%s);",[name, cameraid, sT, eT, taguser])
		cur.execute("INSERT INTO event (description,camera_id,starttime,endtime,tag) VALUES (%s,%s,%s,%s,%s);",[name, cameraid, sT, eT, taguser])
	except Exception,e:
		print e
		cur, cur_images = connect_database()
		cur.execute("INSERT INTO event (description,camera_id,starttime,endtime,tag) VALUES (%s,%s,%s,%s,%s);",[name, cameraid, sT, eT, taguser])
	return "inserted data"

#load existing event
@app.route("/load_event")
def loadevent():
	global cur
	global cur_images
	try:
		cur.execute("select id, description, camera_id, starttime, endtime, tag from event")
	except Exception,e:
		print "Exception happened when accessing the database, reconnecting..."
		print e
		cur, cur_images = connect_database()
		cur.execute("select id, description, camera_id, starttime, endtime, tag from event")
	data = cur.fetchall()
	return jsonify(coords = data)

# --------------------------------------------------------
# Faster R-CNN

@app.route("/user_play_status")
def updateUserPlayStatus():
	global user_play_status
	args = request.args
	play_status = args.get('play_status')
	user_play_status[current_user] = (play_status, time.time())
	print "update --------------------------------------------play_status " + play_status
	return play_status

@app.route("/user_object_detection_status")
def updateUserObjectDetectionStatus():
        global user_object_detection_status
        args = request.args
        user_object_detection_status = args.get('detection_status')
        return user_object_detection_status

@app.route("/user_object_tracking_status")
def updateUserObjectTrackingStatus():
	global user_object_tracking_status
	args = request.args
	user_object_tracking_status = args.get('tracking_status') 
	return user_object_tracking_status

def vis_detections(im, class_name, dets, image_dir, image_name, thresh=0.5):
	"""Draw detected bounding boxes."""
	global frame_count
	global frame_1_dir
	global frame_1_location
	global frame_1_location_box
	global frame_2_dir
	global frame_2_location
	global frame_2_location_box
	global runner
	global im_velocity_array

	coordinates = {}
	coordinates['boundingBoxes'] = []
	coordinates['trackings'] = []

	frame_1_dir_len = len(frame_1_dir)
	frame_2_dir_len = len(frame_2_dir)
	frame_1_dir = frame_2_dir
	frame_1_location = frame_2_location
	frame_1_location_box = frame_2_location_box
	if frame_count > 1:
		for i in range(frame_2_dir_len):
			im_temp = cv2.imread(image_dir+'/2_box_'+str(i)+'.jpg')
			cv2.imwrite(image_dir+'/1_box_'+str(i)+'.jpg', im_temp)
			# os.system('mv'+' '+image_dir+'/2_box_'+str(i)+'.jpg'+' '+image_dir+'/1_box_'+str(i)+'.jpg')
			frame_1_dir[i] = image_dir+'/1_box_'+str(i)+'.jpg'
	frame_2_dir = []
	frame_2_location = []
	frame_2_location_box = []

	inds = np.where(dets[:, -1] >= thresh)[0]
	"""Record number of detections for bus car person"""
	if len(inds) == 0:
		return

	im = im[:, :, (2, 1, 0)]
#	print im
#	print
#	print im.shape
#	print
	for i in inds:
		bbox = dets[i, :4]
		print "bbox: "
		print bbox
		print "bbox.shape: "
		print bbox.shape
		coordinates['boundingBoxes'].append([int(round(dets[i][0])), int(round(dets[i][1])), int(round(dets[i][2])), int(round(dets[i][3])), int(round(dets[i][4]))])
		image_num = str(i)
		crop_im = im[int(round(bbox[1])):int(round(bbox[3])), int(round(bbox[0])):int(round(bbox[2]))]
		box_dir = image_dir+'/2_box_'+image_num+'.jpg'
		cv2.imwrite(box_dir, crop_im)
		frame_2_dir.append(box_dir)
		frame_2_location.append([[(bbox[0]+bbox[2])/2, (bbox[1]+bbox[3])/2]])
		frame_2_location_box.append([bbox])
		score = dets[i, -1]
	gal_data_list = []
	gal_data_list.extend(frame_1_dir)
	gal_data_list.extend(frame_2_dir)
	gal_data = np.asarray(gal_data_list)


	if (user_object_tracking_status == 'true'):
		ti = time.time()
		frame_1_dir, frame_1_location, frame_1_location_box, frame_2_dir, frame_2_location, frame_2_location_box = runner.test_model_velocity(frame_count, frame_1_dir, frame_1_location, frame_1_location_box, frame_2_dir, frame_2_location, frame_2_location_box, gal_data, 1.2)
		print "object tracking time: " + str(time.time() - ti)
		print len(frame_2_dir)
		print
		print len(frame_2_location)
		print
		print len(frame_2_location_box)
		print
		for i in range(len(frame_2_location)):
			plot_x = []
			plot_y = []
			for j in range(len(frame_2_location[i])):
				plot_x.append(frame_2_location[i][j][0])
				plot_y.append(frame_2_location[i][j][1])
			coordinates['trackings'].append({"plot_x": plot_x, "plot_y": plot_y})

			plot_len = len(plot_x) - 1
			if plot_len == 0:
				continue


	if frame_count > 1:
		im_temp = cv2.imread(image_dir+'/'+'2_1'+'_'+class_name+'.jpg')
		cv2.imwrite(image_dir+'/'+'1_1'+'_'+class_name+'.jpg', im_temp)
	return coordinates


def vis_detections_precomputed(im, class_name, image_dir, image_nam, camid, endtime, thresh=0.5):

	global frame_count
	global frame_1_dir
	global frame_1_location
	global frame_1_location_box
	global frame_2_dir
	global frame_2_location
	global frame_2_location_box
	global runner
	global im_velocity_array

	frame_1_dir_len = len(frame_1_dir)
	frame_2_dir_len = len(frame_2_dir)
	frame_1_dir = frame_2_dir
	frame_1_location = frame_2_location
	frame_1_location_box = frame_2_location_box
	if frame_count > 1:
		for i in range(frame_2_dir_len):
			im_temp = cv2.imread(image_dir+'/2_box_'+str(i)+'.jpg')
			cv2.imwrite(image_dir+'/1_box_'+str(i)+'.jpg', im_temp)
			frame_1_dir[i] = image_dir+'/1_box_'+str(i)+'.jpg'
	frame_2_dir = []
	frame_2_location = []
	frame_2_location_box = []


	cursor = connect_database1("larsde_other", "flask", "larsde.cs.columbia.edu", "dvmm32123")
	cursor.execute("SELECT * from extracted_bounding_boxes WHERE imgcamid='" + camid + "' AND imgtime='" + endtime + "';")
	rows = cursor.fetchall()
	im = im[:, :, (2, 1, 0)]
	#print im
	#print im.shape
	fig, ax = plt.subplots(figsize=(12, 12))
	ax.imshow(im, aspect='equal')
	for i in range(len(rows)):
		row = rows[i]
		image_num = str(i)
		ax.add_patch(
			plt.Rectangle((row[3], row[4]),
				row[5] - row[3],
				row[6] - row[4], fill=False,
				edgecolor='red', linewidth=3.5)
			)
		ax.text(row[3], row[4] - 2,
			'{:s}'.format(class_name),
			bbox=dict(facecolor='blue', alpha=0.5),
			fontsize=14, color='white')
		if (user_object_tracking_status != 'false'):
			plot_x, plot_y = getObjectTrackingCoordinates(row[0])
			plt.plot(plot_x, plot_y, color='yellow', linewidth=10)

			plot_len = len(plot_x) - 1
			if plot_len == 0:
				continue
			plot_x_1 = []
			plot_y_1 = []
			plot_x_1.append(plot_x[0])
			plot_y_1.append(plot_y[0])
			plot_x_1.append(plot_x[0]+5)
			plot_y_1.append(plot_y[0])
			plot_x_1.append(plot_x[0])
			plot_y_1.append(plot_y[0]+5)
			plot_x_1.append(plot_x[0]-5)
			plot_y_1.append(plot_y[0])
			plot_x_1.append(plot_x[0])
			plot_y_1.append(plot_y[0]-5)
			plot_x_1.append(plot_x[0]+5)
			plot_y_1.append(plot_y[0])
			plt.plot(plot_x_1, plot_y_1, color='green', linewidth=10)

			plot_x_1 = []
			plot_y_1 = []
			plot_x_1.append(plot_x[plot_len])
			plot_y_1.append(plot_y[plot_len])
			plot_x_1.append(plot_x[plot_len]+5)
			plot_y_1.append(plot_y[plot_len])
			plot_x_1.append(plot_x[plot_len])
			plot_y_1.append(plot_y[plot_len]+5)
			plot_x_1.append(plot_x[plot_len]-5)
			plot_y_1.append(plot_y[plot_len])
			plot_x_1.append(plot_x[plot_len])
			plot_y_1.append(plot_y[plot_len]-5)
			plot_x_1.append(plot_x[plot_len]+5)
			plot_y_1.append(plot_y[plot_len])
			plt.plot(plot_x_1, plot_y_1, color='blue', linewidth=10)


	plt.axis('off')
	plt.draw()
	if frame_count > 1:
		im_temp = cv2.imread(image_dir+'/'+'2_1'+'_'+class_name+'.jpg')
		cv2.imwrite(image_dir+'/'+'1_1'+'_'+class_name+'.jpg', im_temp)
	fig.savefig(os.path.join(image_dir, '2_1'+'_'+ 'car' +'.jpg'), bbox_inches='tight', pad_inches=-1)
	plt.close(fig)
	print "save path: " + os.path.join(image_dir, '2_1'+'_'+ 'car' +'.jpg')

def getObjectTrackingCoordinates(labelid):
	plot_x = []
	plot_y = []
	cursor = connect_database1("larsde_other", "flask", "larsde.cs.columbia.edu", "dvmm32123")
	rowTracking = (labelid, labelid)
	while (rowTracking != None):
		labelid = rowTracking[0]
		cursor.execute("SELECT * from extracted_bounding_boxes WHERE labelid=" + str(labelid) + ";")
		rowBoundingBox = cursor.fetchone()
		plot_x.append((rowBoundingBox[3] + rowBoundingBox[5]) / 2)
		plot_y.append((rowBoundingBox[4] + rowBoundingBox[6]) / 2)
		cursor.execute("SELECT * from extracted_tracking WHERE labelid2='" + str(labelid) + "';")
		rowTracking = cursor.fetchone()
	return plot_x, plot_y

#
def demo(net, image_dir, image_name, camid, endtime):
    """Detect object classes in an image using pre-computed object proposals."""

    # Load the demo image
    im_file = os.path.join(image_dir, image_name)
    im = cv2.imread(im_file)
    
    if im is None:
		print 'return now'
		return
    
    # Detect all object classes and regress object bounds
    timer = Timer()
    timer.tic()
    if (endtime < precomputed_start_time or endtime > precomputed_end_time):
        scores, boxes = im_detect(net, im)
        timer.toc()
        print ('Detection took {:.3f}s for '
               '{:d} object proposals').format(timer.total_time, boxes.shape[0])

        start_time = time.time()
        # Visualize detections for each class
        CONF_THRESH = 0.9
        NMS_THRESH = 0.3
        for cls_ind, cls in enumerate(CLASSES[1:]):
            cls_ind += 1 # because we skipped background
            cls_boxes = boxes[:, 4*cls_ind:4*(cls_ind + 1)]
            cls_scores = scores[:, cls_ind]
            dets = np.hstack((cls_boxes, cls_scores[:, np.newaxis])).astype(np.float32)
            keep = nms(dets, NMS_THRESH)
            dets = dets[keep, :]
            vis_detections(im, cls, dets, image_dir, image_name, thresh=CONF_THRESH)
        print "visulize time: " + str(time.time() - start_time)
    else:
        vis_detections_precomputed(im, CLASSES[1], image_dir, image_name, camid, endtime)
# --------------------------------------------------------



def getCoordinates(net, image_dir, image_name, camid, endtime):
    """Detect object classes in an image using pre-computed object proposals."""

    # Load the demo image
    im_file = os.path.join(image_dir, image_name)
    im = cv2.imread(im_file)
    
    if im is None:
		print 'return now'
		return
    
    # Detect all object classes and regress object bounds
    timer = Timer()
    timer.tic()
    scores, boxes = im_detect(net, im)
    timer.toc()
    print ('Detection took {:.3f}s for '
           '{:d} object proposals').format(timer.total_time, boxes.shape[0])
    start_time = time.time()
    # Visualize detections for each class
    CONF_THRESH = 0.9
    NMS_THRESH = 0.3
    vis_detections_result = []
    for cls_ind, cls in enumerate(CLASSES[1:]):
        cls_ind += 1 # because we skipped background
        cls_boxes = boxes[:, 4*cls_ind:4*(cls_ind + 1)]
        cls_scores = scores[:, cls_ind]
        dets = np.hstack((cls_boxes, cls_scores[:, np.newaxis])).astype(np.float32)
        keep = nms(dets, NMS_THRESH)
        dets = dets[keep, :]
        vis_detections_result.append(vis_detections(im, cls, dets, image_dir, image_name, thresh=CONF_THRESH))
    print "visulize time: " + str(time.time() - start_time)
    print vis_detections_result
    return vis_detections_result
# --------------------------------------------------------






#User class
class User(UserMixin):

	def __init__(self, id, username):
		self.id = id
		self.username = username

	def __repr__(self):
		return "%s" % (self.username)

	def getId(self):
		return self.id

def checkUsername(Username):
	global cur
	global cur_images
	try:
		cur.execute("SELECT * from users WHERE username=%s;" , [Username])
	except Exception,e:
		print "error in checkusername"
		print e
		cur, cur_images = connect_database()
		cur.execute("SELECT * from users WHERE username=%s;" , [Username])
	row = cur.fetchall()
	return row

#check user existence
def checkUserStatus(userid):
	global cur
	global cur_images
	try:
		cur.execute("SELECT failuretime from userstatus WHERE id=%s;" , [userid])
	except Exception,e:
		print "error in checkusername"
		print e
		cur, cur_images = connect_database()
		cur.execute("SELECT failuretime from userstatus WHERE id=%s;" , [userid])
	row = cur.fetchall()
	return row

#check if blocked
def addFailuretime(userid, failtimes):
	global cur
	global cur_images
	try:
		cur.execute("UPDATE userstatus SET failuretime=failuretime+1 WHERE id=%s;", [userid])
	except Exception,e:
		print "error in increase failuretime"
		print e
		cur, cur_images = connect_database()
		cur.execute("UPDATE userstatus SET failuretime=failuretime+1 WHERE id=%s;", [userid])
	return "failtimes increased"

#encode password
def encrypt_password(password, salt=None):
	# create salt
	if salt is None:
		salt = os.urandom(8)

	assert 8 == len(salt)
	assert isinstance(salt, str)

	if isinstance(password, unicode):
		password = password.encode('UTF-8')

	assert isinstance(password, str)

	result = password
	for i in xrange(10):
		result = HMAC(result, salt, sha256).digest()

	return salt+result

def validate_password(hashed, input_password):
	return hashed == encrypt_password(input_password, salt=hashed[:8])

def resetFailuretime(userid):
	global cur
	global cur_images
	try:
		cur.execute("UPDATE userstatus SET failuretime=0 WHERE id=%s;", [userid])
	except Exception,e:
		print "error in failuretime reset"
		print e
		cur, cur_images = connect_database()
		cur.execute("UPDATE userstatus SET failuretime=0 WHERE id=%s;", [userid])
	return "failtimes reset"

#login page support
@app.route("/login",methods=["GET","POST"])
def login():
	global acceptfailurelogintime
	error = None
	if request.method == 'POST':
		#print "POST Login"
		Username = request.form['Username']
		Password = request.form['Password']
		Rememberme = request.form.getlist("Rememberme")
		row = checkUsername(Username)
		if (len(row) == 0):
			print "Incorrect Username or Password"
			flash("Incorrect Username or Password")
			return render_template("login.html")
		else:
			getid = row[0][0]
			getusername = row[0][1]
			getpassword = row[0][2]
			getpassword.rstrip()
			getpassword = str(base64.b64decode(getpassword))
			statusrow = checkUserStatus(getid)
			failtimes = statusrow[0][0]
			if (failtimes>=acceptfailurelogintime):
				print "Blocked User"
				flash("Blocked User")
				return render_template("login.html")
			elif (validate_password(getpassword, Password)):
				print "correct login"
				user = User(getid, Username)
				resetFailuretime(getid)
				if (len(Rememberme)==0):
					login_user(user)
				else:
					login_user(user, remember=True)
				return redirect(url_for("dashboard"))
			else:
				print "Incorrect Username or Password"
				flash("Incorrect Username or Password")
				addFailuretime(getid,failtimes)
				return render_template("login.html")
	else:
		#print "GET Login"
		return render_template("login.html")

# callback to reload the user object
@login_manager.user_loader
def load_user(userid):
	global cur
	global cur_images
	#print userid
	try:
		cur.execute("SELECT id, username from users WHERE id=%s;" , [userid])
	except Exception,e:
		print "error in login"
		print e
		cur, cur_images = connect_database()
		cur.execute("SELECT id, username from users WHERE id=%s;" , [userid])
	row = cur.fetchall()
	username = row[0][1]
	nowuserid = row[0][0]
	nowuser = User(userid, username)
	if (unicode(nowuserid) == userid):
		#print 'load user'
		return nowuser
	else:
		#print 'no load'
		return None

if __name__ == "__main__":	
	app.run(host='0.0.0.0',port=5004)
