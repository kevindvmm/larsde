The app.py is the server of of visualization tool with automatic car detection running on port number 5001.
The _init_paths.py defines the dependency of automatic car detection.
To make the app.py run correctly, the path defined in the _init_paths.py should be adjusted according to the path the lib is placed and create a 'viz' folder under the folder 'static' in 'website', which can be changed in the app.py

machine:
pomelo.cs.columbia.edu
athena.cs.columbia.edu
yael.cs.columbia.edu