////////// MAIN
//Define the front end function of map, street cameras and social media

// Global Variables
var gCamID=0;
var gStartTime = new Date('2015/12/05 08:00:00');
var gEndTime = new Date('2015/12/05 14:00:00');
var gStartDate = new Date('2015/12/05 00:00:00');
var gEndDate = new Date('2015/12/20 00:00:00');
var mainStartDT = new Date('2016/1/10 00:00:00');
var mainEndDT = new Date('2016/1/30 00:00:00');
var vcurtime  = mainStartDT;
var imagestep = 60 * 60 * 1000;
var deltaT = 1000;
var timedomain = [new Date('2015/12/1 00:00:00'),new Date('2016/2/3 00:00:00')];
var latest_bounding_box = [	{"lat": 40.697052, "lng": -74.025939}, 
							{"lat": 40.881874, "lng": -74.025939}, 
							{"lat": 40.881874, "lng": -73.905072}, 
							{"lat": 40.697052, "lng": -73.905072} ];
var drawedbox = false;

var lastTimeout;
var time_updated_for_images = false;

var alerttype = "";
var alertsLOC = [];

var imageOn = false;
var play = false;
document.getElementById("Playpause").src='static/img/play.png';
var markers = new Array();
var cams = [];

var heatmap_switched = false;
var heatType = "car";
var checkbox = document.getElementById("heatmap_on");
var alltweets = [];
var norm_den = 24.0;
var norm_req = false;

var isloading = true;

var handle;

var vx;
var vxaxis;
var vxaxis;
var handle;
var vbrush;

var margin = {top: 0, right: 10, bottom: 0, left: 10};
var height = 30;
var width = parseInt(d3.select("#slider").style("width"), 10)-margin.left - margin.right;

var wazenumber = 0;

var xfilter, all, date, hour, datesGroup, hoursGroup;
var formatNumber, formatChange, formatDate, formatTime;
var nestByDate;
var charts;
var chart;

var findimageflag=false;

var datatype;
var showcircles = new Array();
test_text = "";
var toggleObjectDetection = false;
var toggleObjectTracking = false;

//event set
var event_set=[];

//**** detected event set
var detected_event_set={};
//dot and heat map definition

var wt_dotLayer = L.layerGroup();
var wt_switched = false;
var wt_heatLayer = new L.TileLayer.HeatCanvas({},{'step':1,
'degree':HeatCanvas.LINEAR, 'opacity':0.7});
var typeorsub = true;

var multiload=0;

//set page according to browser size
var bodywidth=document.getElementById("maincontainer").clientWidth-55;
//console.log(bodywidth);
windowresize(bodywidth);

window.addEventListener("resize", getnewwindowwidth);

// Map Module

var map = L.map('map').setView([40.7841484, -73.9661407], 12);
var tile = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {
  maxZoom: 18,
  attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
    '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="http://mapbox.com">Mapbox</a>',
  id: 'mapbox.streets'
}).addTo(map);

var drawnItems = new L.FeatureGroup();
map.addLayer(drawnItems);
      
var drawControl = new L.Control.Draw({
    position: 'topleft',
    draw:{
        rectangle: {
            shapeOptions: {
                color: '#f357a1',
                weight: 1
            }
        }
    },
    edit: {
        featureGroup: drawnItems,
        edit: false,
        remove: false,
    }
});


map.addControl(drawControl);
map.on('draw:created', function (e) {
    latest_bounding_box = e.layer.getLatLngs();
    //console.log (latest_bounding_box);
    if (!drawedbox) {
      $("#wordoff").parent().button('toggle');
      $("#wordoff").checked=false;
      $("#wordon").parent().button('toggle');
      $("#wordon").checked=true;
      drawedbox = true;
    }
    update_wordcloud ();
});

// scetch camera icons

var cam = L.icon({
    iconUrl: 'static/img/video_camera_black.png',//black
    shadowUrl: 'static/img/video_camera_grey.png',

    iconSize:     [22, 22], // size of the icon
    shadowSize:   [24, 24], // size of the shadow
    iconAnchor:   [12, 12], // point of the icon which will correspond to marker's location
    shadowAnchor: [13, 13],  // the same for the shadow
    popupAnchor:  [-3, -10] // point from which the popup should open relative to the iconAnchor
});
var camNoFacing = L.icon({
    iconUrl: 'static/img/Flat-Camera-Black.png',//black
    shadowUrl: 'static/img/Flat-Camera-Grey.png',

    iconSize:     [18, 18], // size of the icon
    shadowSize:   [17, 16], // size of the shadow
    iconAnchor:   [9, 9], // point of the icon which will correspond to marker's location
    shadowAnchor: [8.5, 8],  // the same for the shadow
    popupAnchor:  [-3, -10] // point from which the popup should open relative to the iconAnchor
});

var camLayer = L.layerGroup().addTo(map);

$.get( "/camcoords",{}, function( data, status ) {
  // console.log(data.coords[0])
  for (i=0; i<data.coords.length; i++){
    cams.push ({camera: data.coords[i][0], lat: data.coords[i][1], log: data.coords[i][2], facing: data.coords[i][3], id: data.coords[i][4]});
  }
  // console.log(cams[0])
  for(var i = 0; i<cams.length; ++i){
    if(cams[i].facing != '\r'){
          var marker = L.marker([cams[i].lat,cams[i].log], {icon: cam},{title: cams[i].camera}).addTo(map);
      if(cams[i].facing == 'East\r'){
        marker.setIconAngle(29);
      }else if(cams[i].facing == 'West\r'){
        marker.setIconAngle(209);
      }else if(cams[i].facing == 'North\r'){
        marker.setIconAngle(299);
      }else if(cams[i].facing == 'South\r'){
        marker.setIconAngle(119);
      }
    }else{
          var marker = L.marker([cams[i].lat,cams[i].log], {icon: camNoFacing},{title: cams[i].camera}).addTo(map);

    }

    window.markers.push(marker);
    camLayer.addLayer(marker);
    window.markers[i].bindPopup(cams[i].camera);//originally width=\"352\" height=\"240\" name = "myCam"
    window.markers[i].camid = cams[i].id;

    window.markers[i].on('click',function(e){
      if (!(imageOn&&play)){
        imageOn = true;
        play = true;
        document.getElementById("Playpause").src='static/img/pause.png';
        gCamID = e.target.camid;

        // console.log(e);
        lastTimeout = setTimeout(refreshImage,deltaT,gCamID,mainStartDT,mainEndDT,mainStartDT);
      }
      else {
        if (gCamID == e.target.camid){
          play = false;
          document.getElementById("Playpause").src='static/img/play.png';
          try{
            clearTimeout (lastTimeout);
          }
          catch(err){
            console.log (err);
          }
        }
        else {
          gCamID = e.target.camid;
          time_updated_for_images = true;
        }
      }      

      /*console.log(gStartTime);
      console.log(gEndTime);*/
    });
  }
});

//******** TempVariable for testing
var testGlobalEventData;
var types = ["ACCIDENT","HAZARD_CAR_STOPPED","ROAD_CLOSED", "CONSTRUCTION","POLICE","PARTIAL_CLOSURE"];
// standard types: "ACCIDENT" "CAR_STOPPED" "ROAD_CLOSED" "CONSTRUCTION" "POLICE" 
var tags = ["waze","waze_untrimmed"];
//load events
$.get("/load_event",{}, function( data, status ){
  //console.log(data.coords[0]);
  testGlobalEventData = data;
  for (i=0; i<data.coords.length; i++){
    event_set.push ({id: data.coords[i][0], description: data.coords[i][1], camera: data.coords[i][2], starttime: data.coords[i][3], endtime: data.coords[i][4], tag: data.coords[i][5]});
  }
  for (var i = 0; i<event_set.length; ++i){
    tempdate = Date.parse(event_set[i].starttime);
    newstarttime = new Date(tempdate+18000000).toLocaleString();
    tempdate = Date.parse(event_set[i].endtime);
    newendtime = new Date(tempdate+18000000).toLocaleString();
    newline = "<tr class='clickable-row' style='word-warp: break-word; white-space: -moz-pre-wrap; white-space: pre-wrap; cursor: pointer;' onclick='setloadedevent("+i.toString()+",false)'><td>"+event_set[i].id.toString()+"</td><td>"+event_set[i].description+"</td><td>"+event_set[i].camera.toString()+"</td><td>"+newstarttime+"</td><td>"+newendtime+"</td><td>"+event_set[i].tag+"</tr>"
    $("#load_event_list").append(newline);
  }
  //********** For event detection
  load_detection_results();
});


//////////////////hide layer when change tab
$('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
  var currentTab = $(e.target).text(); // get current tab
  if (currentTab==="Social Media"){
    if(map.hasLayer(camLayer)){
      map.removeLayer(camLayer);
      map.addLayer(wt_dotLayer);
      console.log("change to social");
    }
  }
  else if (currentTab==="Street Cameras"){
    if(!map.hasLayer(camLayer)){
      map.addLayer(camLayer);
      console.log("change to camera");
    }
  } 
});

////////////////// dot map and heat map Module

$('#radioHeat').on('change', function () {
  wt_switched = true;
  if(map.hasLayer(wt_dotLayer)){
    map.removeLayer(wt_dotLayer);
  }
  if(!map.hasLayer(wt_heatLayer)){
    map.addLayer(wt_heatLayer);
  }
  console.log("show heat-map");
});
$('#radioDot').on('change', function () {
  wt_switched = true;
  if(map.hasLayer(wt_heatLayer)){
    map.removeLayer(wt_heatLayer);
  }
  if(!map.hasLayer(wt_dotLayer)){
    map.addLayer(wt_dotLayer);
  }
  console.log("show dot-map");
});

// On and Off Twitter Word Cloud Update
$('#wordoff').on('change', function () {
  drawedbox = false;
  console.log("Twitter Word Cloud Off");
});
$('#wordon').on('change', function () {
  drawedbox = true;
  console.log("Twitter Word Cloud On");
});

// jams dot map
var jam_checked = [false,false,false,false,false];
var jam_color = ["#C5EB17","#FDE101","#FD4601","#CB0116","#680200"];
var jam_drawn = false;
var jamLayer=[];
for(var i = 0; i<5; i++){
  jamLayer.push(L.layerGroup());
}

//Adding the dotLayer of detected events
var detected_dotLayers = {};
var curEventType = "";
var curEventLayer = L.layerGroup();
var curEventTag = "";

var baseMaps = {};
var overlayMaps ={
  "Cams":camLayer,
  "Dotmap":wt_dotLayer,
  "Heatmap":wt_heatLayer,
  "EventMap":curEventLayer
};
L.control.layers(baseMaps,overlayMaps).addTo(map);


// time selection plot Module
$.get( "/alltweets",{}, function( data, status ) {
  // console.log("all tweets");
  // console.log(data.tweets[0]);
  for (i=0; i<data.tweets.length; i++){
    //console.log(data.tweets[i][0]);
    alltweets.push ({index: i, date: new Date(data.tweets[i][0])});
  }
// Create the crossfilter for the relevant dimensions and groups.
  xfilter= crossfilter(alltweets);
  all = xfilter.groupAll();
  date = xfilter.dimension(function(d) { return d.date; });
  hour = xfilter.dimension(function(d) { return d.date.getHours() + d.date.getMinutes() / 60; });

  datesGroup = date.group(d3.time.day).reduceSum (function(d) { return 1; });
  hoursGroup = hour.group(Math.floor).reduceSum (function(d) { return 1; });

  // d3 stuff
    // Various formatters.
  formatNumber = d3.format(",d");
  formatChange = d3.format("+,d");
  formatDate = d3.time.format("%B %d, %Y");//%B - full month name.%d - zero-padded day of the month as a decimal number [01,31].%Y - year with century as a decimal number.
  formatTime = d3.time.format("%I:%M %p");//%I - hour (12-hour clock) as a decimal number [01,12].%M - minute as a decimal number [00,59].%p - either AM or PM.

    // A nest operator, for grouping the flight list.
  nestByDate = d3.nest()
    .key(function(d) { return d3.time.day(d.date); });

  charts = [
      barChart()
          .dimension(hour)
          .group(hoursGroup)
        .x(d3.scale.linear()
          .domain([0, 24])
          .rangeRound([0, 10 * 24]))
        .filter([8, 14]),

      barChart()
          .dimension(date)
          .group(datesGroup)
        .x(d3.time.scale()
          .domain(timedomain)
          .rangeRound([0, 22*30]))
          .filter([new Date(2015, 11, 5), new Date(2015, 11, 20)])
   ];

    // Given our array of charts, which we assume are in the same order as the
    // .chart elements in the DOM, bind the charts to the DOM and render them.
    // We also listen to the chart's brush events to update the display.
  chart = d3.selectAll([document.getElementById("hour-chart"), document.getElementById("date-chart")])
    .data(charts)
    .each(function(chart) { chart.on("brush", renderAll).on("brushend", renderAll); });

  renderAll();
});


// Video Playback Module

vspeed1 = d3.select('#vspeed1')
            .call(d3.slider()
                    .orientation("vertical")
                    .scale(d3.scale.linear()
                      .domain([1,10]))// 0.5,5 speed range
                    .value(1)
                    .axis(d3.svg.axis()
                      .orient("left")
                      .ticks(11)//11
                      .tickFormat(function(d){return 11-d;} ))
                    .on("slide",function(evt, value){
                      deltaT = 1000/value;
                    })
                  );

//box for mouse position
var div_tooltip = d3.select("body").append("div")
                    .style("z-index", "10")	
                    .attr("class", "tooltip")	
                    .style("position", "absolute")
                    .style("visibility", "hidden")	
                    .text("tooltip");	

initialize_slider();

vslider = d3.select("#slider");
draw_vslider();


//the datetime picker
$(function () {
    //console.log("set date");
    $('#datetimepicker1').datetimepicker({
        useCurrent: false, //Important! See issue #1075
        defaultDate: mainStartDT,
        useSeconds: true,
        format: "MM/DD/YYYY HH:mm:ss"
//        sideBySide: true,
        // format: "MM/DD/YYYY"
    });
    $('#datetimepicker2').datetimepicker({
        
        defaultDate: mainEndDT,
        useSeconds: true,
        format: "MM/DD/YYYY HH:mm:ss"
        // format: "MM/DD/YYYY"//ddd for day
    });
    $("#datetimepicker1").on("dp.change", function (e) {
          //console.log("change in min 1");
        $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
    });
    $("#datetimepicker2").on("dp.change", function (e) {
          //console.log("change in min 2");
        $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
    });
});

$('#datetimepicker1').on("dp.show",function(e){
    $(document).find('.picker-switch a[data-action="togglePicker"]').click();
});
$('#datetimepicker2').on("dp.show",function(e){
    $(document).find('.picker-switch a[data-action="togglePicker"]').click();
});

$('#datetimepicker1').on("dp.change",function(e){
  //console.log("change in 1");
  mainStartDT = e.date._d;
  mainDTChanged();
});
$('#datetimepicker2').on("dp.change",function(e){
  //console.log("change in 2");
  mainEndDT = e.date._d;
  mainDTChanged();
});

//add active to list group item
$(function(){
    console.log('page is ready');
    $("#load_event_list tr").click(function(e) {
        e.preventDefault()
        console.log('event clicked');
        $that = $(this);

        $that.parent().find('tr').removeClass('success');
        $that.addClass('success');
    });
});



///////// FUNCTIONS

function getnewwindowwidth(){
  bodywidth=document.getElementById("maincontainer").clientWidth-55;
  //console.log(bodywidth);
  windowresize(bodywidth);
}

function windowresize(bodywidth){
  console.log("start zoom");
  var newmapcolwidth=bodywidth-710;
  if (newmapcolwidth<=370){
    return;
  }
  else {
    newmapwidth=newmapcolwidth-10;
    newmapheight=Math.round(newmapwidth/5*6);
    newmappanelheight=newmapheight+40;
    if (newmapwidth>600) {
      newmapwidth=600;
      newmapheight=720;
      newmappanelheight=newmapheight+40;
      newmapcolwidth=610;
    }
    document.getElementById("mapcol").style.width= newmapcolwidth.toString()+"px";
    document.getElementById("mappanel").style.height= newmappanelheight.toString()+"px";
    document.getElementById("map").style.width= newmapwidth.toString()+"px";
    document.getElementById("map").style.height= newmapheight.toString()+"px";
  }
}

function mainDTChanged(){
  time_updated_for_images = true;
  console.log("time chagned")
  d3.select("#slider svg").remove();
  initialize_slider();
  draw_vslider();
  changetimebrush();
}

//show and hide loading mask
function showMask(){
  $("#popDiv").css("display","block");
  $("#loading").css("display","block");
}

function hideMask(){
  $("#popDiv").css("display","none");
  $("#loading").css("display","none");
}

//special video mask
function showvideoMask(){
  $("#videopopDiv").css("display","block");
}

function hidevideoMask(){
  $("#videopopDiv").css("display","none");
}

//change to event camera and time
function setloadedevent(index ,isDetectedEvent){/** When isDetectedEvent = true, index is a event object*/
  var start;
  var end;
  document.getElementById("myPic").src='';
  findimageflag = false;
  console.log('event clicked');
  $("#load_event_list tr").removeClass("success");
  $(this).addClass('success');

  if(isDetectedEvent){
    var singleEvent = index;
    start = singleEvent.starttime;
    end = singleEvent.endtime;
    gCamID = singleEvent.camera;
  }else{//
    gCamID = event_set[index].camera;
    for(var i = 0; i<cams.length; ++i){
      if (cams[i].id===gCamID) {
        window.markers[i].openPopup();//THis is opening the camera popup
      }
    }
    start= event_set[index].starttime;
    end = event_set[index].endtime;
  }
    tempdate = Date.parse(start);
    mainStartDT = new Date(tempdate+18000000);
    tempdate = Date.parse(end);
    mainEndDT = new Date(tempdate+18000000);
    /****************************************  why 18000000  *****************************************/
    //console.log(mainStartDT);
    //console.log(mainEndDT);
  //  $('#datetimepicker1').data("DateTimePicker").maxDate(moment(mainEndDT));
  //  $('#datetimepicker2').data("DateTimePicker").minDate(moment(mainStartDT));
    $('#datetimepicker1').data('DateTimePicker').date(moment(mainStartDT));
    $('#datetimepicker2').data('DateTimePicker').date(moment(mainEndDT));
    $('#datetimepicker1').data('DateTimePicker').date(moment(mainStartDT));

    imageOn = true;
    if(play){
      play = false;
      document.getElementById("Playpause").src='static/img/play.png';
      try{
        clearTimeout (lastTimeout);
      }
      catch(err){
        console.log (err);
      }
    }
    vcurtime = mainStartDT;
    forward();

}

//rewind button function
function rewind(){
  console.log('rewind');
  findimageflag = false;
  if (imageOn){
    console.log('To find image');
    if(play){
      play = false;
      document.getElementById("Playpause").src='static/img/play.png';
      try{
        clearTimeout (lastTimeout);
      }
      catch(err){
        console.log (err);
      }      
        console.log('To find image');
        var nexttime = new Date(vcurtime.valueOf()-imagestep);
        strcurtime = vcurtime.toLocaleString();
        strstarttime = mainStartDT.toLocaleString();
        strendtime = mainEndDT.toLocaleString();
        strnexttime = nexttime.toLocaleString();

        //test if it is the last image in the time peried
        if(mainStartDT.getTime() > nexttime.getTime() ){
           $.get( "/images",{camid:gCamID, starttime: strstarttime,endtime: strcurtime}, function( data, status ) {
            // data = JSON.parse(data);
            if(data.byteImage.length>100){
              findimageflag = true;
              console.log('find image');
              // document.getElementById("myPic").src='data:image/jpeg;base64, '+data+'';              
              canvasLoadImage("myPic", 'data:image/jpeg;base64, '+data.byteImage+'', data);
            }            
            handle.attr("cx", vx(vcurtime));
            vcurtime = mainEndDT;
            var currentDiv = document.getElementById("cursorposition");
            currentDiv.innerHTML = "Date: " + vcurtime.toString();
            if (!findimageflag) {
              rewind();
            }
            hidevideoMask();
           });
           showvideoMask();           
        }else{
           $.get( "/images",{camid:gCamID,starttime:strnexttime ,endtime:strcurtime}, function( data, status ) {
              // data = JSON.parse(data);
              if(data.byteImage.length>100){
                findimageflag = true;
                console.log('find image');
                // document.getElementById("myPic").src='data:image/jpeg;base64, '+data+'';
                canvasLoadImage("myPic", 'data:image/jpeg;base64, '+data.byteImage+'', data);
              }
              handle.attr("cx", vx(vcurtime));
              var currentDiv = document.getElementById("cursorposition");
              currentDiv.innerHTML = "Date: " + vcurtime.toString();
              vcurtime = nexttime;
              if (!findimageflag) {
               rewind();
              }
              hidevideoMask();              
           });
           showvideoMask();
         }
        }else{
        console.log('To find image');
        var nexttime = new Date(vcurtime.valueOf()-imagestep);
        strcurtime = vcurtime.toLocaleString();
        strstarttime = mainStartDT.toLocaleString();
        strendtime = mainEndDT.toLocaleString();
        strnexttime = nexttime.toLocaleString();

        //test if it is the last image in the time peried
        if(mainStartDT.getTime() > nexttime.getTime() ){
           $.get( "/images",{camid:gCamID, starttime: strstarttime,endtime: strcurtime}, function( data, status ) {
            // data = JSON.parse(data);
            if(data.byteImage.length>100){
              findimageflag = true;
              console.log('find image');
              // document.getElementById("myPic").src='data:image/jpeg;base64, '+data+'';
              canvasLoadImage("myPic", 'data:image/jpeg;base64, '+data.byteImage+'', data);
            }
            handle.attr("cx", vx(vcurtime));
            var currentDiv = document.getElementById("cursorposition");
            currentDiv.innerHTML = "Date: " + vcurtime.toString();
            vcurtime = mainEndDT;
            if (!findimageflag) {
              rewind();
            }
            hidevideoMask();
           });
           showvideoMask();           
        }else{
           $.get( "/images",{camid:gCamID,starttime:strnexttime ,endtime:strcurtime}, function( data, status ) {
              // data = JSON.parse(data);
              if(data.byteImage.length>100){
                findimageflag = true;
                console.log('find image');
                // document.getElementById("myPic").src='data:image/jpeg;base64, '+data+'';
                canvasLoadImage("myPic", 'data:image/jpeg;base64, '+data.byteImage+'', data);
              }
              handle.attr("cx", vx(vcurtime));
              var currentDiv = document.getElementById("cursorposition");
              currentDiv.innerHTML = "Date: " + vcurtime.toString();
              vcurtime = nexttime;
              if (!findimageflag) {
                rewind();
              }
              hidevideoMask();
           });
           showvideoMask(); 
         }
       }
  }
}

//foward button function
function forward(){
  console.log('forward');
  findimageflag = false;
  if (imageOn){
    if(play){
      play = false;
      document.getElementById("Playpause").src='static/img/play.png';
      try{
        clearTimeout (lastTimeout);
      }
      catch(err){
        console.log (err);
      }
      var nexttime = new Date(vcurtime.valueOf()+imagestep);
      strcurtime = vcurtime.toLocaleString();
      strstarttime = mainStartDT.toLocaleString();
      strendtime = mainEndDT.toLocaleString();
      strnexttime = nexttime.toLocaleString();

      //test if it is the last image in the time peried
      if(mainEndDT.getTime() < nexttime.getTime() ){
         $.get( "/images",{camid:gCamID, starttime: strcurtime,endtime: strendtime}, function( data, status ) {
          // data = JSON.parse(data);
          if(data.byteImage.length>100){
            findimageflag = true;
            // document.getElementById("myPic").src='data:image/jpeg;base64, '+data+'';
            canvasLoadImage("myPic", 'data:image/jpeg;base64, '+data.byteImage+'', data);
          }
          handle.attr("cx", vx(vcurtime));
          var currentDiv = document.getElementById("cursorposition");
          currentDiv.innerHTML = "Date: " + vcurtime.toString();
          vcurtime = mainStartDT;
          if (!findimageflag) {
            forward();
          }
          hidevideoMask();
         });
         showvideoMask(); 
      }else{
         $.get( "/images",{camid:gCamID,starttime:strcurtime ,endtime:strnexttime}, function( data, status ) {
            // data = JSON.parse(data);
            if(data.byteImage.length>100){
              findimageflag = true;
              // document.getElementById("myPic").src='data:image/jpeg;base64, '+data+'';
              canvasLoadImage("myPic", 'data:image/jpeg;base64, '+data.byteImage+'', data);
            }
            handle.attr("cx", vx(vcurtime));
            var currentDiv = document.getElementById("cursorposition");
            currentDiv.innerHTML = "Date: " + vcurtime.toString();
            vcurtime = nexttime;
            if (!findimageflag) {
              forward();
            }
            hidevideoMask();
         });
         showvideoMask();  
       }
      }else{
      var nexttime = new Date(vcurtime.valueOf()+imagestep);
      strcurtime = vcurtime.toLocaleString();
      strstarttime = mainStartDT.toLocaleString();
      strendtime = mainEndDT.toLocaleString();
      strnexttime = nexttime.toLocaleString();

      //test if it is the last image in the time peried
      if(mainEndDT.getTime() < nexttime.getTime() ){
         $.get( "/images",{camid:gCamID, starttime: strcurtime,endtime: strendtime}, function( data, status ) {
          // data = JSON.parse(data);
          if(data.byteImage.length>100){
            findimageflag = true;
            // document.getElementById("myPic").src='data:image/jpeg;base64, '+data+'';
            canvasLoadImage("myPic", 'data:image/jpeg;base64, '+data.byteImage+'', data);
          }
          handle.attr("cx", vx(vcurtime));
          var currentDiv = document.getElementById("cursorposition");
          currentDiv.innerHTML = "Date: " + vcurtime.toString();
          vcurtime = mainStartDT;
          if (!findimageflag) {
            forward();
          }
          hidevideoMask(); 
         });
         showvideoMask();  
      }else{
         $.get( "/images",{camid:gCamID,starttime:strcurtime ,endtime:strnexttime}, function( data, status ) {
            // data = JSON.parse(data);
            if(data.byteImage.length>100){
              findimageflag = true;
              // document.getElementById("myPic").src='data:image/jpeg;base64, '+data+'';
              canvasLoadImage("myPic", 'data:image/jpeg;base64, '+data.byteImage+'', data);
            }
            handle.attr("cx", vx(vcurtime));
            var currentDiv = document.getElementById("cursorposition");
            currentDiv.innerHTML = "Date: " + vcurtime.toString();
            vcurtime = nexttime;
            if (!findimageflag) {
              forward();
            }
            hidevideoMask();
         });
         showvideoMask();   
       }
    }
  }
}

function mousemove() { 
  var x_cor = d3.mouse(this)[0];                              // **********
  var y_cor = d3.mouse(this)[1];
  var x0 = vx.invert(d3.mouse(this)[0]);              // **********
  div_tooltip.style("opacity", .9);		
  div_tooltip.style("visibility", "visible")
             .style("top", (d3.event.pageY-20)+"px")
             .style("left",(d3.event.pageX-160)+"px");  
  div_tooltip.text("Date: " + x0.toString());
}

function draw_vslider(){
  //console.log(margin);
  svg = vslider.append("svg")
               .attr("width",width + margin.left + margin.right)
               .attr("height", height + margin.top + margin.bottom)
               .append("g")
                 .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  svg.append("g")
     .attr("class", "vaxis")
     .attr("transform","translate(0,"+height/2+")")
     .call(vxaxis)      
     .style("pointer-events", "all") 
       .select(".domain")
       .select(function() { return this.parentNode.appendChild(this.cloneNode(true)); })
         .attr("class", "halo");

  vbrush = svg.append("g")//originally slider
              .attr("class","vbrush")
              .call(brush)
              .on("mouseover", function(){return div_tooltip.style("visibility", "visible");})
              .on("mouseout", function() {return div_tooltip.style("visibility", "hidden");})
              .on("mousemove", mousemove)                             // **********
              .on("mousedown", brushed);

  vbrush.selectAll(".extent,.resize")
        .remove();
  vbrush.select(".background")
        .attr("height", height);

  handle = vbrush.append("circle")
                 .attr("class", "vhandle")
                 .attr("transform", "translate(0," + height / 2 + ")")
                 .attr("r",5);
}

function brushed() {
  var value = brush.extent()[0];
  // vcurtime = value;
  if (d3.event.sourceEvent) { // not a programmatic event
    value = vx.invert(d3.mouse(this)[0]);
    brush.extent([value, value]);
    vcurtime = value;
    if(play){
          clearTimeout(lastTimeout);
          lastTimeout = setTimeout(refreshImage,deltaT,gCamID,mainStartDT,mainEndDT,vcurtime);
    }else{
      refreshImage(gCamID,mainStartDT,mainEndDT,vcurtime,true);
      clearTimeout(lastTimeout);
    }
  }
  
  handle.attr("cx", vx(value));
  var currentDiv = document.getElementById("cursorposition");
  currentDiv.innerHTML = "Date: " + value.toString();
}

//set step size
function step_size(size){
  console.log(size);
  imagestep=size;
}

function initialize_slider(){
  //console.log("initialize_slider");
  vx = d3.time.scale()
      .domain([mainStartDT,mainEndDT])
      .range([0,width])
      .clamp(true);
  brush = d3.svg.brush()
            .x(vx)
            .extent([0,0])
            .on("brush",brushed);
  vxaxis = d3.svg.axis()
                .orient("bottom")
                .scale(vx)
                .ticks(d3.time.days,1)
                .tickSize(0)
                .tickPadding(height/4)
                .tickFormat("");
}

function createEvent2(){
  eventname = document.getElementById('eventname').value;
  sT = mainStartDT.toLocaleString();
  eT = mainEndDT.toLocaleString();
  console.log(sT);
  console.log(eT);
  if ((gCamID!=0) && (eventname.length!=0)){
    $.post('/event',{
      description:eventname,
      cameraid:gCamID,
      startTime:sT,
      endTime:eT,
    }, function(data, status) {
        hideMask();
    });
    showMask();}
  else{
    console.log("invalid event");
  }
}

function update_wordcloud()
{
    var sD = gStartDate.toLocaleDateString();
    var eD = gEndDate.toLocaleDateString();
    var sT = gStartTime.toLocaleTimeString();
    var eT = gEndTime.toLocaleTimeString();
    $.post('/wordCloud',{
      coord:JSON.stringify(latest_bounding_box),
      startDate: sD,
      endDate: eD,
      startTime: sT,
      endTime: eT
    }, function(data, status) {
      document.getElementById('word_cloud').src="static/img/wordcloud.png?" + new Date().getTime();
      if (multiload===0){
        hideMask();
      }
      else{
        multiload--;
      }
      //console.log(multiload);
    });
    showMask();
}


function playpause(){
  console.log('playpause(): to ' +(play?"pause":"play"));
  if (imageOn){
    if(play){
      play = false;
      document.getElementById("Playpause").src='static/img/play.png';
      update_backend_user_play_status(play);
      try{
        clearTimeout (lastTimeout);
      }
      catch(err){
        console.log (err);
      }
    }else{
      play = true;
      document.getElementById("Playpause").src='static/img/pause.png';
      update_backend_user_play_status(play);
      lastTimeout = setTimeout(refreshImage,deltaT,gCamID,mainStartDT,mainEndDT,vcurtime);
    }
  }
}

function refreshImage(cID,starttime,endtime,curtime,isvbrush=false) {//the times are Date object

  var nexttime = new Date(curtime.valueOf()+imagestep);
  strcurtime = vcurtime.toLocaleString();
  strstarttime = starttime.toLocaleString();
  strendtime = endtime.toLocaleString();
  strnexttime = nexttime.toLocaleString();
  d = (new Date()).getTime();

  //test if it is the last image in the time peried
  if(endtime.getTime() < nexttime.getTime() ){
    $.get( "/images",{camid:cID, starttime: strcurtime,endtime: strendtime}, function( data, status ) {//
      // data = JSON.parse(data);
      //console.log(data);
      console.log("diff: " + ((new Date()).getTime() - d));
      if(data.byteImage.length>100&&play){
        // document.getElementById("myPic").src='data:image/jpeg;base64, '+data+'';
        canvasLoadImage("myPic", 'data:image/jpeg;base64, '+data.byteImage+'', data);
      }
    });
    vcurtime = starttime;
  }else{
    $.get( "/images",{camid:cID,starttime:strcurtime ,endtime:strnexttime}, function( data, status ) {//   
      // data = JSON.parse(data);
      console.log(data);
      console.log("diff: " + ((new Date()).getTime() - d));
      if(data.byteImage.length>100&&play){
        // document.getElementById("myPic").src='data:image/jpeg;base64, '+data+'';
        canvasLoadImage("myPic", 'data:image/jpeg;base64, '+data.byteImage+'', data);
      }
    });
    vcurtime = nexttime;
  }
if(!isvbrush){
  vbrush
        .call(brush.event)
      .transition()
        .duration(deltaT)
        .ease("linear")
        .call(brush.extent([vcurtime,vcurtime]))//same as vcurtime
        .call(brush.event);
}
  if (time_updated_for_images){
    lastTimeout = setTimeout(refreshImage,deltaT,gCamID,mainStartDT,mainEndDT,mainStartDT);
    time_updated_for_images = false;
  }
  else
    lastTimeout = setTimeout(refreshImage,deltaT,cID,starttime,endtime,vcurtime);
}

function waze_twitter(type_sub, change, name){
  if (change){
    datatype = name;
    typeorsub = type_sub
    var alertBtn = document.getElementById("alertBtn");
    alertBtn.innerHTML=name+"<span class='caret'></span>";
    if(name === 'Alert Type'){//meaning reset
      alertBtn.innerHTML="Please Select and Alert Type"+"<span class='caret'></span>";
      var radios = document.getElementById("radioDot");
      var keywordtype = document.getElementById("tweets_keywords");
      wt_heatLayer.data=[];
      if(map.hasLayer(wt_heatLayer)){
        map.removeLayer(wt_heatLayer);
        $("#radioHeat").parent().button('toggle');
        $("#radioHeat").checked=false;
      }
      wt_dotLayer.clearLayers();
      if(!map.hasLayer(wt_dotLayer)){
        map.addLayer(wt_dotLayer);
        $("#radioDot").parent().button('toggle');
        radios.checked=true;
      }
      console.log("show dot-map");
      //console.log(jam_checked);
      keywordtype.value="";
      for (level=1;level<=5;level++){
        jamid="jamselect"+level.toString();
        if (jam_checked[level-1]){
          //console.log(jamid);
          document.getElementById(jamid).checked = false;
          $("#"+jamid).parent().button('toggle');
          map.removeLayer(jamLayer[level-1]);
          jam_checked[level-1] = false;
        }
      }
      return;
    }
  }

  var keywords = "";
  if(datatype == 'TWITTER'){
    console.log("twitter")
    keywords = document.getElementById("tweets_keywords").value;
    console.log(keywords);
  }

  var sD = gStartDate.toLocaleDateString();
  var eD = gEndDate.toLocaleDateString();
  var sT = gStartTime.toLocaleTimeString();
  var eT = gEndTime.toLocaleTimeString();

  $.get( "/waze_twitter",{type_sub: typeorsub, typename: datatype, startD: sD, startT: sT, endD: eD, endT: eT, keywords: keywords}, function( data, status ) {
    //startDT and endDT are does not have real meaning here,  gStartTime.toLocaleString gEndTime
    //it should be break down into time and date on the backend
    //data should be a json
    //need to add code in render all for drawing layer.
    alertsLOC = data.coords;
    //console.log(alertsLOC.length);
    wt_dotLayer.clearLayers();
    wt_heatLayer.data = [];
    showcircles = [];
    circlecenters = [];
    for (var i = alertsLOC.length - 1; i >= 0; i--) {
        circlecenters.push({lat: alertsLOC[i][0], log: alertsLOC[i][1]});
    }
    for (var i = alertsLOC.length - 1; i >= 0; i--) {
      var c = L.circleMarker(alertsLOC[i], { radius : 3, color : '#3566CD', opacity:1, fillColor: '#3566CD',fillOpacity: 1 });
      window.showcircles.push(c);      
      wt_dotLayer.addLayer(c);
      window.wt_heatLayer.pushData(alertsLOC[i][0], alertsLOC[i][1], 10);

      //add pop up. The minwidth is go with the "white-space: pre-wrap"    
      var popup = L.popup({maxHeight:300, minWidth:300});
      window.showcircles[alertsLOC.length - 1 - i].on('click',function(e) {
        circlepos = e.target.getLatLng();
        //console.log(circlepos);
        wt_dotLayer.removeLayer(popup);
        console.log("To get Popup content");
        $.get( "/click_content",{type_sub: typeorsub, typename: datatype, startD: sD, startT: sT, endD: eD, endT: eT, lat: circlepos.lat, lng: circlepos.lng, keywords: keywords}, function(data, status) {
             console.log("Popup content get");
             data_content = data.coords;
             //console.log(data_content.length);
             test_text = "";
             if (datatype == 'TWITTER'){
               for (var contentid = data_content.length - 1;contentid > 0; contentid--) {

                 test_text = test_text + "Time: " + String(data_content[contentid][0]) + "<br />" + "Content: " + String(data_content[contentid][1]) + "<br /><br />";
               }
               test_text = test_text + "Time: " + String(data_content[0][0]) + "<br />" + "Content: " + String(data_content[0][1]) + "<br />";
               test_text = "<p style=\"white-space: pre-wrap; word-wrap:break-word\">" + test_text + "</p>";
             }
             else {
               for (var contentid = data_content.length - 1;contentid > 0; contentid--) {
                 test_text = test_text + "Time: " + String(data_content[contentid][0]) + "<br />" + "Type: " + String(data_content[contentid][1]) + "<br />" + "Subtype: " + String(data_content[contentid][2])  + "<br /><br />";
               }
               test_text = test_text + "Time: " + String(data_content[contentid][0]) + "<br />" + "Type: " + String(data_content[contentid][1]) + "<br />" + "Subtype: " + String(data_content[contentid][2])  + "<br />";
               test_text = "<p style=\"white-space: pre-wrap; word-wrap:break-word\">" + test_text + "</p>";   
             }
             //console.log(test_text);
             popup.setLatLng(circlepos).setContent(test_text);
             wt_dotLayer.addLayer(popup);
             hideMask();
        });
        showMask();
      });
    }
    if(alertsLOC.length == 0){//if no data
      window.wt_heatLayer.pushData(40.7841484, -73.9661407, 0); 
    }
    if(document.getElementById("radioHeat").checked){
      wt_heatLayer.redraw();
      console.log("redrawing the heat-map");
    }
    if (multiload===0){ 
      hideMask();
    }
    else{
      multiload--;
    }
    //console.log(multiload);
  });
  showMask();
}

function search_tweets(){
  waze_twitter(false, true, 'TWITTER');
  console.log("in search_tweets");
  //check if radio on
}

function setTime(time,num){
  var hour = Math.floor(num);
  var min = (num - hour)*60;
  time.setHours(hour,min);
}

function setDate(date,dObj){
  date = new Date(dObj.getFullYear(),dObj.getMonth(), dObj.getDate(), 0,0,0,0);
}


function switch_heatmap(){
    if(checkbox.checked == true){
        map.addLayer(heatmap);
    }
    else {
        map.removeLayer(heatmap);
    }
};


function jam_level(level){//this logic is not efficient
  if(level<=5 &&level>0){
    if(jam_checked[level-1] == false){
      map.addLayer(jamLayer[level-1]);
      jam_checked[level-1] = true;
    }else{
      map.removeLayer(jamLayer[level-1]);
      jam_checked[level-1] = false;
      return;//when removing layer no need to update jam
    }
  }
  update_jam();
}

//show jam points
function update_jam(){

    var sD = gStartDate.toLocaleDateString();
    var eD = gEndDate.toLocaleDateString();
    var sT = gStartTime.toLocaleTimeString();
    var eT = gEndTime.toLocaleTimeString();
    $.get( "/jam_level",{startD: sD, startT: sT, endD: eD, endT: eT}, function( data, status ) {
      jam_loc = data.jam;
      for(var i = 0; i<5 ; ++i){
        jamLayer[i].clearLayers();
      }
      var i = 0;
      //console.log(jam_loc[0]);
      for(var j = 1; j< 6; ++j){
        while(i <jam_loc.length && jam_loc[i][2] == j) {
          var c = L.circleMarker([jam_loc[i][0],jam_loc[i][1]], { radius : 3, color : jam_color[j-1], opacity:1, fillColor: jam_color[j-1], fillOpacity: 1 });
          jamLayer[j-1].addLayer(c);
          //window.wt_heatLayer.pushData(jam_loc[i][0], jam_loc[i][1], 10); 
          ++i;
        }
      }
    if (multiload===0){ 
      hideMask();
    }
    else{
      multiload--;
    }
    //console.log(multiload);
    });
  showMask();
}


function update_backend_user_play_status(status) {
  $.get( "/user_play_status",{play_status:status}, function( data, status ) {//
    //console.log(data);
    //console.log("backend user play status: " + data);
  });
}

$("#toggleObjectDetection").change(function() {
  toggleObjectDetection = !toggleObjectDetection;
  $.get("/user_object_detection_status", {detection_status: toggleObjectDetection}, function(data, status){
    console.log("toggle detection: " + toggleObjectDetection);
  });
});


$("#toggleObjectTracking").change(function() {
  toggleObjectTracking = !toggleObjectTracking;
  $.get("/user_object_tracking_status", {tracking_status: toggleObjectTracking}, function(data, status){
    console.log("toggle tracking: " + toggleObjectTracking);
  });
});

//D3 FUNCTIONS

  // Renders the specified chart or list.
  function render(method) {
    d3.select(this).call(method);
  }

  // Whenever the brush moves, re-rendering everything.
  function renderAll() {
    chart.each(render);
    d3.select("#active").text(formatNumber(all.value()));
  }

  // Like d3.time.format, but faster.
  function parseDate(d) {
    return new Date(
        d.substring(0, 4),
        d.substring(5, 7) - 1,
        d.substring(8, 10),
        d.substring(11, 13),
        d.substring(14, 16),
        d.substring(17, 19));
  }

  window.filter = function(filters) {
    filters.forEach(function(d, i) { charts[i].filter(d); });
    renderAll();
  };

  window.reset = function(i) {
    charts[i].filter(null);
    renderAll();
  };
  
  function barChart() {
    if (!barChart.id) barChart.id = 0;

    var margin = {top: 10, right: 10, bottom: 20, left: 10},
        x,
        y = d3.scale.linear().range([100, 0]),
        id = barChart.id++,
        axis = d3.svg.axis().orient("bottom"),
        brush = d3.svg.brush(),
        brushDirty,
        dimension,
        group,
        round;

    function chart(div) {
      var width = x.range()[1],
          height = y.range()[0];

      y.domain([0, group.top(1)[0].value]);

      div.each(function() {
        var div = d3.select(this),
            g = div.select("g");

        // Create the skeletal chart.
        if (g.empty()) {
          div.select(".title").append("a")
              .attr("href", "javascript:reset(" + id + ")")
              .attr("class", "reset")
              .text("reset")
              .style("display", "none");

          g = div.append("svg")
              .attr("width", width + margin.left + margin.right)
              .attr("height", height + margin.top + margin.bottom)
            .append("g")
              .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

          g.append("clipPath")
              .attr("id", "clip-" + id)
            .append("rect")
              .attr("width", width)
              .attr("height", height);

          g.selectAll(".bar")
              .data(["background", "foreground"])
            .enter().append("path")
              .attr("class", function(d) { return d + " bar"; })
              .datum(group.all());

          g.selectAll(".foreground.bar")
              .attr("clip-path", "url(#clip-" + id + ")");

          g.append("g")
              .attr("class", "axis")
              .attr("transform", "translate(0," + height + ")")
              .call(axis);

          // Initialize the brush component with pretty resize handles.
          var gBrush = g.append("g").attr("class", "brush").call(brush);
          gBrush.selectAll("rect").attr("height", height);
          gBrush.selectAll(".resize").append("path").attr("d", resizePath);
        }

        // Only redraw the brush if set externally.
        if (brushDirty) {
          brushDirty = false;
          g.selectAll(".brush").call(brush);
          div.select(".title a").style("display", brush.empty() ? "none" : null);
          if (brush.empty()) {
            g.selectAll("#clip-" + id + " rect")
                .attr("x", 0)
                .attr("width", width);
          } else {
            var extent = brush.extent();
            g.selectAll("#clip-" + id + " rect")
                .attr("x", x(extent[0]))
                .attr("width", x(extent[1]) - x(extent[0]));
          }
        }

        g.selectAll(".bar").attr("d", barPath);
      });

      function barPath(groups) {
        var path = [],
            i = -1,
            n = groups.length,
            d;
        while (++i < n) {
          d = groups[i];
          path.push("M", x(d.key), ",", height, "V", y(d.value), "h9V", height);
        }
        return path.join("");
      }

      function resizePath(d) {
        var e = +(d == "e"),
            x = e ? 1 : -1,
            y = height / 3;
        return "M" + (.5 * x) + "," + y
            + "A6,6 0 0 " + e + " " + (6.5 * x) + "," + (y + 6)
            + "V" + (2 * y - 6)
            + "A6,6 0 0 " + e + " " + (.5 * x) + "," + (2 * y)
            + "Z"
            + "M" + (2.5 * x) + "," + (y + 8)
            + "V" + (2 * y - 8)
            + "M" + (4.5 * x) + "," + (y + 8)
            + "V" + (2 * y - 8);
      }
    }

    brush.on("brushstart.chart", function() {
      var div = d3.select(this.parentNode.parentNode.parentNode);
      div.select(".title a").style("display", null);
    });

    brush.on("brush.chart", function() {
      var g = d3.select(this.parentNode),
          extent = brush.extent();
      if (round) g.select(".brush")
          .call(brush.extent(extent = extent.map(round)))
        .selectAll(".resize")
          .style("display", null);
      g.select("#clip-" + id + " rect")
          .attr("x", x(extent[0]))
          .attr("width", x(extent[1]) - x(extent[0]));
      dimension.filterRange(extent);
      if(id == 0){
        gStartTime.setHours (extent[0]);
        gStartTime.setMinutes ((extent[0]-Math.floor(extent[0])) * 60);
        gStartTime.setSeconds ((extent[0]*100-Math.floor(extent[0]*100)) * 60);

        if (extent[1]===24){
          gEndTime.setHours(23);
          gEndTime.setMinutes(59);
          gEndTime.setSeconds(59);
        }
        else {
        gEndTime.setHours (extent[1]);
        gEndTime.setMinutes ((extent[1]-Math.floor(extent[1])) * 60);
        gEndTime.setSeconds ((extent[1]*100-Math.floor(extent[1]*100)) * 60);
        }

        /*console.log(extent[1]);
        console.log(gStartTime);
        console.log(gEndTime);*/

      }else{
        var tempdate = new Date();
        tempdate.setTime (extent[0].getTime());
        if (tempdate.getHours()>=12){
          gStartDate.setFullYear(tempdate.getFullYear())
          gStartDate.setMonth(tempdate.getMonth())
          gStartDate.setDate(tempdate.getDate()+1)
          gStartDate.setHours(0);
          gStartDate.setMinutes(0);
          gStartDate.setSeconds(0);
        }
        else{
          gStartDate.setFullYear(tempdate.getFullYear())
          gStartDate.setMonth(tempdate.getMonth())
          gStartDate.setDate(tempdate.getDate())
          gStartDate.setHours(0);
          gStartDate.setMinutes(0);
          gStartDate.setSeconds(0);
        }
        tempdate.setTime (extent[1].getTime());
        if (tempdate.getHours()>=12){
          gEndDate.setFullYear(tempdate.getFullYear())
          gEndDate.setMonth(tempdate.getMonth())
          gEndDate.setDate(tempdate.getDate()+1)
          gEndDate.setHours(0);
          gEndDate.setMinutes(0);
          gEndDate.setSeconds(0);
        }
        else{
          gEndDate.setFullYear(tempdate.getFullYear())
          gEndDate.setMonth(tempdate.getMonth())
          gEndDate.setDate(tempdate.getDate())
          gEndDate.setHours(0);
          gEndDate.setMinutes(0);
          gEndDate.setSeconds(0);
        }
        //console.log(gStartDate);
        //console.log(gEndDate);
      }
    });

    brush.on("brushend.chart", function() {
      if (brush.empty()) {
        var div = d3.select(this.parentNode.parentNode.parentNode);
        div.select(".title a").style("display", "none");
        div.select("#clip-" + id + " rect").attr("x", null).attr("width", "100%");
        dimension.filterAll();

        if(id == 0){
          gStartTime = new Date('2015/12/05 00:00:00');
          gEndTime = new Date('2015/12/05 23:59:59');
    	}else{
          gStartDate = new Date('2015/12/01 00:00:00');
          gEndDate = new Date('2016/02/03 00:00:00');

    	}
      }

      if (drawedbox){
        update_wordcloud();
        multiload++;
      }
      multiload=multiload+1;
      //console.log(multiload);
      waze_twitter(false, false, "");
      update_jam();
    });

    chart.margin = function(_) {
      if (!arguments.length) return margin;
      margin = _;
      return chart;
    };

    chart.x = function(_) {
      if (!arguments.length) return x;
      x = _;
      axis.scale(x);
      brush.x(x);
      return chart;
    };

    chart.y = function(_) {
      if (!arguments.length) return y;
      y = _;
      return chart;
    };

    chart.dimension = function(_) {
      if (!arguments.length) return dimension;
      dimension = _;
      return chart;
    };

    chart.filter = function(_) {
      if (_) {
        brush.extent(_);
        dimension.filterRange(_);
      } else {
        brush.clear();
        dimension.filterAll();
      }
      brushDirty = true;
      return chart;
    };

    chart.group = function(_) {
      if (!arguments.length) return group;
      group = _;
      return chart;
    };

    chart.round = function(_) {
      if (!arguments.length) return round;
      round = _;
      return chart;
    };

    return d3.rebind(chart, brush, "on");
  }
//added based on $.get("/load_event",{}, function( data, status ){})
//      Design choice: added type argument. Or delete the type argument and let the API provide all information.

var randRadius = 0.00025;
function load_detection_results(){
  //This following line for standard api
  // $.get("/detection_results",{type}, function( data, status ){
  // Now data use a global variable for testing
  
  var data = testGlobalEventData;
  // console.log(data);
  //each data point is like :[id, decription(combination of types), cameraID, starttime, endtime, tag]
  for(var tag in tags){
    detected_event_set[tags[tag]] = {}
    detected_dotLayers[tags[tag] ] = {};
    newline = '<li><a tabindex="-1" id="dropDown'+tags[tag]+'" onclick = "showDetectionDotMap(curEventType,\''+tags[tag]+'\',false)">'+tags[tag]+'</a></li>'
    $("#tagDropDown").append(newline);
  }
  newline = '<li><a tabindex="-1" id="dropDownAllTags" onclick = "showDetectionDotMap(curEventType,\'\',false)">All Tags</a></li>'
  $("#tagDropDown").append(newline);

  for(var typeIdx in types){ 
    for (i=0; i<data.coords.length; i++){
      var eventTag = data.coords[i][5];
      if(typeof(detected_event_set[eventTag][types[typeIdx] ]) == 'undefined')
        {detected_event_set[eventTag][types[typeIdx] ]={};}
      if(typeof(detected_event_set[eventTag][types[typeIdx] ][data.coords[i][2]/*The cam id*/]) == 'undefined')
        {detected_event_set[eventTag][types[typeIdx] ][data.coords[i][2]]=[];}
      if(data.coords[i][1].indexOf(types[typeIdx]) != -1)//description contains the type 
        //add the event to the camera
        // console.log()
        {detected_event_set[eventTag][types[typeIdx] ][data.coords[i][2]/*The cam id*/].push ({ description: data.coords[i][1], camera: data.coords[i][2], starttime: data.coords[i][3], endtime: data.coords[i][4], tag: data.coords[i][5]});}
    }
    // console.log(detected_event_set);
    //detected_event_set is ready
    var detected_dot_array = [];//This datastructure is for later popup

    //Now iterate through the list of data, and add the data points onto the dotmap
    // detected_dotLayers[type] = L.layerGroup();
      // if (detected_event_set.hasOwnProperty(type)) {
        // console.log(type + " -> " + detected_event_set[type]);
      for(var tagIdx in tags){
        if(typeof(detected_dotLayers[tags[tagIdx]][types[typeIdx]]) == "undefined"){
          detected_dotLayers[tags[tagIdx]][types[typeIdx]] = L.layerGroup();
          detected_dotLayers[tags[tagIdx]][types[typeIdx]].data = [];
        }
      
      // }
      // console.log(detected_event_set[types[typeIdx]]);
      for(var cam_id in detected_event_set[tags[tagIdx]][types[typeIdx]]){
        // console.log(cam_id,detected_event_set[tags[tagIdx]][types[typeIdx]][cam_id])
        if (detected_event_set[tags[tagIdx]][types[typeIdx]].hasOwnProperty(cam_id)) {
          //the data for each cam 
          var cam_idx;
          for(var i in cams){
            if (cams[i].id == cam_id) {
              cam_idx = i;
              break;
            }
          }
          //cams[cam_idx] has 
            // {camera: "Broadway_@_169_Street", 
            //  lat: 40.841733, 
            //  log: -73.939404, 
            //  facing: "South", 
            //  id: 65
            // }

          for(var i = 0; i< detected_event_set[tags[tagIdx]][types[typeIdx]][cam_id].length;i++){
            // each element has { description: data.coords[i][1], camera: data.coords[i][2], starttime: data.coords[i][3], endtime: data.coords[i][4], tag: data.coords[i][5]}
            var cam_coord = randomCoordGen({lat:cams[cam_idx].lat, log: cams[cam_idx].log},randRadius);
            var c = L.circleMarker([cam_coord.lat,cam_coord.log], { radius : 4, color : '#0500CD', opacity:1, fillColor: '#FDE101',fillOpacity: 1 });
            c.eventIndex = i;
            c.eventType = types[typeIdx];
            c.eventTag = tags[tagIdx];
            c.eventCamID = cam_id;
            var singleEvent = detected_event_set[tags[tagIdx]][types[typeIdx]][cam_id][i];
            var event_text = "Description: " + "<br />" + singleEvent.description + "<br />" + "Start Time: " + singleEvent.starttime + "<br />" + "End Time: " + singleEvent.endtime + "<br />Tag: "+singleEvent.tag +"<br />";
            event_text = "<p style=\"word-wrap:break-word \">" + event_text + "</p>";   
            c.bindPopup(event_text);
            detected_dotLayers[tags[tagIdx]][types[typeIdx]].addLayer(c);
            // console.log(c)
            var event_ppup = L.popup({maxHeight:300});
            // console.log(event_ppup);
            // console.log(c);
            c.on('click',function(e) {
              // console.log("dot clicked");
              // console.log(e);
              // dot_pos = e.target.getLatLng();
              // console.log(dot_pos);
              var singleEvent = detected_event_set[e.target.eventTag][e.target.eventType][e.target.eventCamID][e.target.eventIndex];
              // event_ppup_text = "";
              // console.log(detected_event_set[e.target.eventTag][e.target.eventType][e.target.eventCamID]);
              // console.log(singleEvent);
              // event_ppup_text = "Description: " + String(singleEvent.description) + "<br />" + "Start Time: " + String(singleEvent.starttime) + "<br />" + "End Time: " + String(singleEvent.endtime)  + "Tag: "+String(singleEvent.tag)+"<br />";
              // event_ppup_text = "<p style=\"white-space: pre-wrap; word-wrap:break-word\">" + event_ppup_text + "</p>";   
              
              // event_ppup.setLatLng(dot_pos).setContent(event_ppup_text);

              // detected_dotLayers[tags[tagIdx]][types[typeIdx]].addLayer(event_ppup);
              // console.log("singleEvent",singleEvent);
              // hideMask();
              // showMask();
              setloadedevent(singleEvent,true);
            });
            detected_dot_array.push(c);
          }
        }
      }
    }

  // });//The get function for standard api
  //test if data loaded
  // console.log(detected_dot_array);
  // console.log(types[typeIdx]);
  newline = '<li><a tabindex="-1" id="dropDown'+types[typeIdx]+'" onclick = "showDetectionDotMap(\''+types[typeIdx]+'\',curEventTag,false)">'+types[typeIdx]+'</a></li>'
  $("#dctEvtDropDown").append(newline);
  }
  newline = '<li><a tabindex="-1" id="dropDownAllTypes" onclick = "showDetectionDotMap(\'\',curEventTag,false)">All Types</a></li>'
  $("#dctEvtDropDown").append(newline);
  $("#dctEvtDropDown").dropdown();
}

function randomCoordGen(center,radius){
  var x = Math.random()*radius*2-radius;
  var delta = Math.sqrt(Math.pow(radius,2)-Math.pow(x,2));
  var y = Math.random()*delta*2-delta;
  var randCoord={};
  randCoord.lat = center.lat+y;
  randCoord.log = center.log+x;
  return randCoord;
}
//not implemented yet
function circleCoordGen(center,radius){//might also need the total number of points and the idx
  return center;
}
//need a variable to record which layer is on so it is easier to switch .



function showDetectionDotMap(type,tag,isReset){
  // console.log(type);
  // curEventType = type;
  // curEventTag = tag;

  if(tag != ""){
    document.getElementById("tagBtn").innerHTML = tag+" <span class='caret'></span>";
  }
  if(type != ""){
    document.getElementById("dctEvtBtn").innerHTML = type+" <span class='caret'></span>";
  }
  if(isReset){
    console.log("reset");
    // if(curEventTag != "" || curEventType != ""){
      curEventLayer.clearLayers();
      map.removeLayer(curEventLayer);
      curEventTag = "";
      curEventType = "";
      // clearTimeout(lastTimeout);
      // console.log("map has camlayer is: ",map.hasLayer(camLayer));
      if(!map.hasLayer(camLayer)){

        map.addLayer(camLayer);
      }            
      return;
    // }

  }
  // else if(tag == "All"){ return showDetectionDotMap(type,"",isReset)}
  else{

    if(map.hasLayer(camLayer)){
      map.removeLayer(camLayer);
    }
    if(curEventTag != tag || curEventType != type){
      if(curEventType != type || curEventTag != tag){
        curEventLayer.clearLayers();
        map.removeLayer(curEventLayer);
      }else{
        return;
      }
    }
    curEventType = type;
    curEventTag = tag;
    // 加 Layer 
    // if(tag == "All" && type == "All"){
    //   for( var tagIdx in tags){
    //     for(var typeIdx in types){
    //       curEventLayer.addLayer(detected_dotLayers[tags[tagIdx]]type[typeIdx]);
    //     }
    //   }
    //     map.addLayer(curEventLayer);
    // }else if(tag != "All" && type == "All"){}

    if(tag != "" && type != ""){
      curEventLayer.addLayer(detected_dotLayers[tag][type]);
      map.addLayer(curEventLayer);
    }else if(tag == "" && type !=""){
      document.getElementById("tagBtn").innerHTML = "All Tags <span class='caret'></span>";
      for( var tagIdx in tags){
        curEventLayer.addLayer(detected_dotLayers[tags[tagIdx]][type]);
        
      }
      map.addLayer(curEventLayer);
    }else if(type == "" && tag != ""){
      document.getElementById("dctEvtBtn").innerHTML = "All Types <span class='caret'></span>";
      for(var typeIdx in types){
        curEventLayer.addLayer(detected_dotLayers[tag][types[typeIdx]]);
      }
      map.addLayer(curEventLayer);
    }else{//Both ""
      document.getElementById("tagBtn").innerHTML = "All Tags <span class='caret'></span>";
      document.getElementById("dctEvtBtn").innerHTML = "All Types <span class='caret'></span>";
      for( var tagIdx in tags){
        for(var typeIdx in types){
          curEventLayer.addLayer(detected_dotLayers[tags[tagIdx]][types[typeIdx]]);
        }
      }
      map.addLayer(curEventLayer);
    }


  }
}



function canvasLoadImage(canvasId, imgSrc, data) {
    var canvas = document.getElementById(canvasId);
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    var image = new Image();

    image.onload = function() {
        ctx.drawImage(image, 0, 0);
        if (toggleObjectDetection == true) {
            var boundingBoxes = data.coordinates[0].boundingBoxes;
            // draw boxes
            ctx.beginPath();
            for (var i = 0; i < boundingBoxes.length; i++) {
                ctx.lineWidth = "1.5";
                ctx.strokeStyle = "red";
                ctx.rect(boundingBoxes[i][0], boundingBoxes[i][1], boundingBoxes[i][2] - boundingBoxes[i][0], boundingBoxes[i][3] - boundingBoxes[i][1]);
            }
            ctx.stroke();
        }
   
        if (toggleObjectDetection == true && toggleObjectTracking == true) {
            var trackings = data.coordinates[0].trackings; 
            // draw tracking lines
            for (var i = 0; i < trackings.length; i++) {
                var plot_x = trackings[i]['plot_x'];
                var plot_y = trackings[i]['plot_y'];
                var plot_len = plot_x.length - 1;

                drawLine(plot_x, plot_y, "yellow", "5", ctx);

                if (plot_len == 0)
                    continue;

                var plot_x_1 = [plot_x[0], plot_x[0] + 5, plot_x[0], plot_x[0] - 5, plot_x[0], plot_x[0] + 5];
                var plot_y_1 = [plot_y[0], plot_y[0], plot_y[0] + 5, plot_y[0], plot_y[0] - 5, plot_y[0]];
                drawLine(plot_x_1, plot_y_1, "green", "5", ctx);

                var plot_x_2 = [plot_x[plot_len], plot_x[plot_len] + 5, plot_x[plot_len], plot_x[plot_len] - 5, plot_x[plot_len], plot_x[plot_len] + 5];
                var plot_y_2 = [plot_y[plot_len], plot_y[plot_len], plot_y[plot_len] + 5, plot_y[plot_len], plot_y[plot_len] - 5, plot_y[plot_len]];
                drawLine(plot_x_2, plot_y_2, "blue", "5", ctx);
            }
        }
    };
    image.src = imgSrc;
    console.log(data);
}

function drawLine(plot_x, plot_y, strokeStyle, lineWidth, ctx) {
    ctx.beginPath();
    ctx.lineWidth = lineWidth;
    ctx.strokeStyle = strokeStyle;
    ctx.moveTo(plot_x[0], plot_y[0]);
    for (var i = 1; i < plot_x.length; i++) {
        ctx.lineTo(plot_x[i], plot_y[i]);
        ctx.moveTo(plot_x[i], plot_y[i]);
    }
    ctx.stroke();
}
