//d3 slider and axis
{
  
  // console.log(vsvg.style("width"));
  // console.log(vsvg.style("height"));
  var vmargin = {top: 5, right: 5, bottom: 5, left: 5},
      vwidth = 400 - vmargin.left - vmargin.right,
      vheight = 40 - vmargin.bottom - vmargin.top;

  var vx = d3.scale.linear()
      .domain([mainStartDT,mainEndDT])
      .range([0, vwidth])
      .clamp(true);

  var vbrush = d3.svg.brush()
      .x(vx)
      .extent([0, 0])
      .on("brush", brushed);

  var vsvg = d3.select("#slider").append("svg")
       .attr("width", vwidth + vmargin.left + vmargin.right)
       .attr("height", vheight + vmargin.top + vmargin.bottom)
    .append("g")
       .attr("transform", "translate(" + vmargin.left + "," + vmargin.top + ")");

  vsvg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + vheight / 2 + ")")
      .call(d3.svg.axis()
        .scale(vx)
        .orient("bottom")
        .tickFormat(function(d) { date = new Date(d); return date.getFullYear()+"/"+(date.getMonth()+1)+"/"+date.getDate();})
        .ticks(5)
        .tickSize(0)
        .tickPadding(1))
    .select(".domain")
    .select(function() { return this.parentNode.appendChild(this.cloneNode(true)); })
      .attr("class", "halo");

  var vslider = vsvg.append("g")
      .attr("class", "slider")
      .call(vbrush);

  vslider.selectAll(".extent,.resize")
      .remove();

  vslider.select(".background")
      .attr("height", vheight);

  var vhandle = vslider.append("circle")
      .attr("class", "handle")
      .attr("transform", "translate(0," + vheight / 2 + ")")
      .attr("r", 5);

  // vslider
  //     .call(vbrush.event)
    // .transition() // gratuitous intro!
    //   .duration(7500)
    //   .call(vbrush.extent([70, 70]))
    //   .call(vbrush.event);

  function brushed() {
    var value = vbrush.extent()[0];

    if (d3.event.sourceEvent) { // not a programmatic event
      value = vx.invert(d3.mouse(this)[0]);
      vbrush.extent([value, value]);
    }
    // vbrush.append()
    vhandle.attr("cx", vx(value));
    d3.select("body").style("background-color", d3.hsl(value, .8, .8));
    console.log("value",value);
    
    vcurtime = new Date(value);
    console.log("date: ", vcurtime);
  }

}
//d3 speed slider

{
  var smargin = {top: 1, right: 1, bottom: 1, left: 10},
      swidth = 40 - smargin.left - smargin.right,
      sheight = 240 - smargin.bottom - smargin.top;

  var sx = d3.scale.linear()
      .domain([0.5,2])
      .range([0, sheight])
      .clamp(true);

  var sbrush = d3.svg.brush()
      .y(sx)
      .extent([0, 0])
      .on("brush", brushed);

  var ssvg = d3.select("#vspeed").append("svg")
       .attr("width", swidth + smargin.left + smargin.right)
       .attr("height", sheight + smargin.top + smargin.bottom)
    .append("g")
       .attr("transform", "translate(" + smargin.left + "," + smargin.top + ")");

  ssvg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + swidth / 2 + ")")
      .call(d3.svg.axis()
        .scale(sx)
        .orient("left")
        .tickFormat(function(d) { return d;})
        .ticks(5)
        .tickSize(0)
        .tickPadding(1))
    // .select(".domain")
    // .select(function() { return this.parentNode.appendChild(this.cloneNode(true)); })
    //   .attr("class", "halo");

  var sslider = ssvg.append("g")
      .attr("class", "slider")
      .call(sbrush);

  sslider.selectAll(".extent,.resize")
      .remove();

  sslider.select(".background")
      .attr("height", sheight);

  var shandle = sslider.append("circle")
      .attr("class", "handle")
      .attr("transform", "translate(0," + sheight / 2 + ")")
      .attr("r", 5);

  sslider
      .call(sbrush.event)
    // .transition() // gratuitous intro!
      // .duration(7500)
      // .call(sbrush.extent([70, 70]))
      // .call(sbrush.event);

  function brushed() {
    var value = sbrush.extent()[0];

    if (d3.event.sourceEvent) { // not a programmatic event
      value = sx.invert(d3.mouse(this)[0]);
      sbrush.extent([value, value]);
    }
    // sbrush.append()
    shandle.attr("cx", sx(value));
    // d3.select("body").style("background-color", d3.hsl(value, .8, .8));
    console.log("value",value);
    console.log("date: ", new Date(value));

  }
}