# --------------------------------------------------------
# This python file is the server of visualization tool
# with automatic car detection
# run on port number 5001
# --------------------------------------------------------
from flask import Flask, render_template, request, url_for, jsonify, Response, redirect, flash
import psycopg2
import base64
from datetime import timedelta
from datetime import datetime
from pprint import pprint
import json
from wordcloud import WordCloud
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import re
import random
import shlex
from flask.ext.login import LoginManager, UserMixin, login_required, login_user, logout_user, current_user
import os
from hashlib import sha256
from hmac import HMAC
import csv

app = Flask(__name__)
# app.config['DEBUG'] = True
app.config.update(DEBUG = False, SECRET_KEY = 'dvmm32123')

# flask-login
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

acceptfailurelogintime = 5

# --------------------------------------------------------
# Faster R-CNN
import _init_paths
from fast_rcnn.config import cfg
from fast_rcnn.test import im_detect
from fast_rcnn.nms_wrapper import nms
from utils.timer import Timer
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
import caffe, os, sys, cv2
import argparse
from PIL import Image
CLASSES = ('__background__',
		   'car')
NETS = {'vgg16': ('VGG16',
				  'VGG16_faster_rcnn_final.caffemodel'),
		'zf': ('ZF',
				  'ZF_faster_rcnn_final.caffemodel')}
# --------------------------------------------------------

import os
import sys
import random as rand
import gzip
import glob
import cPickle as pickle
import numpy as np
import csv
import numpy.random
import scipy
import cv2
import skimage
import skimage.transform

def parse_args():
    """Parse input arguments."""
    parser = argparse.ArgumentParser(description='Faster R-CNN demo')
    parser.add_argument('--gpu', dest='gpu_id', help='GPU device id to use [0]',
                        default=0, type=int)
    parser.add_argument('--cpu', dest='cpu_mode',
                        help='Use CPU mode (overrides --gpu)',
                        action='store_true')
    parser.add_argument('--net', dest='demo_net', help='Network to use [vgg16]',
                        choices=NETS.keys(), default='vgg16')

    args = parser.parse_args()

    return args

# --------------------------------------------------------

# --------------------------------------------------------
# Faster R-CNN	
cfg.TEST.HAS_RPN = True  # Use RPN for proposals

args = parse_args()

prototxt = os.path.join('/home/zhengshou/work01/larsde/py-faster-rcnn-onecar', 'models', 'ZF',
						'faster_rcnn_end2end', 'test.prototxt')
caffemodel = os.path.join('/home/zhengshou/work01/larsde/py-faster-rcnn-onecar', 'output/faster_rcnn_end2end/voc_2007_trainval','zf_faster_rcnn_iter_50000.caffemodel')

if not os.path.isfile(caffemodel):
	raise IOError(('{:s} not found.\nDid you run ./data/script/'
				   'fetch_faster_rcnn_models.sh?').format(caffemodel))

if args.cpu_mode:
   caffe.set_mode_cpu()
else:
	caffe.set_mode_gpu()
	caffe.set_device(args.gpu_id)
	cfg.GPU_ID = args.gpu_id
net = caffe.Net(prototxt, caffemodel, caffe.TEST)

print '\n\nLoaded network {:s}'.format(caffemodel)

# Warmup on a dummy image
im = 128 * np.ones((300, 500, 3), dtype=np.uint8)
for i in xrange(2):
	_, _= im_detect(net, im)
# --------------------------------------------------------

def connect_database():
	try:
		conn = psycopg2.connect("dbname='larsde_other' user='flask' host='larsde.cs.columbia.edu' password='dvmm32123'")
		conn_images = psycopg2.connect("dbname='larsde_images' user='flask' host='larsde.cs.columbia.edu' password='dvmm32123'")
		conn.autocommit = True
		conn_images.autocommit = True
	except Exception as e:
		print e, "Connection Unsucessful"

	cur = conn.cursor()
	cur_images = conn_images.cursor()
	return cur, cur_images

cur, cur_images = connect_database()

def triplet():
	cur.execute("select * from obj_match")
	obj_match = cur.fetchall()
	obj_match_len = len(obj_match)
	cur.execute("select * from obj_annot")
	obj_annot = cur.fetchall()
	obj_annot_len = len(obj_annot)
	'''
	cur_images.execute("select * from images")
	obj_image = cur_images.fetchall()
	obj_image_len = len(obj_image)
	'''
	filelist = []
	for i in range(obj_match_len):
		labelid1 = obj_match[i][0]
		labelid2 = obj_match[i][1]
		for j in range(obj_annot_len):
			labelid = obj_annot[j][0]
			if labelid1 == labelid:
				imgtime = obj_annot[j][1]
				imgcamid = obj_annot[j][2]
				coord_x1 = obj_annot[j][3]
				coord_y1 = obj_annot[j][4]
				coord_x2 = obj_annot[j][5]
				coord_y2 = obj_annot[j][6]
				objecttype = obj_annot[j][7]
				cur_images.execute("select content from images where time=%s and new_id=%s", [imgtime, imgcamid])
				row = cur_images.fetchone()
				abspath = os.path.abspath(__file__)
				current_dir = os.path.dirname(abspath)
				image_dir = '../../triplet_loss/data2'
				image_num = str(i)
				image_name = '1.jpg'
				new_path = os.path.join(current_dir, image_dir, image_num)
				if not os.path.exists(new_path):
					os.makedirs(new_path)
				fh = open(os.path.join(new_path, image_name),'wb')
				fh.write(row[0])
				fh.close()
				image_full_dir = new_path
				im_file = os.path.join(image_full_dir, image_name)
				im = cv2.imread(im_file)
				crop_im = im[int(round(coord_y1)):int(round(coord_y2)), int(round(coord_x1)):int(round(coord_x2))]
				image_name_crop = 'anchor.jpg'
				im_file_crop = os.path.join(image_full_dir, image_name_crop)
				cv2.imwrite(im_file_crop, crop_im)
		for j in range(obj_annot_len):
			labelid = obj_annot[j][0]
			if labelid2 == labelid:
				imgtime = obj_annot[j][1]
				imgcamid = obj_annot[j][2]
				coord_x1 = obj_annot[j][3]
				coord_y1 = obj_annot[j][4]
				coord_x2 = obj_annot[j][5]
				coord_y2 = obj_annot[j][6]
				objecttype = obj_annot[j][7]
				cur_images.execute("select content from images where time=%s and new_id=%s", [imgtime, imgcamid])
				row = cur_images.fetchone()
				abspath = os.path.abspath(__file__)
				current_dir = os.path.dirname(abspath)
				image_dir = '../../triplet_loss/data2'
				image_num = str(i)
				image_name = '2.jpg'
				new_path = os.path.join(current_dir, image_dir, image_num)
				if not os.path.exists(new_path):
					os.makedirs(new_path)
				fh = open(os.path.join(new_path, image_name),'wb')
				fh.write(row[0])
				fh.close()
				image_full_dir = new_path
				im_file = os.path.join(image_full_dir, image_name)
				im = cv2.imread(im_file)
				crop_im = im[int(round(coord_y1)):int(round(coord_y2)), int(round(coord_x1)):int(round(coord_x2))]
				image_name_crop = 'positive.jpg'
				im_file_crop = os.path.join(image_full_dir, image_name_crop)
				cv2.imwrite(im_file_crop, crop_im)
		for j in range(obj_annot_len):
			labelid = obj_annot[j][0]
			if labelid2 == labelid:
				imgtime = obj_annot[j][1]
				imgcamid = obj_annot[j][2]
				coord_x1 = obj_annot[j][3]
				coord_y1 = obj_annot[j][4]
				coord_x2 = obj_annot[j][5]
				coord_y2 = obj_annot[j][6]
				objecttype = obj_annot[j][7]
				cur.execute("select * from obj_annot where imgtime=%s and imgcamid=%s and labelid<>%s", [imgtime, imgcamid, labelid2])
				obj_annot_negative = cur.fetchall()
				obj_annot_negative_len = len(obj_annot_negative)
				rand.shuffle(obj_annot_negative)
				if obj_annot_negative_len == 0:
					continue
				labelid_negative = obj_annot_negative[0][0]
				imgtime_negative = obj_annot_negative[0][1]
				imgcamid_negative = obj_annot_negative[0][2]
				coord_x1_negative = obj_annot_negative[0][3]
				coord_y1_negative = obj_annot_negative[0][4]
				coord_x2_negative = obj_annot_negative[0][5]
				coord_y2_negative = obj_annot_negative[0][6]
				objecttype_negative = obj_annot_negative[0][7]
				cur_images.execute("select content from images where time=%s and new_id=%s", [imgtime_negative, imgcamid_negative])
				row = cur_images.fetchone()
				abspath = os.path.abspath(__file__)
				current_dir = os.path.dirname(abspath)
				image_dir = '../../triplet_loss/data2'
				image_num = str(i)
				image_name = '2.jpg'
				new_path = os.path.join(current_dir, image_dir, image_num)
				if not os.path.exists(new_path):
					os.makedirs(new_path)
				fh = open(os.path.join(new_path, image_name),'wb')
				fh.write(row[0])
				fh.close()
				image_full_dir = new_path
				im_file = os.path.join(image_full_dir, image_name)
				im = cv2.imread(im_file)
				crop_im = im[int(round(coord_y1_negative)):int(round(coord_y2_negative)), int(round(coord_x1_negative)):int(round(coord_x2_negative))]
				image_name_crop = 'negative.jpg'
				im_file_crop = os.path.join(image_full_dir, image_name_crop)
				cv2.imwrite(im_file_crop, crop_im)
		if obj_annot_negative_len > 0:
			filelist.append([image_full_dir+'/anchor.jpg', image_full_dir+'/positive.jpg', image_full_dir+'/negative.jpg'])
			print ("obj_match: %d" %i)
	rand.shuffle(filelist)
	train_data = np.asarray(filelist, dtype=object)
	np.save(image_full_dir+'/../../data3/train_data.npy', train_data)

if __name__ == "__main__":
	triplet()