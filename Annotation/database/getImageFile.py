#!/usr/bin/python
###################
# this python file is used to create task in which all camera are involved 
###################
import csv
import psycopg2
import base64
from datetime import timedelta
from datetime import datetime
# import time
from pprint import pprint
import io
###################### 
# define default variables
userid=1 #check the user you want to assign this task
label_task_id=15 #check the last task_id in the annot_task table
match_task_id=16 #check the last task_id in the annot_task table
dates=["2015-12-07"] #date need to be analyzed
num_frames=2 #the number of continuous images
hours=["08:00:00","20:00:00"] #the time need to be analyzed
interval=1 #maximum number of second need between two continuous images
objecttype="cars"
#####################

times=[]
label_item_id=1
match_item_id=1

for date in dates:
	for hour in hours:
		# 2016-01-23 08:00:00
		exactTime=date+" "+hour
		times.append(exactTime)

# Part1 connect to database first
try:
  conngetevent=psycopg2.connect("dbname='larsde_other' user='xinyi' host='larsde.cs.columbia.edu' password='dvmm32123'")
  conn = psycopg2.connect("dbname='larsde_other' user='flask' host='larsde.cs.columbia.edu' password='dvmm32123'")
  conn_images = psycopg2.connect("dbname='larsde_images' user='flask' host='larsde.cs.columbia.edu' password='dvmm32123'")
  conn.autocommit = True
  conn_images.autocommit = True
except Exception as e:
  print e, "Connection Unsucessful"
curevent=conngetevent.cursor()
cur=conn.cursor()
cur_images = conn_images.cursor()



# get cameraid from generated file
with open("cameraId.csv","rb") as f:
	reader = csv.reader(f)
	cameraid =list(reader)
print len(cameraid)



# go for a loop
for cid in cameraid:
	getTime=[]
	for exactTime in times:
		getTime=[]
		startTime=exactTime
		while(len(getTime)<num_frames and (datetime.strptime(startTime,"%Y-%m-%d %H:%M:%S")-datetime.strptime(exactTime,"%Y-%m-%d %H:%M:%S")).seconds<3600):
			print "int the loop: "+ str((datetime.strptime(startTime,"%Y-%m-%d %H:%M:%S")-datetime.strptime(exactTime,"%Y-%m-%d %H:%M:%S")).seconds)
			endTime=datetime.strptime(exactTime,"%Y-%m-%d %H:%M:%S")+timedelta(hours=2)
			qry = "SELECT new_id, time FROM images WHERE new_id=\'"+str(cid[0])+"\' AND time>=\'"+str(startTime)+"\' AND time<\'"+str(endTime)+"\' ORDER BY time ASC LIMIT \'"+str(num_frames)+"\';"
			try:
				cur_images.execute(qry)
			except Exception,e:
				print ("Exception executing query: ",e)

			if cur_images.rowcount==0:
				print "is none"
				break
			for row in cur_images:
				timeaaa=datetime.strptime(str(row[1]),"%Y-%m-%d %H:%M:%S")
				getTime.append(timeaaa)
				print str(row[0])+", "+str(row[1])
			num=1
			flag=0
			for num in range(1,len(getTime),1):
				print(getTime[num]-getTime[num-1]).seconds
				if (getTime[num]-getTime[num-1]).seconds>interval or getTime[num]>datetime.strptime(exactTime,"%Y-%m-%d %H:%M:%S")+timedelta(hours=1):
					if getTime[num]>datetime.strptime(exactTime,"%Y-%m-%d %H:%M:%S")+timedelta(hours=1):
						print "yes"
						flag=1
					startTime=str(getTime[num])
					getTime=[]
					num=1
					break
			if flag==1:
				break
			if num==num_frames-1:
				print "vaild!!"
				for n in range(0,len(getTime),1):
					qry="INSERT INTO obj_annot_task_items (task_id, item_id, image_time,image_camid,finished) VALUES(%s,%s,\'%s\',%s,0);" %(label_task_id,label_item_id,getTime[n],cid[0])
					try:
						cur.execute(qry)
						print "insert into annot!"
						label_item_id +=1
					except Exception,e:
						print ("Exception executing query: ",e)
					if n<num_frames-1:
						qry1="INSERT INTO obj_match_task_items (task_id, item_id,image1_time, image1_camid, image2_time, image2_camid,finished) VALUES(%s,%s,\'%s\',%s,\'%s\',%s,0);" %(match_task_id,match_item_id,getTime[n],cid[0],getTime[n+1],cid[0])
						try:
							cur.execute(qry1)
							print "insert into annot!"
							match_item_id +=1
						except Exception,e:
							print ("Exception executing query: ",e)

# insert into task table 
qry4="SELECT count(*) from obj_annot_task_items WHERE task_id=\'"+str(label_task_id)+"\';"
qry5="SELECT count(*) from obj_match_task_items WHERE task_id=\'"+str(match_task_id)+"\';"
try:
	cur.execute(qry4)
except Exception,e:
	print ("Exception executing query: ",e)
try:
	curevent.execute(qry5)
except Exception,e:
	print ("Exception executing query: ",e)
row=cur.fetchone()
row1=curevent.fetchone()
num_label=int(row[0])
num_match=int(row1[0])

qry6="INSERT INTO annot_task (userid,lastpageseen,numdone,totalpage,objecttype,annotationtype,task_id) values(%d,1,0,%d,'%s','label',%d);" %(userid,num_label,objecttype,label_task_id)
qry7="INSERT INTO annot_task (userid,lastpageseen,numdone,totalpage,objecttype,annotationtype,task_id) values(%d,1,0,%d,'%s','match',%d);" %(userid,num_match,objecttype,match_task_id)
try:
	cur.execute(qry6)
	cur.execute(qry7)
	print "insert into task!"
except Exception,e:
	print ("Exception executing query: ",e)
					
