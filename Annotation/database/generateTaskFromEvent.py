###############
# this python file is used to create task from event
###############
import psycopg2
import base64
from datetime import timedelta
from datetime import datetime
from pprint import pprint
import io
###################### 
# define default variables
userid=1 #check the user you want to assign this task
label_task_id=11 #check the last task_id in the annot_task table
match_task_id=12 #check the last task_id in the annot_task table
num_frames=2 #the number of continuous images
gap=1 #maximum number of second need between two continuous images
interval=60 #get the images every 60mins
objecttype="cars"
####################

label_item_id=1
match_item_id=1

# Part1 connect to database first
try:
  conngetevent=psycopg2.connect("dbname='larsde_other' user='xinyi' host='larsde.cs.columbia.edu' password='dvmm32123'")
  conn = psycopg2.connect("dbname='larsde_other' user='flask' host='larsde.cs.columbia.edu' password='dvmm32123'")
  conn_images = psycopg2.connect("dbname='larsde_images' user='flask' host='larsde.cs.columbia.edu' password='dvmm32123'")
  conn.autocommit = True
  conn_images.autocommit = True
except Exception as e:
  print e, "Connection Unsucessful"
curevent=conngetevent.cursor()
cur=conn.cursor()
cur_images = conn_images.cursor()


# get information of all waze events
qry="select * from event where tag='waze';"
try:
	curevent.execute(qry)
except Exception,e:
	print ("Exception executing query: ",e)
# if cur_images.rowcount==0:
# 	print "is none"
# 	break
for row in curevent:
	getTime=[]
	print "time type from databse: "+str(type(row[3]))
	print "camid type from databse: "+str(type(row[2]))
	startTime=str(row[3])
	endTime=str(row[4])
	trueStart=startTime
	while(len(getTime)<num_frames and datetime.strptime(endTime,"%Y-%m-%d %H:%M:%S")>datetime.strptime(startTime,"%Y-%m-%d %H:%M:%S") and (datetime.strptime(endTime,"%Y-%m-%d %H:%M:%S")-datetime.strptime(startTime,"%Y-%m-%d %H:%M:%S")).seconds>0):
		print "int the loop end: "+ str((datetime.strptime(endTime,"%Y-%m-%d %H:%M:%S")))
		print "int the loop start: "+ str((datetime.strptime(startTime,"%Y-%m-%d %H:%M:%S")))
		if (datetime.strptime(endTime,"%Y-%m-%d %H:%M:%S")-datetime.strptime(startTime,"%Y-%m-%d %H:%M:%S")).seconds>interval*60:
			trueEnd=datetime.strptime(startTime,"%Y-%m-%d %H:%M:%S")+timedelta(minutes=interval)
		else:
			trueEnd=datetime.strptime(endTime,"%Y-%m-%d %H:%M:%S")
		qry1="SELECT new_id, time FROM images WHERE new_id=\'"+str(row[2])+"\' AND time>=\'"+str(trueStart)+"\' AND time<\'"+str(trueEnd)+"\' ORDER BY time ASC LIMIT \'"+str(num_frames)+"\';" 
		try:
			cur_images.execute(qry1)
		except Exception,e:
			print ("Exception executing query: ",e)

		if cur_images.rowcount<num_frames:
			print "is none"
			break
		for row1 in cur_images:
			# 2016-01-24 20:00:08
			timeaaa=datetime.strptime(str(row1[1]),"%Y-%m-%d %H:%M:%S")
			getTime.append(timeaaa)
			print str(row[0])+", "+str(row1[1])
		num=1
		flag=0
		for num in range(1,len(getTime),1):
			print(getTime[num]-getTime[num-1]).seconds
			if (getTime[num]-getTime[num-1]).seconds>gap:
				trueStart=str(getTime[num])
				getTime=[]
				num=1
				flag=1
				break
		if num==num_frames-1 and flag==0:
			print "vaild!!"
			startTime=str(datetime.strptime(startTime,"%Y-%m-%d %H:%M:%S")+timedelta(minutes=interval))
			trueStart=startTime
			print "updated start time: "+ startTime
			for n in range(0,len(getTime),1):
				print"really insert sth"
				qry2="INSERT INTO obj_annot_task_items (task_id, item_id, image_time,image_camid,finished) VALUES(%s,%s,\'%s\',%s,0);" %(label_task_id,label_item_id,getTime[n],row[2])
				try:
					cur.execute(qry2)
					print "insert into annot!"
					label_item_id +=1
				except Exception,e:
					print ("Exception executing query: ",e)
				if n<num_frames-1:
					qry3="INSERT INTO obj_match_task_items (task_id, item_id,image1_time, image1_camid, image2_time, image2_camid,finished) VALUES(%s,%s,\'%s\',%s,\'%s\',%s,0);" %(match_task_id,match_item_id,getTime[n],row[2],getTime[n+1],row[2])
					try:
						cur.execute(qry3)
						print "insert into annot!"
						match_item_id +=1
					except Exception,e:
						print ("Exception executing query: ",e)
			getTime=[]
			
# insert into task table 
qry4="SELECT count(*) from obj_annot_task_items WHERE task_id=\'"+str(label_task_id)+"\';"
qry5="SELECT count(*) from obj_match_task_items WHERE task_id=\'"+str(match_task_id)+"\';"
try:
	cur.execute(qry4)
except Exception,e:
	print ("Exception executing query: ",e)
try:
	curevent.execute(qry5)
except Exception,e:
	print ("Exception executing query: ",e)
row=cur.fetchone()
row1=curevent.fetchone()
num_label=int(row[0])
num_match=int(row1[0])

qry6="INSERT INTO annot_task (userid,lastpageseen,numdone,totalpage,objecttype,annotationtype,task_id) values(%d,1,0,%d,'%s','label',%d);" %(userid,num_label,objecttype,label_task_id)
qry7="INSERT INTO annot_task (userid,lastpageseen,numdone,totalpage,objecttype,annotationtype,task_id) values(%d,1,0,%d,'%s','match',%d);" %(userid,num_match,objecttype,match_task_id)
try:
	cur.execute(qry6)
	cur.execute(qry7)
	print "insert into task!"
except Exception,e:
	print ("Exception executing query: ",e)
			