#!/usr/bin/env python2.7
#############
#This is the backend python flask code for the annotation tool
#############
import os
from flask import Flask, request, render_template, g, redirect, Response, url_for, jsonify,session,send_file, flash
import psycopg2
import base64
from datetime import timedelta
from datetime import datetime
from pprint import pprint
import json
import io
import csv
from flask.ext.login import LoginManager, UserMixin, login_required, login_user, logout_user, current_user
import os
from hashlib import sha256
from hmac import HMAC


app = Flask(__name__)
# app.config['DEBUG'] = True
app.config.update(DEBUG = True, SECRET_KEY = 'xinyi123')

# flask-login
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"
acceptfailurelogintime = 5

#the dictionary to store the task for each loggedin user
task={}
#the dictionary to store the task object type for each loggedin user doing localization task
objecttype={}

#connect to database
def connect_database():
  try:
    conn = psycopg2.connect("dbname='larsde_other' user='flask' host='larsde.cs.columbia.edu' password='dvmm32123'")
    conn_images = psycopg2.connect("dbname='larsde_images' user='flask' host='larsde.cs.columbia.edu' password='dvmm32123'")
    conn.autocommit = True
    conn_images.autocommit = True
    print "connect successful"
  except Exception as e:
    print e, "Connection Unsucessful"

  cur = conn.cursor()
  cur_images = conn_images.cursor()
  return cur, cur_images
cur, cur_images = connect_database()

@app.after_request
def add_header(response):
  response.headers['X-UA-Compatible']='IE=Edge,chrome=1'
  response.headers['Cache-Control']='public,max-age=0'
  return response

@app.route('/')
def homepage():
  if not current_user.is_authenticated:
    print "not authenticated"
    return render_template("login.html")
  else:
    print "authenticated"
    return render_template("index.html")

# query database get all the task for the querying user
@app.route('/getTasks')
def getTasks():
  global cur
  global cur_images
  uid=current_user.get_id()
  qry="SELECT * FROM annot_task WHERE userid=\'"+str(uid)+"\' ORDER BY task_id;"
  try:
    cur.execute(qry)
    print "already query task on index page!"
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry)
  data=cur.fetchall()
  return jsonify(tasks=data)

# get the taskid and object type the querting user chose 
@app.route('/setTask',methods=['POST'])
def setTask():
  global task
  uid=current_user.get_id()
  tasktype=request.form['tasktype']
  if(tasktype=="match"):
    task[uid]=int(request.form['taskid'])
    objecttype[uid]=request.form['objecttp']
  else:
    task[uid]=int(request.form['taskid'])
    objecttype[uid]=request.form['objecttp']
  return "set task success"

##########
# functions for label
##########

# query database get num of items in the task for the querying user
@app.route('/getNumImage')
def getNumImage():
  global cur
  global cur_images
  global task
  uid=current_user.get_id()
  qry="SELECT totalpage FROM annot_task WHERE task_id=\'"+str(task[uid])+"\';"
  try:
    cur.execute(qry)
    print "already query task!"
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry)
  row=cur.fetchone()
  return jsonify(num=row[0])

# query database get num of items that have been submitted in the task for the querying user
@app.route('/getFinished')
def getFinished():
  global cur
  global cur_images
  global task
  uid=current_user.get_id()
  qry="SELECT count(*) FROM obj_annot_task_items WHERE finished=1 AND task_id=\'"+str(task[uid])+"\';"
  try:
    cur.execute(qry)
    print "already query numdone!"
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry)
  row=cur.fetchone()
  qry2="UPDATE annot_task SET numdone = \'"+str(row[0])+"\' WHERE task_id=\'"+str(task[uid])+"\';"
  qry3="SELECT numdone FROM annot_task WHERE task_id=\'"+str(task[uid])+"\';"
  try:
    cur.execute(qry2)
    cur.execute(qry3)
    print "already query numdone!"
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry2)
    cur.execute(qry3)
  row=cur.fetchone()
  return jsonify(num=row[0])

# query database get the last seen page in the task for the querying user
@app.route('/getlastseen')
def getlastseen():
  global cur
  global cur_images
  global task
  uid=current_user.get_id()
  qry="SELECT lastpageseen FROM annot_task WHERE task_id=\'"+str(task[uid])+"\';"
  try:
    cur.execute(qry)
    print "already query lastpageseen!"
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry)
  row=cur.fetchone()
  return jsonify(num=row[0])

# update the last seen page in the task for the querying user
@app.route('/savelastpageseen',methods=['POST'])
def savelastpageseen():
  global cur
  global cur_images
  global task
  uid=current_user.get_id()
  imageid=int(request.form['imageid'])
  qry3="UPDATE annot_task SET(lastpageseen)= (\'"+str(imageid)+"\') WHERE task_id=\'"+str(task[uid])+"\';"
  try:
    cur.execute(qry3)
    print "already query qry3!"
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry3)
  return "save lastseen!"

#save the the new tag for the existed label to database
@app.route('/savenewtag',methods=['POST'])
def savenewtag():
  global cur
  global cur_images
  global task
  uid=current_user.get_id()
  labelid=int(request.form['labelid'])
  tagcon=request.form['tag'];
  qry3="INSERT INTO label_tags VALUES(%s,'%s');" %(int(labelid),tagcon)
  try:
    cur.execute(qry3)
    print "already saved tag!"
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry3)
  return "save lastseen!"

#query the database to get the tags for the selected bounding box
@app.route('/gettags')
def gettags():
  global cur
  global cur_images
  global task
  global objecttype
  uid=current_user.get_id()
  args = request.args
  labelid=int(args.get('label_id'))
  qry1="SELECT tag from label_tags WHERE labelid=\'"+str(labelid)+ "\' ;"
  try:
    cur.execute(qry1)
    print "already get tags!"
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry1)
  data=cur.fetchall()
  return jsonify(labels = data)

#delete tag from database
@app.route('/deletetag',methods=['POST'])
def deletetag():
  global cur
  global cur_images
  global task
  uid=current_user.get_id()
  labelid=int(request.form['labelid'])
  tagcon=request.form['tag'];
  qry3="DELETE FROM label_tags WHERE labelid=\'"+str(labelid)+ "\' and tag=\'"+str(tagcon)+ "\' ;"
  try:
    cur.execute(qry3)
    print "already saved tag!"
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry3)
  return "save lastseen!"

# query database check whether the current item has been submitted or not
@app.route('/annotsubmitornot')
def annotsubmitornot():
  global cur
  global cur_images
  global task
  uid=current_user.get_id()
  args = request.args
  pageid=int(args.get('pageid'))
  qry="SELECT finished FROM obj_annot_task_items WHERE task_id=\'"+str(task[uid])+"\' AND item_id=\'"+str(pageid)+"\';"
  try:
    cur.execute(qry)
    print "already query qry!"
  except Exception,e:
      print ("Exception executing query: ",e)
      cur, cur_images = connect_database()
      cur.execute(qry)
  row= cur.fetchone()
  if row[0]==1: 
    return "submitted"
    
  else:
    return "unsubmitted"

# fitch image
@app.route('/fetchpic.jpg', methods=['GET','POST'])
def fetchpic():
  global cur
  global cur_images
  global task
  uid=current_user.get_id()
  args=request.args
  info=int(args.get('no'))
  qry="SELECT image_camid, image_time from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(info) +"\';"
  try:
    cur.execute(qry)
    print "already query!"
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry)
  row=cur.fetchone()
  qry1 = "SELECT content from images WHERE new_id=\'"+str(row[0])+ "\' AND time=\'"+str(row[1])+ "\' ORDER BY time ASC LIMIT 1;"
  try:
    cur_images.execute(qry1)
    print "already query!"
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur_images.execute(qry1)
  row = cur_images.fetchone()
  try:
    bindata = base64.b64encode(row[0]).decode('utf-8')
  except Exception as e:
    print e
    return "error using base64"
  return send_file(io.BytesIO(row[0]))

# get existed bounding boxes for the current image
@app.route('/getlabel')
def getlabel():
  global cur
  global cur_images
  global task
  global objecttype
  uid=current_user.get_id()
  args = request.args
  imageid=int(args.get('imageid'))
  qry1="SELECT labelid, coord_x1, coord_y1, coord_x2, coord_y2 from obj_annot WHERE objecttype=\'"+str(objecttype[uid])+ "\' and imgcamid= (SELECT image_camid from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\') AND imgtime=(SELECT image_time from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\');"
  try:
    cur.execute(qry1)
    print "already get labels!"
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry1)
  data=cur.fetchall()
  return jsonify(labels = data)

# check after clearall button is clicked
@app.route('/getnumoflabelsonthispage')
def getnumoflabelsonthispage():
  global cur
  global cur_images
  global task
  global objecttype
  uid=current_user.get_id()
  args=request.args
  imageid=int(args.get('imageid'))
  qry="SELECT count(labelid) FROM obj_annot WHERE objecttype=\'"+str(objecttype[uid])+ "\' AND coord_x1 is not null and imgcamid=(SELECT image_camid from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\') AND imgtime=(SELECT image_time from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\');"
  try:
    cur.execute(qry)
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry)
  row=cur.fetchone()
  if row[0]>0:
    return "must confirm"
  else:
    return "pass"

# after click clear all, check all the bounding boxes, whether they are matched or not
@app.route('/allMatchedOrNot')
def allMatchedOrNot():
  global cur
  global cur_images
  global task
  global objecttype
  uid=current_user.get_id()
  args=request.args
  imageid=int(args.get('imageid'))
  qry="SELECT labelid FROM obj_annot WHERE objecttype=\'"+str(objecttype[uid])+ "\' and imgcamid=(SELECT image_camid from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\') AND imgtime=(SELECT image_time from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\');"  
  try:
    cur.execute(qry)
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry)
  for row in cur:
    qry2="SELECT * FROM obj_match WHERE labelid1=\'"+str(row[0])+ "\' OR labelid2=\'"+str(row[0])+ "\';"
    try:
      cur.execute(qry2)
    except Exception,e:
      print ("Exception executing query: ",e)
      cur, cur_images = connect_database()
      cur.execute(qry2)
    if cur.rowcount!=0:
      return "already matched"
  return "nothing matched"

# check the selected deleted bounding boxes, whether matched or not
@app.route('/labelMatchedOrNot',methods=['POST'])
def labelMatchedOrNot():
  global cur
  global cur_images
  labels=json.loads(request.form['labels'])
  for key in labels:
    qry="SELECT * FROM obj_match WHERE labelid1=\'"+key+ "\' OR labelid2=\'"+key+ "\';"
    try:
      cur.execute(qry)
    except Exception,e:
      print ("Exception executing query: ",e)
      cur, cur_images = connect_database()
      cur.execute(qry)
    if cur.rowcount!=0:
      return "already matched"
  return "nothing matched"

# delete bounding box from database
@app.route('/deleteSavedLabel', methods=['POST'])
def deleteSavedLabel():
  global cur
  global cur_images
  labels=json.loads(request.form['deleteLabels'])
  for key,value in labels.iteritems():
    qry="DELETE FROM obj_match WHERE labelid1 =\'"+key+ "\' OR labelid2=\'"+key+ "\';"
    qry1="DELETE FROM obj_annot WHERE labelid=\'"+key+ "\';"
    try:
      cur.execute(qry)
      cur.execute(qry1)
      print "delete one success!"
    except Exception,e:
      print ("Exception executing query: ",e)
      cur, cur_images = connect_database()
      cur.execute(qry)
      cur.execute(qry1)
  return "delete success"

# add new bounding boxes to database
@app.route('/addNewLabel',methods=['POST'])
def addNewLabel():
  global cur
  global cur_images
  global task
  global objecttype
  uid=current_user.get_id()
  imageid=int(request.form['imageid'])
  labels=json.loads(request.form['newLabel'])
  tags=json.loads(request.form['newtag'])
  if len(labels)!=0:
    for key,value in labels.iteritems():
      coords=value.split(",")
      qry="INSERT INTO obj_annot (imgtime,imgcamid,coord_x1,coord_y1,coord_x2,coord_y2,objecttype) VALUES((SELECT image_time from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\'),(SELECT image_camid from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\'),%s,%s,%s,%s,'%s');" %(int(coords[0]),int(coords[1]),int(coords[2]),int(coords[3]),objecttype[uid])
      try:
        cur.execute(qry)
      except Exception,e:
        print ("Exception executing query: ",e)
        cur, cur_images = connect_database()
        cur.execute(qry)
      qry01="SELECT labelid from obj_annot where imgtime=(SELECT image_time from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\') and imgcamid=(SELECT image_camid from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\') and coord_x1=\'"+str(coords[0]) +"\' and coord_y1=\'"+str(coords[1]) +"\' and coord_x2=\'"+str(coords[2]) +"\' and coord_y2=\'"+str(coords[3]) +"\' and objecttype=\'"+str(objecttype[uid]) +"\';"
      try:
        cur.execute(qry01)
      except Exception,e:
        print ("Exception executing query: ",e)
        cur, cur_images = connect_database()
        cur.execute(qry01)
      row=cur.fetchone()
      for key1,value1 in tags.iteritems():
        if key1==key:
          for tag in value1:
            qry02="INSERT INTO label_tags VALUES(%s,'%s');" %(int(row[0]),tag)
            try:
              cur.execute(qry02)
            except Exception,e:
              print ("Exception executing query: ",e)
              cur, cur_images = connect_database()
              cur.execute(qry02)

    qry4="DELETE FROM obj_annot WHERE objecttype=\'"+str(objecttype[uid])+ "\' and imgcamid=(SELECT image_camid from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\') AND imgtime= (SELECT image_time from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\') AND coord_x1 IS NULL;"
    try:
      cur.execute(qry4)
    except Exception,e:
      print ("Exception executing query: ",e)
      cur, cur_images = connect_database()
      cur.execute(qry4)
  if len(labels)==0:
    qry1="SELECT * FROM obj_annot WHERE objecttype=\'"+str(objecttype[uid])+ "\' and imgcamid=(SELECT image_camid from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\') AND imgtime=(SELECT image_time from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\');"
    try:
      cur.execute(qry1)
    except Exception,e:
      print ("Exception executing query: ",e)
      cur, cur_images = connect_database()
      cur.execute(qry1)
    if cur.rowcount==0:
      # qry11="UPDATE annot_task SET(numdone)=((SELECT numdone FROM annot_task WHERE filename=\'"+labelfile+"\')+1) WHERE filename=\'"+labelfile+"\';"
      qry2="INSERT INTO obj_annot (imgtime,imgcamid,coord_x1,coord_y1,coord_x2,coord_y2,objecttype) VALUES((SELECT image_time from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\'),(SELECT image_camid from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\'), NULL,NULL,NULL,NULL,\'"+str(objecttype[uid]) +"\');" 
      try:
        # cur.execute(qry11)
        cur.execute(qry2)
      except Exception,e:
        print ("Exception executing query: ",e)
        cur, cur_images = connect_database()
        cur.execute(qry2)
  qry4="UPDATE obj_annot_task_items SET finished = 1 WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\';"
  # qry3="UPDATE annot_task SET(lastpageseen)= (\'"+str(imageid)+"\') WHERE filename=\'"+labelfile+"\';"
  try:
    cur.execute(qry4)
  except Exception,e:
      print ("Exception executing query: ",e)
      cur, cur_images = connect_database()
      cur.execute(qry4)
  return "add success"

# save coords changes to databse
@app.route('/saveChangedLabel', methods=['POST'])
def saveChangedLabel():
  global cur
  global cur_images
  labels=json.loads(request.form['changedLabel'])
  for key,value in labels.iteritems():
    coords=value.split(",")
    qry="UPDATE obj_annot SET(coord_x1,coord_y1,coord_x2,coord_y2)=(%s,%s,%s,%s) WHERE labelid=(\'%s\');" %(int(coords[0]),int(coords[1]),int(coords[2]),int(coords[3]),key)
    try:
      cur.execute(qry)
      print "already query!"
    except Exception,e:
      print ("Exception executing query: ",e)
      cur, cur_images = connect_database()
      cur.execute(qry)
  return "change success"

# delete all the bounding boxes for current selected item
@app.route('/clearLabels',methods=['POST'])
def clearLabels():
  global cur
  global cur_images
  global task
  global objecttype
  uid=current_user.get_id()
  imageid=int(request.form['imageid'])
  qry0="DELETE FROM obj_match WHERE labelid1 IN (SELECT labelid FROM obj_annot WHERE objecttype=\'"+str(objecttype[uid])+ "\' and imgcamid=(SELECT image_camid from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\') AND imgtime=(SELECT image_time from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\'));"
  qry1="DELETE FROM obj_match WHERE labelid2 IN (SELECT labelid FROM obj_annot WHERE objecttype=\'"+str(objecttype[uid])+ "\' and imgcamid=(SELECT image_camid from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\') AND imgtime=(SELECT image_time from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\'));"
  qry2="DELETE FROM obj_annot WHERE objecttype=\'"+str(objecttype[uid])+ "\' and imgcamid=(SELECT image_camid from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\') AND imgtime=(SELECT image_time from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\');"
  qry3="INSERT INTO obj_annot (imgtime,imgcamid,coord_x1,coord_y1,coord_x2,coord_y2,objecttype) VALUES((SELECT image_time from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\'),(SELECT image_camid from obj_annot_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\'), NULL,NULL,NULL,NULL,\'"+str(objecttype[uid])+ "\');" 
  qry4="UPDATE obj_annot_task_items SET finished = 1 WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(imageid) +"\';"
  # qry0="SELECT labelid FROM obj_annot WHERE imgcamid=\'"+imagesToBeLabeled[imageid][0]+"\' AND imgtime=\'"+imagesToBeLabeled[imageid][1]+"\';"
  
  try: 
    cur.execute(qry0)
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry0)
  try:
    cur.execute(qry1)
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry1)
  try:
    cur.execute(qry2)
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry2)
  try:
    cur.execute(qry3)
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry3)
  try:
    cur.execute(qry4)
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry4)
  return "reset success"

##########
# match functions
#########

# query database get num of items in the task for the querying user
@app.route('/getNumMatch')
def getNumberMatch():
  global cur
  global cur_images
  global task
  uid=current_user.get_id()
  qry="SELECT totalpage FROM annot_task WHERE task_id=\'"+str(task[uid])+"\';"
  try:
    cur.execute(qry)
    print "already query task!"
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry)
  row=cur.fetchone()
  return jsonify(num=row[0])

# update the num of items done in the task to the database
@app.route('/saveMatchDone',methods=['POST'])
def saveMatchDone():
  global cur
  global cur_images
  global task
  uid=current_user.get_id()
  pageid=int(request.form['pageid'])
  qry="UPDATE obj_match_task_items SET finished=1 where item_id=\'"+str(pageid)+"\'and task_id=\'"+str(task[uid])+"\';"
  try:
    cur.execute(qry)
    print "already count match numdone!"
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry)
  return "save done success"

# query database get num of items that have been submitted in the task for the querying user
@app.route('/getFinishedmatch')
def getFinishedmatch():
  global cur
  global cur_images
  global task
  uid=current_user.get_id()
  qry="SELECT count(*) FROM obj_match_task_items WHERE task_id=\'"+str(task[uid])+"\' AND finished=1;"
  try:
    cur.execute(qry)
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry)
  row=cur.fetchone()
  qry1="UPDATE annot_task SET numdone = \'"+str(row[0])+"\' WHERE task_id=\'"+str(task[uid])+"\';"
  qry2="SELECT numdone FROM annot_task WHERE task_id=\'"+str(task[uid])+"\';"
  try:
    cur.execute(qry1)
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry1)
  try:
    cur.execute(qry2)
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry2)
  row=cur.fetchone()
  return jsonify(num=row[0])

# query database get last seen items in the task for the querying user
@app.route('/getlastseenmatch')
def getlastseenmatch():
  global cur
  global cur_images
  global task
  uid=current_user.get_id()
  qry="SELECT lastpageseen FROM annot_task WHERE task_id=\'"+str(task[uid])+"\';"
  try:
    cur.execute(qry)
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry)
  row=cur.fetchone()
  return jsonify(num=row[0])

# update the last seen item in the task for the querying user
@app.route('/savematchlastpageseen',methods=['POST'])
def savematchlastpageseen():
  global cur
  global cur_images
  global task
  uid=current_user.get_id()
  imageid=int(request.form['imageid'])
  qry3="UPDATE annot_task SET(lastpageseen)= (\'"+str(imageid)+"\') WHERE task_id=\'"+str(task[uid])+"\';"
  try:
    cur.execute(qry3)
  except Exception,e:
      print ("Exception executing query: ",e)
      cur, cur_images = connect_database()
      cur.execute(qry3)
  return "save lastseen!"

# query database check whether the current item has been submitted or not
@app.route('/submitornot')
def submitornot():
  global cur
  global cur_images
  global task
  uid=current_user.get_id()
  args = request.args
  pageid=int(args.get('pageid'))
  qry="SELECT finished FROM obj_match_task_items WHERE task_id=\'"+str(task[uid])+"\' AND item_id=\'"+str(pageid)+"\';"
  try:
    cur.execute(qry)
  except Exception,e:
      print ("Exception executing query: ",e)
      cur, cur_images = connect_database()
      cur.execute(qry)
  row= cur.fetchone()
  if row[0]==1: 
    return "submitted"
    
  else:
    return "unsubmitted"
    
# fetch images
@app.route('/fetchmatchpic.jpg', methods=['GET','POST'])
def fetchmatchpic():
  global cur
  global cur_images
  global task
  uid=current_user.get_id()
  args=request.args
  info=int(args.get('no'))
  index=int(args.get('index'))
  if index==1:
    qry="SELECT image1_camid, image1_time from obj_match_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(info) +"\';"
  else:
    qry="SELECT image2_camid, image2_time from obj_match_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(info) +"\';"
  try:
    cur.execute(qry)
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry)
  row=cur.fetchone()
  qry2 = "SELECT content from images WHERE new_id=\'"+str(row[0])+ "\' AND time=\'"+str(row[1])+ "\' ORDER BY time ASC LIMIT 1;"
  try:
    cur_images.execute(qry2)
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur_images.execute(qry2)
  row = cur_images.fetchone()
  try:
    bindata = base64.b64encode(row[0]).decode('utf-8')
  except Exception as e:
    print e
    return "error using base64"
  return send_file(io.BytesIO(row[0]))

# get the bounding boxes for the loaded image
@app.route('/getmatchlabel')
def getmatchlabel():
  global cur
  global cur_images
  global task
  global objecttype
  uid=current_user.get_id()
  args=request.args
  pageid=int(args.get('pageid'))
  imageid=int(args.get('imageid'))
  if imageid==1:
    qry="SELECT labelid, coord_x1, coord_y1, coord_x2, coord_y2 from obj_annot where objecttype=\'"+str(objecttype[uid])+ "\' and imgcamid=(SELECT image1_camid from obj_match_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(pageid) +"\') AND imgtime=(SELECT image1_time from obj_match_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(pageid) +"\') ORDER BY imgtime ASC;"
  else:
    qry="SELECT labelid, coord_x1, coord_y1, coord_x2, coord_y2 from obj_annot where objecttype=\'"+str(objecttype[uid])+ "\' and imgcamid=(SELECT image2_camid from obj_match_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(pageid) +"\') AND imgtime=(SELECT image2_time from obj_match_task_items WHERE task_id=\'"+str(task[uid])+ "\' AND item_id=\'"+str(pageid) +"\') ORDER BY imgtime ASC;"
  try:
    cur.execute(qry)
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry)
  data=cur.fetchall()
  return jsonify(labels = data)

# get the matched pairs from database
@app.route('/getmatches')
def getmatches():
  global cur
  global cur_images
  args=request.args
  labelid=args.get('rectid')
  qry="SELECT * FROM obj_match WHERE labelid1=\'"+labelid+"\';"
  try:
    cur.execute(qry)
  except Exception,e:
    print ("Exception executing query: ",e)
    cur, cur_images = connect_database()
    cur.execute(qry)
  data=cur.fetchall()
  return jsonify(matches = data)

# save new match pair to database
@app.route('/addNewMatch',methods=['POST'])
def addNewMatch():
  global cur
  global cur_images
  matches=json.loads(request.form['matchings'])
  for key,value in matches.iteritems():
    labels=value.split(",")
    qry0="SELECT * FROM obj_match WHERE labelid1=\'"+labels[0]+"\' ;"
    try:
      cur.execute(qry0)
    except Exception,e:
      print ("Exception executing query: ",e)
      cur, cur_images = connect_database()
      cur.execute(qry0)
    if cur.rowcount==0:
      qry="INSERT INTO obj_match (labelid1,labelid2) VALUES(%s,%s);" %(int(labels[0]),int(labels[1]))
      try:
        cur.execute(qry)
      except Exception,e:
        print ("Exception executing query: ",e)
        cur, cur_images = connect_database()
        cur.execute(qry)
  return "add new match success"

# save the saved match pair to database
@app.route('/addSavedMatch',methods=['POST'])
def addSavedMatch():
  global cur
  global cur_images
  matches=json.loads(request.form['matchings'])
  for key,value in matches.iteritems():
    labels=value.split(",")
    qry0="SELECT * FROM obj_match WHERE labelid1=\'"+labels[0]+"\' ;"
    try:
      cur.execute(qry0)
    except Exception,e:
      print ("Exception executing query: ",e)
      cur, cur_images = connect_database()
      cur.execute(qry0)
    if cur.rowcount==0:
      qry="INSERT INTO obj_match (labelid1,labelid2) VALUES(%s,%s);" %(int(labels[0]),int(labels[1]))
      try:
        cur.execute(qry)
      except Exception,e:
        print ("Exception executing query: ",e)
        cur, cur_images = connect_database()
        cur.execute(qry)
  return "add saved match success"

# delete the backup match pairs from the database
@app.route('/deleteBackupMatch',methods=['POST'])
def addBackupMatch():
  global cur
  global cur_images
  matches=json.loads(request.form['matchings'])
  for key,value in matches.iteritems():
    labels=value.split(",")
    qry0="DELETE FROM obj_match WHERE labelid1=\'"+labels[0]+"\' ;"
    try:
      cur.execute(qry0)
    except Exception,e:
      print ("Exception executing query: ",e)
      cur, cur_images = connect_database()
      cur.execute(qry0)
  return "backup match deleted"

# render label.html
@app.route('/label')
def label():
  if not current_user.is_authenticated:
    print "not authenticated"
    return render_template("login.html")
  else:
    print "authenticate"
    return render_template("label.html")

# render match.html
@app.route('/match')
def match():
  if not current_user.is_authenticated:
    return render_template("login.html")
  else:
    return render_template("match.html")

# render login.html and code for user management
@app.route('/logout')
@login_required
def logout():
  logout_user();
  return redirect(url_for("homepage"))

class User(UserMixin):

  def __init__(self, id, username):
    self.id = id
    self.username = username

  def __repr__(self):
    return "%s%s" % (self.id, self.username)

  def getId(self):
    return self.id

def checkUsername(Username):
  global cur
  global cur_images
  try:
    cur.execute("SELECT * from annot_users WHERE username=%s;" , [Username])
  except Exception,e:
    print "error in checkusername"
    print e
    cur, cur_images = connect_database()
    cur.execute("SELECT * from annot_users WHERE username=%s;" , [Username])
  row = cur.fetchall()
  return row

#check user existence
def checkUserStatus(userid):
  global cur
  global cur_images
  try:
    cur.execute("SELECT failuretime from annot_users_status WHERE id=%s;" , [userid])
  except Exception,e:
    print "error in checkusername"
    print e
    cur, cur_images = connect_database()
    cur.execute("SELECT failuretime from annot_users_status WHERE id=%s;" , [userid])
  row = cur.fetchall()
  return row

#check if blocked
def addFailuretime(userid, failtimes):
  global cur
  global cur_images
  try:
    cur.execute("UPDATE userstatus SET failuretime=failuretime+1 WHERE id=%s;", [userid])
  except Exception,e:
    print "error in increase failuretime"
    print e
    cur, cur_images = connect_database()
    cur.execute("UPDATE userstatus SET failuretime=failuretime+1 WHERE id=%s;", [userid])
  return "failtimes increased"

#encode password
def encrypt_password(password, salt=None):
  # create salt
  if salt is None:
    salt = os.urandom(8)

  assert 8 == len(salt)
  assert isinstance(salt, str)

  if isinstance(password, unicode):
    password = password.encode('UTF-8')

  assert isinstance(password, str)

  result = password
  for i in xrange(10):
    result = HMAC(result, salt, sha256).digest()

  return salt+result

def validate_password(hashed, input_password):
  return hashed == encrypt_password(input_password, salt=hashed[:8])

def resetFailuretime(userid):
  global cur
  global cur_images
  try:
    cur.execute("UPDATE userstatus SET failuretime=0 WHERE id=%s;", [userid])
  except Exception,e:
    print "error in failuretime reset"
    print e
    cur, cur_images = connect_database()
    cur.execute("UPDATE userstatus SET failuretime=0 WHERE id=%s;", [userid])
  return "failtimes reset"


@app.route("/login",methods=["GET","POST"])
def login():
  # global user_id
  global acceptfailurelogintime
  error = None
  if request.method == 'POST':
    print "POST Login"
    Username = request.form['Username']
    Password = request.form['Password']
    Rememberme = request.form.getlist("Rememberme")
    row = checkUsername(Username)
    if (len(row) == 0):
      print "Incorrect Username or Password"
      flash("Incorrect Username or Password")
      return render_template("login.html")
    else:
      getid = row[0][0]
      getusername = row[0][1]
      getpassword = row[0][2]
      getpassword.rstrip()
      getpassword = str(base64.b64decode(getpassword))
      statusrow = checkUserStatus(getid)
      failtimes = statusrow[0][0]
      if (failtimes>=acceptfailurelogintime):
        print "Blocked User"
        flash("Blocked User")
        return render_template("login.html")
      elif (validate_password(getpassword, Password)):
        print "correct login"
        # user_id=getid
        # print "user_id: "+str(user_id)
        user = User(getid, Username)
        resetFailuretime(getid)
        if (len(Rememberme)==0):
          login_user(user)
        else:
          login_user(user, remember=True)
        return redirect(url_for("homepage"))
      else:
        print "Incorrect Username or Password"
        flash("Incorrect Username or Password")
        addFailuretime(getid,failtimes)
        return render_template("login.html")
  else:
    print "GET Login"
    return render_template("login.html")

      # callback to reload the user object
@login_manager.user_loader
def load_user(userid):
  global cur
  global cur_images
  # print userid
  try:
    cur.execute("SELECT id, username from annot_users WHERE id=%s;" , [userid])
  except Exception,e:
    print "error in login"
    print e
    cur, cur_images = connect_database()
    cur.execute("SELECT id, username from annot_users WHERE id=%s;" , [userid])
  row = cur.fetchall()
  username = row[0][1]
  nowuserid = row[0][0]
  nowuser = User(userid, username)
  if (unicode(nowuserid) == userid):
    print 'load user'
    return nowuser
  else:
    print 'no load'
    return None

if __name__ == "__main__":
  #import click

  #@click.command()
  #@click.option('--debug', is_flag=True)
  #@click.option('--threaded', is_flag=True)
  #@click.argument('HOST', default='0.0.0.0')
  #@click.argument('PORT', default=8111, type=int)
  def run(debug, threaded, host, port):
    """
    This function handles command line parameters.
    Run the server using

        python server.py

    Show the help text using

        python server.py --help

    """

    HOST, PORT = host, port
    print "running on %s:%d" % (HOST, PORT)
    app.run(host=HOST, port=PORT, debug=debug, threaded=threaded)


  run(True, True, '0.0.0.0', 8111)
