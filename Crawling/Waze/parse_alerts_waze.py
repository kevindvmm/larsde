import psycopg2
import os
import csv

PATH = './output/'
CONFIG = 'db_config.txt'

def getDirList(p):
	p = str(p)
	if p == "":
		return []
	a = os.listdir( p )
	return a

fconf = open(CONFIG, 'r')
conf = fconf.read()
conn = psycopg2.connect(conf)
conn.autocommit = True
cur = conn.cursor()

firstfilelist = getDirList(PATH)

index_error_count = 0
database_error_count = 0
success_count = 0
duplicate_count = 0
programming_error_count = 0

for year in firstfilelist:
	if (year == '.DS_Store'):
		continue
	else: 
		fin = open(os.path.join (PATH, year),'r')
		reader = csv.reader(fin)
		
		for csv in reader:
			row = {}

			try:
				row['alertsid'] = csv[0]
				row['type'] = csv[1]
				row['subtype'] = csv[2]
				row['reportDescription'] = csv[3]
				row['pubMillis'] = csv[4]
				row['reliability'] = csv[5]
				row['location_desc'] = csv[6]
				row['location_x'] = csv[7]
				row['location_y'] = csv[8]

				query_string = 'INSERT INTO alerts_waze (alertid, type, subtype, time, reliability, description, location_desc, location_lat, location_long) VALUES (\'%s\', \'%s\', \'%s\', to_timestamp(%d), %s, \'%s\', \'%s\', %s, %s)' % (row ['alertsid'], row['type'], row['subtype'], int(row['pubMillis'])/1000, row['reliability'], row['reportDescription'], row['location_desc'], row['location_x'], row['location_y'])
				cur.execute(query_string);
				success_count = success_count + 1
			except IndexError:
				index_error_count = index_error_count + 1
				pass
			except psycopg2.IntegrityError as ie:
				duplicate_count = duplicate_count + 1
				pass
			except psycopg2.ProgrammingError as pe:
				programming_error_count = programming_error_count + 1
				pass
			except psycopg2.Error as e:
				database_error_count = database_error_count + 1
				pass
		fin.close()

		conn.commit()

# Finished parsed
print "Successfully added %d events" % success_count
print "Index errors: %d" % index_error_count
print "Database errors: %d" % database_error_count
print "Duplicate errors: %d" % duplicate_count
print "Programming errors: %d" % programming_error_count