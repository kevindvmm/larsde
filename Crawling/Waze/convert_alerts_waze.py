import json
import csv
import os
import sys

IN_PATH = './data/'
OUT_PATH = './output/'

def getDirList(p):
	p = str(p)
	if p == "":
		return []
	a = os.listdir( p )
	return a

if __name__ == '__main__':
	good_count = 0
	key_error_count = 0
	unicode_error_count = 0
	other_error_count = 0
	if not os.path.exists(OUT_PATH):
		os.makedirs(OUT_PATH)

	firstfilelist = getDirList(IN_PATH)
	for firstfile in firstfilelist:
		if (firstfile == '.DS_Store'):
			continue
		else:
			secondfilelist = getDirList(os.path.join(IN_PATH, firstfile))
			g = open(os.path.join(OUT_PATH, firstfile +'.csv'), "w") 
			for secondfile in secondfilelist:
				if(secondfile == '.DS_Store'): continue
				else:
	#				print secondfile
					f = open(os.path.join(IN_PATH, firstfile , secondfile), "r") 
					data = json.load(f)
					f.close()
					template = ["id", "type", "subtype", "reportDescription", "pubMillis", "reliability"]

					# Creates a CSV row with columns (id, type, subtype, reportDescription, pubMillis, reliability, locationDescription, locationLat, locationLong)
					
					csv_file = csv.writer(g) 
					for item in data["alerts"]:
						s = []
						try:
							for subItem in template:
								if subItem in item:
									s.append(item[subItem])
								else:
									s.append("")
							if 'additionalInfo' in item:
								additionalInfo = json.loads(item['additionalInfo'])
								if 'location' in additionalInfo:
									location_str = additionalInfo['location']
									s.append(additionalInfo['location'])
								else:
									s.append("")
							else:
								s.append("")
							if 'location' in item:
								if 'x' in item['location']:
									s.append(item['location']['x'])
									s.append(item['location']['y'])
								else:
									s.append("")
									s.append("")
							csv_file.writerow(s)
							good_count = good_count + 1
						except KeyError:
							key_error_count = key_error_count + 1
							pass
						except UnicodeEncodeError:
							unicode_error_count = unicode_error_count + 1
							pass
						except TypeError as te:
							other_error_count = other_error_count + 1
							pass
						except:
							other_error_count = other_error_count + 1
							pass 
			g.close()

	# Finished converting
	print "Successfully converted %d events" % good_count
	print "Key errors: %d" % key_error_count
	print "Encoding errors: %d" % unicode_error_count
	print "Other error: %d" % other_error_count