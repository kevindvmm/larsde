# Waze Data Parser

To use these scripts, place all of your Waze data inside of data/. This should be all of our different subdirectories of data, which should presumably be dates i.e. 2015_12_1. Inside of each of those directories should be one or more JSON files with Waze data.

The scripts will likely take several hours to run, as it takes about 30-40 minutes to complete a single day's worth of data. There may be issues related to memory that cause the script to quit early.

The data ends up in the LARSDE database. To make sure you're adding Waze events currently, try running this query before, during, and after running the script:

SELECT COUNT(*) FROM alerts_waze;