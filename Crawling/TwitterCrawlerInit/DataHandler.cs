﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

using System.Data;

namespace TwitterCrawler
{


    class DataHandler
    {
        
        [NonSerialized]
        public static MySqlConnection conn = null;


        public static void MysqlInit()
        {
            string cs = @"server=dvmmdata.cs.columbia.edu;userid=twitter;
            password=jLvTKwaeTcWJsWAr;database=Twitter;charset=utf8";
            

            if (conn == null || !conn.Ping())
            {
                try
                {
                    conn = new MySqlConnection(cs);

                    conn.Open();
                    MySqlCommand setformat = new MySqlCommand("set names utf8", conn);
                    setformat.ExecuteNonQuery();
                    setformat.Dispose();
                    //Console.WriteLine("MySQL version : {0}", conn.ServerVersion);

                }
                catch (MySqlException ex)
                {
                    Console.WriteLine("Error: {0}", ex.ToString());

                }
            }
        }

        public static bool InsertTweet(string query, byte[] data, int timestamp, string TweetURL)
        {

            MysqlInit();




            MySqlCommand cmd1 = new MySqlCommand();
            cmd1.Connection = conn;
            cmd1.CommandText = "select id from `tweet` where TweetURL = @TweetURL";
            cmd1.Prepare();
            cmd1.Parameters.AddWithValue("@TweetURL", TweetURL);
            MySqlDataReader rdr = cmd1.ExecuteReader();

            if (!rdr.HasRows)
            {
                if (!rdr.IsClosed)
                {
                    rdr.Close();
                }

                cmd1.Dispose();

                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = conn;
                try
                {
                    cmd.CommandText = "INSERT INTO  `tweet` (`query` ,`data` ,`time`, `TweetURL`) VALUES ( @query, @data, @time, @TweetURL);";

                    cmd.Prepare();


                    cmd.Parameters.AddWithValue("@query", query);
                    cmd.Parameters.AddWithValue("@data", data);
                    cmd.Parameters.AddWithValue("@time", timestamp);
                    cmd.Parameters.AddWithValue("@TweetURL", TweetURL);

                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                cmd.Dispose();
                return true;
            }
            else
            {
                if (!rdr.IsClosed)
                {
                    rdr.Close();
                }
                return false;
            }

        }

        public static string GetTweet(int id)
        {
            MysqlInit();
            MySqlCommand cmd1 = new MySqlCommand();
            cmd1.Connection = conn;
            cmd1.CommandText = "select OCTET_LENGTH(data) as length, data from tweet where id = @id";
            cmd1.Prepare();
            cmd1.Parameters.AddWithValue("@id", id);
            MySqlDataReader rdr = cmd1.ExecuteReader();
            string ret = "";
            if (rdr.HasRows)
            {
                rdr.Read(); // Read first 4 bytes to get length
                int length = rdr.GetInt32("length");
                if (length > 0)
                {
                    try
                    {
                        byte[] zipedContent = new byte[length];
                        rdr.GetBytes(rdr.GetOrdinal("data"), 0, zipedContent, 0, length);
                        ret = TopsyParser.UnZipStringUTF8(zipedContent);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }

            if (!rdr.IsClosed)
            {
                rdr.Close();
            }
            cmd1.Dispose();
            return ret;
        }



    }
}
