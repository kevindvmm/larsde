﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

// This program needs the Newtonsoft.Json package
// Run "Install-Package Newtonsoft.Json -Version 6.0.8" in the Package Manager console

namespace TwitterCrawler
{
    class Program
    {

        [DllImport("urlmon.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern Int32 URLDownloadToFile(
        [MarshalAs(UnmanagedType.IUnknown)] object pCaller,
        [MarshalAs(UnmanagedType.LPWStr)] string szURL,
        [MarshalAs(UnmanagedType.LPWStr)] string szFileName,
        Int32 dwReserved,
        IntPtr lpfnCB);

        static CookieContainer _cookieContainer = new CookieContainer();
        static StreamReader sr;
        static WebResponse response;
        static string content;
        static HttpWebRequest request;

        public static string GetResponse(string url)
        {
            //ServicePointManager.CertificatePolicy = new NCertificatePolicy();
            request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";

            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.43 Safari/537.31";
            request.KeepAlive = true;
            request.CookieContainer = _cookieContainer;
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded; charset=utf-8";
            //request.AutomaticDecompression = DecompressionMethods.GZip;
            //request.Credentials = new NetworkCredential("ansvoice11@gmail.com", "ansinasj");
            request.AllowAutoRedirect = true;
            request.Timeout = 10000;

            //request.CookieContainer.Add(new Uri("http://www.google.com/"), new Cookie("sadf", "asdfasd"));

            //using (var client = new WebClient())
            //{
            //    // TODO: put your real email and password in the request string
            //    var response1 = client.DownloadString("https://www.google.com/accounts/ClientLogin?accountType=GOOGLE&Email=ansvoice11@gmail.com&Passwd=ansinasj&service=trendspro&source=test-test-v1");
            //    // The SID is the first line in the response
            //    var sid = response1.Split('\n')[0];
            //    client.Headers.Add("Cookie", sid);
            //    byte[] csv = client.DownloadData("http://www.google.com/insights/search/overviewReport?q=test&cmpt=q&content=1&export=2");

            //    // TODO: do something with the downloaded csv file:
            //    Console.WriteLine(Encoding.UTF8.GetString(csv));
            //    File.WriteAllBytes("report.csv", csv);
            //}
            request.Headers["Cookie"] = "__utma=173272373.1073062480.1355254901.1361566979.1365478766.5; __utmb=173272373.4.9.1365478773864; __utmc=173272373; __utmz=173272373.1365478766.5.2.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); __utmv=173272373.|2=soph=qOper=1; I4SUserLocale=en_US; __utma=173272373.1073062480.1355254901.1361566979.1365478766.5; __utmb=173272373.5.9.1365478773864; __utmc=173272373; __utmz=173272373.1365478766.5.2.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); __utmv=173272373.|2=soph=qOper=1; NID=67=voT5XeNnTr_3XOgy5WWL6uSdiVZJfHCxA4YL_I4UNgvG0DyCTT6S5DBEk7WNO3TFR4lICRX-5oLvbeoT-fiMzpDKif5u6HHkKjYIxouQV6m1eEFuInTauIcVwoujEHjb3aONnZZ1YDCBJ_hD_O1p91n70BTjFKGPbRfHLL3aafoawQxkdaTyRgMyWdBw3gHxf-FBYT4dqkMKPRisfk2sNOz_q-MA5kO1JoJ6m19ziXIPyBzqr0FiEulgfl_e_somE0jc4Vb8MMpH-aayC9HrDV3WqyIYh8KwPXmGM_58yyD1trNX1kA; GMAIL_LOGIN=T1365461403595/1365461403595/1365461423599; HSID=A0mDmoiVdm4pz409o; APISID=2yI2fZLf6bWFcXU4/A9JvGRmURAxGb7rD_; PREF=ID=e066861d718180fd:U=0963a147bd892d23:FF=0:LD=en:NR=10:NW=1:TM=1354915517:LM=1365478753:GM=1:SG=1:S=FeRohpSC1C2a8YVX; SID=DQAAAE4CAAA4TPwbYDknqMncfbbakhIKPq5S7nUU-UHVCujAuLZQhZO6oh-OwOTiUTu0W2sGXLjOQ4Zbl2CFuqnmhoVVwHiB8qzO4gqzp7R8E6-kthQs5B9FvK1rTB37DYnqNuBsxoTpKaRVJbZjehINU7GGJ6TOar0GJkMN5h_lQJ1Q0JwpvZtCPSN3pS5f9Q_sRmpp9TV56AiyKEIDGZV-FoHHe1gXT6Yrz8illUcKERGqTJzH4LAYQ_-WOHPv5B_ILw9Xg7W4cH5WHaTiGD0WZ9zJHZbcQKV_09z3N07uMD34jXYFiWmI1zNkhDmKBXT2C4MDwUYDFOaV8ooDro4LpFckPI_yeoK-idcSJtjCyCFTurbgPai9cgnrqzAkoYqZjyHz8yyFY3ZO2c41_o7A6BF1d8Wp2xt_lvQoLOm12iIb8ni4uLZ7HdkjGMiwJWzUSykPYXkWIBK3paF2ObhnsF5_u9ryH_EO0KNWc8P5JvB2WGeErTPyLTez4RLHdg_jncKN95EUNZGcMpLkXXNZYvdgtuw8HDm48AmSt5ZlpgYq-4AM81NcToEpffhTD6-tZIDBSMFsC7zKAbOm4aemdJYNCcyt3zIy9Z7IEz1QUfzsRa2dNh5130LrHUAY0-paITlGRSOshB5fmU-2LZ0SFmcxchG7j9N7I3Ps7SokeWwcgoMfFQrg_mbqAA5nNqDqb2ijmE6Gi-TYWpAziYrN428pjUvJ6V7ToiP0tolfrTW8GoC_krCoriZLoA96yedquY4Elg1_pLFho--C7s6i8rOJpUoF; S=izeitgeist-ad-metrics=MwA4oLd0Uvs";

            request.Headers.Add("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.3");

            request.Headers.Add("Accept-Encoding", "gzip,deflate,sdch");
            request.Headers.Add("Accept-Language", "en-US,en;q=0.8");

            request.Headers.Add("X-Chrome-Variations", "COu1yQEIibbJAQigtskBCKW2yQEIp7bJAQiptskBCLi2yQEI7IPKAQiGhMoB");

            request.Host = "www.google.com";



            //for (int i = 0; i < request.Headers.Count; i++)
            //{
            //    Console.Write(request.Headers.Keys[i] + "   :   ");
            //    Console.WriteLine(request.Headers[i]);
            //}

            response = request.GetResponse();
            //sr = new StreamReader(response.GetResponseStream());

            //string rawstring = sr.ReadToEnd();
            //sr.Close();





            System.IO.Compression.GZipStream gsm = new System.IO.Compression.GZipStream(response.GetResponseStream(), System.IO.Compression.CompressionMode.Decompress);

            sr = new StreamReader(gsm);

            string rawstring = sr.ReadToEnd();
            sr.Close();


            //for (int i = 0; i < response.Headers.Count; i++)
            //{
            //    Console.Write(response.Headers.Keys[i] + "   :   ");
            //    Console.WriteLine(response.Headers[i]);
            //}
            response.Close();

            return rawstring;

        }
        static string getTimestamp(DateTime date)
        {
            return (date.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString();
        }



            
                        

        static void Main(string[] args)
        {
            //Console.WriteLine(DataHandler.GetTweet(1));
            //return;

            /*
            page	optional	page number of the result set. (default: 1, max: 10)
            perpage	optional	limit number of results per page. (default: 10, max: 100)
            offset	optional	offset from which to start the results, should be set to last_offset parameter returned in the previous page.
            mintime	optional	earliest date/time to restrict a result set. unix-timestamp format. ( Note - search.json can be used with either "window" or "mintime" and/or "maxtime" but not both. Using "window" as well as "mintime"/"maxtime" can provide 0 or incorrect results)
            maxtime	optional	most recent date/time to restrict a result set. unix-timestamp format. (Note - search.json can be used with either "window" or "mintime" and/or "maxtime" but not both. Using "window" as well as "mintime"/"maxtime" can provide 0 or incorrect results)
            allow_lang	optional	Language filter which lets you specify the languages you would like to see results in. Currently supports ja, zh, ko and en. Option also takes a comma separated list of languages.
            */

            //string d = File.ReadAllText(@"e:\t.log");
            //TopsyParser.ProcessJsonResponse(d);
            //return;

            int maxpage = 10;
            
            // It is possible to search for image/link/video tweets only
            // & type=video
            string urlpattern = "http://otter.topsy.com/search.txt?q={0}&type=video&apikey=09C43A9B270A470B8EB8F2946A9369F3&perpage=100&page={1}&mintime={2}&maxtime={3}";


            /*string query = "Protest Baltimore";
            DateTime starttime = new DateTime(2015, 4, 27);
            DateTime endtime = new DateTime(2015, 4, 28);*/

            string query = "%23blockupy";
            DateTime starttime = new DateTime(2015, 2, 19);
            DateTime endtime = new DateTime(2015, 4, 19);
            int hoursShift = 24;

            DateTime t = starttime.AddHours(hoursShift);
            if (t>endtime)
            {
                t = endtime;
            }


            while (t < endtime)
            {
                int page = 1;
                int totalPage = page;
                int totaltweet = -1;

                Console.WriteLine("Crawling tweets from: " + starttime + " To " + t);

                while (page <= totalPage && page <= maxpage)
                {
                    try
                    {
                        Console.WriteLine("(current page/total pages): " + page + "/" + totalPage + ", MaxPage = " + maxpage);

                        string res = GetResponse(string.Format(urlpattern, query.Replace(" ", "%20"), page.ToString(), getTimestamp(starttime), getTimestamp(t)));
                        res = res.Replace("<pre>", "").Replace("</pre>", "");
                        int processedtweet;
                        int numTweet = TopsyParser.ProcessJsonResponse(res, query, out totalPage, out totaltweet, out processedtweet);
                        Console.WriteLine("Got " +processedtweet +" tweets in this round, " + numTweet + " unique tweets! "+totaltweet +" tweets are hit by this query");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    page++;
                    //sw.WriteLine(res);


                    int sleepingT = 30;
                    for (int i = 0; i < sleepingT; i++)
                    {
                        System.Threading.Thread.Sleep(1000);
                        Console.Write("Sleep for " + (sleepingT - i) + " seconds...\r");
                    }
                    Console.WriteLine();

                }

                starttime = t;
                t = starttime.AddHours(hoursShift);
                if (t > endtime)
                {
                    t = endtime;
                }

            }
        }
    }
}
