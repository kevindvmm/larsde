﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TwitterCrawler
{
    class TopsyParser
    {
        public static byte[] ZipStringUTF8(string text)
        {
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(text);
            MemoryStream ms = new MemoryStream();
            using (System.IO.Compression.GZipStream zip = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Compress, true))
            {
                zip.Write(buffer, 0, buffer.Length);
            }

            ms.Position = 0;
            MemoryStream outStream = new MemoryStream();

            byte[] compressed = new byte[ms.Length];
            ms.Read(compressed, 0, compressed.Length);

            byte[] gzBuffer = new byte[compressed.Length + 4];
            System.Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
            System.Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gzBuffer, 0, 4);
            return gzBuffer;
        }

        public static string UnZipStringUTF8(byte[] gzBuffer)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                int msgLength = BitConverter.ToInt32(gzBuffer, 0);
                ms.Write(gzBuffer, 4, gzBuffer.Length - 4);

                byte[] buffer = new byte[msgLength];

                ms.Position = 0;
                using (System.IO.Compression.GZipStream zip = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Decompress))
                {
                    zip.Read(buffer, 0, buffer.Length);
                }

                return System.Text.Encoding.UTF8.GetString(buffer, 0, buffer.Length);
            }
        }


        public static int ProcessJsonResponse(string data, string query,out int totalpage, out int totaltweet, out int processedtweet)
        {
            JObject robj = JObject.Parse(data);

            JObject resObj = JObject.Parse(robj.GetValue("response").ToString());



            int page = Int32.Parse(resObj.GetValue("page").ToString());

            int offset = Int32.Parse(resObj.GetValue("last_offset").ToString());
            int total = Int32.Parse(resObj.GetValue("total").ToString());
            int perpage = Int32.Parse(resObj.GetValue("perpage").ToString());
            totalpage = total / perpage;
            totaltweet = total;
            processedtweet = 0;
            List<string> tweetId = new List<string>();
            List<string> tweetCont = new List<string>();
            List<string> time = new List<string>();

            int counter = 0;
            foreach (var item in resObj.GetValue("list"))
            {
                try
                {
                    JObject tweetObj = JObject.Parse(item.ToString());
                    string permalink = tweetObj.GetValue("trackback_permalink").ToString();
                    string timestr = tweetObj.GetValue("firstpost_date").ToString();
                    time.Add(timestr);

                    tweetId.Add(permalink);
                    tweetCont.Add(item.ToString());
                    processedtweet++;
                    if (DataHandler.InsertTweet(query, ZipStringUTF8(item.ToString()), Int32.Parse(timestr), permalink))
                    {
                        counter++;
                    }
                }
                catch(Exception e )
                {
                    Console.WriteLine(e.Message);
                }
            }

            return counter;

        }
    }
}
