# ---------------- 
# testReadDB
# Created: May 11 15 
# Python
# Written by Svebor Karaman <svebor.karaman@columbia.edu>
# PyCharm Community Edition 

import DBConnect
import StringGZip
import struct
import json
import datetime
import requests

cnx = DBConnect.connectReadOnly()
cursor = cnx.cursor()

querySQL = ("SELECT * FROM `tweet`")

cursor.execute(querySQL)

for (id, query, data, time, tweetURL) in cursor: # Potential connection loss here???
    tweet_data = StringGZip.gunzip_text(data[4:])  # As JSON
    tweet_length = struct.unpack('i', data[:4])
    # print "Tweet of length",str(tweet_length[0])+":",tweet_data
    print "__________________"
    print "Tweet of length", str(tweet_length[0])
    tweet_json = json.loads(tweet_data)
    print tweet_json
    print "Type:", tweet_json['mytype']
    print "Author:", tweet_json['trackback_author_nick']
    print "Tweet trackback:", tweet_json['trackback_permalink']
    print "Tweet id:", tweet_json['trackback_permalink'][tweet_json['trackback_permalink'].rfind("/")+1:]
    print "Tweet content:", tweet_json['content']
    #print "Tweet firstpost date:", tweet_json['firstpost_date'], datetime.datetime.fromtimestamp(
    print "Tweet firstpost date:", datetime.datetime.fromtimestamp(tweet_json['firstpost_date']).strftime('%Y-%m-%d %H:%M:%S')
    try:
        for one_url in range(len(tweet_json['url_expansions'])):
            try:
                fullURL=tweet_json['url_expansions'][one_url]['topsy_expanded_url']
            except:
                fullURL=tweet_json['url_expansions'][one_url]['expanded_url']
            if fullURL.find("youtu.be/")!=-1 or fullURL.find(".ly/")!=-1 or fullURL.find("t.co/")!=-1: # We are probably dealing with a shorten URL
                try:
                    fullURL=requests.head(fullURL).headers['location']
                except:
                    print "Didn't manage to get expanded URL"
            extURL = fullURL[-3:]
            if extURL == "jpg" or extURL == "png":  #Some tweet have actually a link towards a gallery, but we just get the first image. Not clear how to deal with this...
                print "Tweet image:", fullURL
            elif tweet_json['mytype']=="video" or fullURL.find("youtube.com")!=-1 or fullURL.find("vine.co")!=-1 or fullURL.find("vimeo.com")!=-1: # Look for vine.co, youtube.com, vimeo for videos? Or use  tweet_json['mytype']
                #print "Tweet link:", tweet_json['url_expansions'][one_url]['topsy_expanded_url']
                print "Tweet video:", fullURL
            else:
                print "Tweet link:", fullURL
    except:
        print "No expanded urls."

cursor.close()
cnx.close()