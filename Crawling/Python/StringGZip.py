# ----------------
# StringGZip
# Created: May 11 15
# Python
# Written by Svebor Karaman <svebor.karaman@columbia.edu>
# PyCharm Community Edition


import StringIO
import gzip

def gzip_text(text):
	"""Returns the string text as a gzipped string."""
	out = StringIO.StringIO()
	with gzip.GzipFile(fileobj=out, mode="w") as f:
  		f.write(text)
  	#print out.getvalue() #just for debugging purpose
	return out.getvalue()


def gunzip_text(text): 
	"""Returns the gzipped text as an unzipped string."""
	infile = StringIO.StringIO()
	infile.write(text)
	with gzip.GzipFile(fileobj=infile, mode="r") as f:
		f.rewind()
		out = f.read()
	#print out #just for debugging purpose
	return out

### example read from file
#with open('tweet-data.bin', 'r') as content_file:
#    content = content_file.read()
#tweet_data=gunzip_text(content[4:]) #As JSON
#tweet_length=struct.unpack('i',content[:4])