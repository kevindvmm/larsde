# ----------------
# DBConnect
# Created: May 11 15
# Python
# Written by Svebor Karaman <svebor.karaman@columbia.edu>
# PyCharm Community Edition

import mysql.connector
from config import *
from mysql.connector import errorcode

def connectReadOnly():
    try:
        cnx = mysql.connector.connect(user=userRO,
                                      password=passRO,
                                      host=host,
                                      database=db)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        return cnx

def connectReadWrite():
    try:
        cnx = mysql.connector.connect(user=userRW,
                                      password=passRW,
                                      host=host,
                                      database=db)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        return cnx