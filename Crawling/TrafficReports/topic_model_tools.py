from bs4 import BeautifulSoup
import requests
from nltk.tokenize import RegexpTokenizer
from stop_words import get_stop_words
from nltk.stem.porter import PorterStemmer
import gensim, sys
from utils import connect_to_larsde_database
from config import LARSDE_TEMPLATE_STRING, MODEL_FETCH_STRING, TWEET_FETCH_STRING
from optparse import OptionParser

# Global variables for our topic modeling
CBS_LOCAL_SOURCE = 'cbs-local'
NUM_TOPICS = 3
tokenizer = RegexpTokenizer(r'\w+')
stops = get_stop_words('en')
p_stemmer = PorterStemmer()

# Remove the case, break it into tokens, remove stop tokens from our text
def clean_text(text):
  raw_text = text.lower()
  tokens = tokenizer.tokenize(raw_text)
  stop_tokens = [i for i in tokens if not i in stops]
  stemmed_tokens = [p_stemmer.stem(i) for i in stop_tokens]
  return stemmed_tokens

# Generate our LDA topic model using the documents that were input
def generate_topic_model(documents, number_of_topics, number_of_words):
  cleaned_texts = []

  for doc in documents: 
    cleaned_text = clean_text(doc)
    cleaned_texts.append(cleaned_text)

  dictionary = gensim.corpora.Dictionary(cleaned_texts)
    
  # Create a document term matrix from our dictionary.
  corpus = [dictionary.doc2bow(text) for text in cleaned_texts]

  # generate LDA model
  lda_model = gensim.models.ldamodel.LdaModel(corpus, num_topics=number_of_topics, id2word = dictionary, passes=20)

  return (lda_model, dictionary)

# Pull up-to-date traffic reports from CBS Local
def fetch_traffic_reports():
  CBS_LOCAL_URL = 'http://newyork.cbslocal.com/traffic/'
  traffic_results = requests.get(CBS_LOCAL_URL).text

  soup = BeautifulSoup(traffic_results, 'html.parser')
  incidents = soup.findAll('div', attrs={'class': 'incident'})

  times = []
  details = []

  for incident in incidents:
    time = incident.find('div', attrs = {'class' : 'inc-time'}).text.strip()
    detail = incident.find('div', attrs = {'class' : 'inc-details'}).find('span', attrs = {'class' : 'inc-report'}).text.strip()
    times.append(str(time))
    details.append(str(detail))
  
  return (times, details)

# Dump our traffic reports to our LARSDE database
def dump_traffic_reports(times, details, connect):
  good_results = 0
  bad_results = 0
  for (time, detail) in zip(times, details):
    lat = 0.0
    lng = 0.0
    # (content, long, lat, addr, time, source)
    insert_string = LARSDE_TEMPLATE_STRING % (detail, lng, lat, 'New York', time, CBS_LOCAL_SOURCE)
    try:
      results = connect.execute(insert_string)
      good_results = good_results + 1
    except Exception as e:
      bad_results = bad_results + 1
  print 'Added ' + str(good_results) + ' results, Failed ' + str(bad_results) + ' results'

# Go through our topic model, look at the different documents we have
def analyze_documents_with_model(lda_model, documents, dictionary):
  for doc in documents:
    cleaned_text = clean_text(doc['content'])
    words = dictionary.doc2bow(cleaned_text)
    lda_doc = lda_model[words]
    print doc['content']
    print lda_doc

# Pull data from LARSDE that we can use as input to our topic model
def load_unclassified_documents(source):
  if source == 'twitter':
    query_string = TWEET_FETCH_STRING
  elif source == 'cbs-local':
    query_string = MODEL_FETCH_STRING
  else:
    return None

  print 'Fetching documents from ' + source + '...'
  connect = connect_to_larsde_database()
  results = connect.execute(query_string)
  cleaned_results = []
  for row in results:
    cleaned_results.append(row)
  connect.close()
  return cleaned_results

# Setup the options our script will take on the command line
def setup_options_parser():
    parser = OptionParser()
    parser.add_option("--load", dest="load", action="store_true",
                  help="Pull traffic reports from CBS Local, store them in LARSDE")
    parser.add_option("--gen", dest="gen", action="store_true",
                  help="Generate our topic model, store it locally")
    parser.add_option("--classify", dest="classify", action="store_true",
                  help="Classify our documents using our previously generated topic model")
    return parser

if __name__ == '__main__':
  parser = setup_options_parser()
  (options, args) = parser.parse_args()

  # Load new reports from CBS Local, add them to LARSDE
  if options.load:
    (times, details) = fetch_traffic_reports()
    connect = connect_to_larsde_database()
    dump_traffic_reports(times, details, connect)
    connect.close()
    print 'Successfully added reports to database...'
  # Generate a topic model using the existing CBS Local reports
  elif options.gen:
    connect = connect_to_larsde_database()
    results = connect.execute(MODEL_FETCH_STRING)
    stories = []
    for result in results:
      stories.append(result['content'])
    (lda_model, dictionary) = generate_topic_model(stories, NUM_TOPICS, 1)
    lda_model.save('model.lda')
    dictionary.save('model.dict')
    connect.close()
    print 'Topic model saved...'
  # Classify the documents we have using the topic model we generated
  elif options.classify:
    lda_model = gensim.models.ldamodel.LdaModel.load('model.lda')
    dictionary = gensim.corpora.dictionary.Dictionary.load('model.dict')
    documents = load_unclassified_documents('cbs-local')
    analyze_documents_with_model(lda_model, documents, dictionary)
    print 'Successfully analyzed documents...'
  else:
    parser.print_help()