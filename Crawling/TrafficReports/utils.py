import re
from config import *
from sqlalchemy import *

def connect_to_newsrover_database():
  engine_string = 'mysql+mysqldb://%s:%s@%s/%s' % (NR_USER, NR_PASSWORD, NR_HOST, NR_DATABASE)
  database = create_engine(engine_string)
  connect = database.connect()
  return connect

def connect_to_larsde_database():
  engine_string = 'postgresql://%s:%s@%s/%s' % (LARS_USER, LARS_PASSWORD, LARS_HOST, LARS_DATABASE)
  database = create_engine(engine_string)
  connect = database.connect()
  return connect