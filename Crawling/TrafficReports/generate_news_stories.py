from sqlalchemy import *
import zlib
from utils import connect_to_newsrover_database
import json

START_DATE = '2015-12-01'
END_DATE = '2016-02-01'
OUTPUT_FILE = 'generated-stories.json'

# TODO: Actually store these snippets (and more) in LARSDE database.

# Pull news transcripts from the NewsRover database
def load_newsrover_stories():
  connect = connect_to_newsrover_database()
  query_string = "SELECT transcript FROM VideoStory WHERE time BETWEEN '%s' AND '%s'" % (START_DATE, END_DATE)
  results = connect.execute(query_string)
  stories = []
  for row in results:
    transcript = uncompress_newsrover_story(row['transcript'])
    print transcript
    stories.append(transcript)
  connect.close()
  return stories

# This is a script sent by Joe from NewsRover to decompress the data
def uncompress_newsrover_story(transcript):
  raw_bytes = bytearray(transcript)[4:]
  # Important! We get \x00 after each character as a legacy binary issue.
  # We added this replace statement to remove them.
  return zlib.decompress(bytes(raw_bytes), 15 + 32).replace('\x00', '') #.rstrip().lower()

# Pull data from multiple news sources. We'll add more later.
def output_stories_data():
  newsrover_stories = load_newsrover_stories()
  # TODO: Add other news sources here later.
  stories = newsrover_stories

if __name__ == '__main__':
  output_stories_data()