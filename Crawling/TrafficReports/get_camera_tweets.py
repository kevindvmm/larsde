from sqlalchemy import *
import math
from utils import connect_to_larsde_database

# Calculated using an online tool available here:
# http://www.csgnetwork.com/degreelenllavcalc.html
LAT_DEGREES_PER_FOOT = 1.0 / 364334.03
LNG_DEGREES_PER_FOOT = 1.0 / 277034.24

# How many feet we want to be within in both latitude and longitude
LAT_FOOT_LIMIT = 150
LNG_FOOT_LIMIT = 150
TOTAL_FOOT_LIMIT = math.sqrt(math.pow(LAT_FOOT_LIMIT, 2) + math.pow(LNG_FOOT_LIMIT, 2))

# Thresholds for the number of degrees away from a camera a tweet should be.
LAT_THRESHOLD = LAT_DEGREES_PER_FOOT * LAT_FOOT_LIMIT
LNG_THRESHOLD = LNG_DEGREES_PER_FOOT * LNG_FOOT_LIMIT

# Join cameras and good news text from sources that we know to be good
# Verify that each lat and lng are within LAT_FOOT_LIMIT and LNG_FOOT_LIMIT
QUERY_STRING = "SELECT c.new_id, c.camname, n.time, n.content \
  FROM cameras c, news_event_text n \
  WHERE n.source = 'twitter-511NYC' OR n.source = 'twitter-totaltrafficnyc' \
    AND TIME BETWEEN '12-01-2015' AND '02-01-2016' \
    AND ABS(c.location_lat - n.lat) <= %f \
    AND ABS(c.location_long - n.long) <= %f" % (LAT_THRESHOLD, LNG_THRESHOLD)

if __name__ == '__main__':
  connect = connect_to_larsde_database()
  print 'Fetching all traffic-related tweets within %f feet of a camera...' % (TOTAL_FOOT_LIMIT)
  results = connect.execute(QUERY_STRING)
  for row in results:
    print row