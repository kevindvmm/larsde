# How to Use these Files
`config.py` - stores usernames and constants for accessing different systems
`fetch_tweets.py` - fetches a small number (100) of tweets from LARSDE
`generate_news_stories.py` - pull news stories from NewsRover and creates a JSON file from them, these could easily be added to the database
`get_camera_tweets.py` - get all the traffic tweets that are within a couple hundred feet of a camera
`load_traffic_tweets.py` - pulls tweets from Twitter and stores them in LARSDE (use `-h` to see options)
`topic_model_tools.py` - experiment with LDA topic modeling using traffic reports from CBS Local (use `-h` to see options)
`utils.py` - a collection of utility methods used by other functions

# Description of Existing Data

## Classified Data:
- Loaded 35,502 traffic-related tweets from verified sources, including NYC traffic Twitter accounts and CBS Local newsfeed.
```
         source          | count 
-------------------------+-------
 twitter-511NYC          |  9152
 twitter-totaltrafficnyc | 11711
 cbs-local               |  7188
 twitter-Traffic4NY      |  3374
 twitter-WazeTrafficNYC  |  4077
```

`SELECT source, COUNT(*) FROM news_event_text GROUP BY 1;`

## Unclassified Data:
- 24,833 news articles from NewsRover
- 823,020 tweets from all across NYC 

## Quality Data:
- 8039 tweets from verified Twitter accounts between the months with tagged locations
