import tweepy, json
from HTMLParser import HTMLParser
import datetime, sys
from sqlalchemy import *
from sqlalchemy import exc
from utils import connect_to_larsde_database
from config import LARSDE_TEMPLATE_STRING
import pdb
from optparse import OptionParser

# Twitter access keys
ckey = 'IjBSIuuEPA45OzXjh37eLUrKQ'
csecret ='VuvSMHApV00gM2Hve43jud7nHVIN2RrqB16K1hYEBShXPqd2dZ'
atoken = '848962465-UOwj2ne53EBo9DzIhYVnDZ9j3waAEDtsAERsOqtn'
asecret = '4wWoBF3m5dKGISrTKlTfj9Kx9Pl8vC4K3yYdBk6y9yosu'

# A list of known traffic accounts and their IDs
TWITTER_ACCOUNTS =  ['511NYC', 'WazeTrafficNYC', 'Traffic4NY', 'totaltrafficnyc']
TWITTER_ACCOUNT_IDS = ['52272942', '2869012466', '531603768', '42640432']

# JSON handler for for datetime strings
def datetime_handler(x):
    if isinstance(x, datetime.datetime):
        return x.isoformat()
    raise TypeError("Unknown type")

# Create the query that will select tweets from within our timeframe and from specific accounts.
def format_historic_query_string():
    QUERY_STRING = 'SELECT * FROM tweets WHERE userid IN ('
    for i in range(len(TWITTER_ACCOUNT_IDS)):
        QUERY_STRING = QUERY_STRING + TWITTER_ACCOUNT_IDS[i]
        if i != len(TWITTER_ACCOUNT_IDS) - 1:
            QUERY_STRING = QUERY_STRING + ','
    QUERY_STRING = QUERY_STRING + ') '
    QUERY_STRING = QUERY_STRING + "AND time BETWEEN '12-01-2015' AND '02-01-2016'"
    return QUERY_STRING

# Pull tweets from our general body of tweets that are from traffic accounts
def pull_traffic_tweets_from_existing(username, user_id):
    connect = connect_to_larsde_database()
    query_string = 'SELECT * FROM tweets WHERE userid = ' + user_id
    results = connect.execute(query_string)
    tweets = []
    for row in results:
        tweet_data = {}
        tweet_data['tweet_id'] = row['tweetid']
        tweet_data['content'] = row['content']
        tweet_data['user_id'] = row['userid']
        tweet_data['time'] = row['time']
        tweet_data['lat'] = row['lat']
        tweet_data['lng'] = row['long']
        tweet_data['source'] = 'twitter-' + username
        tweets.append(tweet_data)
    connect.close()
    return tweets

# Actually go through and pull the tweets from user timelines using TweePy API
def pull_tweets(api, username):
    tweets = []
    for page in tweepy.Cursor(api.user_timeline, id=username).items():
        tweet_data = {}
        tweet_data['tweet_id'] = page.id
        tweet_data['content'] = page.text
        tweet_data['user_id'] = page.user.id
        tweet_data['time'] = page.created_at
        tweet_data['source'] = 'twitter-' + username
        if page.coordinates is not None:
          tweet_data['lat'] = page.coordinates['coordinates'][1]
          tweet_data['lng'] = page.coordinates['coordinates'][0]
        tweets.append(tweet_data)
    return tweets

# Get the raw JSON from TweePy instead of the formatted JSON objects we normally use
def pull_raw_json(api, username):
    searched_tweets = [status._json for status in tweepy.Cursor(api.user_timeline, id=username).items()]
    json_strings = [json.dumps(json_obj) for json_obj in searched_tweets]
    return json_strings

# Take a list of tweets we've pulled from the Twitter API and add them to our LARSDE database
def dump_tweets_to_database(tweets, connect):
    insert_count = 0
    fail_count = 0
    for tweet in tweets:
        lat = 0.0 if not 'lat' in tweet else tweet['lat']
        lng = 0.0 if not 'lng' in tweet else tweet['lng']
        # (content, long, lat, addr, time, source)
        insert_string = LARSDE_TEMPLATE_STRING % (tweet['content'], lng, lat, 'New York', tweet['time'], tweet['source'])
        try:
            results = connect.execute(insert_string)
            insert_count = insert_count + 1
        except exc.SQLAlchemyError as e:
            fail_count = fail_count + 1

    print "Successfully inserted " + str(insert_count) + " tweets"
    print "Failed to insert " + str(fail_count) + " tweets"

# Setup the options our script will take on the command line
def setup_options_parser():
    parser = OptionParser()
    parser.add_option("--load", dest="load", action="store_true",
                  help="Load tweets from Traffic twitter accounts and add them to LARSDE")
    parser.add_option("--raw", dest="raw", action="store_true",
                  help="Pull a small sample of raw tweets")
    parser.add_option("--historic", dest="historic", action="store_true",
                  help="Pull historic tweets from the traffic accounts we've identified")

    return parser

if __name__ == '__main__':
    parser = setup_options_parser()
    (options, args) = parser.parse_args()

    auth = tweepy.OAuthHandler(ckey, csecret)
    auth.set_access_token(atoken, asecret)

    api = tweepy.API(auth_handler = auth)

    # Load tweets from traffic accounts, dump them to LARSDE
    if options.load:
        tweets = []
        total_count = 0

        for handle in TWITTER_ACCOUNTS:
            account_tweets = pull_tweets(api, handle)
            total_count = total_count + len(account_tweets)
            tweets = tweets + account_tweets

        connect = connect_to_larsde_database()
        dump_tweets_to_database(tweets, connect)
        connect.close()
    # Pull a small sample of raw tweets. Used for TwitIE.
    elif options.raw:
        raw_data = pull_raw_json(api, 'Traffic4NY')
        print raw_data
        with open('training_data/sample-tweets.json', 'w') as f:
            for data in raw_data:
                f.write(data + '\n\n')
    # Backfill tweets from our traffic accounts.
    elif options.historic:
        for (handle, account_id) in zip(TWITTER_ACCOUNTS, TWITTER_ACCOUNT_IDS):
            tweets = pull_traffic_tweets_from_existing(handle, account_id)
            connect = connect_to_larsde_database()
            dump_tweets_to_database(tweets, connect)
            connect.close()
    else:
        parser.print_help()