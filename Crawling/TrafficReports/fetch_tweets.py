from utils import connect_to_larsde_database
from config import TWEET_FETCH_STRING

# Fetch 100 raw tweets from the LARSDE database
def fetch():
  connect = connect_to_larsde_database()
  results = connect.execute(TWEET_FETCH_STRING)
  for row in results:
    print row['content'].encode('utf-8')
  connect.close()

if __name__ == '__main__':
  fetch()