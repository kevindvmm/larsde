import os,sys,errno
import math
import time
from datetime import datetime
import simplejson as json
from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
from geopy import geocoders

manhattan_bbox=[-74.0193557739,40.69807989,-73.9057159424,40.8776989647]
# estimated from http://boundingbox.klokantech.com/


# Twitter acess keys
ckey = 'IjBSIuuEPA45OzXjh37eLUrKQ'
csecret ='VuvSMHApV00gM2Hve43jud7nHVIN2RrqB16K1hYEBShXPqd2dZ'
atoken = '848962465-UOwj2ne53EBo9DzIhYVnDZ9j3waAEDtsAERsOqtn'
asecret = '4wWoBF3m5dKGISrTKlTfj9Kx9Pl8vC4K3yYdBk6y9yosu'

def mkdir_p(path):
    try:
        print "Creating directory: "+path
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

class listener(StreamListener):
    def on_data(self, data):
        if data!=None:
            data = json.loads(data)
            user_lang = unicode(data['user']['lang']).encode("ascii","ignore")
            if user_lang=="en":
                datereq = datetime.utcnow().strftime("%Y-%m-%d")
                logfile = open(os.path.join(outputlocation,address+"-"+datereq+".csv"), "a")
                tweet_date = unicode(data['created_at']).encode("ascii","ignore")[:10] + " " + unicode(data['created_at']).encode("ascii","ignore")[-4:]
                tweet_time = unicode(data['created_at']).encode("ascii","ignore")[11:-11]
                user_id = unicode(data['user']['id']).encode("ascii","ignore")
                text_id = unicode(data['id']).encode("ascii","ignore")
                user_location = unicode(data['user']['location']).encode("ascii","ignore")
                text_msg = unicode(data['text']).encode("utf8","ignore").replace(',', ' ')
                
                if data['coordinates'] == None : # just discard those tweets
                    logfile.close()
                    return True
                    #print >>logfile, '[No geotag for this tweet]'
                    longitude = ""
                    latitude = ""
                    print text_msg,user_location
                else:
                    coord = unicode(data['coordinates']['coordinates']).encode("ascii","ignore")
                    longitude = coord[:coord.rfind(',')].replace("[","")
                    latitude = coord.split(" ")[1].replace("]","")

                    print text_msg,user_location,longitude,latitude

                stream_log = longitude + "," + latitude + "," + tweet_date + "," + tweet_time + "," + user_id  + "," + user_location + "," + user_lang + "," + text_id + "," + text_msg
                stream_log = stream_log.replace('\n', ' ')

                print stream_log
                print >>logfile, stream_log
                logfile.close()
            return True

    def on_error(self, statut):
        print statut

# degrees to radians
def deg2rad(degrees):
    return math.pi*degrees/180.0
# radians to degrees
def rad2deg(radians):
    return 180.0*radians/math.pi

# Semi-axes of WGS-84 geoidal reference
WGS84_a = 6378137.0  # Major semiaxis [m]
WGS84_b = 6356752.3  # Minor semiaxis [m]

# Earth radius at a given latitude, according to the WGS-84 ellipsoid [m]
def WGS84EarthRadius(lat):
    # http://en.wikipedia.org/wiki/Earth_radius
    An = WGS84_a*WGS84_a * math.cos(lat)
    Bn = WGS84_b*WGS84_b * math.sin(lat)
    Ad = WGS84_a * math.cos(lat)
    Bd = WGS84_b * math.sin(lat)
    return math.sqrt( (An*An + Bn*Bn)/(Ad*Ad + Bd*Bd) )

# Bounding box surrounding the point at given coordinates,
# assuming local approximation of Earth surface as a sphere
# of radius given by WGS84
def boundingBox(latitudeInDegrees, longitudeInDegrees, halfSideInKm):
    lat = deg2rad(latitudeInDegrees)
    lon = deg2rad(longitudeInDegrees)
    halfSide = 1000*halfSideInKm

    # Radius of Earth at given latitude
    radius = WGS84EarthRadius(lat)
    # Radius of the parallel at given latitude
    pradius = radius*math.cos(lat)

    latMin = lat - halfSide/radius
    latMax = lat + halfSide/radius
    lonMin = lon - halfSide/pradius
    lonMax = lon + halfSide/pradius

    return (rad2deg(latMin), rad2deg(lonMin), rad2deg(latMax), rad2deg(lonMax))

def main():
    g = geocoders.GoogleV3()
    place, (lat, lng) = g.geocode(address)
    location = [boundingBox(lng,lat,halfradius)[0],boundingBox(lng,lat,halfradius)[1],boundingBox(lng,lat,halfradius)[2],boundingBox(lng,lat,halfradius)[3]]
    #print "Location of "+ address+" :",location
    print "Using following manhattan_bbox:",manhattan_bbox
    auth = OAuthHandler(ckey, csecret)
    auth.set_access_token(atoken, asecret)
    twitterStream = Stream(auth, listener())
    twitterStream.filter(locations=manhattan_bbox,track=keywords)

if __name__=="__main__":
    if len(sys.argv)<3:
        print "Usage: python streaming.py place output halfradius keywords"
        sys.exit(0)
    # Crawl parameters, to be read from command line
    address=str(sys.argv[1]) #Location (City name, address...)
    print address
    outputlocation=str(sys.argv[2])
    print outputlocation
    mkdir_p(outputlocation)
    try:
        halfradius=float(sys.argv[3]) #Radius in Km of the Bounding Box
        print halfradius
    except:
        halfradius=1.0
    try:
        keywords=[str(tmp) for tmp in sys.argv[4:]]
        print keywords
    except:
        keywords=[]
    print "Starting inifite loop."
    while 1:
        try:
            print "Launching main."
            main()
        except:
            print "Caught error."
            time.sleep(10)
