import os,sys,errno
import datetime
import urllib2
import hashlib
import base64
import multiprocessing
import time
from multiprocessing import managers

# parameters
baseOutDir="./"
baseCCTV='http://207.251.86.238/cctv'
servedHash="zxdaTRalpY0aSTcYcMX2Eg=="
serveTime=600
waitTime=1
sleepTime=30
verbose=2

# initialization
checkServed=datetime.timedelta(seconds=serveTime)
minWait=datetime.timedelta(seconds=waitTime)
workers={}
manager=None
servedCam=None
dictMD5Hash=None
lastCheck=None
lastHour=None

def mkdir_p(path):
    global verbose
    try:
        if verbose>=1:
            print "Creating directory: "+path
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def totimestamp(dt, epoch=datetime.datetime(1970,1,1)):
    td = dt - epoch
    return (td.microseconds + (td.seconds + td.days * 86400) * 10**6) / 10**6

def getPath(dt,camId,lastHour):
    #global lastHour
    dirOut=baseOutDir+str(dt.year)+"/"+str(dt.month)+"/"+str(dt.day)+"/"+str(camId)+"/"+str(dt.hour)+"/"
    if camId not in lastHour.keys() or lastHour[camId]!=dt.hour:
        mkdir_p(dirOut)
        lastHour[camId]=dt.hour
    outpath=dirOut+str(camId)+"_"+str(dt.year)+"-"+str(dt.month)+"-"+str(dt.day)+"_"+str(dt.minute)+"-"+str(dt.second)+'.jpg'
    return outpath

def splitCamList(camList, nbThreads):
    start = 0
    for i in xrange(nbThreads):
        stop = start + len(camList[i::nbThreads])
        yield camList[start:stop]
        start = stop

def checkHash(camId,md5hash,dt,servedCam,dictMD5Hash):
    #global servedCam,dictMD5Hash
    if camId not in dictMD5Hash.keys():
        dictMD5Hash[camId]=md5hash
    else:
        if dictMD5Hash[camId]==md5hash:
            if verbose>=1:
                print "[{}] Image of camera {} hasn't changed (md5hash={}). Skipping.".format(dt,camId,md5hash)
            if md5hash==servedHash:
                if verbose>=1:
                    print "[{}] Camera {} is being served.".format(dt,camId)
                servedCam[camId]=dt
            return False
        else:
            dictMD5Hash[camId]=md5hash
    return True

def oneCamWorker(camId,servedCam,lastCheck,dictMD5Hash,lastHour):
    #global servedCam,lastCheck
    if servedCam==None:
         init_shared()
    #time.sleep(10)
    try:
        while True:
            now=datetime.datetime.now()
            # every day we should get:
            # - http://www.nyc.gov/html/dot/html/motorist/wkndtraf.shtml
            # - http://www.nyc.gov/html/dot/html/motorist/weektraf.shtml
            # - http://www.nyc.gov/html/dot/html/motorist/trafalrt.shtml
            if camId in servedCam.keys():
                if now-servedCam[camId]<checkServed:
                    return servedCam
            if camId in lastCheck.keys():
                if now-lastCheck[camId]<minWait:
                    time.sleep(waitTime)
                    continue
            lastCheck[camId]=now
            ts=totimestamp(now)
            urlImg=baseCCTV+str(camId)+'.jpg?math='+str(ts)
            outImgPath=getPath(now,camId,lastHour)
            retriever=urllib2.urlopen(urlImg)
            content=retriever.read()
            md5hash = base64.b64encode(hashlib.md5(base64.b64encode(content)).digest())
            if not checkHash(camId,md5hash,now,servedCam,dictMD5Hash):
                continue
            with open(outImgPath,"wb") as outImg:
                outImg.write(content)
            elapsed=datetime.datetime.now()-now
            if verbose>=2:
                print "[{}] Capture for camera {} took {}.".format(now,camId,elapsed)
    except:
        print "Caught error in oneCamWorker for cam {}.".format(camId)

def readCamListFile(camListFile):
    camList=[]
    with open(camListFile,"rb") as camListFP:
        print "Reading camList"
        for line in camListFP:
            camList.append(line.rstrip())
    print("Found {} cameras in file {}.".format(len(camList),camListFile))
    return camList

def init_shared():
    # these are shared objects
    global manager,servedCam,dictMD5Hash,lastCheck,lastHour
    manager=multiprocessing.Manager()
    servedCam=manager.dict()
    dictMD5Hash=manager.dict()
    lastCheck=manager.dict()
    lastHour=manager.dict()

def show_shared():
    print servedCam
    print dictMD5Hash
    print lastCheck
    print lastHour

def merge_servedCam(oneServedCam):
    global servedCam
    if oneServedCam==None:
        return
    for oneCam in oneServedCam.keys():
        if oneCam not in servedCam.keys():
            print "Adding cam {} as being served.".format(oneCam)
            servedCam[oneCam]=oneServedCam[oneCam]

if __name__ == "__main__":
    multiprocessing.freeze_support()
    if len(sys.argv)<2:
         print "\nPlease provide a list of cameras."
         print "Usage: python crawlCams.py camList.txt [outDir]\n"
         quit()
    if len(sys.argv)>=3:
         baseOutDir=sys.argv[2]+"/"

    camListFile=sys.argv[1]
    camList=readCamListFile(camListFile)

    #camList=[157,172]
    init_shared()
    #show_shared()
    while True:
        try:
            for wid in range(len(camList)):
                now=datetime.datetime.now()
                camId=camList[wid]
                if camId in servedCam.keys():
                    if now-servedCam[camId]>checkServed:
                        print "Didn't check cam {} since {} seconds. Will check.".format(camId,serveTime)
                    else:
                        continue
                if wid not in workers.keys():
                    print "Starting worker {} for cam {}.".format(wid,camId)
                    workers[wid]=multiprocessing.Process(target=oneCamWorker, args=(camId,servedCam,lastCheck,dictMD5Hash,lastHour))
                    workers[wid].start()
                elif not workers[wid].is_alive():
                    print "Dead worker {}: {} for cam {}. Cleaning.".format(wid,workers[wid],camId)
                    res=workers[wid].join()
                    print res
                    merge_servedCam(res)
                    del workers[wid]
            print "There are {} cameras being served out of {}.".format(len(servedCam.keys()),len(camList))
            time.sleep(sleepTime)
            print servedCam
        except:
            print "Error in main loop..."
            sys.exit(1)
