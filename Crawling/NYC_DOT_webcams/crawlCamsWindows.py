import os,sys,errno
import datetime
import urllib2
import hashlib
import base64
import multiprocessing
import time
from multiprocessing import managers

# parameters
baseOutDir="./"
baseCCTV='http://207.251.86.238/cctv'
servedHash="zxdaTRalpY0aSTcYcMX2Eg=="
serveTime=600
waitTime=1
imgret_timeout=1
sleepTime=1
verbose=0

# initialization
checkServed=datetime.timedelta(seconds=serveTime)
minWait=datetime.timedelta(seconds=waitTime)
workers={}
manager=None
servedCam=None
dictMD5Hash=None
lastCheck=None
lastHour=None

def mkdir_p(path):
    global verbose
    try:
        if verbose>=1:
            print "Creating directory: "+path
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def totimestamp(dt, epoch=datetime.datetime(1970,1,1)):
    td = dt - epoch
    return (td.microseconds + (td.seconds + td.days * 86400) * 10**6) / 10**6

def getDayPath(dt,baseOutDir):
    return baseOutDir+str(dt.year)+"/"+str(dt.month)+"/"+str(dt.day)+"/"
    
def getPath(dt,camId,lastHour):
    #global lastHour
    dirOut=getDayPath(dt)+str(camId)+"/"+str(dt.hour)+"/"
    if camId not in lastHour.keys() or lastHour[camId]!=dt.hour:
        mkdir_p(dirOut)
        lastHour[camId]=dt.hour
    outpath=dirOut+str(camId)+"_"+str(dt.year)+"-"+str(dt.month)+"-"+str(dt.day)+"_"+str(dt.minute)+"-"+str(dt.second)+'.jpg'
    return outpath
    
def getPrettyPath(dt,camIdFull,lastHour,baseOutDir):
    #global lastHour
    camId=camIdFull[1]
    dirOut=getDayPath(dt,baseOutDir)+str(camIdFull[0])+"-"+str(camIdFull[1])+"_"+camIdFull[2].replace(" ","_")+"/"+str(dt.hour)+"/"
    if camId not in lastHour.keys() or lastHour[camId]!=dt.hour:
        mkdir_p(dirOut)
        lastHour[camId]=dt.hour
    outpath=dirOut+str(camId)+"_"+str(dt.year)+"-"+str(dt.month)+"-"+str(dt.day)+"_"+str(dt.minute)+"-"+str(dt.second)+'.jpg'
    return outpath

def splitCamList(camList, nbThreads):
    start = 0
    for i in xrange(nbThreads):
        stop = start + len(camList[i::nbThreads])
        yield camList[start:stop]
        start = stop

def checkHash(camId,md5hash,dt,servedCam,dictMD5Hash):
    #global servedCam,dictMD5Hash
    if camId not in dictMD5Hash.keys():
        dictMD5Hash[camId]=md5hash
    else:
        if dictMD5Hash[camId]==md5hash:
            if verbose>=1:
                print "[{}] Image of camera {} hasn't changed (md5hash={}). Skipping.".format(dt,camId,md5hash)
            if md5hash==servedHash:
                if verbose>=0:
                    print "[{}] Camera {} is being served.".format(dt,camId)
                servedCam[camId]=dt
            return False
        else:
            dictMD5Hash[camId]=md5hash
    return True
    
def dlOneImg(baseCCTV,camIdFull,now,servedCam,dictMD5Hash,lastHour,baseOutDir):    
    ts=totimestamp(now)
    camId=camIdFull[1]
    urlImg=baseCCTV+str(camId)+'.jpg?math='+str(ts)    
    try:
        retriever=urllib2.urlopen(urlImg,timeout=imgret_timeout)
    except:
        if verbose>=2:
            print "Oops, could not retrieve image for cam {} {} {}?".format(camId,camIdFull[1],camIdFull[2])
        return
    content=retriever.read()
    md5hash = base64.b64encode(hashlib.md5(base64.b64encode(content)).digest())
    if not checkHash(camId,md5hash,now,servedCam,dictMD5Hash):
        return
    outImgPath=getPrettyPath(now,camIdFull,lastHour,baseOutDir)
    with open(outImgPath,"wb") as outImg:
        outImg.write(content)
    if verbose>=2:
        elapsed=datetime.datetime.now()-now
        print "[{}] Capture for camera {} took {}.".format(now,camId,elapsed)

def oneCamWorker(camIdFull,servedCam,lastCheck,dictMD5Hash,lastHour,baseOutDir):
    #global servedCam,lastCheck
    if servedCam==None:
         init_shared()
    #time.sleep(10)
    camId=str(camIdFull[1])
    #print camId
    try:
        while True:
            now=datetime.datetime.now()
            if camId in servedCam.keys():
                if now-servedCam[camId]<checkServed:
                    return servedCam
            if camId in lastCheck.keys():
                if now-lastCheck[camId]<minWait:
                    time.sleep(waitTime/2.0)
                    continue
            lastCheck[camId]=now
            ## TODO: use a pool of downloaders and a queue to push images to be downloaded
            # cf: http://stackoverflow.com/questions/9038711/python-pool-with-worker-processes
            # cf2: http://stackoverflow.com/questions/17241663/filling-a-queue-and-managing-multiprocessing-in-python
            ## Way too many forks
            #dlWorker=multiprocessing.Process(target=dlOneImg, args=(baseCCTV,camIdFull,now,servedCam,dictMD5Hash,lastHour,baseOutDir))            
            #dlWorker.start()
            ## The download of one image may get stuck for a few seconds, delaying next image acquisition....
            dlOneImg(baseCCTV,camIdFull,now,servedCam,dictMD5Hash,lastHour,baseOutDir)
    except:
        print "Caught error in oneCamWorker for cam {} {} {}.".format(camIdFull[0],camId,camIdFull[2])        

def readCamListFile(camListFile):
    camList=[]
    with open(camListFile,"rb") as camListFP:
        print "Reading camList"
        for line in camListFP:
            camList.append(line.rstrip())
    print("Found {} cameras in file {}.".format(len(camList),camListFile))
    return camList
    
def correctCamList(camList):
    # baseShowURL='http://nyctmc.org/multiview2.php?listcam='
    baseShowURL='http://dotsignals.org/multiview2.php?listcam='
    splitting = '\n'
    baseURL='http://207.251.86.238/cctv'
    prev_title='TitleCam'
    title_start='<font color=white>'
    title_end='</font>'
    offset=len(baseURL)
    correctCamList=[]
    for camId in camList:        
        urlShow=baseShowURL+str(camId)
        retriever=urllib2.urlopen(urlShow)    
        page=retriever.read()
        lines=page.split(splitting)
        title=None
        for line in lines:
            if line.find(prev_title)!=-1:
                pos_title=line.find(title_start)                
                if pos_title!=-1:
                    pos_title_end=line[pos_title+len(title_start):].find(title_end)
                    title=line[pos_title+len(title_start):pos_title+len(title_start)+pos_title_end]
            pos=line.find(baseURL)
            if pos!=-1:
                jpgpos=line[pos+offset:].find('.jpg')
                correctCamId=line[pos+offset:jpgpos+pos+offset]
                print camId,correctCamId,title
                correctCamList.append([camId,correctCamId,title])
    return correctCamList
                
def init_shared():
    # these are shared objects
    global manager,servedCam,dictMD5Hash,lastCheck,lastHour
    manager=multiprocessing.Manager()
    servedCam=manager.dict()
    dictMD5Hash=manager.dict()
    lastCheck=manager.dict()
    lastHour=manager.dict()

def show_shared():
    print servedCam
    print dictMD5Hash
    print lastCheck
    print lastHour

def getDOTAnn(dt,baseOutDir):
    outpath=getDayPath(dt,baseOutDir)
    mkdir_p(outpath)
    baseURL='http://www.nyc.gov/html/dot/html/motorist/'
    # every day we should get:
    # - http://www.nyc.gov/html/dot/html/motorist/wkndtraf.shtml
    # - http://www.nyc.gov/html/dot/html/motorist/weektraf.shtml
    # - http://www.nyc.gov/html/dot/html/motorist/trafalrt.shtml
    for page in ['wkndtraf.shtml','weektraf.shtml','trafalrt.shtml']:
        urlPage=baseURL+page
        outPagePath=outpath+page
        retriever=urllib2.urlopen(urlPage)
        content=retriever.read()
        with open(outPagePath,"wb") as outPage:
            outPage.write(content)

if __name__ == "__main__":
    multiprocessing.freeze_support()
    #global baseOutDir
    #print "Args:",str(len(sys.argv))
    
    if len(sys.argv)<2:
        print "\nPlease provide a list of cameras."
        print "Usage: python crawlCams.py camList.txt [outDir]\n"
        quit()    
    if len(sys.argv)>=3:
        baseOutDir=sys.argv[2]+"/"
        print "Saving to: "+baseOutDir

    camListFile=sys.argv[1]
    tmpCamList=readCamListFile(camListFile)
    #tmpCamList=[157,172]
    print "Starting correct camList"
    camList=correctCamList(tmpCamList)
    print "size:"
    print len(camList)
    
    init_shared()
    #show_shared()
    day=None

    while True:
        try:
            now=datetime.datetime.now()
            if day==None or day!=now.day:
                getDOTAnn(now,baseOutDir)
                day=now.day
            for wid in range(len(camList)):
                now=datetime.datetime.now()
                camId=camList[wid][1]
                if wid not in workers.keys():                    
                    if camId in servedCam.keys():
                        if now-servedCam[camId]>checkServed:
                            print "Didn't check cam {} since {} seconds. Will check.".format(camId,serveTime)
                        else:
                            continue
                    print "Starting worker {} for cam {}.".format(wid,camId)
                    workers[wid]=multiprocessing.Process(target=oneCamWorker, args=(camList[wid],servedCam,lastCheck,dictMD5Hash,lastHour,baseOutDir))
                    workers[wid].start()
                elif not workers[wid].is_alive():
                    print "Dead worker {}: {} for cam {}. Cleaning.".format(wid,workers[wid],camId)                    
                    del workers[wid]
            if verbose>=1:
                print "There are {} cameras being served out of {}.".format(len(servedCam.keys()),len(camList))
            time.sleep(sleepTime)
            #print servedCam
        except:
            print "Error in main loop..."
            time.sleep(sleepTime)
        #    sys.exit(1)
